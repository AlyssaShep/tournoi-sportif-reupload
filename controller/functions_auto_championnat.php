<?php

// recup les equipes
    function recup_idEquipe($idTournois, $rang){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $tournois = array($requete->fetchAll());

          $equipes = array($tournois[0][0]['idEquipe']);
          $longueur = strlen($equipes[0]);
          $idrecup = "";
          $nbr = 0;

          for ($i=0; $i < $longueur; $i++) {
            if (($equipes[0][$i] == ",") && ($rang == $nbr)) {
              return $idrecup;
            }
            if ($equipes[0][$i] == ",") {
              $idrecup = "";
              $nbr++;
            }
            else {
              $idrecup = $idrecup.$equipes[0][$i];
            }
          }
          return null;

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }


// planifie automatiquement toutes les rencontres de la poule
    function planification_auto($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        // $requete2 = $db->exec("DELETE FROM tournois.rencontre WHERE idTournois = '{$idTournois}'");


        $requete2 = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();
        $execution_requete2 = $requete2->execute();

        if($execution_requete && $execution_requete2){
          $rencontres = array($requete->fetchAll());
          $tournois = array($requete2->fetchAll());
          $nbr_equipes = $tournois[0][0]['nbr_equipes'];

          $idEquipes = array();

          for($i = 0; $i < $nbr_equipes; $i++){
            $idEquipes[0][$i] = recup_idEquipe($idTournois, $i);
          }

          for ($i=0; $i < $nbr_equipes; $i++) {
            for ($j=$i; $j < $nbr_equipes; $j++) {
              if ($i != $j) {

                $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois='{$idTournois}' AND idEquipe1 = '{$idEquipes[0][$i]}' AND idEquipe2 = '{$idEquipes[0][$j]}'");

                $recherche = $requete1->execute();

                $resutats_requetes = array($requete1->fetchAll());

                if(empty($resutats_requetes[0])){
                  $requete = $db->prepare('INSERT INTO tournois.rencontre (idMatch, idTournois, dateRencontre, heureRencontre, idEquipe1, idEquipe2) VALUES (NULL, :idTournois, :dateRencontre, :heureRencontre, :idEquipe1, :idEquipe2)');

                  $date = date("Y-m-d");
                  $time = date("H:i:s");
                  $requete->bindValue(':idTournois', $idTournois, PDO::PARAM_INT);
                  $requete->bindValue(':dateRencontre', $date, PDO::PARAM_STR);
                  $requete->bindValue(':heureRencontre', $time, PDO::PARAM_STR);
                  $requete->bindValue(':idEquipe1', $idEquipes[0][$i], PDO::PARAM_INT);
                  $requete->bindValue(':idEquipe2', $idEquipes[0][$j], PDO::PARAM_INT);

                  $correct_insertion = $requete->execute();

                  $requete4 = $db->prepare('INSERT INTO tournois.rencontre (idMatch, idTournois, dateRencontre, heureRencontre, idEquipe1, idEquipe2) VALUES (NULL, :idTournois, :dateRencontre, :heureRencontre, :idEquipe1, :idEquipe2)');

                  $date = date("Y-m-d");
                  $time = date("H:i:s");
                  $requete4->bindValue(':idTournois', $idTournois, PDO::PARAM_INT);
                  $requete4->bindValue(':dateRencontre', $date, PDO::PARAM_STR);
                  $requete4->bindValue(':heureRencontre', $time, PDO::PARAM_STR);
                  $requete4->bindValue(':idEquipe1', $idEquipes[0][$i], PDO::PARAM_INT);
                  $requete4->bindValue(':idEquipe2', $idEquipes[0][$j], PDO::PARAM_INT);

                  $correct_insertion4 = $requete4->execute();

                  if(!$correct_insertion || !$correct_insertion4){
                    return false;
                  }
                }
                else {
                  if(count($resutats_requetes[0]) == 1){

                    $requete = $db->prepare('INSERT INTO tournois.rencontre (idMatch, idTournois, dateRencontre, heureRencontre, idEquipe1, idEquipe2) VALUES (NULL, :idTournois, :dateRencontre, :heureRencontre, :idEquipe1, :idEquipe2)');

                    $date = date("Y-m-d");
                    $time = date("H:i:s");
                    $requete->bindValue(':idTournois', $idTournois, PDO::PARAM_INT);
                    $requete->bindValue(':dateRencontre', $date, PDO::PARAM_STR);
                    $requete->bindValue(':heureRencontre', $time, PDO::PARAM_STR);
                    $requete->bindValue(':idEquipe1', $idEquipes[0][$i], PDO::PARAM_INT);
                    $requete->bindValue(':idEquipe2', $idEquipes[0][$j], PDO::PARAM_INT);

                    $correct_insertion = $requete->execute();

                    if(!$correct_insertion){
                      return false;
                    }

                  }
                }
              }
            }
          }
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    $idTournois = $_GET['id'];
    $pseudo = $_GET['pseudo'];

    $send = planification_auto($idTournois);

    session_start();

    if ($send) {
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }
    else {
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }


 ?>
