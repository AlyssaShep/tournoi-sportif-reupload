<?php
    // envoie les infos d un membre de l equipe liste
    function send_info_member($lastname, $pseudo, $name, $idEquipe){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare("INSERT INTO tournois.joueur (idJoueur, idEquipeJ, pseudo, nom, prenom) VALUES (NULL, :idEquipeJ, :nom, :pseudo, :prenom)");

        // on lie les elemnents a une clef
        $requete->bindValue(':nom', $lastname, PDO::PARAM_STR);
        $requete->bindValue(':prenom', $name, PDO::PARAM_STR);
        $requete->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
        $requete->bindValue(':idEquipeJ', $idEquipe, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
    }

    // envoie les infos d un membre de l equipe liste avec en info le numero de tel et l email
    function send_info_member_tel_et_email($lastname, $name, $pseudo, $idEquipe, $num, $email){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare('INSERT INTO tournois.joueur VALUES (NULL, :idEquipeJ, :pseudo, :nom, :prenom, :telephone, :adresse)');

        // on lie les elemnents a une clef
        $requete->bindValue(':idEquipeJ', $idEquipe, PDO::PARAM_INT);
        $requete->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
        $requete->bindValue(':nom', $lastname, PDO::PARAM_STR);
        $requete->bindValue(':prenom', $name, PDO::PARAM_STR);
        $requete->bindValue(':telephone', $num, PDO::PARAM_INT);
        $requete->bindValue(':adresse', $email, PDO::PARAM_STR);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
    }

    //envoie les infos sur l equipe en general
    function send_info_equipe($nom_equipe, $niveau, $adresse, $num, $nbJ, $Esport){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare('INSERT INTO tournois.equipe (idEquipe, nom_equipe, niveau, adresse_equipe, equipe_tel, nb_joueur, Esport) VALUES (NULL, :nom_equipe, :niveau, :adresse_equipe, :equipe_tel, :nb_joueur, :Esport)');

        //on lie les elemnents a une clef
        $requete->bindValue(':nom_equipe', $nom_equipe, PDO::PARAM_STR);
        $requete->bindValue(':niveau', $niveau, PDO::PARAM_INT);
        $requete->bindValue(':adresse_equipe', $adresse, PDO::PARAM_STR);
        $requete->bindValue(':equipe_tel', $num, PDO::PARAM_INT);
        $requete->bindValue(':nb_joueur', $nbJ, PDO::PARAM_INT);
        $requete->bindValue(':Esport', $Esport, PDO::PARAM_STR);

        $correct_insertion = $requete->execute();

        if($correct_insertion){

        }
        else {
          $message = "Echec de la création";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }

    }

    //add capitaine a une equipe
    function add_capitaine_equipe($idJoueur, $idEquipe){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.equipe SET idCapitaine=:idCapitaine WHERE idEquipe = '{$idEquipe}'");

          $requete->bindValue(':idCapitaine', $idJoueur, PDO::PARAM_INT);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              return true;
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }

    }

    //ajoute le role de la personne en joueur
    function send_info_role_isjoueur($idJoueur){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          // insertion
          //a modifier les noms des cles et ajouter pseudo a la BDD
          $requete = $db->prepare("INSERT INTO tournois.role (id_role, is_joueur) VALUES (NULL, :is_joueur)");

          // on lie les elemnents a une clef
          $requete->bindValue(':is_joueur', $idJoueur, PDO::PARAM_INT);

          $correct_insertion = $requete->execute();

          if($correct_insertion){
            $message =  "role Joueur ajouté";
            echo $message;
          }
          else {
            $message = "Echec de l ajout du role de joueur";
            echo $message;
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;

        }
    }

    //ajoute le role de Capitaine
    function send_info_role_iscapitaine($idJoueur){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          // insertion
          //a modifier les noms des cles et ajouter pseudo a la BDD
          $requete = $db->prepare("INSERT INTO tournois.role (id_role, is_capitaine) VALUES (NULL, :is_capitaine)");

          // on lie les elemnents a une clef
          $requete->bindValue(':is_capitaine', $idJoueur, PDO::PARAM_INT);

          $correct_insertion = $requete->execute();

          if($correct_insertion){
            return true;
          }
          else {
            return false;
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;

        }
    }

    //recupere l id de l equipe
    function get_idEquipe($nom_equipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
            $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE nom_equipe = '{$nom_equipe}'");

        //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = $requete->fetch();
              if($utilisateur == null){
                return false;
              }
              else {
                return $utilisateur['idEquipe'];
              }
            }
            else {
              print "Erreur de l excution de la requete";
            }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
        }
    }

    //recupere l id du joueur
    function get_idJoueur($idEquipe, $nom, $prenom){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
            $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE idEquipeJ = '{$idEquipe}' AND nom = '{$nom}' AND prenom = '{$prenom}'");

        //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = $requete->fetch();
              if($utilisateur == null){
                return false;
              }
              else {
                return $utilisateur['idJoueur'];
              }
            }
            else {
              print "Erreur de l excution de la requete";
            }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
        }
    }

    //voir si le nom de l equipe est deja pris
    function nomequipe_pris($nom_equipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
            $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE nom_equipe = '{$nom_equipe}'");

        //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = $requete->fetch();
              if($utilisateur == null){
                return false;
              }
              else {
                return true;
              }
            }
            else {
              print "Erreur de l excution de la requete";
            }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
        }
    }

    function verification_personne($nom, $prenom, $pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
            $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE nom = '{$nom}' AND prenom = '{$prenom}' AND pseudo = '{$pseudo}'");

        //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = $requete->fetch();
              if($utilisateur == null){
                return false;
              }
              else {
                return true;
              }
            }
            else {
              print "Erreur de l excution de la requete";
            }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
        }
    }

    // verif si les pseudos sont tous differents
    function identique_pseudo($pseudo, $pseudo2, $pseudo3, $pseudo4, $pseudo5){
      if ($pseudo == $pseudo2) {
        return true;
      }

      if ($pseudo == $pseudo3) {
        return true;
      }

      if($pseudo == $pseudo4){
        return true;
      }

      if($pseudo == $pseudo5){
        return true;
      }

      if ($pseudo2 == $pseudo3) {
        return true;
      }

      if($pseudo2 == $pseudo4){
        return true;
      }

      if($pseudo2 == $pseudo5){
        return true;
      }

      if ($pseudo3 == $pseudo4) {
        return true;
      }

      if($pseudo3 == $pseudo5){
        return true;
      }

      if($pseudo4 == $pseudo5){
        return true;
      }

      return false;
    }

    $nom_equipe = $_POST["Nom_Tournois"];
    $nomequipepris = nomequipe_pris($nom_equipe);

    $Esport = $_POST["Esport"];
    $niveau = $_POST["Niveau"];
    $adresse = $_POST["Email"];
    $num = $_POST["Numero"];
    // var_dump($Esport);
    if($Esport == "LOL" || $Esport == "CS"){
        $nbJ = 5;
    }
    else {
      $nbJ = 2;
    }

    $pseudoCapitaine = $_POST["pseudoCapitaine"];
    $nomCapitaine = $_POST["nomCapitaine"];
    $prenomCapitaine = $_POST["prenomCapitaine"];
    $emailCapitaine = $_POST["emailCapitaine"];
    $numCapitaine = $_POST["NumeroCapitaine"];

    $pseudo2 = $_POST["pseudo2"];
    $nom2 = $_POST["nomEP2"];
    $prenom2 = $_POST["prenomEP2"];
    if (empty($_POST['emailEP2'])) {
      $mail2 = null;
    }
    else {
      $mail2 = $_POST["emailEP2"];
    }

    if (empty($_POST['NumeroEP2'])) {
      $num2 = null;
    }
    else {
      $num2 = $_POST["NumeroEP2"];
    }


    if (isset($_POST['pseudo3'])) {
      $pseudo3 = $_POST["pseudo3"];
      $nom3 = $_POST["nomEP3"];
      $prenom3 = $_POST["prenomEP3"];

      if (empty($_POST['emailEP3'])) {
        $mail3 = null;
      }
      else {
        $mail3 = $_POST["emailEP3"];
      }

      if (empty($_POST['NumeroEP3'])) {
        $num3 = null;
      }
      else {
        $num3 = $_POST["NumeroEP3"];
      }

    }

    if (isset($_POST['pseudo4'])) {
      $pseudo4 = $_POST["pseudo4"];
      $nom4 = $_POST["nomEP4"];
      $prenom4 = $_POST["prenomEP4"];

      if (empty($_POST['emailEP4'])) {
        $mail4 = null;
      }
      else {
        $mail4 = $_POST["emailEP4"];
      }

      if (empty($_POST['NumeroEP4'])) {
        $num4 = null;
      }
      else {
        $num4 = $_POST["NumeroEP4"];
      }

    }

    if (isset($_POST['pseudo5'])) {
      $pseudo5 = $_POST["pseudo5"];
      $nom5 = $_POST["nomEP5"];
      $prenom5 = $_POST["prenomEP5"];

      if (empty($_POST['emailEP5'])) {
        $mail5 = null;
      }
      else {
        $mail5 = $_POST["emailEP5"];
      }

      if (empty($_POST['NumeroEP5'])) {
        $num5 = null;
      }
      else {
        $num5 = $_POST["NumeroEP5"];
      }

    }

    session_start();

    if (!empty($_POST['pseudo3']) && !empty($_POST['pseudo4']) && !empty($_POST['pseudo5'])) {
        $verif1 = identique_pseudo($pseudoCapitaine, $pseudo2, $pseudo3, $pseudo4, $pseudo5);
    }

    // var_dump($verif1);
    // var_dump(empty($_POST['pseudo5']));
    if (isset($verif1)) {
      if ($verif1) {
        header("Location:../vue/creation_equipe.php?pseudo=".$_SESSION["pseudo"]);
        exit();
      }
    }

    if ($Esport == "SB") {
      $verificationJ2 = verification_personne($nom2, $prenom2, $pseudo2);
      var_dump($verificationJ2);
      var_dump($nomequipepris);

      if($nomequipepris && !$verificationJ2){
        $_SESSION["pseudo"] = $pseudoCapitaine;
        header("Location:../vue/creation_equipe.php?pseudo=".$_SESSION["pseudo"]);
        exit();
      }
      else {
        //creation de l equipe
        send_info_equipe($nom_equipe, $niveau, $adresse, $num, $nbJ, $Esport);
        //on recupere l id de l equipe
        $idEquipe = get_idEquipe($nom_equipe);
        //envoie des donnees des joueurs
        send_info_member_tel_et_email($nomCapitaine, $prenomCapitaine, $pseudoCapitaine, $idEquipe, $numCapitaine ,$emailCapitaine);
        send_info_member_tel_et_email($nom2, $prenom2, $pseudo2, $idEquipe, $num2, $mail2);

        $idCapitaine = get_idJoueur($idEquipe, $nomCapitaine, $prenomCapitaine);
        $idE2 = get_idJoueur($idEquipe, $nom2, $prenom2);
        add_capitaine_equipe($idCapitaine, $idEquipe);

        //ajout des roles
        send_info_role_iscapitaine($idCapitaine);
        send_info_role_isjoueur($idE2);

        $_SESSION["pseudo"] = $pseudoCapitaine;
        header("Location:../vue/mesEquipes.php?pseudo=".$_SESSION["pseudo"]);
        exit();
      }
    }
    else {
      if( empty($nom2) || empty($prenom2) || empty($pseudo2) || empty($nom3) || empty($prenom3) || empty($pseudo3) || empty($nom4) || empty($prenom4) || empty($pseudo4) || empty($nom5) || empty($prenom5) || empty($pseudo5) ){
        // echo "sortie";
        $_SESSION["pseudo"] = $pseudoCapitaine;
        header("Location:../vue/creation_equipe.php?pseudo=".$_SESSION['pseudo']);
        exit();
      }
      else {
        // echo "passage";
        $verificationJ2 = verification_personne($nom2, $prenom2, $pseudo2);
        $verificationJ3 = verification_personne($nom3, $prenom3, $pseudo3);
        $verificationJ4 = verification_personne($nom4, $prenom4, $pseudo4);
        $verificationJ5 = verification_personne($nom5, $prenom5, $pseudo5);

      }
      var_dump($verificationJ2);

      if($nomequipepris && $verificationJ2 && $verificationJ3 && $verificationJ4 && $verificationJ5){
        // echo "verification";
        $_SESSION["pseudo"] = $pseudoCapitaine;
        header("Location:../vue/creation_equipe.php?pseudo=".$_SESSION["pseudo"]);
        exit();
      }
      else {
        // echo "la";
        // creation de l equipe
        send_info_equipe($nom_equipe, $niveau, $adresse, $num, $nbJ, $Esport);
        //on recupere l id de l equipe
        $idEquipe = get_idEquipe($nom_equipe);
        //envoie des donnees des joueurs
        send_info_member_tel_et_email($nomCapitaine, $prenomCapitaine, $pseudoCapitaine, $idEquipe, $numCapitaine ,$emailCapitaine);
        send_info_member_tel_et_email($nom2, $prenom2, $pseudo2, $idEquipe, $num2, $mail2);
        send_info_member_tel_et_email($nom3, $prenom3, $pseudo3, $idEquipe, $num3, $mail3);
        send_info_member_tel_et_email($nom4, $prenom4, $pseudo4, $idEquipe, $num4, $mail4);
        send_info_member_tel_et_email($nom5, $prenom5, $pseudo5, $idEquipe, $num5, $mail5);
        //retourne les id des joueurs pour les ajouter a l equipe et ajouter le capitaine a lequipe
        $idCapitaine = get_idJoueur($idEquipe, $nomCapitaine, $prenomCapitaine);
        $idE2 = get_idJoueur($idEquipe, $nom2, $prenom2);
        $idE3 = get_idJoueur($idEquipe, $nom3, $prenom3);
        $idE4 = get_idJoueur($idEquipe, $nom4, $prenom4);
        $idE5 = get_idJoueur($idEquipe, $nom5, $prenom5);
        add_capitaine_equipe($idCapitaine, $idEquipe);

        //ajout des roles
        send_info_role_iscapitaine($idCapitaine);
        send_info_role_isjoueur($idE2);
        send_info_role_isjoueur($idE3);
        send_info_role_isjoueur($idE4);
        send_info_role_isjoueur($idE5);

        $_SESSION["pseudo"] = $pseudoCapitaine;
        header("Location:../vue/mesEquipes.php?pseudo=".$_SESSION["pseudo"]);
        exit();
      }
    }


 ?>
