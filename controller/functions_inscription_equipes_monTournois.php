<?php

// compte le nombre d equipes
    function compt_nbr_equipes_tournois($idTournois){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $tournois = array($requete->fetchAll());
            $idEquipes = $tournois[0][0]['idEquipe'];
            $size = strlen($idEquipes);
            $nbr = 0;

            for ($i=0; $i < $size; $i++) {
              if ($idEquipes[$i] == ",") {
                $nbr++;
              }
            }

              return $nbr;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

// verifie si une equipe est deja preinscrite dans une competition
    function deja_preinscrits_autre_competition($recupEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        if (empty($recupEquipe[0])) {
          return false;
        }

        $size = count($recupEquipe[0]);
        for ($i=0; $i < $size; $i++) {
            $idEquipe = $recupEquipe[0][$i];
            $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}' AND idTournois IS NOT NULL");

            //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = array($requete->fetchAll());

              if(!(empty($utilisateur[0]))){
                return true;
              }

          }
        }

        return false;


      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si l equipe est déjà inscripte a une competition en cours
    function deja_inscrits_autre_competition($idEquipes){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        if (empty($idEquipes[0])) {
          return false;
        }

        $size = count($idEquipes[0]);
        for ($i=0; $i < $size; $i++) {
            $idEquipe = $idEquipes[0][$i];
            $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE ADDDATE(dateDebut, INTERVAL duree DAY) >= NOW() AND dateDebut <= NOW() AND (idEquipe LIKE '%{$idEquipe},%') ORDER BY Esport ASC");

            //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = array($requete->fetchAll());

              if(!(empty($utilisateur[0]))){
                return true;
              }

          }
        }

        return false;


      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les joueurs d une equipe
    function get_joueurs_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE idEquipeJ = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if(empty($utilisateur[0])){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne l id du joueur
    function get_id_joueur($pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE pseudo = '{$pseudo}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if(empty($utilisateur[0])){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les infos sur la competition
    function get_info_competition($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if(empty($utilisateur[0])){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les equipes qui y participent
    function get_equipes_participantes($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          $equipes = array($utilisateur[0][0]['idEquipe']);
          $longueur = strlen($equipes[0]);
          $idrecuparray = array();
          $idrecup = "";
          $id = 0;

          for ($i=0; $i < $longueur; $i++) {
            if ($equipes[0][$i] == ",") {
                $idrecuparray[0][$id] = $idrecup;
                $id++;
                $idrecup = "";
            }
            else {
              $idrecup = $idrecup.$equipes[0][$i];
            }
          }
          return $idrecuparray;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les info sur l equipe
    function get_info_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if(empty($utilisateur[0])){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les équipes d un joueur et selectionne celle qui ont le bon Esport
    function get_equipe_joueur($pseudo, $Esport){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE pseudo = '{$pseudo}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if(empty($utilisateur[0])){
            // le joueur n a pas d équipe du tout
            return false;
          }
          else {
            // on selectionne celle qui ont le bon Esport
            $size = count($utilisateur[0]);
            $idSelect = array();
            $id = 0;

            for ($i=0; $i < $size; $i++) {
              $info = get_info_equipe($utilisateur[0][$i]['idEquipeJ']);
                if($info[0][0]['Esport'] == $Esport && $info[0][0]['idTournois'] == null){
                  $idSelect[0][$id] = $utilisateur[0][$i]['idEquipeJ'];
                  $id++;
                }
            }
            return $idSelect;
          }
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // avoir toutes les equipes
    function get_all_equipes($pseudo, $Esport){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE pseudo = '{$pseudo}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if(empty($utilisateur[0])){
            // le joueur n a pas d équipe du tout
            return false;
          }
          else {
            // on selectionne celle qui ont le bon Esport
            $size = count($utilisateur[0]);
            $idSelect = array();
            $id = 0;

            for ($i=0; $i < $size; $i++) {
              $info = get_info_equipe($utilisateur[0][$i]['idEquipeJ']);
                if($info[0][0]['Esport'] == $Esport && $utilisateur[0][$i]['idJoueur'] == $info[0][0]['idCapitaine']){
                  $idSelect[0][$id] = $utilisateur[0][$i]['idEquipeJ'];
                  $id++;
              }
            }
            return $idSelect;
          }
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne le pseudo du capitaine avec son id
    function get_pseudo_capitaine($idCapitaine){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE idjoueur = '{$idCapitaine}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if(empty($utilisateur[0])){
            return false;
          }
          else {
            return $utilisateur[0][0]['pseudo'];
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // verification de si la personne n est pas déjà inscripte a la competition
    function verif_inscription($recupEquipe, $idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            if(empty($utilisateur[0]) || empty($recupEquipe[0])){
              return false;
            }
            $size = count($recupEquipe[0]);
            $equipesTournois = $utilisateur[0][0]['idEquipe'];
            $sizeT = strlen($equipesTournois);
            $idrecup = "";
            for ($i=0; $i < $sizeT; $i++) {
              // var_dump($idrecup);
              if ($equipesTournois[$i] == ",") {
                for ($j=0; $j < $size; $j++) {
                  // var_dump($recupEquipe[0][$j]);
                  if ($idrecup == $recupEquipe[0][$j]) {
                    return true;
                  }
                }
                $idrecup = "";
              }
              else {
                $idrecup = $idrecup.$equipesTournois[$i];
              }
            }
            return false;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // verifie si l equipe est preinscipte a un tournois
    function verif_preinscription($recupEquipe, $idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idTournois = '{$idTournois}' ");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if(empty($utilisateur[0])){
            return false;
          }
          else {
            if(empty($recupEquipe)){
              return false;
            }
            $size = count($recupEquipe[0]);
            $sizeT = count($utilisateur[0]);
            for ($i=0; $i < $sizeT; $i++) {
              // var_dump($idrecup);
                for ($j=0; $j < $size; $j++) {
                  // var_dump($recupEquipe[0][$j]);
                  if ($utilisateur[0][$i]['idEquipe'] == $recupEquipe[0][$j]) {
                    return true;
                  }
                }
            }
            return false;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // preinsciption de l équipe
    function preinsciption($idEquipe, $idTournois){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.equipe SET idTournois=:idTournois WHERE idEquipe = '{$idEquipe}'");

          $requete->bindValue(':idTournois', $idTournois, PDO::PARAM_INT);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              return true;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

// verfie si une competition est complete
    function verif_compet_complete($idTournois){
      $nbre_equipes_inscrites = compt_nbr_equipes_tournois($idTournois);
      $nbre_equipes_total = get_info_competition($idTournois)[0][0]['nbr_equipes'];
      if ($nbre_equipes_total == $nbre_equipes_inscrites) {
        return true;
      }
      return false;
    }

    if(isset($_POST['Valider'])){
      $nbre_equipes = $_POST['nbre_equipes'];
      $check = 0;

      for ($i=0; $i < $nbre_equipes; $i++) {
        if (isset($_POST['check'.$i])) {
          if ($_POST['check'.$i] == "on") {
            $idEquipe = $_POST['idEquipe'.$i];
            $check++;
          }
        }
      }

      session_start();

      if ($check == 0 || $check > 2) {
        // ne doit etre selectionné qu une seule équipe
        $_SESSION['pseudo'] = $_POST['pseudo'];
        $_SESSION['id'] = $idTournois;
        header("Location:../vue/inscription_equipes_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
        exit();
      }
      else {
        $idTournois = $_POST['idTournois'];
        $send = preinsciption($idEquipe, $idTournois);

        if ($send) {
          $_SESSION['pseudo'] = $_POST['pseudo'];
          header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
          exit();
        }
        else {
          $_SESSION['pseudo'] = $_POST['pseudo'];
          $_SESSION['id'] = $idTournois;
          header("Location:../vue/inscription_equipes_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
          exit();
        }
      }
    }

 ?>
