<?php

  // envoie les infos d un membre de l equipe liste
  function send_info_member($lastname, $name, $idEquipe){

    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      // insertion
      //a modifier les noms des cles et ajouter pseudo a la BDD
      $requete = $db->prepare("INSERT INTO tournois.joueur (idJoueur, idEquipeJ, nom, prenom) VALUES (NULL, :idEquipeJ, :nom, :prenom)");

      // on lie les elemnents a une clef
      $requete->bindValue(':nom', $lastname, PDO::PARAM_STR);
      $requete->bindValue(':prenom', $name, PDO::PARAM_STR);
      $requete->bindValue(':idEquipeJ', $idEquipe, PDO::PARAM_INT);

      $correct_insertion = $requete->execute();

      if($correct_insertion){
        $message =  "Equipier ajouté";
        echo $message;
      }
      else {
        $message = "Echec de l ajout de l équipier";
        echo $message;
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;

    }
  }

  // envoie les infos d un membre de l equipe liste avec en info le numero de tel et l email
  function send_info_member_tel_et_email($lastname, $name, $idEquipe, $num, $email){

    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      // insertion
      //a modifier les noms des cles et ajouter pseudo a la BDD
      $requete = $db->prepare('INSERT INTO tournois.joueur VALUES (NULL, :idEquipeJ, :nom, :prenom, :telephone, :adresse)');

      // on lie les elemnents a une clef
      $requete->bindValue(':nom', $lastname, PDO::PARAM_STR);
      $requete->bindValue(':prenom', $name, PDO::PARAM_STR);
      $requete->bindValue(':idEquipeJ', $idEquipe, PDO::PARAM_INT);
      $requete->bindValue(':telephone', $num, PDO::PARAM_INT);
      $requete->bindValue(':adresse', $email, PDO::PARAM_STR);

      $correct_insertion = $requete->execute();

      if($correct_insertion){
        $message =  "Equipier ajouté";
        echo $message;
      }
      else {
        $message = "Echec de l ajout de l équipier";
        echo $message;
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;

    }
  }

  // envoie les infos sur la competition
  function send_info_compet($dateD, $duree, $lieu, $type, $nom_tournois, $idEquipe, $idGestionnaire, $nbreEquipe, $Esport){

    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      // insertion
      //a modifier les noms des cles et ajouter pseudo a la BDD
      $requete = $db->prepare('INSERT INTO tournois.tournois VALUES (NULL, :dateDebut, :duree, :idGestionnaire, :lieu, :type, :idEquipe, :nom_tournois, :nbr_equipes, :Esport)');

      //on lie les elemnents a une clef
      $requete->bindValue(':dateDebut', $dateD, PDO::PARAM_STR);
      $requete->bindValue(':duree', $duree, PDO::PARAM_INT);
      $requete->bindValue(':lieu', $lieu, PDO::PARAM_STR);
      $requete->bindValue(':type', $type, PDO::PARAM_STR);
      $requete->bindValue(':nom_tournois', $nom_tournois, PDO::PARAM_STR);
      $requete->bindValue(':idEquipe', $idEquipe, PDO::PARAM_INT);
      $requete->bindValue(':nbr_equipes', $nbreEquipe, PDO::PARAM_INT);
      $requete->bindValue(':Esport', $Esport, PDO::PARAM_STR);
      $requete->bindValue(':idGestionnaire', $idGestionnaire, PDO::PARAM_INT);

      $correct_insertion = $requete->execute();

      if($correct_insertion){
        $message =  "Competition créée";
        echo $message;
      }
      else {
        $message = "Echec de la création";
        echo $message;
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;

    }

  }

  //envoie les infos sur l equipe en general
  function send_info_equipe($nom_equipe, $niveau, $adresse, $num, $nbJ, $idCapitaine, $Esport){

    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      // insertion
      //a modifier les noms des cles et ajouter pseudo a la BDD
      $requete = $db->prepare('INSERT INTO tournois.equipe (idEquipe, nom_equipe, niveau, adresse_equipe, equipe_tel, nb_joueur, idCapitaine, Esport) VALUES (NULL, :nom_equipe, :niveau, :adresse_equipe, :equipe_tel, :nb_joueur, :idCapitaine, :Esport)');

      //on lie les elemnents a une clef
      $requete->bindValue(':nom_equipe', $nom_equipe, PDO::PARAM_STR);
      $requete->bindValue(':niveau', $niveau, PDO::PARAM_INT);
      $requete->bindValue(':adresse_equipe', $adresse, PDO::PARAM_STR);
      $requete->bindValue(':equipe_tel', $num, PDO::PARAM_INT);
      $requete->bindValue(':nb_joueur', $nbJ, PDO::PARAM_INT);
      $requete->bindValue(':idCapitaine', $idCapitaine, PDO::PARAM_INT);
      $requete->bindValue(':Esport', $Esport, PDO::PARAM_STR);

      $correct_insertion = $requete->execute();

      if($correct_insertion){
        $message =  "Gestion de l équipe créée";
        echo $message;
      }
      else {
        $message = "Echec de la création";
        echo $message;
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;

    }

  }
  //ajoute le role de la personne en admin
  function send_info_role_isadmin($idAdmin){

    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      // insertion
      //a modifier les noms des cles et ajouter pseudo a la BDD
      $requete = $db->prepare("INSERT INTO tournois.role (id_role, is_admin) VALUES (NULL, :is_admin)");

      // on lie les elemnents a une clef
      $requete->bindValue(':is_admin', $idAdmin, PDO::PARAM_INT);

      $correct_insertion = $requete->execute();

      if($correct_insertion){
        $message =  "role d admin ajouté";
        echo $message;
      }
      else {
        $message = "Echec de l ajout du role admin";
        echo $message;
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;

    }
  }

  //ajoute le role de la personne en Gestionnaire
  function send_info_role_isgestionnaire($idGestionnaire){

    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      // insertion
      //a modifier les noms des cles et ajouter pseudo a la BDD
      $requete = $db->prepare("INSERT INTO tournois.role (id_role, is_gestionnaire) VALUES (NULL, :is_gestionnaire)");

      // on lie les elemnents a une clef
      $requete->bindValue(':is_gestionnaire', $idGestionnaire, PDO::PARAM_INT);

      $correct_insertion = $requete->execute();

      if($correct_insertion){
        $message =  "role de Gestionnaire ajouté";
        echo $message;
      }
      else {
        $message = "Echec de l ajout du role Gestionnaire";
        echo $message;
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;

    }
  }

  //ajoute le role de la personne en joueur
  function send_info_role_isjoueur($idJoueur){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare("INSERT INTO tournois.role (id_role, is_joueur) VALUES (NULL, :is_joueur)");

        // on lie les elemnents a une clef
        $requete->bindValue(':is_joueur', $idJoueur, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "role Joueur ajouté";
          echo $message;
        }
        else {
          $message = "Echec de l ajout du role de joueur";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
  }




 ?>
