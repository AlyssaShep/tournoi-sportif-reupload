<?php

// renvoie le classement du championnat
    function classement_championnat($all_equipes, $gagnant){
      $classement = array();
      $classement[0][0]['Equipe'] = $gagnant[0][0]['Equipe'];
      $classement[0][0]['pts'] = $gagnant[0][0]['pts'];
      $max = $gagnant[0][0]['pts'];
      $size = count($all_equipes[0]);
      for ($i=0; $i < $size; $i++) {
        for ($j=0; $j < $size; $j++) {
          if ($all_equipes[0][$i]['pts'] > $all_equipes[0][$j]['pts'] && $i > $j) {
            $temp[0][0]['Equipe'] = $all_equipes[0][$i]['Equipe'];
            $temp[0][0]['pts'] = $all_equipes[0][$i]['pts'];

            $all_equipes[0][$i]['pts'] = $all_equipes[0][$j]['pts'];
            $all_equipes[0][$i]['Equipe'] = $all_equipes[0][$i]['Equipe'];

            $all_equipes[0][$j]['pts'] = $temp[0][0]['pts'];
            $all_equipes[0][$j]['Equipe'] = $temp[0][0]['Equipe'];
          }
        }
      }

      for ($i=0; $i < $size; $i++) {
        $classement[0][$i]['Equipe'] = $all_equipes[0][$i]['Equipe'];
        $classement[0][$i]['pts'] = $all_equipes[0][$i]['pts'];
      }

      return $classement;
    }

    // renvoie le classement du championnat avec des egalites
    function classement_championnat_egalite($all_equipes, $gagnant){
        $classement = array();
        $classement[0][0]['Equipe'] = $gagnant[0][0]['Equipe'];
        $classement[0][0]['pts'] = $gagnant[0][0]['pts'];
        $max = $gagnant[0][0]['pts'];
        $size = count($all_equipes[0]);
        for ($i=0; $i < $size; $i++) {
          for ($j=0; $j < $size; $j++) {
            if ($all_equipes[0][$i]['pts'] > $all_equipes[0][$j]['pts'] && $i > $j) {
              $temp[0][0]['Equipe'] = $all_equipes[0][$i]['Equipe'];
              $temp[0][0]['pts'] = $all_equipes[0][$i]['pts'];

              $all_equipes[0][$i]['pts'] = $all_equipes[0][$j]['pts'];
              $all_equipes[0][$i]['Equipe'] = $all_equipes[0][$i]['Equipe'];

              $all_equipes[0][$j]['pts'] = $temp[0][0]['pts'];
              $all_equipes[0][$j]['Equipe'] = $temp[0][0]['Equipe'];
            }
          }
        }

        for ($i=1; $i < $size; $i++) {
          $classement[0][$i]['Equipe'] = $all_equipes[0][$i]['Equipe'];
          $classement[0][$i]['pts'] = $all_equipes[0][$i]['pts'];
        }

        return $classement;

    }

// envoie les infos de la poule dans la bdd
    function send_info_poule($idTournois, $id1, $id2, $id3, $id4){
      $user = 'root';
      $pass = '';
      $serveur = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($serveur,$user,$pass);

      try {
        // insertion
        //a modifier le nom de la table et les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare('INSERT INTO tournois.poule (idPoule, idTournois, idEP1, idEP2, idEP3, idEP4) VALUES (NULL, :idTournois, :idEP1, :idEP2, :idEP3, :idEP4)');

        //on lie les elemnents a une clef
        $requete->bindValue(':idTournois', $idTournois, PDO::PARAM_INT);
        $requete->bindValue(':idEP1', $id1, PDO::PARAM_STR);
        $requete->bindValue(':idEP2', $id2, PDO::PARAM_STR);
        $requete->bindValue(':idEP3', $id3, PDO::PARAM_INT);
        $requete->bindValue(':idEP4', $id4, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// envoie le tableau des selectionnes dans la bdd
    function envoie_tab_select($toutgagnant, $idTournois){
      $user = 'root';
      $pass = '';
      $serveur = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($serveur,$user,$pass);

      try {

        $requete = $db->prepare("UPDATE tournois.tournois SET arbreBin=:arbreBin WHERE idTournois = '{$idTournois}'");

        //on lie les elemnents a une clef
        $requete->bindValue(':arbreBin', serialize($toutgagnant), PDO::PARAM_STR);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne le tableau des gagnants des selections
    function recup_4($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          $selections = unserialize($utilisateur[0][0]['arbreBin']);
          return $selections;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si des rencontres ont ete prevues pour ces equipes la
    function quart_final($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];
        // var_dump($eq1);
        // var_dump($eq2);
        // var_dump($eq3);
        // var_dump($eq4);

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND ( (idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq3}') OR (idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq1}') )");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND ( (idEquipe1 = '{$eq4}' AND idEquipe2 = '{$eq2}') OR (idEquipe1 = '{$eq2}' AND idEquipe2 = '{$eq4}') )");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();
        $execution_requete2 = $requete2->execute();

        if($execution_requete && $execution_requete2){
          $re1 = array($requete->fetchAll());
          $re2 = array($requete2->fetchAll());

          if (empty($re1[0]) || empty($re2[0])) {
            return false;
          }
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si le tableau des selections est dans la bdd
    function tab_select($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE arbreBin IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if (empty($utilisateur[0])) {
            return true;
          }

          return false;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on recupere les gagnants de la poule
    function recup_gagnants_poules($idPoule){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.poule WHERE idPoule = '{$idPoule}' AND idGagnant1 IS NOT NULL AND idGagnant2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          $gagnants = array();
          $gagnants[0][0] = $utilisateur[0][0]['idGagnant1'];
          $gagnants[0][1] = $utilisateur[0][0]['idGagnant2'];
          return $gagnants;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si les poules ont déjà été créées
    function poules($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.poule WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if ($utilisateur[0] == null) {
            // on crée les poules si c est pas deja fait
            $requete1 = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");
            $execution_requete = $requete1->execute();

            $tournois = array($requete1->fetchAll());

            $size = strlen($tournois[0][0]['idEquipe']);
            $parcours = $tournois[0][0]['idEquipe'];
            $equipes = array();
            $idrecup = "";
            $id = 0;

            for ($i=0; $i < $size; $i++) {
              if ($parcours[$i] == ",") {
                $equipes[0][$id] = $idrecup;
                $id++;
                $idrecup = "";
              }else {
                $idrecup = $idrecup.$parcours[$i];
              }
            }

            $size1 = count($equipes[0]);
            $compt = 0;
            $eq1 = 0;
            $eq2 = 0;
            $eq3 = 0;
            $eq4 = 0;

            for ($j=0; $j < $size1; $j++) {

              if ($compt == 0) {
                $eq1 = $equipes[0][$j];
                $compt++;
              }else {
                if ($compt == 1) {
                  $eq2 = $equipes[0][$j];
                  $compt++;
                }else {
                  if ($compt == 2) {
                    $eq3 = $equipes[0][$j];
                    $compt++;
                  }else {
                    if ($compt == 3) {
                      $eq4 = $equipes[0][$j];
                      $compt = 0;
                      $send = send_info_poule($idTournois, $eq1, $eq2, $eq3, $eq4);

                      if ($send == false) {
                        return false;
                      }

                      $eq1 = 0;
                      $eq2 = 0;
                      $eq3 = 0;
                      $eq4 = 0;

                    }
                  }
                }
              }

            }

            return true;

          }

        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// recupere toutes les poules d un tournoi
    function get_poule_tournois($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.poule WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// regarde si les equipes se sont toutes affrontées
    function verif_affrontees($idEquipes, $idRencontreE){
        $sizeE = count($idEquipes[0]);
        $sizeR = count($idRencontreE[0]);
        $change = false;

        for ($i=0; $i < $sizeR; $i++) {
          $change = false;
          for ($j=0; $j < $sizeE; $j++) {
            if($idEquipes[0][$j] == $idRencontreE[0][$i]){
                $change = true;
            }
            if ($j == ($sizeE - 1) && $change == false) {
              return false;
            }
          }
        }

        return true;
    }

// renvoie les rencontres du tournoi
    function get_rencontre_tournois($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' ORDER BY idEquipe1");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// ajout des donnees pour la rencontre
    function ajout_donnees_rencontre($idTournois, $dateRencontre, $heureRencontre, $id1, $id2){
      $user = 'root';
      $pass = '';
      $serveur = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($serveur,$user,$pass);

      try {
        // insertion
        //a modifier le nom de la table et les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare('INSERT INTO tournois.rencontre (idMatch, idTournois, dateRencontre, heureRencontre, idEquipe1, idEquipe2) VALUES (NULL, :idTournois, :dateRencontre, :heureRencontre, :idEquipe1, :idEquipe2)');

        //on lie les elemnents a une clef
        $requete->bindValue(':idTournois', $idTournois, PDO::PARAM_INT);
        $requete->bindValue(':dateRencontre', $dateRencontre, PDO::PARAM_STR);
        $requete->bindValue(':heureRencontre', $heureRencontre, PDO::PARAM_STR);
        $requete->bindValue(':idEquipe1', $id1, PDO::PARAM_INT);
        $requete->bindValue(':idEquipe2', $id2, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// recupere les equipes du tournois
    function recup_idEquipe($idTournois, $rang){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $tournois = array($requete->fetchAll());

          $equipes = array($tournois[0][0]['idEquipe']);
          $longueur = strlen($equipes[0]);
          $idrecup = "";
          $nbr = 0;

          for ($i=0; $i < $longueur; $i++) {
            if (($equipes[0][$i] == ",") && ($rang == $nbr)) {
              return $idrecup;
            }
            if ($equipes[0][$i] == ",") {
              $idrecup = "";
              $nbr++;
            }
            else {
              $idrecup = $idrecup.$equipes[0][$i];
            }
          }
          return null;

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si une equipe s est deja affronté 2 fois pour un type championnat
    function championnat_deja_affronte($idTournois, $id1, $id2){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND ( (idEquipe1 = '{$id1}' AND idEquipe2 = '{$id2}') OR (idEquipe1 = '{$id2}' AND idEquipe2 = '{$id1}') )");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          // var_dump($utilisateur);
          // echo "<br>";
          // echo "<br>";

          if(empty($utilisateur)){
            return false;
          }
          else {
            if (count($utilisateur[0]) == 2) {
              return true;
            }
            return false;
          }
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    function championnat_a_score($idTournois, $id1, $id2){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND ( (idEquipe1 = '{$id1}' AND idEquipe2 = '{$id2}') OR (idEquipe1 = '{$id2}' AND idEquipe2 = '{$id1}') AND score1 IS NOT NULL AND score2 IS NOT NULL )");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          // var_dump($utilisateur);
          // echo "<br>";
          // echo "<br>";

          if(empty($utilisateur)){
            return false;
          }
          else {
            if (count($utilisateur[0]) == 2) {
              return true;
            }
            return false;
          }
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si une equipe s est deja rencontre pour un type poule
    function poule_deja_affronte($idPoule, $id1, $id2){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idPoule}' AND idEquipe1 = '{$id1}' AND idEquipe2 = '{$id2}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          if ($utilisateur == null) {
            return false;
          }
          $size = count($utilisateur[0]);

          if($size == 1){
            return true;
          }
          else {
            return false;
          }
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // verif si le tournoi est terminé
    function championnat_termine($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          $equipes = array();
          $det = $utilisateur[0][0]['idEquipe'];
          $longueur = strlen($utilisateur[0][0]['idEquipe']);
          $idrecup = "";
          $id = 0;

          // recuperation sous forme de tableau des equipes
          for ($i=0; $i < $longueur; $i++) {
            // var_dump($det[$i]);
            if ($det[$i] == ",") {
              $equipes[0][$id] = $idrecup;
              $idrecup = "";
              $id++;
            }
            else {
              $idrecup = $idrecup.$det[$i];
            }
          }

          // on regarde si elles se sont deja affrontées 2 fois
          $size = count($equipes[0]);
          $deja_affronte = 0;
          for ($i=0; $i < $size; $i++) {
            for ($j=0; $j < $size; $j++) {
              if($i != $j){
                // echo "comparaison".$i."-".$j;
                // echo "equipes".$equipes[0][$i]."-".$equipes[0][$j];
                // echo "<br>";
                $bool = championnat_deja_affronte($idTournois, $equipes[0][$i], $equipes[0][$j]);
                if ($bool) {
                  $deja_affronte++;
                }
              }
            }
          }

          // nombre_equipe_total * (nombre_equipe_total - 1)
          $nbre_affrontement_total = $size * ($size - 1);
          // var_dump($deja_affronte);

          if ($deja_affronte < $nbre_affrontement_total) {
            return false;
          }

          return true;

        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si toutes les equipes ont leurs scores
    function championnat_scores_et_termines($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          $equipes = array();
          $det = $utilisateur[0][0]['idEquipe'];
          $longueur = strlen($utilisateur[0][0]['idEquipe']);
          $idrecup = "";
          $id = 0;

          // recuperation sous forme de tableau des equipes
          for ($i=0; $i < $longueur; $i++) {
            // var_dump($det[$i]);
            if ($det[$i] == ",") {
              $equipes[0][$id] = $idrecup;
              $idrecup = "";
              $id++;
            }
            else {
              $idrecup = $idrecup.$det[$i];
            }
          }

          // on regarde si elles se sont deja affrontées 2 fois
          $size = count($equipes[0]);
          $deja_affronte = 0;
          for ($i=0; $i < $size; $i++) {
            for ($j=0; $j < $size; $j++) {
              if($i != $j){
                // echo "comparaison".$i."-".$j;
                // echo "equipes".$equipes[0][$i]."-".$equipes[0][$j];
                // echo "<br>";
                $bool = championnat_a_score($idTournois, $equipes[0][$i], $equipes[0][$j]);
                if ($bool) {
                  $deja_affronte++;
                }
              }
            }
          }

          // nombre_equipe_total * (nombre_equipe_total - 1)
          $nbre_affrontement_total = $size * ($size - 1);
          // var_dump($deja_affronte);

          if ($deja_affronte < $nbre_affrontement_total) {
            return false;
          }

          return true;

        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // verif si la poule est terminé
    function poule_termine($idPoule){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.poule WHERE idPoule = '{$idPoule}' AND idGagnant1 IS NOT NULL AND idGagnant2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          // var_dump(isset($utilisateur[0]));

          if (empty($utilisateur[0])) {
            return false;
          }
          return true;

          // // on regarde si elles se sont deja affrontées 1 fois
          // $id = 1;
          // for ($i=0; $i < 4; $i++) {
          //   $equipes[0][$i] = $utilisateur[0][0]['idEp'.$id];
          //   $id++;
          // }
          //
          // $size = count($equipes[0]);
          // $deja_affronte = 0;
          // for ($i=0; $i < $size; $i++) {
          //   for ($j=0; $j < $size; $j++) {
          //     if($i != $j){
          //       // echo "comparaison".$i."-".$j;
          //       // echo "equipes".$equipes[0][$i]."-".$equipes[0][$j];
          //       // echo "<br>";
          //       $bool = poule_deja_affronte($idPoule, $equipes[0][$i], $equipes[0][$j]);
          //       if ($bool) {
          //         $deja_affronte++;
          //       }
          //     }
          //   }
          // }
          //
          // // Somme des nbre_equipes - i de 1 a nbr_equipes
          // $nbre_affrontement_total = 0;
          // // echo $size;
          // for ($i=1; $i <= $size; $i++) {
          //   $nbre_affrontement_total = $nbre_affrontement_total + ($size - $i);
          // }
          // // echo $nbre_affrontement_total;
          // // echo $size;
          // // echo $deja_affronte;
          //
          // if ($deja_affronte < $nbre_affrontement_total) {
          //   return false;
          // }
          //
          // return true;

        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// recupere les infos de la competition
    function recup_info_compet($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// recupere les infos sur l equipe
    function recup_info_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// info sur le Capitaine
    function info_capitaine($idCapitaine){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE idJoueur = '{$idCapitaine}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// recupere les idRencontre
    function recup_idrencontre($idTournois, $eq1, $eq3){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq3}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $rencontre1 = array($requete->fetchAll());

          if (empty($rencontre1[0])) {
            return false;
          }
          return $rencontre1[0][0]['idMatch'];
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verif si les quarts de ces equipes a eu lieu
    function verif_quart($base, $idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq3}' AND CONCAT(dateRencontre,' ',heureRencontre) < NOW()");
        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq2}' AND idEquipe2 = '{$eq4}' AND CONCAT(dateRencontre,' ',heureRencontre) < NOW()");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();
        $execution_requete1 = $requete1->execute();

        if($execution_requete || $execution_requete1){
          $rencontre1 = array($requete->fetchAll());
          $rencontre2 = array($requete1->fetchAll());
          // var_dump($rencontre1);
          // var_dump($rencontre2);

          if (empty($rencontre1[0]) || empty($rencontre2[0])) {
            return false;
          }
          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les resultas du quart de final ont ete mis
    function miss_ajout_resultats_quart_final($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq3}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq2}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();
        $execution_requete = $requete1->execute();

        if($execution_requete){
          $rencontre1 = array($requete->fetchAll());
          $rencontre2 = array($requete1->fetchAll());

          if (empty($rencontre1[0]) || empty($rencontre2[0])) {
            return true;
          }
          return false;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// recup des equipes qui ont gagné les quarts
    function recup_2($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq3}'");
        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq2}' AND idEquipe2 = '{$eq4}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();
        $execution_requete1 = $requete1->execute();

        if($execution_requete || $execution_requete1){
          $rencontre1 = array($requete->fetchAll());
          $rencontre2 = array($requete1->fetchAll());
          $gagnants = array();
          $gagnants[0][0] = $rencontre1[0][0]['idGagnant'];
          $gagnants[0][1] = $rencontre2[0][0]['idGagnant'];
          // var_dump($gagnants);

          return $gagnants;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si la demi-final est prevu
    function demi_final($idTournois,$recup_eq_demi){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $recup_eq_demi[0][0];
        $eq2 = $recup_eq_demi[0][1];

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $rencontre = array($requete->fetchAll());

          if(empty($rencontre[0])){
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si la demi-final a eu lieu
    function verif_demi($idTournois, $id1, $id2){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$id1}' AND idEquipe2 = '{$id2}' AND CONCAT(dateRencontre, ' ',heureRencontre) < NOW()");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $rencontre = array($requete->fetchAll());

          if(empty($rencontre[0])){
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si la rencontre a bien des scores
    function verif_miss_ajout_demi($idTournois, $id1, $id2){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$id1}' AND idEquipe2 = '{$id2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        $execution_requete = $requete->execute();

        if($execution_requete){
          $rencontre = array($requete->fetchAll());

          if(empty($rencontre[0])){
            return true;
          }

          return false;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne l id du gagnant
    function gagnant($idTournois, $id1, $id2){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$id1}' AND idEquipe2 = '{$id2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        $execution_requete = $requete->execute();

        if($execution_requete){
          $rencontre = array($requete->fetchAll());

          return $rencontre[0][0]['idGagnant'];
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne l id des equipes lors d un tournoi de size 2
    function recup_2_size2($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          $selections = unserialize($utilisateur[0][0]['arbreBin']);
          return $selections;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne les id equipes lors d un tournois de size 8
    function recup_8($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          $selections = unserialize($utilisateur[0][0]['arbreBin']);
          return $selections;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si les huitiemes de coupe sont prevus
    function huitieme_coupe($idTournois, $recupEq){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $recupEq[0][0]['Equipe'];
        $eq2 = $recupEq[0][1]['Equipe'];
        $eq3 = $recupEq[0][2]['Equipe'];
        $eq4 = $recupEq[0][3]['Equipe'];
        $eq5 = $recupEq[0][4]['Equipe'];
        $eq6 = $recupEq[0][5]['Equipe'];
        $eq7 = $recupEq[0][6]['Equipe'];
        $eq8 = $recupEq[0][7]['Equipe'];

        $eq9 = $recupEq[0][8]['Equipe'];
        $eq10 = $recupEq[0][9]['Equipe'];
        $eq11 = $recupEq[0][10]['Equipe'];
        $eq12 = $recupEq[0][11]['Equipe'];
        $eq13 = $recupEq[0][12]['Equipe'];
        $eq14 = $recupEq[0][13]['Equipe'];
        $eq15 = $recupEq[0][14]['Equipe'];
        $eq16 = $recupEq[0][15]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}'");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}'");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}'");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}'");
        $requete5 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq9}' AND idEquipe2 = '{$eq10}'");
        $requete6 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq11}' AND idEquipe2 = '{$eq12}'");
        $requete7 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq13}' AND idEquipe2 = '{$eq14}'");
        $requete8 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq15}' AND idEquipe2 = '{$eq16}'");


        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();
        $execution_requete = $requete5->execute();
        $execution_requete = $requete6->execute();
        $execution_requete = $requete7->execute();
        $execution_requete = $requete8->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());
          $rencontre5 = array($requete5->fetchAll());
          $rencontre6 = array($requete6->fetchAll());
          $rencontre7 = array($requete7->fetchAll());
          $rencontre8 = array($requete8->fetchAll());

          $compt = 0;

          if(!empty($rencontre1[0])){
            $compt++;
          }
          if(!empty($rencontre2[0])){
            $compt++;
          }
          if(!empty($rencontre3[0])){
            $compt++;
          }
          if(!empty($rencontre4[0])){
            $compt++;
          }

          if(!empty($rencontre5[0])){
            $compt++;
          }
          if(!empty($rencontre6[0])){
            $compt++;
          }
          if(!empty($rencontre7[0])){
            $compt++;
          }
          if(!empty($rencontre8[0])){
            $compt++;
          }

          return $compt;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si les huitiemes de coupe sont passes
    function huitieme_passes($idTournois, $recupEq){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $recupEq[0][0]['Equipe'];
        $eq2 = $recupEq[0][1]['Equipe'];
        $eq3 = $recupEq[0][2]['Equipe'];
        $eq4 = $recupEq[0][3]['Equipe'];
        $eq5 = $recupEq[0][4]['Equipe'];
        $eq6 = $recupEq[0][5]['Equipe'];
        $eq7 = $recupEq[0][6]['Equipe'];
        $eq8 = $recupEq[0][7]['Equipe'];

        $eq9 = $recupEq[0][8]['Equipe'];
        $eq10 = $recupEq[0][9]['Equipe'];
        $eq11 = $recupEq[0][10]['Equipe'];
        $eq12 = $recupEq[0][11]['Equipe'];
        $eq13 = $recupEq[0][12]['Equipe'];
        $eq14 = $recupEq[0][13]['Equipe'];
        $eq15 = $recupEq[0][14]['Equipe'];
        $eq16 = $recupEq[0][15]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete5 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq9}' AND idEquipe2 = '{$eq10}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete6 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq11}' AND idEquipe2 = '{$eq12}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete7 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq13}' AND idEquipe2 = '{$eq14}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete8 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq15}' AND idEquipe2 = '{$eq16}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");


        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();
        $execution_requete = $requete5->execute();
        $execution_requete = $requete6->execute();
        $execution_requete = $requete7->execute();
        $execution_requete = $requete8->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());
          $rencontre5 = array($requete5->fetchAll());
          $rencontre6 = array($requete6->fetchAll());
          $rencontre7 = array($requete7->fetchAll());
          $rencontre8 = array($requete8->fetchAll());

          $compt = 0;

          if(!empty($rencontre1[0])){
            $compt++;
          }
          if(!empty($rencontre2[0])){
            $compt++;
          }
          if(!empty($rencontre3[0])){
            $compt++;
          }
          if(!empty($rencontre4[0])){
            $compt++;
          }

          if(!empty($rencontre5[0])){
            $compt++;
          }
          if(!empty($rencontre6[0])){
            $compt++;
          }
          if(!empty($rencontre7[0])){
            $compt++;
          }
          if(!empty($rencontre8[0])){
            $compt++;
          }

          if ($compt == 8) {
            return true;
          }

          return false;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les scores de huitieme de coupe ont ete mis
    function huitieme_scores($idTournois, $recupEq){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $recupEq[0][0]['Equipe'];
        $eq2 = $recupEq[0][1]['Equipe'];
        $eq3 = $recupEq[0][2]['Equipe'];
        $eq4 = $recupEq[0][3]['Equipe'];
        $eq5 = $recupEq[0][4]['Equipe'];
        $eq6 = $recupEq[0][5]['Equipe'];
        $eq7 = $recupEq[0][6]['Equipe'];
        $eq8 = $recupEq[0][7]['Equipe'];

        $eq9 = $recupEq[0][8]['Equipe'];
        $eq10 = $recupEq[0][9]['Equipe'];
        $eq11 = $recupEq[0][10]['Equipe'];
        $eq12 = $recupEq[0][11]['Equipe'];
        $eq13 = $recupEq[0][12]['Equipe'];
        $eq14 = $recupEq[0][13]['Equipe'];
        $eq15 = $recupEq[0][14]['Equipe'];
        $eq16 = $recupEq[0][15]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete5 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq9}' AND idEquipe2 = '{$eq10}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete6 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq11}' AND idEquipe2 = '{$eq12}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete7 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq13}' AND idEquipe2 = '{$eq14}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete8 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq15}' AND idEquipe2 = '{$eq16}' AND score1 IS NOT NULL AND score2 IS NOT NULL");


        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();
        $execution_requete = $requete5->execute();
        $execution_requete = $requete6->execute();
        $execution_requete = $requete7->execute();
        $execution_requete = $requete8->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());
          $rencontre5 = array($requete5->fetchAll());
          $rencontre6 = array($requete6->fetchAll());
          $rencontre7 = array($requete7->fetchAll());
          $rencontre8 = array($requete8->fetchAll());

          $compt = 0;

          if(!empty($rencontre1[0])){
            $compt++;
          }
          if(!empty($rencontre2[0])){
            $compt++;
          }
          if(!empty($rencontre3[0])){
            $compt++;
          }
          if(!empty($rencontre4[0])){
            $compt++;
          }

          if(!empty($rencontre5[0])){
            $compt++;
          }
          if(!empty($rencontre6[0])){
            $compt++;
          }
          if(!empty($rencontre7[0])){
            $compt++;
          }
          if(!empty($rencontre8[0])){
            $compt++;
          }

          if ($compt == 8) {
            return true;
          }

          return false;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne si les matchs n ont pas été prevus
    function huitieme($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];
        $eq5 = $base[0][4]['Equipe'];
        $eq6 = $base[0][5]['Equipe'];
        $eq7 = $base[0][6]['Equipe'];
        $eq8 = $base[0][7]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq3}'");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq2}' AND idEquipe2 = '{$eq4}'");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq7}'");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq6}' AND idEquipe2 = '{$eq8}'");


        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());

          $compt = 0;

          if(!empty($rencontre1[0])){
            $compt++;
          }
          if(!empty($rencontre2[0])){
            $compt++;
          }
          if(!empty($rencontre3[0])){
            $compt++;
          }
          if(!empty($rencontre4[0])){
            $compt++;
          }

          return $compt;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// verifie si les matchs sont passes
    function huitieme_passe($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];
        $eq5 = $base[0][4]['Equipe'];
        $eq6 = $base[0][5]['Equipe'];
        $eq7 = $base[0][6]['Equipe'];
        $eq8 = $base[0][7]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq3}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq2}' AND idEquipe2 = '{$eq4}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq7}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq6}' AND idEquipe2 = '{$eq8}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");


        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());

          $compt = 0;

          if(!empty($rencontre1[0])){
            $compt++;
          }
          if(!empty($rencontre2[0])){
            $compt++;
          }
          if(!empty($rencontre3[0])){
            $compt++;
          }
          if(!empty($rencontre4[0])){
            $compt++;
          }

          return $compt;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les resultats des huitiemes sont ajoutés
    function verif_ajout_resultats_huitieme_final($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];
        $eq5 = $base[0][4]['Equipe'];
        $eq6 = $base[0][5]['Equipe'];
        $eq7 = $base[0][6]['Equipe'];
        $eq8 = $base[0][7]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND score1 IS NOT NULL AND score2 IS NOT NULL");


        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());
          $compt = 0;

          if(!empty($rencontre1[0])){
            $compt++;
          }
          if(!empty($rencontre2[0])){
            $compt++;
          }
          if(!empty($rencontre3[0])){
            $compt++;
          }
          if(!empty($rencontre4[0])){
            $compt++;
          }

          return $compt;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // on verifie si les demis finales ont des scores
    function scores_gagnants_8($idTournois,$base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0];
        $eq2 = $base[0][1];
        $eq3 = $base[0][2];
        $eq4 = $base[0][3];
        $eq5 = $base[0][4];
        $eq6 = $base[0][5];
        $eq7 = $base[0][6];
        $eq8 = $base[0][7];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND score1 IS NOT NULL AND score2 IS NOT NULL");


        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());
          $compt = 0;

          // var_dump($rencontre1);
          // echo "<br>";
          // var_dump($rencontre2);
          // echo "<br>";
          // var_dump($rencontre3);
          // echo "<br>";
          // var_dump(!empty($rencontre4[0]));

          if(!empty($rencontre1[0])){
            $compt++;
          }
          if(!empty($rencontre2[0])){
            $compt++;
          }
          if(!empty($rencontre3[0])){
            $compt++;
          }
          if(!empty($rencontre4[0])){
            $compt++;
          }

          if ($compt == 4) {
            return true;
          }
          return false;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on recupere les gagnants des huitiemes de final
    function recup_gagnants_huitieme($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];
        $eq5 = $base[0][4]['Equipe'];
        $eq6 = $base[0][5]['Equipe'];
        $eq7 = $base[0][6]['Equipe'];
        $eq8 = $base[0][7]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND score1 IS NOT NULL AND score2 IS NOT NULL");


        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());

          $gagnants = array();
          $gagnants[0][0] = $rencontre1[0][0]['idGagnant'];
          $gagnants[0][1] = $rencontre2[0][0]['idGagnant'];
          $gagnants[0][2] = $rencontre3[0][0]['idGagnant'];
          $gagnants[0][3] = $rencontre4[0][0]['idGagnant'];

          return $gagnants;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les quarts de final sont prevus
    function verif_quart_size8($idTournois, $quart_final){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $quart_final[0][0];
        $eq2 = $quart_final[0][3];
        $eq3 = $quart_final[0][1];
        $eq4 = $quart_final[0][2];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}'");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());

          // var_dump($rencontre1);
          // echo "<br>";
          // var_dump($rencontre2);

          if (empty($rencontre1[0]) || empty($rencontre2[0])) {
            return false;
          }
          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les quarts se sont deroules
    function verif_quart_passe($idTournois, $quart_final){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $quart_final[0][0];
        $eq2 = $quart_final[0][3];
        $eq3 = $quart_final[0][1];
        $eq4 = $quart_final[0][2];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());

          if (empty($rencontre1[0]) || empty($rencontre2[0])) {
            return false;
          }
          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les scores ont ete ajoutes
    function verif_ajout_scores_size8($idTournois, $quart_final){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $quart_final[0][0];
        $eq2 = $quart_final[0][3];
        $eq3 = $quart_final[0][1];
        $eq4 = $quart_final[0][2];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());

          if (empty($rencontre1[0]) || empty($rencontre2[0])) {
            return false;
          }
          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on recupere les equipes qui ont gagné les demis
    function get_equipe_demi($idTournois, $quart_final){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $quart_final[0][0];
        $eq2 = $quart_final[0][3];
        $eq3 = $quart_final[0][1];
        $eq4 = $quart_final[0][2];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());

          // var_dump($rencontre1);
          // echo "<br>";
          // var_dump($rencontre2);

          $gagnants = array();
          $gagnants[0][0] = $rencontre1[0][0]['idGagnant'];
          $gagnants[0][1] = $rencontre2[0][0]['idGagnant'];

          return $gagnants;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on regarde si les equipes ont des matchs de prevus pour un tournoi de size 16
    function prevu($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];
        $eq5 = $base[0][4]['Equipe'];
        $eq6 = $base[0][5]['Equipe'];
        $eq7 = $base[0][6]['Equipe'];
        $eq8 = $base[0][7]['Equipe'];
        $eq9 = $base[0][8]['Equipe'];
        $eq10 = $base[0][9]['Equipe'];
        $eq11 = $base[0][10]['Equipe'];
        $eq12 = $base[0][11]['Equipe'];
        $eq13 = $base[0][12]['Equipe'];
        $eq14 = $base[0][13]['Equipe'];
        $eq15 = $base[0][14]['Equipe'];
        $eq16 = $base[0][15]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}'");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}'");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}'");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}'");
        $requete5 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq9}' AND idEquipe2 = '{$eq10}'");
        $requete6 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq11}' AND idEquipe2 = '{$eq12}'");
        $requete7 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq13}' AND idEquipe2 = '{$eq14}'");
        $requete8 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq15}' AND idEquipe2 = '{$eq16}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();
        $execution_requete = $requete5->execute();
        $execution_requete = $requete6->execute();
        $execution_requete = $requete7->execute();
        $execution_requete = $requete8->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());
          $rencontre5 = array($requete5->fetchAll());
          $rencontre6 = array($requete6->fetchAll());
          $rencontre7 = array($requete7->fetchAll());
          $rencontre8 = array($requete8->fetchAll());

          if (empty($rencontre1[0]) || empty($rencontre2[0]) || empty($rencontre3[0]) || empty($rencontre4[0]) || empty($rencontre5[0]) || empty($rencontre6[0]) || empty($rencontre7[0]) || empty($rencontre8[0])){
            return false;
          }
          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les rencontres du tournoi de size 16 sont passes
    function passes($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];
        $eq5 = $base[0][4]['Equipe'];
        $eq6 = $base[0][5]['Equipe'];
        $eq7 = $base[0][6]['Equipe'];
        $eq8 = $base[0][7]['Equipe'];
        $eq9 = $base[0][8]['Equipe'];
        $eq10 = $base[0][9]['Equipe'];
        $eq11 = $base[0][10]['Equipe'];
        $eq12 = $base[0][11]['Equipe'];
        $eq13 = $base[0][12]['Equipe'];
        $eq14 = $base[0][13]['Equipe'];
        $eq15 = $base[0][14]['Equipe'];
        $eq16 = $base[0][15]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");
        $requete5 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq9}' AND idEquipe2 = '{$eq10}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");
        $requete6 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq11}' AND idEquipe2 = '{$eq12}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");
        $requete7 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq13}' AND idEquipe2 = '{$eq14}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");
        $requete8 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq15}' AND idEquipe2 = '{$eq16}' AND CONCAT(dateRencontre, heureRencontre) <= NOW()");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();
        $execution_requete = $requete5->execute();
        $execution_requete = $requete6->execute();
        $execution_requete = $requete7->execute();
        $execution_requete = $requete8->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());
          $rencontre5 = array($requete5->fetchAll());
          $rencontre6 = array($requete6->fetchAll());
          $rencontre7 = array($requete7->fetchAll());
          $rencontre8 = array($requete8->fetchAll());

          $compt = 0;

          if (!empty($rencontre1[0])){
            $compt++;
          }

          if (!empty($rencontre2[0])) {
            $compt++;
          }

          if (!empty($rencontre3[0])) {
            $compt++;
          }

          if (!empty($rencontre4[0])) {
            $compt++;
          }

          if (!empty($rencontre5[0])) {
            $compt++;
          }

          if (!empty($rencontre6[0])) {
            $compt++;
          }

          if (!empty($rencontre7[0])) {
            $compt++;
          }

          if (!empty($rencontre8[0])) {
            $compt++;
          }

          return $compt;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les scores ont été mis sur les rencontres du tournoi de size 16
    function scores_mis_size16($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];
        $eq5 = $base[0][4]['Equipe'];
        $eq6 = $base[0][5]['Equipe'];
        $eq7 = $base[0][6]['Equipe'];
        $eq8 = $base[0][7]['Equipe'];
        $eq9 = $base[0][8]['Equipe'];
        $eq10 = $base[0][9]['Equipe'];
        $eq11 = $base[0][10]['Equipe'];
        $eq12 = $base[0][11]['Equipe'];
        $eq13 = $base[0][12]['Equipe'];
        $eq14 = $base[0][13]['Equipe'];
        $eq15 = $base[0][14]['Equipe'];
        $eq16 = $base[0][15]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete5 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq9}' AND idEquipe2 = '{$eq10}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete6 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq11}' AND idEquipe2 = '{$eq12}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete7 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq13}' AND idEquipe2 = '{$eq14}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete8 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq15}' AND idEquipe2 = '{$eq16}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();
        $execution_requete = $requete5->execute();
        $execution_requete = $requete6->execute();
        $execution_requete = $requete7->execute();
        $execution_requete = $requete8->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());
          $rencontre5 = array($requete5->fetchAll());
          $rencontre6 = array($requete6->fetchAll());
          $rencontre7 = array($requete7->fetchAll());
          $rencontre8 = array($requete8->fetchAll());

          $compt = 0;

          if (empty($rencontre1[0]) || empty($rencontre2[0]) || empty($rencontre3[0]) || empty($rencontre4[0]) || empty($rencontre5[0]) || empty($rencontre6[0]) || empty($rencontre7[0]) || empty($rencontre8[0])){
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on recupere les gagnants sur des rencontres du tournoi de size 16
    function recup_gagnants_8($idTournois, $base){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $base[0][0]['Equipe'];
        $eq2 = $base[0][1]['Equipe'];
        $eq3 = $base[0][2]['Equipe'];
        $eq4 = $base[0][3]['Equipe'];
        $eq5 = $base[0][4]['Equipe'];
        $eq6 = $base[0][5]['Equipe'];
        $eq7 = $base[0][6]['Equipe'];
        $eq8 = $base[0][7]['Equipe'];
        $eq9 = $base[0][8]['Equipe'];
        $eq10 = $base[0][9]['Equipe'];
        $eq11 = $base[0][10]['Equipe'];
        $eq12 = $base[0][11]['Equipe'];
        $eq13 = $base[0][12]['Equipe'];
        $eq14 = $base[0][13]['Equipe'];
        $eq15 = $base[0][14]['Equipe'];
        $eq16 = $base[0][15]['Equipe'];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete5 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq9}' AND idEquipe2 = '{$eq10}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete6 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq11}' AND idEquipe2 = '{$eq12}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete7 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq13}' AND idEquipe2 = '{$eq14}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete8 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq15}' AND idEquipe2 = '{$eq16}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();
        $execution_requete = $requete5->execute();
        $execution_requete = $requete6->execute();
        $execution_requete = $requete7->execute();
        $execution_requete = $requete8->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());
          $rencontre5 = array($requete5->fetchAll());
          $rencontre6 = array($requete6->fetchAll());
          $rencontre7 = array($requete7->fetchAll());
          $rencontre8 = array($requete8->fetchAll());

          $gagnants = array();

          $gagnants[0][0] = $rencontre1[0][0]['idGagnant'];
          $gagnants[0][1] = $rencontre2[0][0]['idGagnant'];
          $gagnants[0][2] = $rencontre3[0][0]['idGagnant'];
          $gagnants[0][3] = $rencontre4[0][0]['idGagnant'];
          $gagnants[0][4] = $rencontre5[0][0]['idGagnant'];
          $gagnants[0][5] = $rencontre6[0][0]['idGagnant'];
          $gagnants[0][6] = $rencontre7[0][0]['idGagnant'];
          $gagnants[0][7] = $rencontre8[0][0]['idGagnant'];

          return $gagnants;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les matchs sont prevus pour les gagnants
    function matchs_8($idTournois, $gagnants_8){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $gagnants_8[0][0];
        $eq2 = $gagnants_8[0][1];
        $eq3 = $gagnants_8[0][2];
        $eq4 = $gagnants_8[0][3];
        $eq5 = $gagnants_8[0][4];
        $eq6 = $gagnants_8[0][5];
        $eq7 = $gagnants_8[0][6];
        $eq8 = $gagnants_8[0][7];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}'");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}'");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}'");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());

          $compt = 0;

          if (!empty($rencontre1[0])) {
            $compt++;
          }

          if (!empty($rencontre2[0])) {
            $compt++;
          }

          if (!empty($rencontre3[0])) {
            $compt++;
          }

          if (!empty($rencontre4[0])) {
            $compt++;
          }

          return $compt;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on recupere les gagnants des matchs precedants
    function match_8_suivant($idTournois, $gagnants_8){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $gagnants_8[0][0];
        $eq2 = $gagnants_8[0][1];
        $eq3 = $gagnants_8[0][2];
        $eq4 = $gagnants_8[0][3];
        $eq5 = $gagnants_8[0][4];
        $eq6 = $gagnants_8[0][5];
        $eq7 = $gagnants_8[0][6];
        $eq8 = $gagnants_8[0][7];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}'");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}'");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}'");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());

          $gagnants = array();
          $gagnants[0][0] = $rencontre1[0][0]['idGagnant'];
          $gagnants[0][1] = $rencontre2[0][0]['idGagnant'];
          $gagnants[0][2] = $rencontre3[0][0]['idGagnant'];
          $gagnants[0][3] = $rencontre4[0][0]['idGagnant'];

          return $gagnants;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si tous les matchs d une poule sont prevus
    function all_poule_prevus($idTournois, $idPoule){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $requete = $db->prepare("SELECT * FROM tournois.poule WHERE idPoule = '{$idPoule}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $poule = array($requete->fetchAll());
          $id1 = $poule[0][0]['idEP1'];
          $id2 = $poule[0][0]['idEP2'];
          $id3 = $poule[0][0]['idEP3'];
          $id4 = $poule[0][0]['idEP4'];

          $affr1 = poule_deja_affronte($idTournois, $id1, $id2);
          $affr2 = poule_deja_affronte($idTournois, $id1, $id3);
          $affr3 = poule_deja_affronte($idTournois, $id1, $id4);

          $affr4 = poule_deja_affronte($idTournois, $id2, $id3);
          $affr5 = poule_deja_affronte($idTournois, $id2, $id4);

          $affr6 = poule_deja_affronte($idTournois, $id3, $id4);

          $compt = 0;

          if ($affr1) {
            $compt++;
          }

          if ($affr2) {
            $compt++;
          }

          if ($affr3) {
            $compt++;
          }

          if ($affr4) {
            $compt++;
          }

          if ($affr5) {
            $compt++;
          }

          if ($affr6) {
            $compt++;
          }

          if ($compt == 6) {
            return true;
          }

          return false;

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si la rencontre exste, est passés et a ses scores
    function rencontre_a_eu_lieu_et_scores($idTournois, $id1, $id2){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND ( (idEquipe1 = '{$id1}' AND idEquipe2 = '{$id2}') OR  (idEquipe1 = '{$id2}' AND idEquipe2 = '{$id1}') ) AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());

          if (empty($rencontre1[0])) {
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// recupere les infos d une rencontre
    function get_info_rencontre($idTournois, $id1, $id2){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND ( (idEquipe1 = '{$id1}' AND idEquipe2 = '{$id2}') OR  (idEquipe1 = '{$id2}' AND idEquipe2 = '{$id1}') ) AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());

          return $rencontre1;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne le max de 4 valeurs
    function max1($tab){
      $a = $tab[0][0]['nbVict'];
      $b = $tab[0][1]['nbVict'];
      $c = $tab[0][2]['nbVict'];
      $d = $tab[0][3]['nbVict'];

      if (($a >= $b) && ($a >= $c) && ($a >= $d)) {
        return $tab[0][0]['idEq'];
      }
      else {
        if (($b >= $a) && ($b >= $c) && ($b >= $d)) {
          return $tab[0][1]['idEq'];
        }
        else {
          if (($c >= $a) && ($c >= $b) && ($c >= $d)) {
            return $tab[0][2]['idEq'];
          }
          else {
            return $tab[0][3]['idEq'];
          }
        }
      }
    }

    // retourne le max de 3 valeurs
    function max2($tab){
      $a = $tab[0][0]['nbVict'];
      $b = $tab[0][1]['nbVict'];
      $c = $tab[0][2]['nbVict'];

      if (($a >= $b) && ($a >= $c)) {
        return $tab[0][0]['idEq'];
      }
      else {
        if (($b >= $a) && ($b >= $c)) {
          return $tab[0][1]['idEq'];
        }
        else {
          return $tab[0][2]['idEq'];
        }
      }
    }

// ajoute les gagnants a une poule
    function ajout_gagnant_poule($idPoule, $max1, $max2){
      $user = 'root';
      $pass = '';
      $serveur = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($serveur,$user,$pass);

      try {

        $requete = $db->prepare("UPDATE tournois.poule SET idGagnant1=:idGagnant1, idGagnant2=:idGagnant2 WHERE idPoule = '{$idPoule}'");

        //on lie les elemnents a une clef
        $requete->bindValue(':idGagnant1', $max1, PDO::PARAM_INT);
        $requete->bindValue(':idGagnant2', $max2, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// veirfie si les poules sont toutes passes
    function verif_all_poule_passes($idTournois, $idPoule){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $requete = $db->prepare("SELECT * FROM tournois.poule WHERE idPoule = '{$idPoule}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $poule = array($requete->fetchAll());
          $idPoule = $poule[0][0]['idPoule'];
          $id1 = $poule[0][0]['idEP1'];
          $id2 = $poule[0][0]['idEP2'];
          $id3 = $poule[0][0]['idEP3'];
          $id4 = $poule[0][0]['idEP4'];

          $affr1 = rencontre_a_eu_lieu_et_scores($idTournois, $id1, $id2);
          $affr2 = rencontre_a_eu_lieu_et_scores($idTournois, $id1, $id3);
          $affr3 = rencontre_a_eu_lieu_et_scores($idTournois, $id1, $id4);

          $affr4 = rencontre_a_eu_lieu_et_scores($idTournois, $id2, $id3);
          $affr5 = rencontre_a_eu_lieu_et_scores($idTournois, $id2, $id4);

          $affr6 = rencontre_a_eu_lieu_et_scores($idTournois, $id3, $id4);

          $compt = 0;

          if ($affr1) {
            $compt++;
          }

          if ($affr2) {
            $compt++;
          }

          if ($affr3) {
            $compt++;
          }

          if ($affr4) {
            $compt++;
          }

          if ($affr5) {
            $compt++;
          }

          if ($affr6) {
            $compt++;
          }

          if ($compt == 6) {
            return true;
          }

          return false;

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les poules sont toutes passes si c est le cas on ajoute les gagnants a la poule
    function all_poule_passes($idTournois, $idPoule){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $requete = $db->prepare("SELECT * FROM tournois.poule WHERE idPoule = '{$idPoule}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $poule = array($requete->fetchAll());
          $idPoule = $poule[0][0]['idPoule'];
          $id1 = $poule[0][0]['idEP1'];
          $id2 = $poule[0][0]['idEP2'];
          $id3 = $poule[0][0]['idEP3'];
          $id4 = $poule[0][0]['idEP4'];

          $affr1 = rencontre_a_eu_lieu_et_scores($idTournois, $id1, $id2);
          $affr2 = rencontre_a_eu_lieu_et_scores($idTournois, $id1, $id3);
          $affr3 = rencontre_a_eu_lieu_et_scores($idTournois, $id1, $id4);

          $affr4 = rencontre_a_eu_lieu_et_scores($idTournois, $id2, $id3);
          $affr5 = rencontre_a_eu_lieu_et_scores($idTournois, $id2, $id4);

          $affr6 = rencontre_a_eu_lieu_et_scores($idTournois, $id3, $id4);

          $compt = 0;

          if ($affr1) {
            $compt++;
          }

          if ($affr2) {
            $compt++;
          }

          if ($affr3) {
            $compt++;
          }

          if ($affr4) {
            $compt++;
          }

          if ($affr5) {
            $compt++;
          }

          if ($affr6) {
            $compt++;
          }

          if ($compt == 6) {
            $idGg1 = get_info_rencontre($idTournois, $id1, $id2);
            $idGg2 = get_info_rencontre($idTournois, $id1, $id3);
            $idGg3 = get_info_rencontre($idTournois, $id1, $id4);

            $idGg4 = get_info_rencontre($idTournois, $id2, $id3);
            $idGg5 = get_info_rencontre($idTournois, $id2, $id4);

            $idGg6 = get_info_rencontre($idTournois, $id3, $id4);

            $idG1 = $idGg1[0][0]['idGagnant'];
            $idG2 = $idGg2[0][0]['idGagnant'];
            $idG3 = $idGg3[0][0]['idGagnant'];

            $idG4 = $idGg4[0][0]['idGagnant'];
            $idG5 = $idGg5[0][0]['idGagnant'];

            $idG6 = $idGg6[0][0]['idGagnant'];

            $nbV1 = 0;
            $nbV2 = 0;
            $nbV3 = 0;
            $nbV4 = 0;
            // var_dump($idG1);

          // match 1
            if ($idG1 == $id1) {
              $nbV1++;
            }

            if ($idG1 == $id2) {
              $nbV2++;
            }

            if ($idG1 == $id3) {
              $nbV3++;
            }

            if ($idG1 == $id4) {
              $nbV4++;
            }

            // match 2
            if ($idG2 == $id1) {
              $nbV1++;
            }

            if ($idG2 == $id2) {
              $nbV2++;
            }

            if ($idG2 == $id3) {
              $nbV3++;
            }

            if ($idG2 == $id4) {
              $nbV4++;
            }

            // match 3
            if ($idG3 == $id1) {
              $nbV1++;
            }

            if ($idG3 == $id2) {
              $nbV2++;
            }

            if ($idG3 == $id3) {
              $nbV3++;
            }

            if ($idG3 == $id4) {
              $nbV4++;
            }

            // match 4
            if ($idG4 == $id1) {
              $nbV1++;
            }

            if ($idG4 == $id2) {
              $nbV2++;
            }

            if ($idG4 == $id3) {
              $nbV3++;
            }

            if ($idG4 == $id4) {
              $nbV4++;
            }

            // match 5
            if ($idG5 == $id1) {
              $nbV1++;
            }

            if ($idG5 == $id2) {
              $nbV2++;
            }

            if ($idG5 == $id3) {
              $nbV3++;
            }

            if ($idG5 == $id4) {
              $nbV4++;
            }

            // match 6
            if ($idG6 == $id1) {
              $nbV1++;
            }

            if ($idG6 == $id2) {
              $nbV2++;
            }

            if ($idG6 == $id3) {
              $nbV3++;
            }

            if ($idG6 == $id4) {
              $nbV4++;
            }

            $tabRecap = array();
            $tabRecap[0][0]['nbVict'] = $nbV1;
            $tabRecap[0][0]['idEq'] = $id1;

            $tabRecap[0][1]['nbVict'] = $nbV2;
            $tabRecap[0][1]['idEq'] = $id2;

            $tabRecap[0][2]['nbVict'] = $nbV3;
            $tabRecap[0][2]['idEq'] = $id3;

            $tabRecap[0][3]['nbVict'] = $nbV4;
            $tabRecap[0][3]['idEq'] = $id4;

            $max1 = max1($tabRecap);

            // var_dump($tabRecap[0]);

            if ($max1 == $id1) {
              if ($nbV2 == $nbV3 && $nbV3 == $nbV4) {
                $newSelect = array();

                $newSelect[0][0]['nbVict'] = $nbV2;
                $newSelect[0][0]['idEq'] = $id2;

                $newSelect[0][1]['nbVict'] = $nbV3;
                $newSelect[0][1]['idEq'] = $id3;

                $newSelect[0][2]['nbVict'] = $nbV4;
                $newSelect[0][2]['idEq'] = $id4;

                $max2 = array_rand($newSelect, 1);

              }
              else {
                $newSelect = array();

                $newSelect[0][0]['nbVict'] = $nbV2;
                $newSelect[0][0]['idEq'] = $id2;

                $newSelect[0][1]['nbVict'] = $nbV3;
                $newSelect[0][1]['idEq'] = $id3;

                $newSelect[0][2]['nbVict'] = $nbV4;
                $newSelect[0][2]['idEq'] = $id4;

                $max2 = max2($newSelect);
              }
            }
            else {
              if ($max1 == $id2) {
                if ($nbV1 == $nbV3 && $nbV3 == $nbV4) {
                  $newSelect = array();

                  $newSelect[0][0]['nbVict'] = $nbV1;
                  $newSelect[0][0]['idEq'] = $id1;

                  $newSelect[0][1]['nbVict'] = $nbV3;
                  $newSelect[0][1]['idEq'] = $id3;

                  $newSelect[0][2]['nbVict'] = $nbV4;
                  $newSelect[0][2]['idEq'] = $id4;

                  $max2 = array_rand($newSelect, 1);

                }
                else {
                  $newSelect = array();

                  $newSelect[0][0]['nbVict'] = $nbV1;
                  $newSelect[0][0]['idEq'] = $id1;

                  $newSelect[0][1]['nbVict'] = $nbV3;
                  $newSelect[0][1]['idEq'] = $id3;

                  $newSelect[0][2]['nbVict'] = $nbV4;
                  $newSelect[0][2]['idEq'] = $id4;

                  $max2 = max2($newSelect);
                }
              }
              else {
                if ($max1 == $id3) {
                  if ($nbV2 == $nbV1 && $nbV1 == $nbV4) {
                    $newSelect = array();

                    $newSelect[0][0]['nbVict'] = $nbV2;
                    $newSelect[0][0]['idEq'] = $id2;

                    $newSelect[0][1]['nbVict'] = $nbV1;
                    $newSelect[0][1]['idEq'] = $id1;

                    $newSelect[0][2]['nbVict'] = $nbV4;
                    $newSelect[0][2]['idEq'] = $id4;

                    $max2 = array_rand($newSelect, 1);

                  }
                  else {
                    $newSelect = array();

                    $newSelect[0][0]['nbVict'] = $nbV2;
                    $newSelect[0][0]['idEq'] = $id2;

                    $newSelect[0][1]['nbVict'] = $nbV1;
                    $newSelect[0][1]['idEq'] = $id1;

                    $newSelect[0][2]['nbVict'] = $nbV4;
                    $newSelect[0][2]['idEq'] = $id4;

                    $max2 = max2($newSelect);
                  }
                }
                else {
                  if ($nbV2 == $nbV3 && $nbV3 == $nbV1) {
                    $newSelect = array();

                    $newSelect[0][0]['nbVict'] = $nbV2;
                    $newSelect[0][0]['idEq'] = $id2;

                    $newSelect[0][1]['nbVict'] = $nbV3;
                    $newSelect[0][1]['idEq'] = $id3;

                    $newSelect[0][2]['nbVict'] = $nbV1;
                    $newSelect[0][2]['idEq'] = $id1;

                    $max2 = array_rand($newSelect, 1);

                  }
                  else {
                    $newSelect = array();

                    $newSelect[0][0]['nbVict'] = $nbV2;
                    $newSelect[0][0]['idEq'] = $id2;

                    $newSelect[0][1]['nbVict'] = $nbV3;
                    $newSelect[0][1]['idEq'] = $id3;

                    $newSelect[0][2]['nbVict'] = $nbV1;
                    $newSelect[0][2]['idEq'] = $id1;

                    $max2 = max2($newSelect);
                  }
                }
              }
            }

            // var_dump($max1);
            // var_dump($max2);
            $send = ajout_gagnant_poule($idPoule, $max1, $max2);

            if ($send) {
              return true;
            }

            return false;

          }

          return false;

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les matchs sont passes quarts de coupe
    function quart_passe_coupe($idTournois, $quart){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $quart[0][0];
        $eq2 = $quart[0][1];
        $eq3 = $quart[0][2];
        $eq4 = $quart[0][3];
        $eq5 = $quart[0][4];
        $eq6 = $quart[0][5];
        $eq7 = $quart[0][6];
        $eq8 = $quart[0][7];


        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW() ");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW() ");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW() ");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW() ");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();


        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());


          if (empty($rencontre1[0]) || empty($rencontre2[0]) || empty($rencontre3[0]) || empty($rencontre4[0]) ){
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les scores ont ete entres
    function quart_score_mis($idTournois, $quart){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $quart[0][0];
        $eq2 = $quart[0][1];
        $eq3 = $quart[0][2];
        $eq4 = $quart[0][3];
        $eq5 = $quart[0][4];
        $eq6 = $quart[0][5];
        $eq7 = $quart[0][6];
        $eq8 = $quart[0][7];


        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL ");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL ");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND score1 IS NOT NULL AND score2 IS NOT NULL ");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND score1 IS NOT NULL AND score2 IS NOT NULL ");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();


        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());


          if (empty($rencontre1[0]) || empty($rencontre2[0]) || empty($rencontre3[0]) || empty($rencontre4[0]) ){
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si la demi-finale est planifié
    function demi_final_planifier($idTournois, $demi_final){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $demi_final[0][0];
        $eq2 = $demi_final[0][1];
        $eq3 = $demi_final[0][2];
        $eq4 = $demi_final[0][3];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' ");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' ");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());


          if (empty($rencontre1[0]) || empty($rencontre2[0]) ){
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne les equipes qui ont gagné les quarts en coupe
    function get_equipe_demi_coupe($idTournois, $quart_final){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $quart_final[0][0];
        $eq2 = $quart_final[0][1];
        $eq3 = $quart_final[0][2];
        $eq4 = $quart_final[0][3];
        $eq5 = $quart_final[0][4];
        $eq6 = $quart_final[0][5];
        $eq7 = $quart_final[0][6];
        $eq8 = $quart_final[0][7];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete3 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq5}' AND idEquipe2 = '{$eq6}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete4 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq7}' AND idEquipe2 = '{$eq8}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();
        $execution_requete = $requete3->execute();
        $execution_requete = $requete4->execute();


        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());
          $rencontre3 = array($requete3->fetchAll());
          $rencontre4 = array($requete4->fetchAll());

          // var_dump($rencontre1);
          // echo "<br>";
          // var_dump($rencontre2);

          $gagnants = array();
          $gagnants[0][0] = $rencontre1[0][0]['idGagnant'];
          $gagnants[0][1] = $rencontre2[0][0]['idGagnant'];
          $gagnants[0][2] = $rencontre3[0][0]['idGagnant'];
          $gagnants[0][3] = $rencontre4[0][0]['idGagnant'];

          return $gagnants;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les equipes de demi final ont leurs matchs de terminés
    function demi_final_deroule($idTournois, $demi_final){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $demi_final[0][0];
        $eq2 = $demi_final[0][1];
        $eq3 = $demi_final[0][2];
        $eq4 = $demi_final[0][3];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());

          // var_dump($rencontre1[0]);
          // var_dump($rencontre2[0]);


          if (empty($rencontre1[0]) || empty($rencontre2[0]) ){
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les equipes de demi finale ont leurs scores
    function demi_scores($idTournois, $demi_final){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $demi_final[0][0];
        $eq2 = $demi_final[0][1];
        $eq3 = $demi_final[0][2];
        $eq4 = $demi_final[0][3];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());

          // var_dump($rencontre1[0]);
          // var_dump($rencontre2[0]);


          if (empty($rencontre1[0]) || empty($rencontre2[0]) ){
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on recupere les equipes qui ont gagne la demi finale
    function get_equipe_finale_coupe($idTournois, $demi_final){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $demi_final[0][0];
        $eq2 = $demi_final[0][1];
        $eq3 = $demi_final[0][2];
        $eq4 = $demi_final[0][3];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");
        $requete2 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq3}' AND idEquipe2 = '{$eq4}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();
        $execution_requete = $requete2->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());
          $rencontre2 = array($requete2->fetchAll());

          // var_dump($rencontre1);
          // echo "<br>";
          // var_dump($rencontre2);

          $gagnants = array();
          $gagnants[0][0] = $rencontre1[0][0]['idGagnant'];
          $gagnants[0][1] = $rencontre2[0][0]['idGagnant'];

          return $gagnants;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si la finale est passe
    function finale_passe($idTournois, $eq_finals){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $eq_finals[0][0];
        $eq2 = $eq_finals[0][1];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND CONCAT(dateRencontre, ' ', heureRencontre) <= NOW()");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());

          if (empty($rencontre1[0])) {
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on verifie si les scores ont ete mis
    function finale_scores($idTournois, $eq_finals){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $eq_finals[0][0];
        $eq2 = $eq_finals[0][1];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());

          if (empty($rencontre1[0])) {
            return false;
          }

          return true;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// on recupere le gagnant de la coupe
    function gagnant_coupe($idTournois, $eq_finals){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $eq1 = $eq_finals[0][0];
        $eq2 = $eq_finals[0][1];

        $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND idEquipe1 = '{$eq1}' AND idEquipe2 = '{$eq2}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();

        if($execution_requete){
          $rencontre1 = array($requete1->fetchAll());

          return $rencontre1[0][0]['idGagnant'];
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// termine la competition une fois un vainqueur est designé
    function termine_competition($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete1 = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}' ");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete1->execute();

        if($execution_requete){
          $tournois = array($requete1->fetchAll());

          $dateDebut = $tournois[0][0]['dateDebut'];
          $duree = $tournois[0][0]['duree'];
          $today = date("Y-m-d");

          // var_dump($today);
          // var_dump($duree);
          // var_dump($dateDebut);

          settype($duree, "int");
          // var_dump($duree);

          $fin = date("Y-m-d", strtotime($dateDebut.'+'. $duree.'day'));

          $newDate = $dateDebut;
          $newDuree = 0;
          while ($newDate < $today) {
            $newDate = date("Y-m-d", strtotime($dateDebut."+".$newDuree."days"));
            $newDuree++;
          }

          $requete = $db->prepare("UPDATE tournois.tournois SET duree=:duree, Agagnant=:Agagant WHERE idTournois = '{$idTournois}'");

          $termine = true;
          $requete->bindValue(':duree', $newDuree, PDO::PARAM_STR);
          $requete->bindValue(':Agagant', $termine, PDO::PARAM_BOOL);

          $correct_insertion = $requete->execute();

          if($correct_insertion){
            return true;
          }
          else {
            return false;
          }
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// coupe
  if (isset($_POST['PlanifierCoupe'])) {
    $pseudo = $_POST['pseudo'];
    $idTournois = $_POST['idTournois'];
    $dateR = array();
    $heureR = array();
    $idEq = array();

    for ($i=0; $i < 16; $i++) {
      $idEq[0][$i] = $_POST["idEq".$i];
    }
    for ($j=0; $j < 8; $j++) {
      $dateR[0][$j] = $_POST['dateR'.$j];
      $heureR[0][$j] = $_POST['heureR'.$j];
    }

    $send1 = ajout_donnees_rencontre($idTournois, $dateR[0][0], $heureR[0][0], $idEq[0][0], $idEq[0][1]);
    $send2 = ajout_donnees_rencontre($idTournois, $dateR[0][1], $heureR[0][1], $idEq[0][2], $idEq[0][3]);
    $send3 = ajout_donnees_rencontre($idTournois, $dateR[0][2], $heureR[0][2], $idEq[0][4], $idEq[0][5]);
    $send4 = ajout_donnees_rencontre($idTournois, $dateR[0][3], $heureR[0][3], $idEq[0][6], $idEq[0][7]);
    $send5 = ajout_donnees_rencontre($idTournois, $dateR[0][4], $heureR[0][4], $idEq[0][8], $idEq[0][9]);
    $send6 = ajout_donnees_rencontre($idTournois, $dateR[0][5], $heureR[0][5], $idEq[0][10], $idEq[0][11]);
    $send7 = ajout_donnees_rencontre($idTournois, $dateR[0][6], $heureR[0][6], $idEq[0][12], $idEq[0][13]);
    $send8 = ajout_donnees_rencontre($idTournois, $dateR[0][7], $heureR[0][7], $idEq[0][14], $idEq[0][15]);

    if ($send1 == false || $send2 == false || $send3 == false || $send4 == false || $send5 == false || $send6 == false || $send7 == false || $send8 == false) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
    else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }

  }

  if (isset($_POST['ValiderQuartCoupe'])) {
    $pseudo = $_POST['pseudo'];
    $idTournois = $_POST['idTournois'];

    $idEq = array();
    $dateR = array();
    $heureR = array();

    for ($i=0; $i < 4; $i++) {
      $dateR[0][$i] = $_POST['dateR'.$i];
      $heureR[0][$i] = $_POST['heureR'.$i];
    }

    for ($i=0; $i < 8; $i++) {
      $idEq[0][$i] = $_POST['idEq'.$i];
    }

    $send1 = ajout_donnees_rencontre($idTournois, $dateR[0][0], $heureR[0][0], $idEq[0][0], $idEq[0][1]);
    $send2 = ajout_donnees_rencontre($idTournois, $dateR[0][1], $heureR[0][1], $idEq[0][2], $idEq[0][3]);
    $send3 = ajout_donnees_rencontre($idTournois, $dateR[0][2], $heureR[0][2], $idEq[0][4], $idEq[0][5]);
    $send4 = ajout_donnees_rencontre($idTournois, $dateR[0][3], $heureR[0][3], $idEq[0][6], $idEq[0][7]);

    if ($send1 && $send2 && $send3 && $send4) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }
    else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

  if (isset($_POST['ValiderDemiCoupe'])) {
    $pseudo = $_POST['pseudo'];
    $idTournois = $_POST['idTournois'];
    $idEq = array();

    for ($i=0; $i < 4; $i++) {
      $idEq[0][$i] = $_POST['idEq'.$i];
    }

    $dateR = $_POST['dateR'];
    $dateR1 = $_POST['dateR1'];

    $heureR = $_POST['heureR'];
    $heureR1 = $_POST['heureR1'];

    $send1 = ajout_donnees_rencontre($idTournois, $dateR, $heureR, $idEq[0][0], $idEq[0][1]);
    $send2 = ajout_donnees_rencontre($idTournois, $dateR1, $heureR1, $idEq[0][2], $idEq[0][3]);

    if ($send1 && $send2) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }
    else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

  if (isset($_POST['ValiderFinaleCoupe'])) {
    $pseudo = $_POST['pseudo'];
    $idTournois = $_POST['idTournois'];

    $idEq1 = $_POST['ideq1'];
    $idEq2 = $_POST['ideq2'];

    $dateR = $_POST['dateR'];
    $heureR = $_POST['heureR'];

    $send1 = ajout_donnees_rencontre($idTournois, $dateR, $heureR, $idEq1, $idEq2);

    if ($send1) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }
    else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

// tournois
// poules de selection
  if (isset($_POST['Planifier'])) {
    $idTournois = $_POST['idTournois'];
    $pseudo = $_POST['pseudo'];
    $idEq1 = $_POST['idEq1'];
    $idEq2 = $_POST['idEq2'];
    $idEq3 = $_POST['idEq3'];
    $idEq4 = $_POST['idEq4'];

    $dateR = $_POST['dateR'];
    $heureR = $_POST['heureR'];
    $compt = 0;
    $equipesSelect = array();
    $id = 0;

    // var_dump(isset($_POST['Eq1']));
    // var_dump(isset($_POST['Eq2']));
    // var_dump(isset($_POST['Eq3']));
    // var_dump(isset($_POST['Eq4']));

    if (isset($_POST['Eq1'])) {
        $compt= $compt + 1;
        $equipesSelect[0][$id] = $idEq1;
        $id++;
    }

    if (isset($_POST['Eq2'])) {
        $compt+=1;
        $equipesSelect[0][$id] = $idEq2;
        $id++;
    }

    if (isset($_POST['Eq3'])) {
        $compt+=1;
        $equipesSelect[0][$id] = $idEq3;
        $id++;
    }

    if (isset($_POST['Eq4'])) {
        $compt+=1;
        $equipesSelect[0][$id] = $idEq4;
        $id++;
    }

    var_dump($compt);

    if ($compt != 2) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
    else {
      $select1 = $equipesSelect[0][0];
      $select2 = $equipesSelect[0][1];
      $deja_affronte = poule_deja_affronte($idTournois, $select1, $select2);

      if ($deja_affronte) {
        session_start();
        $_SESSION["pseudo"] = $pseudo;
        $_SESSION["id"] = $idTournois;
        header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
        exit();
      }
      else {
        $send = ajout_donnees_rencontre($idTournois, $dateR, $heureR, $select1, $select2);
        if ($send) {
          session_start();
          $_SESSION["pseudo"] = $pseudo;
          header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
          exit();
        }
        else {
          session_start();
          $_SESSION["pseudo"] = $pseudo;
          $_SESSION["id"] = $idTournois;
          header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
          exit();
        }
      }
    }
  }

// size 16
  if (isset($_POST['finalSize16'])) {
    $idTournois = $_POST['idTournois'];
    $pseudo = $_POST['pseudo'];
    $idEP1 = $_POST['idEP1'];
    $idEP2 = $_POST['idEP2'];
    $dateR = $_POST['dateR'];
    $heureR = $_POST['heureR'];

    $send = ajout_donnees_rencontre($idTournois, $dateR, $heureR, $idEP1, $idEP2);
    if ($send) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

  if (isset($_POST['size16_demi'])) {
    $idTournois = $_POST['idTournois'];
    $pseudo = $_POST['pseudo'];

    $idEq1 = $_POST['idEq1'];
    $idEq2 = $_POST['idEq2'];
    $idEq3 = $_POST['idEq3'];
    $idEq4 = $_POST['idEq4'];

    $dateR1 = $_POST['dateR1'];
    $heureR1 = $_POST['heureR1'];
    $dateR2 = $_POST['dateR2'];
    $heureR2 = $_POST['heureR2'];

    $send1 = ajout_donnees_rencontre($idTournois, $dateR1, $heureR1, $idEq1, $idEq2);
    $send = ajout_donnees_rencontre($idTournois, $dateR2, $heureR2, $idEq3, $idEq4);

    if ($send1 && $send) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

  if (isset($_POST['size8_8eme'])) {
    $idTournois = $_POST['idTournois'];
    $pseudo = $_POST['pseudo'];

    $idEq1 = $_POST['idEP0'];
    $idEq2 = $_POST['idEP1'];
    $idEq3 = $_POST['idEP2'];
    $idEq4 = $_POST['idEP3'];
    $idEq5 = $_POST['idEP4'];
    $idEq6 = $_POST['idEP5'];
    $idEq7 = $_POST['idEP6'];
    $idEq8 = $_POST['idEP7'];

    $dateR1 = $_POST['dateR1'];
    $heureR1 = $_POST['heureR1'];

    $dateR2 = $_POST['dateR2'];
    $heureR2 = $_POST['heureR2'];

    $dateR3 = $_POST['dateR3'];
    $heureR3 = $_POST['heureR3'];

    $dateR4 = $_POST['dateR4'];
    $heureR4 = $_POST['heureR4'];

    $send1 = ajout_donnees_rencontre($idTournois, $dateR1, $heureR1, $idEq1, $idEq2);
    $send2 = ajout_donnees_rencontre($idTournois, $dateR2, $heureR2, $idEq3, $idEq4);
    $send3 = ajout_donnees_rencontre($idTournois, $dateR3, $heureR3, $idEq5, $idEq6);
    $send4 = ajout_donnees_rencontre($idTournois, $dateR4, $heureR4, $idEq7, $idEq8);

    if ($send1 || $send2 || $send3 || $send4) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

  if (isset($_POST['size8prevu'])) {
    $idTournois = $_POST['idTournois'];
    $pseudo = $_POST['pseudo'];

    $idEq1 = $_POST['idEP0'];
    $idEq2 = $_POST['idEP1'];
    $idEq3 = $_POST['idEP2'];
    $idEq4 = $_POST['idEP3'];
    $idEq5 = $_POST['idEP4'];
    $idEq6 = $_POST['idEP5'];
    $idEq7 = $_POST['idEP6'];
    $idEq8 = $_POST['idEP7'];
    $idEq9 = $_POST['idEP8'];
    $idEq10 = $_POST['idEP9'];
    $idEq11 = $_POST['idEP10'];
    $idEq12 = $_POST['idEP11'];
    $idEq13 = $_POST['idEP12'];
    $idEq14 = $_POST['idEP13'];
    $idEq15 = $_POST['idEP14'];
    $idEq16 = $_POST['idEP15'];

    $dateR0 = $_POST['dateR0'];
    $heureR0 = $_POST['heureR0'];

    $dateR1 = $_POST['dateR1'];
    $heureR1 = $_POST['heureR1'];

    $dateR2 = $_POST['dateR2'];
    $heureR2 = $_POST['heureR2'];

    $dateR3 = $_POST['dateR3'];
    $heureR3 = $_POST['heureR3'];

    $dateR4 = $_POST['dateR4'];
    $heureR4 = $_POST['heureR4'];

    $dateR5 = $_POST['dateR5'];
    $heureR5 = $_POST['heureR5'];

    $dateR6 = $_POST['dateR6'];
    $heureR6 = $_POST['heureR6'];

    $dateR7 = $_POST['dateR7'];
    $heureR7 = $_POST['heureR7'];

    $send1 = ajout_donnees_rencontre($idTournois, $dateR0, $heureR0, $idEq1, $idEq2);
    $send2 = ajout_donnees_rencontre($idTournois, $dateR1, $heureR1, $idEq3, $idEq4);
    $send3 = ajout_donnees_rencontre($idTournois, $dateR2, $heureR2, $idEq5, $idEq6);
    $send4 = ajout_donnees_rencontre($idTournois, $dateR3, $heureR3, $idEq7, $idEq8);
    $send5 = ajout_donnees_rencontre($idTournois, $dateR4, $heureR4, $idEq9, $idEq10);
    $send6 = ajout_donnees_rencontre($idTournois, $dateR5, $heureR5, $idEq11, $idEq12);
    $send7 = ajout_donnees_rencontre($idTournois, $dateR6, $heureR6, $idEq13, $idEq14);
    $send8 = ajout_donnees_rencontre($idTournois, $dateR7, $heureR7, $idEq15, $idEq16);


    if ($send1 || $send2 || $send3 || $send4 || $send5 || $send6 || $send7 || $send8) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

// size 8
if (isset($_POST['size8demi_final'])) {
  $idTournois = $_POST['idTournois'];
  $pseudo = $_POST['pseudo'];

  $idEq1 = $_POST['idEP1'];
  $idEq2 = $_POST['idEP2'];

  $dateR = $_POST['dateR'];
  $heureR = $_POST['heureR'];

  $send1 = ajout_donnees_rencontre($idTournois, $dateR, $heureR, $idEq1, $idEq2);

  if ($send1) {
    session_start();
    $_SESSION["pseudo"] = $pseudo;
    header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
    exit();
  }else {
    session_start();
    $_SESSION["pseudo"] = $pseudo;
    $_SESSION["id"] = $idTournois;
    header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
    exit();
  }

}

if (isset($_POST['size8Quart'])) {
  $idTournois = $_POST['idTournois'];
  $pseudo = $_POST['pseudo'];

  $idEq1 = $_POST['idEq1'];
  $idEq2 = $_POST['idEq2'];
  $idEq3 = $_POST['idEq3'];
  $idEq4 = $_POST['idEq4'];

  $dateR1 = $_POST['dateR1'];
  $heureR1 = $_POST['heureR1'];

  $dateR2 = $_POST['dateR2'];
  $heureR2 = $_POST['heureR2'];

  $send1 = ajout_donnees_rencontre($idTournois, $dateR1, $heureR1, $idEq1, $idEq4);
  $send2 = ajout_donnees_rencontre($idTournois, $dateR2, $heureR2, $idEq2, $idEq3);

  if ($send1 || $send2) {
    session_start();
    $_SESSION["pseudo"] = $pseudo;
    header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
    exit();
  }else {
    session_start();
    $_SESSION["pseudo"] = $pseudo;
    $_SESSION["id"] = $idTournois;
    header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
    exit();
  }
}

if (isset($_POST['huitiemeSize8'])) {
  $nbr_vide = $_POST['nbr_vides'];
  $pseudo = $_POST['pseudo'];
  $idTournois = $_POST['idTournois'];
  $idEP1 = array();
  $idEP2 = array();
  $dateR = array();
  $heureR = array();

  for ($i=0; $i < $nbr_vide; $i++) {
    $idEP1[0][$i] = $_POST['idEq1-'.$i];
    $idEP2[0][$i] = $_POST['idEq2-'.$i];
    $dateR[0][$i] = $_POST['dateR'.$i];
    $heureR[0][$i] = $_POST['heureR'.$i];
  }

  for ($i=0; $i < $nbr_vide; $i++) {
    $send = ajout_donnees_rencontre($idTournois, $dateR[0][$i], $heureR[0][$i], $idEP1[0][$i], $idEP2[0][$i]);
    if ($send == false) {
      session_start();
      print "erreur";
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

  session_start();
  $_SESSION["pseudo"] = $pseudo;
  header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
  exit();

}


// size 4
    // prevoir les quarts de final
  if (isset($_POST['ValiderSize4'])) {
    $pseudo = $_POST['pseudo'];
    $date1erquart = $_POST['dateR1'];
    $heure1erquart = $_POST['heureR1'];
    $EP1_1erquart = $_POST['idEquipe0'];
    $EP2_1erquart = $_POST['idEquipe2'];

    $date1erquart = $_POST['dateR2'];
    $heure1erquart = $_POST['heureR2'];
    $EP1_1erquart_pt2 = $_POST['idEquipe1'];
    $EP2_1erquart_pt2 = $_POST['idEquipe3'];

    $idTournois = $_POST['idTournois'];

    // on crée les quarts
    $send1 = ajout_donnees_rencontre($idTournois, $date1erquart, $heure1erquart, $EP1_1erquart, $EP2_1erquart);
    $send = ajout_donnees_rencontre($idTournois, $date1erquart, $heure1erquart, $EP1_1erquart_pt2, $EP2_1erquart_pt2);
    if ($send1 || $send) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

  if (isset($_POST['demiSize4'])) {
    $pseudo = $_POST['pseudo'];
    $date = $_POST['dateR'];
    $heure = $_POST['heureR'];
    $id1 = $_POST['idEquipe1'];
    $id2 = $_POST['idEquipe2'];
    $idTournois = $_POST['idTournois'];
    $send1 = ajout_donnees_rencontre($idTournois, $date, $heure, $id1, $id2);

    if ($send1) {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }else {
      session_start();
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }

// size 2
if (isset($_POST['demiSize2'])) {
  $pseudo = $_POST['pseudo'];
  $date = $_POST['dateR'];
  $heure = $_POST['heureR'];
  $id1 = $_POST['idEquipe1'];
  $id2 = $_POST['idEquipe2'];
  $idTournois = $_POST['idTournois'];
  $send1 = ajout_donnees_rencontre($idTournois, $date, $heure, $id1, $id2);

  if ($send1) {
    session_start();
    $_SESSION["pseudo"] = $pseudo;
    header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
    exit();
  }else {
    session_start();
    $_SESSION["pseudo"] = $pseudo;
    $_SESSION["id"] = $idTournois;
    header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
    exit();
  }
}


// championnat
    if (isset($_POST['Valider'])) {
      $pseudo = $_POST['pseudo'];
      $idTournois = $_POST['idTournois'];
      $type = $_POST['type'];
      $dateRencontre = $_POST['dateRencontre'];
      $heureRencontre = $_POST['heureRencontre'];
      $nbr_equipes = $_POST['nbr_equipes'];
      $idSelect = array();
      $verif_nbr_equipes = 0;
      $id = 0;

      for ($i=0; $i < $nbr_equipes; $i++) {
        if (isset($_POST['Ajout'.$i])) {
          if ($_POST['Ajout'.$i] == "on") {
            $idSelect[0][$id] = $_POST['idEquipe'.$i];
            $id++;
            $verif_nbr_equipes++;
          }
        }
      }

      if($verif_nbr_equipes > 2 || $verif_nbr_equipes == 1){
        session_start();
        $_SESSION["pseudo"] = $pseudo;
        $_SESSION["id"] = $idTournois;
        header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
        exit();
      }
      else {
        if ($type  == "championnat") {
          $deja_affronte = championnat_deja_affronte($idTournois,  $idSelect[0][0], $idSelect[0][1]);
          if($deja_affronte){
            session_start();
            $_SESSION["pseudo"] = $pseudo;
            $_SESSION["id"] = $idTournois;
            header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
            exit();
          }
        }

        $send = ajout_donnees_rencontre($idTournois, $dateRencontre, $heureRencontre, $idSelect[0][0], $idSelect[0][1]);
        if($send){
          session_start();
          $_SESSION["pseudo"] = $pseudo;
          header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
          exit();
        }
        else {
          session_start();
          $_SESSION["pseudo"] = $pseudo;
          $_SESSION["id"] = $idTournois;
          header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
          exit();
        }
      }

    }

 ?>
