<?php
    // modifier les resultats
    function modif_resultats($idMatch, $score1, $score2, $idGagnant){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.rencontre SET score1=:score1, score2=:score2, idGagnant=:idGagnant WHERE idMatch = '{$idMatch}'");

          $requete->bindValue(':score1', $score1, PDO::PARAM_INT);
          $requete->bindValue(':score2', $score2, PDO::PARAM_INT);
          $requete->bindValue(':idGagnant', $idGagnant, PDO::PARAM_INT);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              return true;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    // renvoie les infos sur la rencontre
    function get_info_rencontre($idMatch){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idMatch = '{$idMatch}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    //renvoie les infos sur la competition
    function get_info_compet($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    //renvoie le nom de l equipe
    function get_info_nom_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur[0][0]['nom_equipe'];
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    if(isset($_POST['Valider'])){
      $idMatch = $_POST['idMatch'];
      $score1 = $_POST['score1'];
      $score2 = $_POST['score2'];
      $idEquipe1 = $_POST['idEquipe1'];
      $idEquipe2 = $_POST['idEquipe2'];

      if($score1 == $score2){
        session_start();
        $_SESSION["pseudo"] = $_POST['pseudo'];
        $_SESSION['id'] = $_POST['idMatch'];
        header("Location:../vue/modifier_resultats_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
        exit();
      }

      if ($score1 < $score2) {
        $idGagnant = $idEquipe2;
      }
      else {
        $idGagnant = $idEquipe1;
      }

      $send = modif_resultats($idMatch, $score1, $score2, $idGagnant);

      session_start();

      if($send){
        $_SESSION["pseudo"] = $_POST['pseudo'];
        header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
        exit();
      }
      else {
        $_SESSION["pseudo"] = $_POST['pseudo'];
        $_SESSION['id'] = $_POST['idMatch'];
        header("Location:../vue/modifier_resultats_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
        exit();
      }
    }

 ?>
