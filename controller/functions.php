<?php

//revoir message dans else apres excution de requete
//modification de suppression pour supprimer le role aussi dans joueur et utilisateur

//envoie des informations -> insertion dans la BDD
    function send_BDD_personnal_info($name, $lastname, $num, $email, $pseudo, $mdp){

      //connexion a la bdd Tournois
        $user = 'root';
        $pass = '';
        $serveur = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($serveur,$user,$pass);

        try {
          // insertion
          //a modifier le nom de la table et les noms des cles et ajouter pseudo a la BDD
          $requete = $db->prepare('INSERT INTO tournois.utilisateur VALUES (NULL, :mail, :mdp, :pseudo, :nom, :prenom, :num)');

          //on lie les elemnents a une clef
          $requete->bindValue(':mail', $email, PDO::PARAM_STR);
          $requete->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
          $requete->bindValue(':nom', $lastname, PDO::PARAM_STR);
          $requete->bindValue(':prenom', $name, PDO::PARAM_STR);
          $requete->bindValue(':num', $num, PDO::PARAM_INT);

          //hachage du mot de passe
          $password = password_hash($mdp,PASSWORD_DEFAULT);
          $requete->bindValue(':mdp', $password, PDO::PARAM_STR);

          $correct_insertion = $requete->execute();

          if($correct_insertion){
            $message =  "Bienvenue";
            echo $message;
          }
          else {
            $message = "Echec de la creation du compte";
            echo $message;
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }

    }

    // envoie les infos d un membre de l equipe liste
    function send_info_member($lastname, $name, $idEquipe){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare("INSERT INTO tournois.joueur (idJoueur, idEquipeJ, nom, prenom) VALUES (NULL, :idEquipeJ, :nom, :prenom)");

        // on lie les elemnents a une clef
        $requete->bindValue(':nom', $lastname, PDO::PARAM_STR);
        $requete->bindValue(':prenom', $name, PDO::PARAM_STR);
        $requete->bindValue(':idEquipeJ', $idEquipe, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "Equipier ajouté";
          echo $message;
        }
        else {
          $message = "Echec de l ajout de l équipier";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
    }

    // envoie les infos d un membre de l equipe liste avec en info le numero de tel et l email
    function send_info_member_tel_et_email($lastname, $name, $idEquipe, $num, $email){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare('INSERT INTO tournois.joueur VALUES (NULL, :idEquipeJ, :nom, :prenom, :telephone, :adresse)');

        // on lie les elemnents a une clef
        $requete->bindValue(':nom', $lastname, PDO::PARAM_STR);
        $requete->bindValue(':prenom', $name, PDO::PARAM_STR);
        $requete->bindValue(':idEquipeJ', $idEquipe, PDO::PARAM_INT);
        $requete->bindValue(':telephone', $num, PDO::PARAM_INT);
        $requete->bindValue(':adresse', $email, PDO::PARAM_STR);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "Equipier ajouté";
          echo $message;
        }
        else {
          $message = "Echec de l ajout de l équipier";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
    }

    // envoie les infos sur la competition
    function send_info_compet($dateD, $duree, $lieu, $type, $nom_tournois, $idEquipe, $idGestionnaire, $nbreEquipe, $Esport){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare('INSERT INTO tournois.tournois VALUES (NULL, :dateDebut, :duree, :idGestionnaire, :lieu, :type, :idEquipe, :nom_tournois, :nbr_equipes, :Esport)');

        //on lie les elemnents a une clef
        $requete->bindValue(':dateDebut', $dateD, PDO::PARAM_STR);
        $requete->bindValue(':duree', $duree, PDO::PARAM_INT);
        $requete->bindValue(':lieu', $lieu, PDO::PARAM_STR);
        $requete->bindValue(':type', $type, PDO::PARAM_STR);
        $requete->bindValue(':nom_tournois', $nom_tournois, PDO::PARAM_STR);
        $requete->bindValue(':idEquipe', $idEquipe, PDO::PARAM_INT);
        $requete->bindValue(':nbr_equipes', $nbreEquipe, PDO::PARAM_INT);
        $requete->bindValue(':Esport', $Esport, PDO::PARAM_STR);
        $requete->bindValue(':idGestionnaire', $idGestionnaire, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "Competition créée";
          echo $message;
        }
        else {
          $message = "Echec de la création";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }

    }

    //envoie les infos sur l equipe en general
    function send_info_equipe($nom_equipe, $niveau, $adresse, $num, $nbJ, $idCapitaine, $Esport){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare('INSERT INTO tournois.equipe (idEquipe, nom_equipe, niveau, adresse_equipe, equipe_tel, nb_joueur, idCapitaine, Esport) VALUES (NULL, :nom_equipe, :niveau, :adresse_equipe, :equipe_tel, :nb_joueur, :idCapitaine, :Esport)');

        //on lie les elemnents a une clef
        $requete->bindValue(':nom_equipe', $nom_equipe, PDO::PARAM_STR);
        $requete->bindValue(':niveau', $niveau, PDO::PARAM_INT);
        $requete->bindValue(':adresse_equipe', $adresse, PDO::PARAM_STR);
        $requete->bindValue(':equipe_tel', $num, PDO::PARAM_INT);
        $requete->bindValue(':nb_joueur', $nbJ, PDO::PARAM_INT);
        $requete->bindValue(':idCapitaine', $idCapitaine, PDO::PARAM_INT);
        $requete->bindValue(':Esport', $Esport, PDO::PARAM_STR);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "Gestion de l équipe créée";
          echo $message;
        }
        else {
          $message = "Echec de la création";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }

    }

    //emvoie les infos de la creation de poules
    function send_info_poule($idE1, $idE2, $idE3, $idE4){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare("INSERT INTO tournois.poule VALUES (NULL, :idEP1, :idEP2, :idEP3, :idEP4)");

        // on lie les elemnents a une clef
        $requete->bindValue(':idEP1', $idE1, PDO::PARAM_INT);
        $requete->bindValue(':idEP2', $idE2, PDO::PARAM_INT);
        $requete->bindValue(':idEP3', $idE3, PDO::PARAM_INT);
        $requete->bindValue(':idEP4', $idE4, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "Poule créé";
          echo $message;
        }
        else {
          $message = "Echec de la création de la poule";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
    }

    //envoie les infos sur la rencontre
    function send_info_rencontre($dateR, $heureR, $idE1, $idE2){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare("INSERT INTO tournois.rencontre (idMatch, dateRencontre, heureRencontre, idEquipe1, idEquipe2) VALUES (NULL, :dateRencontre, :heureRencontre, :idEquipe1, :idEquipe2)");

        // on lie les elemnents a une clef
        $requete->bindValue(':dateRencontre', $dateR, PDO::PARAM_STR);
        $requete->bindValue(':heureRencontre', $heureR, PDO::PARAM_STR);
        $requete->bindValue(':idEquipe1', $idE1, PDO::PARAM_INT);
        $requete->bindValue(':idEquipe2', $idE2, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "Rencontre créé";
          echo $message;
        }
        else {
          $message = "Echec de la création de la rencontre";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
    }

    //envoie les infos sur le resultat de la rencontre
    function send_info_rencontre_resultat($idMatch, $score1, $score2, $idE1, $idE2){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      if($score1 < $score2){
        $scoreG = $score2;
        $idGagnant = $idE2;
      }
      if($score1 > $score2){
        $scoreG = $score1;
        $idGagnant = $idE1;
      }else {
        $idGagnant = 0;
      }

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare("UPDATE tournois.rencontre SET idGagnant=:idGagnant, score1=:score1, score2=:score2 WHERE idMatch = '{$idMatch}'");

        // on lie les elemnents a une clef
        $requete->bindValue(':idGagnant', $idGagnant, PDO::PARAM_INT);
        $requete->bindValue(':score1', $score1, PDO::PARAM_INT);
        $requete->bindValue(':score2', $score2, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "Ajout des resultats de la rencontre";
          echo $message;
        }
        else {
          $message = "Echec de l ajout des resultats de la rencontre";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
    }

    //ajoute le role de la personne en admin
    function send_info_role_isadmin($idAdmin){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare("INSERT INTO tournois.role (id_role, is_admin) VALUES (NULL, :is_admin)");

        // on lie les elemnents a une clef
        $requete->bindValue(':is_admin', $idAdmin, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "role d admin ajouté";
          echo $message;
        }
        else {
          $message = "Echec de l ajout du role admin";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
    }

    //ajoute le role de la personne en Gestionnaire
    function send_info_role_isgestionnaire($idGestionnaire){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare("INSERT INTO tournois.role (id_role, is_gestionnaire) VALUES (NULL, :is_gestionnaire)");

        // on lie les elemnents a une clef
        $requete->bindValue(':is_gestionnaire', $idGestionnaire, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          $message =  "role de Gestionnaire ajouté";
          echo $message;
        }
        else {
          $message = "Echec de l ajout du role Gestionnaire";
          echo $message;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }
    }

    //ajoute le role de la personne en joueur
    function send_info_role_isjoueur($idJoueur){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          // insertion
          //a modifier les noms des cles et ajouter pseudo a la BDD
          $requete = $db->prepare("INSERT INTO tournois.role (id_role, is_joueur) VALUES (NULL, :is_joueur)");

          // on lie les elemnents a une clef
          $requete->bindValue(':is_joueur', $idJoueur, PDO::PARAM_INT);

          $correct_insertion = $requete->execute();

          if($correct_insertion){
            return true;
          }
          else {
            return false;
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;

        }
    }


//verification
    function verif_mdp_identique($mdp, $verif){

      if($mdp == $verif){
        return true;
      }

      return false;

    }

    function verif_email_utilise($email){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT mail FROM tournois.utilisateur WHERE EXISTS (SELECT mail FROM tournois.utilisateur WHERE mail = '{$email}')");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetchAll();

            //si le mail appartient a un utilisateur dire que c est impossible de creer un compte avec cette adresse
            if(count($utilisateur) == 0){
              echo "personne";
              return false;
            }
            else {
              echo "utilisé";
              return true;
            }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    function verif_pseudo_utilise($pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT pseudo FROM tournois.utilisateur WHERE EXISTS (SELECT pseudo FROM tournois.utilisateur WHERE pseudo = '{$pseudo}')");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetchAll();

            //si le mail appartient a un utilisateur dire que c est impossible de creer un compte avec cette adresse
            if(count($utilisateur) == 0){
              echo "Non utilisé";
              return false;
            }
            else {
              echo "Pseudo déjà utilisé";
              return true;
            }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }


//recuperation d informations -> rendre affichage plus joli
    //avoir toutes les infos sur l utilisateur
    function get_info_utilisateur($pseudo){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetch();

              echo $utilisateur['pseudo'];
              echo $utilisateur['nom'];
              echo $utilisateur['prenom'];
              echo $utilisateur['mail'];
              echo $utilisateur['num'];

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recupere le nom de l utilisateur
    function get_name($pseudo){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
            $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

        //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = $requete->fetch();

              echo $utilisateur['nom'];
            }
            else {
              print "Erreur de l excution de la requete";
            }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
        }
    }

    // recupere le pseudo de l utilisateur
    function get_Pseudo($email){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          //creation de la requete
          //voir si les noms sont les bons
          $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE mail = '{$email}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $utilisateur = $requete->fetch();

            echo $utilisateur['pseudo'];
          }
          else {
            print "Erreur de l excution de la requete";
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }

      }

    // recupere le prenom de l utilisateur
    function get_prenom($pseudo){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetch();

          echo $utilisateur['prenom'];
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }

    }

    //recupere le role de l utilisateur
    function get_role_isadmin($pseudo){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          //creation de la requete
          //voir si les noms sont les bons
          $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $utilisateur = $requete->fetch();

            echo $utilisateur['prenom'];
          }
          else {
            print "Erreur de l excution de la requete";
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }
    }

//Revoir message du else apres excution de la requete
//suppression d une ligne entiere de la BDD
    function suppression_role($idRole){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
        $requete = $db->exec("DELETE FROM tournois.role WHERE id_role = '{$idRole}' LIMIT 1");

        if($requete == 1){
          echo "Role supprimé";
        }
        else {
          echo "Erreur de la suppression du Role";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }

    }

    function supprimer_tournois($idTournois){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //voir si les noms sont les bons
          //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
          $requete = $db->exec("DELETE FROM tournois.tournois WHERE idTournois = '{$idTournois}' LIMIT 1");

          // on verifie si la ligne a ete effacée
          if($requete == 1){
            echo "Competition supprimée";
          }
          else {
            print "Aucune competition supprimé";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    function suppresion_equipe($idEquipe){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //voir si les noms sont les bons
          //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
          $requete1 = $db->exec("DELETE FROM tournois.joueur WHERE idEquipeJ = '{$idEquipe}'");
          $requete2 = $db->exec("DELETE FROM tournois.equipe WHERE idEquipe = '{$idEquipe}' LIMIT 1");

          // on verifie si la ligne a ete effacée
          if(($requete1 >= 1) && ($requete2 == 1)){
            echo "equipe supprimée";
          }
          else {
            print "Aucune competition supprimé";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }

    }

    function suppression_joueur($idJoueur){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
        $requete = $db->exec("DELETE FROM tournois.joueur WHERE idJoueur = '{$idJoueur}' LIMIT 1");

        if($requete == 1){
          echo "Joueur supprimé";
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    function suppression_utilisateur($idUser){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
        $requete = $db->exec("DELETE FROM tournois.utilisateur WHERE idUser = '{$idUser}' LIMIT 1");

        if($requete == 1){
          echo "Compte supprimé";
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }

    }

    function suppresion_rencontre($idMatch){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
        $requete = $db->exec("DELETE FROM tournois.rencontre WHERE idMatch = '{$idMatch}' LIMIT 1");

        if($requete == 1){
          echo "Rencontre supprimé";
        }
        else {
          echo "Erreur de la suppression de la Rencontre";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }

    }

    function suppresion_poule($idPoule){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
        $requete = $db->exec("DELETE FROM tournois.poule WHERE idPoule = '{$idPoule}' LIMIT 1");

        if($requete == 1){
          echo "Poule supprimé";
        }
        else {
          echo "Erreur de la suppression de la poule";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }

    }


//A FAIRE
//modification espace abonne -> ne prendra en compte que les donnees modifier par l utilisateur
// -> mettre dans le formulaire en hidden l idUser pour la recuperation et le lancement de la fonction
// -> mettre values = a la recuperation des donnees
    function modification_utilisateur($idUser, $newPrenom, $newNom, $newPseudo, $newNum, $newMail){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.utilisateur SET mail=:mail, pseudo=:pseudo, nom=:nom, prenom=:prenom, num=:num WHERE idUser = '{$idUser}'");

          $requete->bindValue(':pseudo', $newPseudo, PDO::PARAM_STR);
          $requete->bindValue(':prenom', $newPrenom, PDO::PARAM_STR);
          $requete->bindValue(':nom', $newNom, PDO::PARAM_STR);
          $requete->bindValue(':num', $newNum, PDO::PARAM_INT);
          $requete->bindValue(':mail', $newMail, PDO::PARAM_STR);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              echo "Modification réussite";
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }

    }

    function modification_tournois($idTournois, $dateD, $duree, $lieu, $type, $nom_tournois, $nbr_equipes, $Esport){
          $user = 'root';
          $pass = '';
          $connexion = 'mysql:host=localhost;dbname=tournois';
          $db = new PDO($connexion,$user,$pass);

        try {
            //creation de la requete
            //modification des valeurs dans la BDD
            $requete = $db->prepare("UPDATE tournois.tournois SET dateDebut=:dateDebut, duree=:duree, lieu=:lieu, type=:type, nom_tournois=:nom_tournois, nbr_equipes=:nbr_equipes, Esport=:Esport WHERE idTournois = '{$idTournois}'");

            $requete->bindValue(':dateDebut', $dateD, PDO::PARAM_STR);
            $requete->bindValue(':duree', $duree, PDO::PARAM_INT);
            $requete->bindValue(':lieu', $lieu, PDO::PARAM_STR);
            $requete->bindValue(':type', $type, PDO::PARAM_STR);
            $requete->bindValue(':nom_tournois', $nom_tournois, PDO::PARAM_STR);
            $requete->bindValue(':nbr_equipes', $nbr_equipes, PDO::PARAM_INT);
            $requete->bindValue(':Esport', $Esport, PDO::PARAM_STR);

            //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
                echo "Modification réussite";
            }
            else {
              print "Erreur de l'excution de la requete / Modification";
            }

        } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die;
        }
    }

    function modification_role_isadmin($idRole, $idPersonne){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.role SET is_admin=:is_admin WHERE id_role = '{$idRole}'");

          $requete->bindValue(':is_admin', $idPersonne, PDO::PARAM_INT);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              echo "Modification réussite";
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }

    }

    function modification_role_isgestionnaire($idRole, $idPersonne){
          $user = 'root';
          $pass = '';
          $connexion = 'mysql:host=localhost;dbname=tournois';
          $db = new PDO($connexion,$user,$pass);

        try {
            //creation de la requete
            //modification des valeurs dans la BDD
            $requete = $db->prepare("UPDATE tournois.role SET is_gestionnaire=:is_gestionnaire WHERE id_role = '{$idRole}'");

            $requete->bindValue(':is_gestionnaire', $idPersonne, PDO::PARAM_INT);

            //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
                echo "Modification réussite";
            }
            else {
              print "Erreur de l'excution de la requete / Modification";
            }

        } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die;
        }
    }

    function modification_role_isjoueur($idRole, $idPersonne){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.role SET is_joueur=:is_joueur WHERE id_role = '{$idRole}'");

          $requete->bindValue(':is_joueur', $idPersonne, PDO::PARAM_INT);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              echo "Modification réussite";
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    function modification_rencontre($idRencontre, $dateRencontre, $heureRencontre, $idEquipe1, $idEquipe2){
          $user = 'root';
          $pass = '';
          $connexion = 'mysql:host=localhost;dbname=tournois';
          $db = new PDO($connexion,$user,$pass);

        try {
            //creation de la requete
            //modification des valeurs dans la BDD
            $requete = $db->prepare("UPDATE tournois.rencontre SET dateRencontre=:dateRencontre, heureRencontre=:heureRencontre, idEquipe1=:idEquipe1, idEquipe2=:idEquipe2 WHERE idMatch = '{$idRencontre}'");

            $requete->bindValue(':dateRencontre', $dateRencontre, PDO::PARAM_STR);
            $requete->bindValue(':heureRencontre', $heureRencontre, PDO::PARAM_STR);
            $requete->bindValue(':idEquipe1', $idEquipe1, PDO::PARAM_INT);
            $requete->bindValue(':idEquipe2', $idEquipe2, PDO::PARAM_INT);

            //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
                echo "Modification réussite";
            }
            else {
              print "Erreur de l'excution de la requete / Modification";
            }

        } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die;
        }
    }

    function modification_poule($idPoule, $idEP1, $idEP2, $idEP3, $idEP4){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.poule SET idEP1=:idEP1, idEP2=:idEP2, idEP3=:idEP3, idEP4=:idEP4 WHERE idPoule = '{$idPoule}'");

          $requete->bindValue(':idEP1', $idEP1, PDO::PARAM_INT);
          $requete->bindValue(':idEP2', $idEP2, PDO::PARAM_INT);
          $requete->bindValue(':idEP3', $idEP3, PDO::PARAM_INT);
          $requete->bindValue(':idEP4', $idEP4, PDO::PARAM_INT);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              echo "Modification réussite";
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    function modification_joueur($idJoueur ,$idEquipeJ, $nom, $prenom, $telephone, $email){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.joueur SET idEquipeJ=:idEquipeJ, nom=:nom, prenom=:prenom, telephone=:telephone, adresse=:adresse WHERE idJoueur = '{$idJoueur}'");

          $requete->bindValue(':idEquipeJ', $idEquipeJ, PDO::PARAM_INT);
          $requete->bindValue(':nom', $nom, PDO::PARAM_STR);
          $requete->bindValue(':prenom', $prenom, PDO::PARAM_STR);
          $requete->bindValue(':telephone', $telephone, PDO::PARAM_INT);
          $requete->bindValue(':adresse', $email, PDO::PARAM_STR);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              echo "Modification réussite";
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    function modification_equipe($idEquipe, $nom_equipe, $niveau, $adresse_equipe, $equipe_tel, $nb_joueur, $idCapitaine, $Esport){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.equipe SET nom_equipe=:nom_equipe, niveau=:niveau, adresse_equipe=:adresse_equipe, equipe_tel=:equipe_tel, nb_joueur=:nb_joueur, idCapitaine=:idCapitaine, Esport=:Esport WHERE idEquipe = '{$idEquipe}'");

          $requete->bindValue(':nom_equipe', $nom_equipe, PDO::PARAM_STR);
          $requete->bindValue(':niveau', $niveau, PDO::PARAM_INT);
          $requete->bindValue(':adresse_equipe', $adresse_equipe, PDO::PARAM_STR);
          $requete->bindValue(':equipe_tel', $equipe_tel, PDO::PARAM_INT);
          $requete->bindValue(':nb_joueur', $nb_joueur, PDO::PARAM_INT);
          $requete->bindValue(':idCapitaine', $idCapitaine, PDO::PARAM_INT);
          $requete->bindValue(':Esport', $Esport, PDO::PARAM_STR);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              echo "Modification réussite";
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }


//A FAIRE
//Partie pas testée
//connexion/deconnexion -> creation de session /!\ pris de la doc
    function connexion($email, $pseudo, $mdp){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          $requete = $db->prepare("SELECT mail, mdp, pseudo FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

          //executer la requete
          $execution_requete = $requete->execute();

          $utilisateur = $requete->fetch();

          //verification si le mdp est le meme que celui de la BDD (renvoie un booleen)
          $password = password_verify($mdp,$utilisateur['mdp']);

          if($execution_requete == false){
              echo "Avez-vous un compte ?";
          }
          else {
            if (($utilisateur['mail'] == $email) && ($utilisateur['pseudo'] == $pseudo) && ($password) ) {
                session_start();
                $_SESSION['pseudo'] = $pseudo;
                echo "Connexion réussite";
            }
            else {
              echo "Mauvais pseudo, email ou mot de passe ?";
            }
          }

        } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die;
        }

    }

    function deconnexion(){
        // enleve toutes les variables de la variable globale SESSION
        $_SESSION = array();

        //enleve les cookies -> detruit la session et les donnees
        if(init_get("session.use_cookies")){
          $params = session_get_cookie_params();
          setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        }

        //detruire la session
        session_destroy();

    }

    //verifie si une variable existe
    function verification_variable_existe($var){
      if(isset($var)){
        return true;
      }
      return false;
    }

    //verifie si une variable est vide ou non
    function verification_variable_estVide($var){
      if(empty($var)){
        return true;
      }
      return false;
    }

    //donne le resultat d un match entre 2 equipes (assignation des points et vainqueurs)
    function match_resultat(){

    }

    function get_idUser($pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetch();
          return $utilisateur['idUser'];

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    function get_idrole_isadmin($idUser){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          //creation de la requete
          //voir si les noms sont les bons
          $requete = $db->prepare("SELECT * FROM tournois.role WHERE is_admin = '{$idUser}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $utilisateur = $requete->fetch();

            return $utilisateur['id_role'];

          }
          else {
            print "Erreur de l excution de la requete";
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }
    }

    function get_idrole_isgestionnaire($idUser){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          //creation de la requete
          //voir si les noms sont les bons
          $requete = $db->prepare("SELECT * FROM tournois.role WHERE is_gestionnaire = '{$idUser}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $utilisateur = $requete->fetch();

            return $utilisateur['id_role'];
          }
          else {
            print "Erreur de l excution de la requete";
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }
    }

    function get_idrole_isjoueur($idUser){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          //creation de la requete
          //voir si les noms sont les bons
          $requete = $db->prepare("SELECT * FROM tournois.role WHERE is_joueur = '{$idUser}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $utilisateur = $requete->fetch();

            return $utilisateur['id_role'];
          }
          else {
            print "Erreur de l excution de la requete";
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }
    }

    function get_idrole_iscapitaine($idUser){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          //creation de la requete
          //voir si les noms sont les bons
          $requete = $db->prepare("SELECT * FROM tournois.role WHERE is_capitaine = '{$idUser}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $utilisateur = $requete->fetch();
            // return $utilisateur['id_role'];

          }
          else {
            print "Erreur de l excution de la requete";
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }
    }

 ?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>test</title>
  </head>
  <body>

    <form method="post" action="functions.php">

      <!-- <p>date Debut             <input type="date" name="dateDebut" required/></p>
      <p>duree             <input type="number" name="duree" required/></p>
      <p>id Gestionnaire              <input type="number" name="idGestionnaire"required/></p>
      <p>lieu            <input type="text" name="lieu"required/></p>
      <p>type            <input type="text" name="type"required/></p>

      <p>nom tournois           <input type="text" name="nom_tournois"required/></p>
      <p>id Equipe              <input type="number" name="idEquipe"required/></p>
      <p>nbre equipe              <input type="number" name="nbreEquipe"required/></p> -->

      <!-- <p>email              <input type="email" name="email"required/></p> -->

      <!-- <p>num           <input type="text" name="num"required/></p> -->
      <!-- <p>nb_joueur             <input type="number" name="nbreJ"required/></p> -->
      <!-- <p>Esport           <input type="text" name="Esport"required/></p> -->

      <input type="submit" value="Valider"/>

      </form>

      <p> <?php

      // $dateD = $_POST["dateDebut"];
      // $duree = $_POST["duree"];
      // $idGestionnaire = $_POST["idGestionnaire"];
      // $lieu = $_POST["lieu"];
      // $type = $_POST["type"];
      // $idEquipe = $_POST["idEquipe"];
      // $nom_tournois = $_POST["nom_tournois"];
      // $Esport = $_POST["Esport"];
      // $nbreEquipe = $_POST["nbreEquipe"];
      $pseudo = "test";
      $idPersonne = get_idUser($pseudo);
      // $getid = 4;
      $idRole = get_idrole_isgestionnaire($idPersonne);
      modification_role_isadmin($idRole, $idPersonne);
      echo $idRole;

      $idTournois = 5;

      $idEP1 = 2;
      $idEP2 = 1;
      $idEP3 = 3;
      $idEP4 = 5;

      $score1 = 2;
      $score2 = 2;
      $idMatch = 2;

      $idAdmin = 2;
      $idJoueur = 3;


      $idEquipe = 2;
      $idJoueur = 3;
      $idRole = 2;
      $idPoule = 2;
      $idUser = 4;
      $idPersonne = 3;

      $newPrenom = "jai";
      $newNom = "faim";
      $newPseudo = "vraiment";
      $newNum = "0645412123";
      $newMail = "jaivraimentfaim@miam.fr";

      $dateD = "2021-01-13";
      $duree = 12;
      $lieu = "gwoipgehwe";
      $type = "championnat";
      $nom_tournois = "afgilih";
      $nbr_equipes = 132;
      $Esport = "CS";

      $idRencontre = 3;
      $dateRencontre = "2021-06-22";
      $heureRencontre = "22:22:00";
      $idEquipe1 = 9;
      $idEquipe2 = 12;

      $idJoueur = 1;
      $idEquipeJ = 2;
      $prenom = "encore";
      $nom = "dutravail";
      $telephone = "0123456789";
      $email = "encoredutravail@gmail.com";

      $idEquipe = 2;
      $nom_equipe = "mhfc";
      $niveau = 43;
      $adresse_equipe = "jpp@jaimepaslefoot.fr";
      $equipe_tel = "0123456789";
      $nb_joueur = 15;
      $idCapitaine = 32;
      $Esport = "CS";


      // $dateR = $_POST["dateRencontre"];
      // $heureR = $_POST["heureR"];

//Test

      // send_info_rencontre_resultat($idMatch, $score1, $score2, $idE1, $idE2);
      // send_info_role_isadmin($idAdmin);
      // send_info_role_isgestionnaire($idGestionnaire);
      // send_info_role_isjoueur($getid);
      // send_info_rencontre($dateR, $heureR, $idE1, $idE2);
      // send_info_poule($idE1, $idE2, $idE3, $idE4);
      // send_info_equipe($nom_equipe, $niveau, $adresse, $num, $ndJ, $idCapitaine, $Esport);
      // send_info_compet($dateD, $duree, $lieu, $type, $nom_tournois, $idEquipe, $idGestionnaire, $nbreEquipe, $Esport);
      // send_BDD_personnal_info($name, $lastname, $num, $email, $pseudo, $mdp);
      // send_info_member($lastname, $name, $idEquipe);
      // send_info_member_tel_et_email($lastname, $name, $idEquipe, $num, $email);

      // get_info_utilisateur($pseudo);
      // get_name($pseudo);
      // echo " ";
      // get_prenom($pseudo);

      // suppresion_equipe($idEquipe);
      // supprimer_competition($idTournois);
      // suppression_equipier($idJoueur);
      // suppression_compte($idUser);
      // suppresion_rencontre($idMatch);
      // suppression_role($idRole);
      // suppresion_poule($idPoule);

      // modification_utilisateur($idUser, $newPrenom, $newNom, $newPseudo, $newNum, $newMail);
      // modification_tournois($idTournois, $dateD, $duree, $lieu, $type, $nom_tournois, $nbr_equipes, $Esport);
      // modification_role_isadmin($idRole, $idPersonne);
      // modification_role_isgestionnaire($idRole, $idPersonne);
      // modification_role_isjoueur($idRole, $idPersonne);
      // modification_rencontre($idRencontre, $dateRencontre, $heureRencontre, $idEquipe1, $idEquipe2);
      // modification_poule($idPoule, $idEP1, $idEP2, $idEP3, $idEP4);
      // modification_joueur($idJoueur, $idEquipeJ, $nom, $prenom, $telephone, $email);
      // modification_equipe($idEquipe, $nom_equipe, $niveau, $adresse_equipe, $equipe_tel, $nb_joueur, $idCapitaine, $Esport);

      // $res = verif_email_utilise($email);
      // verif_pseudo_utilise($type);
      // $booll = verif_mdp_utilise($lieu, $type);
      // if($res){
      //   echo "utilise";
      // }
      // else {
      //   echo "libre";
      // }
      // echo true;
      // echo get_Pseudo();

      ?> </p>



  </body>
</html>
