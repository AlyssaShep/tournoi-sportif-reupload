<?php

// planifie automatiquement toutes les rencontres de la poule
    function planification_auto($idPoule, $idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.poule WHERE idPoule = '{$idPoule}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $poule = array($requete->fetchAll());
          for ($i=1; $i < 5; $i++) {
            for ($j=$i; $j < 5; $j++) {
              if ($i != $j) {

                $requete1 = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois='{$idTournois}' AND idEquipe1 = '{$poule[0][0]['idEP'.$i]}' AND idEquipe2 = '{$poule[0][0]['idEP'.$j]}'");

                $recherche = $requete1->execute();

                $resutats_requetes = array($requete1->fetchAll());

                if(empty($resutats_requetes[0])){
                  $requete = $db->prepare('INSERT INTO tournois.rencontre (idMatch, idTournois, dateRencontre, heureRencontre, idEquipe1, idEquipe2) VALUES (NULL, :idTournois, :dateRencontre, :heureRencontre, :idEquipe1, :idEquipe2)');

                  $date = date("Y-m-d");
                  $time = date("H:i:s");
                  $requete->bindValue(':idTournois', $idTournois, PDO::PARAM_INT);
                  $requete->bindValue(':dateRencontre', $date, PDO::PARAM_STR);
                  $requete->bindValue(':heureRencontre', $time, PDO::PARAM_STR);
                  $requete->bindValue(':idEquipe1', $poule[0][0]['idEP'.$i], PDO::PARAM_INT);
                  $requete->bindValue(':idEquipe2', $poule[0][0]['idEP'.$j], PDO::PARAM_INT);

                  $correct_insertion = $requete->execute();

                  if(!$correct_insertion){
                    return false;
                  }
                }
              }
            }
          }
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    $idPoule = $_GET['idP'];
    $idTournois = $_GET['idT'];
    $pseudo = $_GET['pseudo'];

    $send = planification_auto($idPoule, $idTournois);

    session_start();

    if ($send) {
      $_SESSION["pseudo"] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }
    else {
      $_SESSION["pseudo"] = $pseudo;
      $_SESSION["id"] = $idTournois;
      header("Location:../vue/planifier_rencontres_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }

 ?>
