<?php

    // donne les info sur l equipe
    function get_info_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les infos sur la competition
    function get_info_competition($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les equipes qui y participent
    function get_equipes_participantes($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          $equipes = array($utilisateur[0][0]['idEquipe']);
          $longueur = strlen($equipes[0]);
          $idrecuparray = array();
          $idrecup = "";
          $id = 0;

          for ($i=0; $i < $longueur; $i++) {
            if ($equipes[0][$i] == ",") {
                $idrecuparray[0][$id] = $idrecup;
                $id++;
                $idrecup = "";
            }
            else {
              $idrecup = $idrecup.$equipes[0][$i];
            }
          }
          return $idrecuparray;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

 ?>
