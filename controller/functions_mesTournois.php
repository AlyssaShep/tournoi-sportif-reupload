<?php
// verifie si la competition a besoin d etre prolongé
    function prolonge_tournoi(){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

    try {
        //creation de la requete
        //modification des valeurs dans la BDD
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE Agagnant = 0");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $tournois = array($requete->fetchAll());
          $date = date("Y-m-d");

          if (empty($tournois[0])) {
            return true;
          }

          $size = count($tournois[0]);

          for ($j=0; $j < $size; $j++) {

            $dateFin = date("Y-m-d", strtotime($tournois[0][$j]['dateDebut'].'+'.$tournois[0][$j]['duree'].'days'));
            // var_dump($dateFin);
            if ($dateFin <= $date) {
              $duree = $tournois[0][$j]['duree'];
              while ($dateFin <= $date) {
                $duree++;
                $dateFin = date("Y-m-d", strtotime($tournois[0][$j]['dateDebut'].'+'.$duree.'days'));
              }
              $duree = $duree + 5;
              // var_dump($duree);
              $idT = $tournois[0][$j]['idTournois'];
              $requete = $db->prepare("UPDATE tournois.tournois SET duree = REPLACE(duree,duree,'{$duree}') WHERE idTournois = '{$idT}'");
              $execution_requete = $requete->execute();
            }

          }

            return true;
        }
        else {
          return false;
        }

    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
    }
    }

    // compte le nombre d equipes
    function compt_nbr_equipes_tournois($idTournois){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $tournois = array($requete->fetchAll());
            $idEquipes = $tournois[0][0]['idEquipe'];
            $size = strlen($idEquipes);
            $nbr = 0;

            for ($i=0; $i < $size; $i++) {
              if ($idEquipes[$i] == ",") {
                $nbr++;
              }
            }

              return $nbr;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    // reporte tous les tournois qui vont commencer demain et qui n ont pas le bon nombre d équipes
    function report_tournois(){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("SELECT * FROM tournois.tournois");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $tournois = array($requete->fetchAll());
            $size = count($tournois[0]);
            $report = array();
            $id = 0;
            $date = date("Y-m-d");

            for ($i=0; $i < $size; $i++) {
              $nbr_obligatoire = $tournois[0][$i]['nbr_equipes'];
              $nbr_reel = compt_nbr_equipes_tournois($tournois[0][$i]['idTournois']);
              $debutTournois = $tournois[0][$i]['dateDebut'];
              if ($nbr_reel != $nbr_obligatoire) {
                if ($debutTournois <= $date) {
                  $report[0][$id] = $tournois[0][$i]['idTournois'];
                  $id++;
                }
              }
            }

            if (empty($report)) {
              return true;
            }

            // var_dump($report[0][0]);
            // var_dump(count($report[0]));
            $nbr_report = count($report[0]);
            $date = date("Y-m-d");
            $newDate = date("Y-m-d", strtotime($date.'+ 3 days'));

            for ($j=0; $j < $nbr_report; $j++) {
              $idT = $report[0][$j];
              // echo $report[0][$j];
              // echo "\n";
              // var_dump($report[0][$j]);
              $requete = $db->prepare("UPDATE tournois.tournois SET dateDebut = REPLACE(dateDebut,dateDebut,'{$newDate}') WHERE idTournois = '{$idT}'");
              $execution_requete = $requete->execute();
            }

              return true;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    // get info utilisateur
    function get_info_membre($idUser){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE idUser = '{$idUser}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les infos d une equipe
    function get_info_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les infos sur la competition
    function get_info_competition($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recherche l idEquipe d un joueur
    function get_equipe_joueur_tournois_encours($pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE pseudo = '{$pseudo}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recherche de tous les tournois que la personne gere avec son id
    function get_tournois_gestion($idUser){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idGestionnaire = '{$idUser}' AND dateDebut > NOW() ORDER BY Esport ASC");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recherche tous les tournois que la personne gere pour plannifier les rencontres (uniquement quand ils ont commencé)
    function get_tournois_plannification($idUser){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idGestionnaire = '{$idUser}' AND dateDebut <= NOW() AND ADDDATE(dateDebut, INTERVAL duree DAY) >= NOW() ORDER BY Esport ASC");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recherche des tournois n ayant pas commencé
    function get_tournois_pour_inscription(){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE dateDebut > NOW() ORDER BY Esport ASC");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recherche des tournois en cours ou la personne participe
    function get_tournois_participation($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE ADDDATE(dateDebut, INTERVAL duree DAY) >= NOW() AND dateDebut <= NOW() AND (idEquipe LIKE '%{$idEquipe},%')");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if(empty($utilisateur[0])){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recherche les rencontres en cours ou le joueur participe
    function get_rencontre_participation($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre AS rencontre INNER JOIN tournois.tournois AS tournois ON rencontre.idTournois = tournois.idTournois WHERE CONCAT(rencontre.dateRencontre,' ',rencontre.heureRencontre) <= NOW() AND (rencontre.idEquipe1 = '{$idEquipe}' OR rencontre.idEquipe2 = '{$idEquipe}') AND tournois.dateDebut <= NOW() AND ADDDATE(tournois.dateDebut, INTERVAL tournois.duree DAY) >= NOW() ");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recherche des tournois gerer ou participé mais terminé
    function get_tournois_participation_terminee($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE ADDDATE(dateDebut, INTERVAL duree DAY) < NOW() AND dateDebut < NOW() AND (idEquipe LIKE '%{$idEquipe},%') ORDER BY Esport ASC");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne toutes les gestions sans distinction
    function get_all_gestion($idUser){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idGestionnaire = '{$idUser}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // retourne les idTournois qui sont terminés
    function get_tournois_gestion_termines($idUser){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idGestionnaire = '{$idUser}' AND ADDDATE(dateDebut, INTERVAL duree DAY) <= NOW()");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne les rencontres terminées
    function get_rencontre_terminees($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE (idEquipe1 = '{$idEquipe}' OR idEquipe2 = '{$idEquipe}') AND CONCAT(dateRencontre,' ',heureRencontre) < NOW() AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recherche les equipes qui veulent participer a un tournois
    function get_equipe_possible_tournois($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // ajout des equipes apres validation du gestionnaire
    function add_equipe_tournois($idTournois, $idEquipe){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.tournois SET idEquipe:=idEquipe WHERE idTournois = '{$idTournois}'");

          $requete->bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              echo "Modification réussite";
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    // recherche des equipes deja selectionnés
    function get_equipes_tournois($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetch();

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur['idEquipe'];
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne le pseudo du gestionnaire a partir de son idea
    function get_pseudo_idGestionnaire($idGestionnaire){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE idUser = '{$idGestionnaire}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur[0][0]['pseudo'];
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les rencontres qui sont terminés et dont les scores ne sont pas encore mis
    function get_rencontre_for_score($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE score1 IS NULL AND score2 IS NULL AND idTournois = '{$idTournois}' AND CONCAT(dateRencontre,' ',heureRencontre) <= NOW() ");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les rencontres d une equipe en cours
    function get_rencontre_en_cours($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND CONCAT(dateRencontre,' ',heureRencontre) > NOW() AND (idEquipe1 = '{$idEquipe}' OR idEquipe2 = '{$idEquipe}')");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les rencontres qu on a le droit de modifier
    function get_modif_rencontre($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND CONCAT(dateRencontre,' ',heureRencontre) > NOW() ");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les rencontres ou les scores peuvent etre modifiés
    function get_rencontre_modif_scores($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND score1 IS NOT NULL AND score2 IS NOT NULL AND ADDDATE(dateRencontre, INTERVAL 2 DAY) > NOW()");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// donne les rencontres terminees
    function get_rencontre_terminees_gestion($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' AND score1 IS NOT NULL AND score2 IS NOT NULL AND dateRencontre <= NOW()");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }
 ?>
