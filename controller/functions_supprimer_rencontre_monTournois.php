<?php
    // renvoie le nom de l equipe
    function get_nom_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur[0][0]['nom_equipe'];
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    //renvoie les infos sur la rencontre
    function get_info_rencontre($idMatch){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idMatch = '{$idMatch}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // renvoie les infos sur la competition
    function get_info_compet($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    //supprime la rencontre
    function sup_rencontre($idMatch){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //voir si les noms sont les bons
          //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
          $requete = $db->exec("DELETE FROM tournois.rencontre WHERE idMatch = '{$idMatch}' LIMIT 1");

          // on verifie si la ligne a ete effacée
          if($requete == 1){
            return true;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    if(isset($_POST['supprimer'])){

      if ($_POST['suppression'] == "oui") {
        $idMatch = $_POST['idMatch'];
        $conf = sup_rencontre($idMatch);
        if($conf){
          $_SESSION["pseudo"] = $_POST['pseudo'];
          header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
          exit();
        }
        else {
          session_start();
          $_SESSION["pseudo"] = $_POST['pseudo'];
          $_SESSION['id'] = $_POST['idMatch'];
          header("Location:../vue/supprimer_rencontre_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
          exit();
        }
      }
      else {
        session_start();
        $_SESSION["pseudo"] = $_POST['pseudo'];
        header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
        exit();
      }

    }


 ?>
