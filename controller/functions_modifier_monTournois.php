<?php
// libere les equipes inscripte du au changement du esport
    function liberation_inscription($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("UPDATE tournois.tournois SET idEquipe = NULL WHERE idTournois = '{$idTournois}' LIMIT 1");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          return true;
        }
        return false;

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }


//libere les equipes de la preselection
    function liberation_equipe($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("UPDATE tournois.equipe SET idTournois = NULL WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          return true;
        }
        return false;

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne toutes les informations d un tournois
    function get_info_tournois($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// modifie le Tournois
    function modification_tournois($idTournois, $dateD, $duree, $lieu, $type, $nom_tournois, $nbr_equipes, $Esport, $resultats_aleatoires, $equipes_aleatoires){
          $user = 'root';
          $pass = '';
          $connexion = 'mysql:host=localhost;dbname=tournois';
          $db = new PDO($connexion,$user,$pass);

        try {
            //creation de la requete
            //modification des valeurs dans la BDD
            $requete = $db->prepare("UPDATE tournois.tournois SET dateDebut=:dateDebut, duree=:duree, lieu=:lieu, type=:type, nom_tournois=:nom_tournois, nbr_equipes=:nbr_equipes, Esport=:Esport, resultats_aleatoires=:resultats_aleatoires, equipes_aleatoires=:equipes_aleatoires  WHERE idTournois = '{$idTournois}'");

            $requete->bindValue(':dateDebut', $dateD, PDO::PARAM_STR);
            $requete->bindValue(':duree', $duree, PDO::PARAM_INT);
            $requete->bindValue(':lieu', $lieu, PDO::PARAM_STR);
            $requete->bindValue(':type', $type, PDO::PARAM_STR);
            $requete->bindValue(':nom_tournois', $nom_tournois, PDO::PARAM_STR);
            $requete->bindValue(':nbr_equipes', $nbr_equipes, PDO::PARAM_INT);
            $requete->bindValue(':Esport', $Esport, PDO::PARAM_STR);
            $requete->bindValue(':resultats_aleatoires', $resultats_aleatoires, PDO::PARAM_BOOL);
            $requete->bindValue(':equipes_aleatoires', $equipes_aleatoires, PDO::PARAM_BOOL);

            //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
                return true;
            }
            else {
              return false;
            }

        } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die;
        }
    }
    if (!isset($_POST['modifier'])) {
      $infoTournois = get_info_tournois($idTournois);
      $dateD = $infoTournois[0][0]['dateDebut'];
      $duree = $infoTournois[0][0]['duree'];
      $lieu = $infoTournois[0][0]['lieu'];
      $type = $infoTournois[0][0]['type'];
      $nom_tournois = $infoTournois[0][0]['nom_tournois'];
      $nbr_equipes = $infoTournois[0][0]['nbr_equipes'];
      $Esport = $infoTournois[0][0]['Esport'];
      $resultats_aleatoires = $infoTournois[0][0]['resultats_aleatoires'];
      $equipes_aleatoires = $infoTournois[0][0]['equipes_aleatoires'];
    }

  if(isset($_POST['modifier'])){

      $idTournois = $_POST['idTournois'];
      $newdateD = $_POST['dateDebut'];
      $newduree = $_POST['duree'];
      $newlieu = $_POST['lieu'];
      $newtype = $_POST['Competition'];
      $newnom_tournois = $_POST['Nom_Tournois'];
      $newnbr_equipes = $_POST['Nbr_equipes'];
      $newEsport = $_POST['Esport'];

      $infoTournois = get_info_tournois($idTournois);
      $dateD = $infoTournois[0][0]['dateDebut'];
      $duree = $infoTournois[0][0]['duree'];
      $lieu = $infoTournois[0][0]['lieu'];
      $type = $infoTournois[0][0]['type'];
      $nom_tournois = $infoTournois[0][0]['nom_tournois'];
      $nbr_equipes = $infoTournois[0][0]['nbr_equipes'];
      $Esport = $infoTournois[0][0]['Esport'];
      $resultats_aleatoires = $infoTournois[0][0]['resultats_aleatoires'];
      $equipes_aleatoires = $infoTournois[0][0]['equipes_aleatoires'];

      session_start();

      if ($Esport != $newEsport) {
        echo "diff_esport";
        $send = liberation_equipe($idTournois);
        $send2 = liberation_inscription($idTournois);
        if (!$send || !$send2) {
          $_SESSION["pseudo"] = $_POST['pseudo'];
          header("Location:../vue/modifier_monTournois.php?pseudo=".$_SESSION["pseudo"]);
          exit();
        }
      }

      if($_POST['resultat_aleatoire'] == "oui"){
        $newresultats = true;
      }
      else {
        $newresultats = false;
      }

      if($_POST['equipe_aleatoire'] == "oui"){
        $newequipes = true;
      }
      else {
        $newequipes = false;
      }

      // var_dump($_POST);
      // echo $newresultats;
      echo "modif";

      $modification = modification_tournois($idTournois, $newdateD, $newduree, $newlieu, $newtype, $newnom_tournois, $newnbr_equipes, $newEsport, $newresultats, $newequipes);
      if($modification == true){
        $_SESSION["pseudo"] = $_POST['pseudo'];
        header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
        exit();
      }
      else {
        $_SESSION["pseudo"] = $_POST['pseudo'];
        header("Location:../vue/modifier_monTournois.php?pseudo=".$_SESSION["pseudo"]);
        exit();
      }
    }
 ?>
