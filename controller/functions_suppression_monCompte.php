<?php
    session_start();

    function suppression_utilisateur($idUser){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
        $requete = $db->exec("DELETE FROM tournois.utilisateur WHERE idUser = '{$idUser}' LIMIT 1");

        if($requete == 1){
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }

    }

    function get_id($pseudo){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
            $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

        //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = $requete->fetch();

              return $utilisateur['idUser'];
            }
            else {
              print "Erreur de l excution de la requete";
            }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
        }
    }


    $pseudo = $_GET["pseudo"];
    $idUser = get_id($pseudo);
    suppression_utilisateur($idUser);
 ?>

 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title>Suppression compte</title>
     <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
     <link rel="stylesheet" href="../assets/mainCSS.css" />
     <link rel="stylesheet" href="../assets/suppression.css" />
   </head>
   <body>
     <div class="BigDiv">
       <h2>A bientôt <?php echo $pseudo; ?></h2>
       <h2>Votre compte a été supprimé avec succès.</h2>
     </div>
   </body>

   <?php
   include '../vue/sidebar.php';
   include '../vue/header.php';
   $_SESSION = array();
   session_destroy();
   header("Refresh:5;url=http://localhost/projet/index.php");
   exit;
    ?>
 </html>
