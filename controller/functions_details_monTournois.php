<?php
// verifie si toutes les equipes ont leurs scores
    function championnat_scores_et_termines($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          $equipes = array();
          $det = $utilisateur[0][0]['idEquipe'];
          $longueur = strlen($utilisateur[0][0]['idEquipe']);
          $idrecup = "";
          $id = 0;

          // recuperation sous forme de tableau des equipes
          for ($i=0; $i < $longueur; $i++) {
            // var_dump($det[$i]);
            if ($det[$i] == ",") {
              $equipes[0][$id] = $idrecup;
              $idrecup = "";
              $id++;
            }
            else {
              $idrecup = $idrecup.$det[$i];
            }
          }

          // on regarde si elles se sont deja affrontées 2 fois
          $size = count($equipes[0]);
          $deja_affronte = 0;
          for ($i=0; $i < $size; $i++) {
            for ($j=0; $j < $size; $j++) {
              if($i != $j){
                // echo "comparaison".$i."-".$j;
                // echo "equipes".$equipes[0][$i]."-".$equipes[0][$j];
                // echo "<br>";
                $bool = championnat_a_score($idTournois, $equipes[0][$i], $equipes[0][$j]);
                if ($bool) {
                  $deja_affronte++;
                }
              }
            }
          }

          // nombre_equipe_total * (nombre_equipe_total - 1)
          $nbre_affrontement_total = $size * ($size - 1);
          // var_dump($deja_affronte);

          if ($deja_affronte < $nbre_affrontement_total) {
            return false;
          }

          return true;

        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// renvoie les rencontres du tournoi
    function get_rencontre_tournois($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {

        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' ORDER BY dateRencontre");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          return $utilisateur;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }


    // recupere les equipes du tournois
        function recup_idEquipe($idTournois, $rang){
          $user = 'root';
          $pass = '';
          $connexion = 'mysql:host=localhost;dbname=tournois';
          $db = new PDO($connexion,$user,$pass);

          try {
            //creation de la requete
            //voir si les noms sont les bons
            $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

            //executer la requete et renvoie un booleen
            $execution_requete = $requete->execute();

            if($execution_requete){
              $tournois = array($requete->fetchAll());

              $equipes = array($tournois[0][0]['idEquipe']);
              $longueur = strlen($equipes[0]);
              $idrecup = "";
              $nbr = 0;

              for ($i=0; $i < $longueur; $i++) {
                if (($equipes[0][$i] == ",") && ($rang == $nbr)) {
                  return $idrecup;
                }
                if ($equipes[0][$i] == ",") {
                  $idrecup = "";
                  $nbr++;
                }
                else {
                  $idrecup = $idrecup.$equipes[0][$i];
                }
              }
              return null;

            }
            else {
              print "Erreur de l excution de la requete";
            }

          } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die;
          }
        }


// ajout des donnees pour la rencontre
    function ajout_donnees_rencontre($idTournois, $dateRencontre, $heureRencontre, $id1, $id2){
      $user = 'root';
      $pass = '';
      $serveur = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($serveur,$user,$pass);

      try {
        // insertion
        //a modifier le nom de la table et les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare('INSERT INTO tournois.rencontre (idMatch, idTournois, dateRencontre, heureRencontre, idEquipe1, idEquipe2) VALUES (NULL, :idTournois, :dateRencontre, :heureRencontre, :idEquipe1, :idEquipe2)');

        //on lie les elemnents a une clef
        $requete->bindValue(':idTournois', $idTournois, PDO::PARAM_INT);
        $requete->bindValue(':dateRencontre', $dateRencontre, PDO::PARAM_STR);
        $requete->bindValue(':heureRencontre', $heureRencontre, PDO::PARAM_STR);
        $requete->bindValue(':idEquipe1', $id1, PDO::PARAM_INT);
        $requete->bindValue(':idEquipe2', $id2, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// renvoie le classement du championnat
    function classement_championnat($all_equipes, $gagnant){
      $classement = array();
      $classement[0][0]['Equipe'] = $gagnant[0][0]['Equipe'];
      $classement[0][0]['pts'] = $gagnant[0][0]['pts'];
      $max = $gagnant[0][0]['pts'];
      $size = count($all_equipes[0]);
      for ($i=0; $i < $size; $i++) {
        for ($j=0; $j < $size; $j++) {
          if ($all_equipes[0][$i]['pts'] > $all_equipes[0][$j]['pts'] && $i != $j) {
            $temp[0][0]['Equipe'] = $all_equipes[0][$i]['Equipe'];
            $temp[0][0]['pts'] = $all_equipes[0][$i]['pts'];

            $all_equipes[0][$i]['pts'] = $all_equipes[0][$j]['pts'];
            $all_equipes[0][$i]['Equipe'] = $all_equipes[0][$i]['Equipe'];

            $all_equipes[0][$j]['pts'] = $temp[0][0]['pts'];
            $all_equipes[0][$j]['Equipe'] = $temp[0][0]['Equipe'];
          }
        }
      }

      for ($i=1; $i < $size; $i++) {
        $classement[0][$i]['Equipe'] = $all_equipes[0][$i]['Equipe'];
        $classement[0][$i]['pts'] = $all_equipes[0][$i]['pts'];
      }

      return $classement;
    }

    // renvoie le classement du championnat avec des egalites
    function classement_championnat_egalite($all_equipes, $gagnant){
        $classement = array();
        $classement[0][0]['Equipe'] = $gagnant[0][0]['Equipe'];
        $classement[0][0]['pts'] = $gagnant[0][0]['pts'];
        $max = $gagnant[0][0]['pts'];
        $size = count($all_equipes[0]);

        for ($i=0; $i < $size; $i++) {
          for ($j=0; $j < $size; $j++) {
            if ($i == 0 && $classement[0][0]['Equipe'] == $all_equipes[0][$j]['Equipe']) {
              $temp[0][0]['Equipe'] = $all_equipes[0][$i]['Equipe'];
              $temp[0][0]['pts'] = $all_equipes[0][$i]['pts'];

              $all_equipes[0][$i]['pts'] = $all_equipes[0][$j]['pts'];
              $all_equipes[0][$i]['Equipe'] = $all_equipes[0][$j]['Equipe'];

              $all_equipes[0][$j]['pts'] = $temp[0][0]['pts'];
              $all_equipes[0][$j]['Equipe'] = $temp[0][0]['Equipe'];
            }
            else {
              if ($all_equipes[0][$i]['pts'] < $all_equipes[0][$j]['pts'] && $i < $j) {
                $temp[0][0]['Equipe'] = $all_equipes[0][$i]['Equipe'];
                $temp[0][0]['pts'] = $all_equipes[0][$i]['pts'];

                $all_equipes[0][$i]['pts'] = $all_equipes[0][$j]['pts'];
                $all_equipes[0][$i]['Equipe'] = $all_equipes[0][$j]['Equipe'];

                $all_equipes[0][$j]['pts'] = $temp[0][0]['pts'];
                $all_equipes[0][$j]['Equipe'] = $temp[0][0]['Equipe'];
              }
            }
          }
        }

        for ($i=1; $i < $size; $i++) {
          $classement[0][$i]['Equipe'] = $all_equipes[0][$i]['Equipe'];
          $classement[0][$i]['pts'] = $all_equipes[0][$i]['pts'];
        }

        return $classement;

    }

    // donne le pseudo du capitaine
    function get_pseudo_capitaine($idJoueur){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE idJoueur = '{$idJoueur}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur[0][0]['pseudo'];
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne le pseudo du gestionnaire
    function getPseudoGestionnaire($idGestionnaire){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE idUser = '{$idGestionnaire}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur[0][0]['pseudo'];
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les joueurs d une equipe
    function get_joueurs_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE idEquipeJ = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    //donne les equipes d un tournois
    function get_all_equipes($idEquipes){
      $size = strlen($idEquipes);
      $tabEquipes = array();
      $id = 0;
      $recup = "";

        for ($i=0; $i < $size; $i++) {
          if($idEquipes[$i] == ","){
            $tabEquipes[$id] = $recup;
            $id++;
            $recup = "";
          }
          else {
            $recup = $recup.$idEquipes[$i];
          }
        }

        return $tabEquipes;
    }

    // donne les infos d une equipe
    function get_info_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les infos sur la competition
    function get_info_competition($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne toutes les rencontres de cette competition ordonnée par date
    function get_all_rencontre($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idTournois = '{$idTournois}' ORDER BY dateRencontre ASC");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $tournoi = array($requete->fetchAll());

          if(empty($tournoi[0])){
            return false;
          }
          else {
            return $tournoi;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }


 ?>
