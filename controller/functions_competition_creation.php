<?php

    // envoie les infos sur la competition
    function send_info_compet($dateD, $duree, $lieu, $type, $nom_tournois, $idGestionnaire, $nbreEquipe, $Esport, $resultat, $equipe){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        // insertion
        //a modifier les noms des cles et ajouter pseudo a la BDD
        $requete = $db->prepare('INSERT INTO tournois.tournois (idTournois, dateDebut, duree, idGestionnaire, lieu, type, nom_tournois, nbr_equipes, Esport, resultats_aleatoires, equipes_aleatoires) VALUES (NULL, :dateDebut, :duree, :idGestionnaire, :lieu, :type, :nom_tournois, :nbr_equipes, :Esport, :resultats_aleatoires, :equipes_aleatoires)');

        //on lie les elemnents a une clef
        $requete->bindValue(':dateDebut', $dateD, PDO::PARAM_STR);
        $requete->bindValue(':duree', $duree, PDO::PARAM_INT);
        $requete->bindValue(':idGestionnaire', $idGestionnaire, PDO::PARAM_INT);
        $requete->bindValue(':lieu', $lieu, PDO::PARAM_STR);
        $requete->bindValue(':type', $type, PDO::PARAM_STR);
        $requete->bindValue(':nom_tournois', $nom_tournois, PDO::PARAM_STR);
        $requete->bindValue(':nbr_equipes', $nbreEquipe, PDO::PARAM_INT);
        $requete->bindValue(':Esport', $Esport, PDO::PARAM_STR);
        $requete->bindValue(':resultats_aleatoires', $resultat, PDO::PARAM_INT);
        $requete->bindValue(':equipes_aleatoires', $equipe, PDO::PARAM_INT);

        $correct_insertion = $requete->execute();

        if($correct_insertion){
          return true;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;

      }

    }

    function Nom_Competition_utilise($nom_tournois, $dateD, $duree){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
            $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE nom_tournois = '{$nom_tournois}' AND (dateDebut + duree) < '{$dateD}'");

        //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = $requete->fetch();
              if($utilisateur == null){
                return false;
              }
              else {
                return true;
              }
            }
            else {
              return false;
            }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
        }
    }

    function get_idUser($pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetch();
          return $utilisateur['idUser'];
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    $pseudo = $_POST["Pseudo"];
    $dateD = $_POST["Debut"];
    $duree = $_POST["Duree"];
    $lieu = $_POST["Lieu"];
    $type = $_POST["Competition"];
    $nom_tournois = $_POST["Nom_Competition"];

    $nbreEquipe = $_POST["NbrEquipe"];
    $Esport = $_POST["Esport"];
    $resultatR = $_POST["resultat_aleatoire"];
    $equipeR = $_POST["equipe_aleatoire"];

    if ($resultatR == "oui") {
      $resultat = true;
    }
    else {
      $resultat = false;
    }

    if ($equipeR == "oui") {
      $equipe = true;
    }
    else {
      $equipe = false;
    }

    $idGestionnaire = get_idUser($pseudo);

    $nom_tournois_utilise = Nom_Competition_utilise($nom_tournois, $dateD, $duree);
    session_start();

    if($nom_tournois_utilise){
      header("Location:../vue/creation_competition.php?pseudo=".$_SESSION["pseudo"]);
    }
    else {

      send_info_compet($dateD, $duree, $lieu, $type, $nom_tournois, $idGestionnaire, $nbreEquipe, $Esport, $resultat, $equipe);

      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }

 ?>
