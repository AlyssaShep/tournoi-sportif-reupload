<?php
    include_once('../controller/function_calendar.php');
    
    $todayDate = date("Y-m-d");
    $tournois_en_cours = tournois_en_cours($todayDate);
    $tableau_full_calendar = array();
    
    foreach ($tournois_en_cours[0] as $id => $tournoi)
    {
        $tab['title'] = $tournoi['nom_tournois'];
        $tab['start'] = $tournoi['dateDebut'];
        $tab['backgroundColor']='#e73434';
        $tab['eventBackgroundColor'] = '#e73434';
        
        
        $dateFin =date('Y-m-d', strtotime($tournoi['dateDebut'].'+'.$tournoi['duree'].'days'));
        
        $tab['end'] = $dateFin; //ou un calcul sur la date de fin à partir de la date debut
        $tableau_full_calendar[] = $tab;
    }


    $tournois_termines = tournois_termines();
    $tableau_termines = array();
    foreach ($tournois_termines[0] as $id => $tournoi)
    {
        $tab['title'] = $tournoi['nom_tournois'];
        $tab['start'] = $tournoi['dateDebut'];
        $tab['backgroundColor']=' #EE9C9C';
        $tab['eventBackgroundColor'] = '#EE9C9C';


        
        $dateFin =date('Y-m-d', strtotime($tournoi['dateDebut'].'+'.$tournoi['duree'].'days'));
        
        $tab['end'] = $dateFin; //ou un calcul sur la date de fin à partir de la date debut
        $tableau_full_calendar[] = $tab;
    }


    $tournois_venir = tournois_a_venir();
    $tableau_venir = array();
    foreach ($tournois_venir[0] as $id => $tournoi)
    {
        $tab['title'] = $tournoi['nom_tournois'];
        $tab['start'] = $tournoi['dateDebut'];
        $tab['backgroundColor']='#880000';
        $tab['eventBackgroundColor'] ='#880000';
        
        
        $dateFin =date('Y-m-d', strtotime($tournoi['dateDebut'].'+'.$tournoi['duree'].'days'));
        
        $tab['end'] = $dateFin; //ou un calcul sur la date de fin à partir de la date debut
        $tableau_full_calendar[] = $tab;
    }

       
    
   echo json_encode($tableau_full_calendar);
    





?>