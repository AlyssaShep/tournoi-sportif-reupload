<?php
// recupere les equipes du joueur ou il n est pas capitaine
    function get_equipe_is_joueur($idJoueur){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        // $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE joueur.idJoueur = '{$idJoueur}' AND joueur.idEquipeJ NOT IN (SELECT idEquipe FROM tournois.equipe WHERE equipe.idCapitaine = '{$idJoueur}') ");
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE joueur.idJoueur = '{$idJoueur}' AND joueur.idEquipeJ NOT IN (SELECT idEquipe FROM tournois.equipe WHERE equipe.idCapitaine = '{$idJoueur}') ");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

//voir si le nom de l equipe est deja pris
    function nomequipe_pris($nom_equipe, $idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
            $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE nom_equipe = '{$nom_equipe}' AND idEquipe != '{$idEquipe}'");

        //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
              $utilisateur = $requete->fetch();
              if($utilisateur == null){
                return false;
              }
              else {
                return true;
              }
            }
            else {
              print "Erreur de l excution de la requete";
            }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
        }
    }

// met a jour la bdd avec les nouvelles donnees
    function maj_equipe($idEquipe, $nom, $email, $tel){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.equipe SET nom_equipe=:nom_equipe, equipe_tel=:equipe_tel, adresse_equipe=:adresse_equipe WHERE idEquipe = '{$idEquipe}'");

          $requete->bindValue(':nom_equipe', $nom, PDO::PARAM_STR);
          $requete->bindValue(':equipe_tel', $tel, PDO::PARAM_INT);
          $requete->bindValue(':adresse_equipe', $email, PDO::PARAM_STR);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              return true;
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

// retourne les infos sur une equipe
    function get_info_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne les equipes d un utilisateur
    function get_equipe($idJoueur){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idCapitaine = '{$idJoueur}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne les infos sur le joueur
    function get_info_joueur($pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE pseudo = '{$pseudo}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne les joueurs d une equipe
    function get_joueurs_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE idEquipeJ = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    if (isset($_POST['Modifier'])) {
  $pseudo = $_POST['pseudo'];
  $idEquipe = $_POST['idEquipe'];
  $email = $_POST['mail'];
  $tel = $_POST['telephone'];
  $nom = $_POST['nom'];
  $verif = nomequipe_pris($nom, $idEquipe);

  session_start();

  if ($verif) {
    $_SESSION['pseudo'] = $pseudo;
    $_SESSION['id'] = $idEquipe;
    header("Location:../vue/voir_monEquipe.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
    exit();
  }
  else {
    $send = maj_equipe($idEquipe, $nom, $email, $tel);
    if ($send) {
      $_SESSION['pseudo'] = $pseudo;
      header("Location:../vue/mesEquipes.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }
    else {
      $_SESSION['pseudo'] = $pseudo;
      $_SESSION['id'] = $idEquipe;
      header("Location:../vue/voir_monEquipe.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
      exit();
    }
  }
}
 ?>
