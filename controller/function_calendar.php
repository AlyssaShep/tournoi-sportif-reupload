<?php 
// cherche tous les tournois en cours

 function tournois_en_cours($todayDate){
    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      //creation de la requete
      //voir si les noms sont les bons
      $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE dateDebut <= '{$todayDate}' AND ADDDATE(dateDebut, INTERVAL duree DAY) >= '{$todayDate}'");

      //executer la requete et renvoie un booleen
      $execution_requete = $requete->execute();

      if($execution_requete){
        $utilisateur = array($requete->fetchAll());
        return $utilisateur;
      }
      else {
        print "Erreur de l excution de la requete";
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;
    }
  }

  // recherche les tournois terminés
  function tournois_termines(){
    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      //creation de la requete
      //voir si les noms sont les bons
      $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE ADDDATE(dateDebut, INTERVAL duree DAY) < NOW()");

      //executer la requete et renvoie un booleen
      $execution_requete = $requete->execute();

      if($execution_requete){
        $utilisateur = array($requete->fetchAll());
        return $utilisateur;
      }
      else {
        print "Erreur de l excution de la requete";
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;
    }
  }

  // recherche les tournois a venir
  function tournois_a_venir(){
    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      //creation de la requete
      //voir si les noms sont les bons
      $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE dateDebut > NOW()");

      //executer la requete et renvoie un booleen
      $execution_requete = $requete->execute();

      if($execution_requete){
        $utilisateur = array($requete->fetchAll());
        return $utilisateur;
      }
      else {
        print "Erreur de l excution de la requete";
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;
    }
  }

  ?>