<?php
// retourne le esport de la competition
    function Esport($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur[0][0]['Esport'];
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// cherche des equipes de facon aleatoire
    function equipes_aleatoire($idTournois, $places, $Esport){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        $requete1 = $db->prepare("SELECT * FROM tournois.equipe WHERE idTournois = '{$idTournois}'");
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        $execution_requete = $requete->execute();

        $execution_requete1 = $requete1->execute();

        if ($execution_requete1 && $execution_requete) {
          $equipes = array($requete1->fetchAll());
          $tournois = array($requete->fetchAll());

          // var_dump($equipes);

          if (empty($equipes[0])) {

              if ($tournois[0][0]['equipes_aleatoires']) {

                for ($i=0; $i < $places; $i++) {
                  $requete = $db->prepare("UPDATE tournois.equipe SET idTournois = '{$idTournois}' WHERE idTournois IS NULL AND Esport = '{$Esport}' LIMIT 1");
                  $execution_requete = $requete->execute();
                  if (!$execution_requete) {
                    return false;
                  }
                }
              }

          }
          else {
            $nbr = count($equipes[0]);
            $nbr_total = $tournois[0][0]['nbr_equipes'];
            // var_dump($nbr);
            // var_dump($nbr_total);
            // var_dump($preinscripts);

            if ($nbr < $nbr_total) {
              $a_faire = $nbr_total - $nbr;
              if ($tournois[0][0]['equipes_aleatoires']) {

                for ($i=0; $i < $a_faire; $i++) {
                  $requete = $db->prepare("UPDATE tournois.equipe SET idTournois = '{$idTournois}' WHERE idTournois IS NULL AND Esport = '{$Esport}' LIMIT 1");
                  $execution_requete = $requete->execute();
                  if (!$execution_requete) {
                    return false;
                  }
                }
              }
            }
          }
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

//libere les equipes de la preselection
    function liberation_equipe($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("UPDATE tournois.equipe SET idTournois = NULL WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          return true;
        }
        return false;

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// retourne toutes les equipes s etant preinscripte aux tournois
    function preinscription_tournois($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          return false;
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// calcule le nombre de places restantes
    function nbres_places_restantes($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $tournois = array($requete->fetchAll());
          $equipes = array($tournois[0][0]['idEquipe']);
          $longueur = strlen($equipes[0]);
          // var_dump($equipes[0][0]);
          // echo $longueur;
          $places_prises = 0;

          for ($i=0; $i < $longueur; $i++) {
            if ($equipes[0][$i] == ",") {
              $places_prises++;
            }
          }

          $places_restantes = $tournois[0][0]['nbr_equipes'] - $places_prises;
          // echo $places_prises;
          // echo " ".$places_restantes;

          return $places_restantes;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

//ajoute les equipes selectionnés
    function ajout_modification_tournois($idTournois, $idEquipe){
          $user = 'root';
          $pass = '';
          $connexion = 'mysql:host=localhost;dbname=tournois';
          $db = new PDO($connexion,$user,$pass);

        try {
            // verification si des equipes n ont pas deja été sélectionnées -> on prend les equipes deja prises et on cacatene avec $idEquipe
            $requete1 = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");
            $exec_requete = $requete1->execute();

            if($exec_requete){
              // si des equipes ont deja ete ajoutés au tournois
              $EquipeExistentes = $requete1->fetch();
              $idEquipes = $EquipeExistentes['idEquipe'].$idEquipe;

              $requete = $db->prepare("UPDATE tournois.tournois SET idEquipe=:idEquipe WHERE idTournois = '{$idTournois}'");

              $requete->bindValue(':idEquipe', $idEquipes, PDO::PARAM_STR);

              //executer la requete
              $execution_requete = $requete->execute();
              if($execution_requete){
                return true;
              }
              else {
                return false;
              }
            }
            else {

              try {

                $requete = $db->prepare("INSERT INTO tournois.tournois (idEquipe) VALUES (:idEquipe)");

                $requete->bindValue(':idEquipe', $idEquipe, PDO::PARAM_INT);

                $correct_insertion = $requete->execute();

                if($correct_insertion){
                  return true;
                }
                else {
                  return false;
                }

              } catch (PDOException $e) {
                print "Erreur : " . $e->getMessage() . "<br/>";
                die;
              }
            }
        } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die;
        }
    }

// donne les equipes deja inscripte
    function equipes_inscriptes($idTournois, $idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          $equipes = array($utilisateur[0][0]['idEquipe']);
          $longueur = strlen($equipes[0]);
          $idrecup = "";

          for ($i=0; $i < $longueur; $i++) {
            if ($equipes[0][$i] == ",") {
              if($idrecup == $idEquipe){
                return true;
              }
              $idrecup = "";
            }
            else {
              $idrecup = $idrecup.$equipes[0][$i];
            }
          }
          return false;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// prend les infos sur l equipe
    function info_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

// info sur le Capitaine
    function info_capitaine($idCapitaine){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE idJoueur = '{$idCapitaine}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());
          return $utilisateur;
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    if(isset($_POST['ajout'])){
      $pseudo = $_POST['pseudo'];
      $idTournois = $_POST['idTournois'];
      $nbr_equipes_total = $_POST['nbr_equipes_total'];
      $places_restantes = $_POST['places_restantes'];
      $idEquipeajout = "";
      $verif_nbr_equipes = 0;

      for ($i=0; $i < $nbr_equipes_total; $i++) {
        if (isset($_POST['Ajout'.$i])) {
          if ($_POST['Ajout'.$i] == "on") {
            $idEquipeajout = $idEquipeajout.$_POST['idEquipe'.$i].',';
            $verif_nbr_equipes++;
          }
        }
      }

      // echo $idEquipeajout;
      // var_dump($_POST);

      session_start();

      if ($verif_nbr_equipes > $places_restantes) {
        // renvoie sur la page d ajout des equipes si le nombre d equipes selectionnés est trop
        $_SESSION["pseudo"] = $pseudo;
        $_SESSION["id"] = $idTournois;
        header("Location:../vue/ajouter_equipes_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
        exit();
      }
      else {
        $verif = ajout_modification_tournois($idTournois, $idEquipeajout);
        if($verif){
          $_SESSION["pseudo"] = $pseudo;
          header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
          exit();
        }
        else {
          $_SESSION["pseudo"] = $pseudo;
          $_SESSION["id"] = $idTournois;
          header("Location:../vue/ajouter_equipes_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
          exit();
        }
      }

    }

 ?>
