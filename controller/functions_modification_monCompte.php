<?php
    $pseudo = $_POST["Pseudo"];

    include 'functions_monCompte.php';

    function suppression_utilisateur($pseudo){

      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        //LIMIT 1 permet de limiter la suppresion a 1 ligne (question de securite)
        $requete = $db->exec("DELETE FROM tournois.utilisateur WHERE idUser = '{$idUser}' LIMIT 1");

        if($requete == 1){
          echo "Compte supprimé";
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }

    }

    function modification_utilisateur($pseudo, $newPrenom, $newNom, $newPseudo, $newNum, $newMail){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.utilisateur SET mail=:mail, pseudo=:pseudo, nom=:nom, prenom=:prenom, num=:num WHERE pseudo = '{$pseudo}'");

          $requete->bindValue(':pseudo', $newPseudo, PDO::PARAM_STR);
          $requete->bindValue(':prenom', $newPrenom, PDO::PARAM_STR);
          $requete->bindValue(':nom', $newNom, PDO::PARAM_STR);
          $requete->bindValue(':num', $newNum, PDO::PARAM_INT);
          $requete->bindValue(':mail', $newMail, PDO::PARAM_STR);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              echo "Modification réussite";
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }

    }

    function verif_pseudo_utilise($pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT pseudo FROM tournois.utilisateur WHERE EXISTS (SELECT pseudo FROM tournois.utilisateur WHERE pseudo = '{$pseudo}')");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetchAll();

            //si le mail appartient a un utilisateur dire que c est impossible de creer un compte avec cette adresse
            if(count($utilisateur) == 0){
              // echo "Non utilisé";
              return false;
            }
            else {
              // echo "Pseudo déjà utilisé";
              return true;
            }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    function modification_role_isadmin($idRole, $idPersonne){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.role SET is_admin=:is_admin WHERE id_role = '{$idRole}'");

          $requete->bindValue(':is_admin', $idPersonne, PDO::PARAM_INT);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              echo "Modification réussite";
          }
          else {
            print "Erreur de l'excution de la requete / Modification";
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }

    }

    function modification_role_isgestionnaire($idRole, $idPersonne){
          $user = 'root';
          $pass = '';
          $connexion = 'mysql:host=localhost;dbname=tournois';
          $db = new PDO($connexion,$user,$pass);

        try {
            //creation de la requete
            //modification des valeurs dans la BDD
            $requete = $db->prepare("UPDATE tournois.role SET is_gestionnaire=:is_gestionnaire WHERE id_role = '{$idRole}'");

            $requete->bindValue(':is_gestionnaire', $idPersonne, PDO::PARAM_INT);

            //executer la requete
            $execution_requete = $requete->execute();

            if($execution_requete){
                echo "Modification réussite";
            }
            else {
              print "Erreur de l'excution de la requete / Modification";
            }

        } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die;
        }
    }

//renvoie l id_role en fonction du role
    function get_idrole_isgestionnaire($idUser){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          //creation de la requete
          //voir si les noms sont les bons
          $requete = $db->prepare("SELECT * FROM tournois.role WHERE is_gestionnaire = '{$idUser}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $utilisateur = $requete->fetch();

            return $utilisateur['id_role'];
          }
          else {
            print "Erreur de l excution de la requete";
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }
    }

    function get_idrole_isjoueur($idUser){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          //creation de la requete
          //voir si les noms sont les bons
          $requete = $db->prepare("SELECT * FROM tournois.role WHERE is_joueur = '{$idUser}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $utilisateur = $requete->fetch();

            return $utilisateur['id_role'];
          }
          else {
            print "Erreur de l excution de la requete";
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }
    }

    function get_idrole_iscapitaine($idUser){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          //creation de la requete
          //voir si les noms sont les bons
          $requete = $db->prepare("SELECT * FROM tournois.role WHERE is_capitaine = '{$idUser}'");

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
            $utilisateur = $requete->fetch();
            return $utilisateur['id_role'];

          }
          else {
            print "Erreur de l excution de la requete";
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }
    }

    function same_pseudo($pseudo, $newPseudo){
      if($pseudo == $newPseudo){
        return true;
      }
      return false;
    }

    $user = 'root';
    $pass = '';
    $connexion = 'mysql:host=localhost;dbname=tournois';
    $db = new PDO($connexion,$user,$pass);

    try {
      //creation de la requete
      //voir si les noms sont les bons
      $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

      //executer la requete et renvoie un booleen
      $execution_requete = $requete->execute();

      if($execution_requete){
        $utilisateur = $requete->fetch();
      }
      else {
        print "Erreur de l excution de la requete";
      }

    } catch (PDOException $e) {
      print "Erreur : " . $e->getMessage() . "<br/>";
      die;
    }


    $mdp = $_POST["Mdp"];
    $password = password_verify($mdp,$utilisateur['mdp']);

    $newPrenom = $_POST["newPrenom"];
    $newNom = $_POST["newNom"];
    $newPseudo = $_POST["newPseudo"];
    $newNum = $_POST["newNum"];
    $newMail = $_POST["newMail"];
    $devenirGestionnaire = false;
    $devenirAdmin = false;
    $samePseudo = same_pseudo($pseudo, $newPseudo);

    if($isJoueur && !($isgestionnaire || $isadmin)){
      if($_POST["devenir_gestionnaire"] == "oui"){
          $devenirGestionnaire = true;
      }

      $idRole = get_idrole_isjoueur($idUser);
    }

    if($isgestionnaire && !$isadmin){
      if($_POST["devenir_admin"] == "oui"){
          $devenirAdmin = true;
      }
      $idRole = get_idrole_isgestionnaire($idUser);
    }

    // echo "isJoueur = ";
    // echo $isJoueur;
    // echo " devenir_gestionnaire=";
    // echo $devenirGestionnaire;
    // echo " is_gestionnaire=";
    // echo $isgestionnaire;
    // echo " devenir_admin=";
    // echo $devenirAdmin;
    // echo " password=";
    // echo $password;
    $utilise = verif_pseudo_utilise($newPseudo);
    // echo " utilise=";
    // echo $utilise;

    session_start();

    if($utilise && !$samePseudo){
      $_SESSION["pseudo"] = $pseudo;
      // header("Location:../vue/monCompte.php?pseudo=".$_SESSION["pseudo"]);
    }
    else {
      if($password){
        modification_utilisateur($pseudo, $newPrenom, $newNom, $newPseudo, $newNum, $newMail);

        if($isJoueur && $devenirGestionnaire){

          modification_role_isgestionnaire($idRole, $idUser);
        }
        else {
            if($isgestionnaire && $devenirAdmin){
              echo "la";
              modification_role_isadmin($idRole, $idUser);
            }
          }

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$newPseudo}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetch();
        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }

      // setcookie('pseudo',$utilisateur['pseudo'], time() + 3600, '/', 'localhost', false, true);
      $_SESSION['pseudo'] = $utilisateur['pseudo'];
      $_SESSION['idUser'] = $utilisateur['idUser'];
      $_SESSION['time'] = time() + 3600;
      header("Location:../index.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }
    else {
      header("Location:../vue/monCompte.php?pseudo=".$_SESSION["pseudo"]);
    }
  }



 ?>
