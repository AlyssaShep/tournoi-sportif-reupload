<?php
session_start();

//envoie des informations
    function send_BDD_personnal_info($name, $lastname, $num, $email, $pseudo, $mdp){

      //connexion a la bdd Tournois
        $user = 'root';
        $pass = '';
        $serveur = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($serveur,$user,$pass);

        try {
          // insertion
          //a modifier le nom de la table et les noms des cles et ajouter pseudo a la BDD
          $requete = $db->prepare('INSERT INTO tournois.utilisateur VALUES (NULL, :mail, :mdp, :pseudo, :nom, :prenom, :num)');

          //on lie les elemnents a une clef
          $requete->bindValue(':mail', $email, PDO::PARAM_STR);
          $requete->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
          $requete->bindValue(':nom', $lastname, PDO::PARAM_STR);
          $requete->bindValue(':prenom', $name, PDO::PARAM_STR);
          $requete->bindValue(':num', $num, PDO::PARAM_INT);

          //hachage du mot de passe
          $password = password_hash($mdp,PASSWORD_DEFAULT);
          $requete->bindValue(':mdp', $password, PDO::PARAM_STR);

          $correct_insertion = $requete->execute();

          if($correct_insertion){
            return true;
          }
          else {
            return false;
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
        }

    }

//envoie le role de joueur a la personne
    function send_info_role_isjoueur($idJoueur){

        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

        try {
          // insertion
          //a modifier les noms des cles et ajouter pseudo a la BDD
          $requete = $db->prepare("INSERT INTO tournois.role (id_role, is_joueur) VALUES (NULL, :is_joueur)");

          // on lie les elemnents a une clef
          $requete->bindValue(':is_joueur', $idJoueur, PDO::PARAM_INT);

          $correct_insertion = $requete->execute();

          if($correct_insertion){
            return true;
          }
          else {
            return false;
          }

        } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;

        }
    }

//verification
    function verif_mdp_identique($mdp, $verif){

      if($mdp == $verif){
        return true;
      }

      return false;

    }

    function verif_pseudo_utilise($pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT pseudo FROM tournois.utilisateur WHERE EXISTS (SELECT pseudo FROM tournois.utilisateur WHERE pseudo = '{$pseudo}')");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetchAll();

            //si le mail appartient a un utilisateur dire que c est impossible de creer un compte avec cette adresse
            if(count($utilisateur) == 0){
              return false;
            }
            else {
              return true;
            }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    function get_idUser($pseudo){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.utilisateur WHERE pseudo = '{$pseudo}'");

        //executer la requete et renvoie un booleen
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = $requete->fetch();
          return $utilisateur['idUser'];

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    $lastname = $_POST["Nom"];
    $name = $_POST["Prenom"];
    $num = $_POST["Numero"];
    $email = $_POST["Email"];
    $pseudo = $_POST["Pseudo"];
    $mdp = $_POST["Mdp"];
    $verif = $_POST["Confirmation"];

    $verification = verif_mdp_identique($mdp, $verif);
    $pseudoVerif = verif_pseudo_utilise($pseudo);


//creation page de bienvenue?
//redirection? dans l url donnee cachee ?
    if($verification && (!$pseudoVerif) && (!empty($_POST))){

      $send = send_BDD_personnal_info($name, $lastname, $num, $email, $pseudo, $mdp);
      $idJoueur = get_idUser($pseudo);
      $sendRole = send_info_role_isjoueur($idJoueur);
      if($send){
          $_SESSION["pseudo"] = $pseudo;
          header("Location:../index.php?pseudo=".$_SESSION["pseudo"]);

        // echo "<meta http-equiv='refresh' content='0; url=../index.php?'>";
        exit();
      }
      else {
        header("Location:../vue/inscription.php");
        // echo "<meta http-equiv='refresh' content='0; url=../vue/inscription.php?'>";
      }
    }
    else {
      // echo "Pseudo deja utilisé ou mot de passe incorrect";
      header("Location:../vue/inscription.php");
      // echo "<meta http-equiv='refresh' content='0; url=../vue/inscription.php?m'>";
    }
 ?>
