<?php
// donne des valeurs aleatoires en fonction de l esport
    function val_aleatoire($Esport){
      if ($Esport == "LOL") {
        $scores = array();
        for ($i=0; $i < 2; $i++) {
          $scores[0][$i] = $i;
        }
        return array_rand($scores[0],2);
      }
      elseif ($Esport == "SB") {
        $scores = array();
        for ($i=0; $i < 4; $i++) {
          $scores[0][$i] = $i;
        }
        return array_rand($scores[0],2);
      }
      else {
        $scores = array();
        for ($i=0; $i < 17; $i++) {
          $scores[0][$i] = $i;
        }
        return array_rand($scores[0],2);
      }
    }

// verifie si les scores ont deja ete mis
    function resultats_deja_ajoutes($idMatch){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

    try {
        //creation de la requete
        //modification des valeurs dans la BDD
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idMatch = '{$idMatch}' AND score1 IS NOT NULL AND score2 IS NOT NULL");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $rencontre = array($requete->fetchAll());
          // var_dump($rencontre);
          if (empty($rencontre[0])) {
            return false;
          }
            return true;
        }
        else {
          return false;
        }

    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
    }
    }

    // ajouter les resultats a la rencontre et mets l id du vainqueur
    function ajout_resultats($idMatch, $score1, $score2, $idGagnant){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.rencontre SET score1=:score1, score2=:score2, idGagnant=:idGagnant WHERE idMatch = '{$idMatch}'");

          $requete->bindValue(':score1', $score1, PDO::PARAM_INT);
          $requete->bindValue(':score2', $score2, PDO::PARAM_INT);
          $requete->bindValue(':idGagnant', $idGagnant, PDO::PARAM_INT);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              return true;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    // ajout la victoire a l equipe
    function ajout_victoire_equipe($idEquipe){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.equipe SET nb_victoire = nb_victoire + 1 WHERE idEquipe = '{$idEquipe}'");
          $requete1 = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

          //executer la requete
          $execution_requete = $requete->execute();
          $execution_requete1 = $requete1->execute();

          if ($execution_requete1) {
            $equipe = array($requete1->fetchAll());
            if ($equipe[0][0]['niveau'] < 100) {
              $requete2 = $db->prepare("UPDATE tournois.equipe SET niveau = niveau + 1 WHERE idEquipe = '{$idEquipe}'");
              $execution_requete2 = $requete2->execute();
              if ($execution_requete2 && $execution_requete) {
                return true;
              }
              else {
                return false;
              }
            }
          }

          if($execution_requete){
              return true;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    // donne les differentes infos sur le match
    function get_info_rencontre($idMatch){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idMatch = '{$idMatch}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les infos sur les equipes qui ont participes
    function get_info_equipe($idEquipe){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass); //utiliser un singleton pour instancier la BDD

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // donne les infos sur la competition
    function get_info_competition($idTournois){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur;
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    // recupere le pseudo du capitaine
    function get_info_pseudo_capitaine($idCapitaine){
      $user = 'root';
      $pass = '';
      $connexion = 'mysql:host=localhost;dbname=tournois';
      $db = new PDO($connexion,$user,$pass);

      try {
        //creation de la requete
        //voir si les noms sont les bons
        $requete = $db->prepare("SELECT * FROM tournois.joueur WHERE idJoueur = '{$idCapitaine}'");

        //executer la requete
        $execution_requete = $requete->execute();

        if($execution_requete){
          $utilisateur = array($requete->fetchAll());

          if($utilisateur == null){
            return false;
          }
          else {
            return $utilisateur[0][0]['pseudo'];
          }

        }
        else {
          print "Erreur de l excution de la requete";
        }

      } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die;
      }
    }

    if (isset($_POST['Ajouter'])) {
      $pseudo = $_POST['pseudo'];
      $score1 = $_POST['score1'];
      $score2 = $_POST['score2'];
      $idMatch = $_POST['idMatch'];
      $idEquipe1 = $_POST['idEquipe1'];
      $idEquipe2 = $_POST['idEquipe2'];

      session_start();

      if($score1 < $score2){
        $idGagnant = $idEquipe2;
      }
      elseif ($score1 > $score2) {
        $idGagnant = $idEquipe1;
      }
      else {
        $_SESSION['pseudo'] = $pseudo;
        $_SESSION['id'] = $idMatch;
        header("Location:../vue/ajouter_resultats_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
        exit();
      }

      ajout_resultats($idMatch, $score1, $score2, $idGagnant);
      ajout_victoire_equipe($idGagnant);

      $_SESSION['pseudo'] = $pseudo;
      header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
      exit();
    }

 ?>
