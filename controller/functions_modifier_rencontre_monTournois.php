<?php
    //renvoie les infos sur la competition
    function get_info_competition($idTournois){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

          $execution_requete = $requete->execute();

          if($execution_requete){
            $rencontre = array($requete->fetchAll());
            return $rencontre;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    // renvoie les infos sur une equipe
    function get_info_equipe($idEquipe){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("SELECT * FROM tournois.equipe WHERE idEquipe = '{$idEquipe}'");

          $execution_requete = $requete->execute();

          if($execution_requete){
            $rencontre = array($requete->fetchAll());
            return $rencontre;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    // renvoie le nom du tournois
    function get_nom_tournois($idTournois){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("SELECT * FROM tournois.tournois WHERE idTournois = '{$idTournois}'");

          $execution_requete = $requete->execute();

          if($execution_requete){
            $rencontre = array($requete->fetchAll());
            return $rencontre[0][0]['nom_tournois'];
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    // donne les infos sur une rencontre
    function get_info_rencontre($idMatch){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("SELECT * FROM tournois.rencontre WHERE idMatch = '{$idMatch}'");

          $execution_requete = $requete->execute();

          if($execution_requete){
            $rencontre = array($requete->fetchAll());
            return $rencontre;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    //envoie les modifications
    function send_modification_rencontre($idMatch, $dateRencontre, $heureRencontre){
        $user = 'root';
        $pass = '';
        $connexion = 'mysql:host=localhost;dbname=tournois';
        $db = new PDO($connexion,$user,$pass);

      try {
          //creation de la requete
          //modification des valeurs dans la BDD
          $requete = $db->prepare("UPDATE tournois.rencontre SET dateRencontre=:dateRencontre, heureRencontre=:heureRencontre WHERE idMatch = '{$idMatch}'");

          $requete->bindValue(':dateRencontre', $dateRencontre, PDO::PARAM_STR);
          $requete->bindValue(':heureRencontre', $heureRencontre, PDO::PARAM_STR);

          //executer la requete
          $execution_requete = $requete->execute();

          if($execution_requete){
              return true;
          }
          else {
            return false;
          }

      } catch (PDOException $e) {
          print "Erreur : " . $e->getMessage() . "<br/>";
          die;
      }
    }

    //verification si la nouvelle date est dans la competition
    function get_verif_date($dateRencontre, $dateDebut, $duree){
      $dateFin = date("Y-m-d", strtotime($dateDebut."+".$duree."days"));
      if ($dateRencontre < $dateDebut || $dateRencontre > $dateFin) {
        return false;
      }
      return true;
    }

    if(isset($_POST['valider'])){
      $newDate = $_POST['newDate'];
      $newHeure = $_POST['newTime'];
      $pseudo = $_POST['pseudo'];
      $idMatch = $_POST['idMatch'];
      $idTournois = $_POST['idTournois'];
      $dateDebut = get_info_competition($idTournois)[0][0]['dateDebut'];
      $duree = get_info_competition($idTournois)[0][0]['duree'];
      $verif = get_verif_date($newDate, $dateDebut, $duree);
      // $dateFin = date("Y-m-d", strtotime($dateDebut."+".$duree."days"));
      // var_dump($dateFin);

      session_start();

      if ($verif) {
        $send = send_modification_rencontre($idMatch, $newDate, $newHeure);
        if ($send) {
          $_SESSION['pseudo'] = $pseudo;
          header("Location:../vue/mesTournois.php?pseudo=".$_SESSION["pseudo"]);
          exit();
        }
        else {
          $_SESSION['pseudo'] = $pseudo;
          $_SESSION['id'] = $idMatch;
          header("Location:../vue/modifier_rencontre_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
          exit();
        }
      }
      else {
        $_SESSION['pseudo'] = $pseudo;
        $_SESSION['id'] = $idMatch;
        header("Location:../vue/modifier_rencontre_monTournois.php?pseudo=".$_SESSION["pseudo"]."&id=".$_SESSION['id']);
        exit();
      }


    }

 ?>
