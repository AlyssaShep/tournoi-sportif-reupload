<?php

/**
 * une liste double chainée
 */

class element {
  private $val;
  private $suivant;
  private $precedant;

  // constructeur
  function __construct($val){
    $this->val = $val;
    $this->suivant = null;
    $this->precedant = null;
  }

  // setter
  function setVal($val){
    $this->val = $val;
  }

  function setSuivant($suivant){
    $this->suivant = $suivant;
  }

  function setPrecedant($precedant){
    $this->precedant = $precedant;
  }

  // getter
  function getVal(){
    return $this->$val;
  }

  function getSuivant(){
    return $this->suivant;
  }

  function getPrecedant(){
    return $this->precedant;
  }

}


/**
 * a la racine de l arbre, la racine contient un noeud
 * et aussi le vainqueur du championnat
 */
class arbreBin extends noeud{
  private $racine;

  function __construct($racine){
    $this->racine = $racine;
  }

  function __construct(){
    $this->racine = null;
  }

  function estVide(){
    if($$this->racine == null){
      return true;
    }
    return false;
  }

  function setRacine($racine){
    $$this->racine = $racine;
  }

  function getRacinte(){
    return $this->racine;
  }

//affichage infixe
  function affichageArbre(){
      if($this->racine == null){
        return;
      }
      $this->racine->affichageArbre();
      echo $this->racine->val;
      $this->racine->affichageArbre();
  }

  // affichage en largeur
  function ParcoursLargeur(){
    if ($this == null) {
      return ;
    }
    $li = new element($this->val);

    while ($li != null) {
      $p 
    }
  }
}

  /**
   * classe ayant les attributs des noeuds d un arbre binaire
   * aura pour valeur l id de l equipe gagnant en noeud et l emplacement de l equipe en feuille
   */
  class noeud{
    private $val;
    private $filsG;
    private $filsD;

    function __construct($val){
      $this->val = $val;
      $this->filsD = null;
      $this->filsG = null;
    }

    function __construct(){
      $this->val = 0;
      $this->filsD = null;
      $this->filsG = null;
    }

    function getVal(){
      return $this->val;
    }

    function getFilsG(){
      return $this->filsG;
    }

    function getFilsD(){
      return $this->filsD;
    }

    function setVal($newVal){
      $this->val = $newVal;
    }

    function setFilsG($newFilsG){
      $this->filsG = $newFilsG;
    }

    function setFilsD($newFilsD){
      $$this->filsD = $newFilsD;
    }

    function estFeuille(){
      if(($this->filsG == null) && ($this->filsD == null)){
        return true;
      }
      return false;
    }

    function estVideN(){
      if($this == null){
        return true;
      }
      return false;
    }

//a changer pour si l equipe n est pas dans l arbre
    function trouverEquipe($val){
      if($val == $this->val){
        return $this;
      }
      if($this->estVideN){
        return 0;
      }

      $this->filsG->trouverEquipe($val);
      $this->filsD->trouverEquipe($val);

    }

  }


 ?>
