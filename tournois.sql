-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 14, 2021 at 10:09 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tournois`
--

-- --------------------------------------------------------

--
-- Table structure for table `equipe`
--

CREATE TABLE `equipe` (
  `idEquipe` int(11) NOT NULL,
  `idTournois` int(11) DEFAULT NULL,
  `nom_equipe` varchar(64) NOT NULL,
  `niveau` int(11) NOT NULL,
  `adresse_equipe` varchar(128) NOT NULL,
  `equipe_tel` int(11) NOT NULL,
  `nb_joueur` int(11) NOT NULL,
  `nb_victoire` int(11) NOT NULL DEFAULT 0,
  `idCapitaine` int(11) NOT NULL DEFAULT 0,
  `Esport` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `equipe`
--

INSERT INTO `equipe` (`idEquipe`, `idTournois`, `nom_equipe`, `niveau`, `adresse_equipe`, `equipe_tel`, `nb_joueur`, `nb_victoire`, `idCapitaine`, `Esport`) VALUES
(121, 2, 'monEquipe', 33, 'test@test.fr', 123456789, 2, 0, 1, 'SB'),
(122, NULL, 'Aastox Team', 74, 'Aastox@Aastox.fr', 123456789, 5, 0, 3, 'LOL'),
(123, NULL, 'Team Anivia', 62, 'Anivia@Anivia.fr', 123456789, 5, 2, 8, 'LOL'),
(124, NULL, 'Team Azir', 69, 'Azir@Azir.fr', 123456789, 5, 4, 13, 'LOL'),
(125, NULL, 'Team Caitlyn', 86, 'Caitlyn@Caitlyn.fr', 123456789, 5, 6, 18, 'LOL'),
(126, NULL, 'Team Darius', 71, 'Darius@Darius.fr', 123456789, 5, 0, 23, 'LOL'),
(127, NULL, 'Team Elise', 12, 'Elise@Elise.fr', 123456789, 5, 0, 28, 'LOL'),
(128, NULL, 'Team Fizz', 72, 'Fizz@Fizz.fr', 123456789, 5, 0, 33, 'LOL'),
(129, NULL, 'Team Gragas', 72, 'Gragas@Gragas.fr', 123456789, 5, 0, 38, 'LOL'),
(130, NULL, 'Team Irelia', 72, 'Irelia@Irelia.fr', 123456789, 5, 0, 43, 'LOL'),
(131, NULL, 'Team Jayce', 72, 'Jayce@Jayce.fr', 123456789, 5, 0, 48, 'LOL'),
(132, NULL, 'Team Karma', 72, 'Karma@Karma.fr', 123456789, 5, 0, 53, 'LOL'),
(133, NULL, 'Team Kayn', 73, 'Kayn@Kayn.fr', 123456789, 5, 0, 58, 'LOL'),
(134, NULL, 'Team KogMaw', 62, 'KogMaw@KogMaw.fr', 123456789, 5, 0, 63, 'LOL'),
(135, NULL, 'Team Lissandra', 72, 'Lissandra@Lissandra.fr', 123456789, 5, 0, 68, 'LOL'),
(136, NULL, 'Team Malphite', 73, 'Malphite@Malphite.fr', 123456789, 5, 0, 73, 'LOL'),
(137, NULL, 'Team Morgana', 72, 'Morgana@Morgana.fr', 123456789, 5, 0, 78, 'LOL'),
(138, 3, 'Team ZywOo', 90, 'ZywOo@ZywOo.fr', 123456789, 5, 0, 83, 'CS'),
(139, 3, 'Team electronic', 95, 'electronic@electronic.fr', 123456789, 5, 0, 88, 'CS'),
(140, 3, 'Team Niko', 88, 'Niko@Niko.fr', 123456789, 5, 0, 93, 'CS'),
(141, 3, 'Team dupreeh', 88, 'dupreeh@dupreeh.fr', 123456789, 5, 0, 98, 'CS'),
(142, 2, 'Final Fantasy VII', 100, 'Sephiroth@Sephiroth.fr', 123456789, 2, 0, 103, 'SB'),
(143, 2, 'Metroid', 90, 'AranSamus@Samus.fr', 123456789, 2, 0, 105, 'SB'),
(144, 2, 'Les Freres Mario', 80, 'Mario@Mario.fr', 123456789, 2, 0, 107, 'SB'),
(145, 2, 'Pikapika', 80, 'Pikachu@Pikachu.fr', 123456789, 2, 0, 109, 'SB'),
(146, 2, 'Star Fox', 90, 'Fox@Fox.fr', 123456789, 2, 0, 111, 'SB'),
(147, 2, 'DK', 77, 'DonkeyKong@DonkeyKong.fr', 123456789, 2, 0, 113, 'SB'),
(148, 2, 'Triforce du Courage', 89, 'Link@Link.fr', 123456789, 2, 1, 115, 'SB'),
(149, NULL, 'Triforce de la Sagesse', 88, 'Zelda@Zelda.fr', 123456789, 2, 0, 117, 'SB'),
(150, NULL, 'Team Princess', 66, 'Peach@Peach.fr', 123456789, 2, 0, 119, 'SB'),
(151, NULL, 'Fire Emblem', 88, 'Marth@Marth.fr', 123456789, 2, 0, 121, 'SB'),
(152, NULL, 'Kirby', 90, 'Kirby@Kirby.fr', 123456789, 2, 0, 123, 'SB'),
(153, NULL, 'Kid Icarus', 88, 'Kid@Kid.fr', 123456789, 2, 0, 125, 'SB'),
(154, NULL, '1er gen', 100, 'MewTwo@MewTwo.fr', 123456789, 2, 0, 127, 'SB'),
(155, NULL, 'Ocarina of Time', 100, 'LinkEnfant@LinkEnfant.fr', 123456789, 2, 0, 129, 'SB'),
(156, NULL, 'Team Prince', 100, 'Roy@Roy.fr', 123456789, 2, 0, 131, 'SB'),
(157, NULL, 'EarthBound', 55, 'Ness@Ness.fr', 123456789, 2, 0, 133, 'SB'),
(158, NULL, 'Team nain', 90, 'IceClimbers@IceClimbers.fr', 123456789, 2, 0, 135, 'SB'),
(159, NULL, 'Empire Bowser', 100, 'Bowser@Bowser.fr', 123456789, 2, 0, 137, 'SB'),
(160, NULL, 'Mario Galaxy', 100, 'Yoshi@Yoshi.fr', 123456789, 2, 0, 139, 'SB'),
(161, NULL, 'Nuls', 55, 'CaptainFalcon@CaptainFalcon.fr', 123456789, 2, 0, 141, 'SB'),
(162, NULL, 'Do they taste good?', 100, 'Snake@Snake.fr', 123456789, 2, 0, 143, 'SB'),
(163, NULL, 'Heavy', 100, 'Wolf@Wolf.fr', 123456789, 2, 0, 145, 'SB'),
(164, NULL, 'Bad Guys', 55, 'RoiDadidou@RoiDadidou.fr', 123456789, 2, 0, 147, 'SB'),
(165, NULL, 'Boule bleue', 99, 'Sonic@Sonic.fr', 123456789, 2, 0, 149, 'SB'),
(166, NULL, 'Laser', 88, 'R.O.B@R.O.B.fr', 123456789, 2, 0, 151, 'SB'),
(167, NULL, 'Jamais utilisé', 22, 'Olimar@Olimar.fr', 123456789, 2, 0, 153, 'SB'),
(168, NULL, 'Combat', 44, 'Amphinobi@Amphinobi.fr', 123456789, 2, 0, 155, 'SB'),
(169, NULL, 'Attrape tout', 22, 'Pacman@Pacman.fr', 123456789, 2, 0, 157, 'SB'),
(170, NULL, 'Gros bras', 100, 'Felinferno@Felinferno.fr', 123456789, 2, 0, 159, 'SB'),
(171, NULL, 'Belmont', 100, 'Ritcher@Ritcher.fr', 123456789, 2, 0, 161, 'SB'),
(172, NULL, 'Va chercher', 67, 'DuoDuckHunt@DuoDuckHunt.fr', 123456789, 2, 0, 163, 'SB'),
(173, NULL, 'Épéiste', 56, 'Daraen@Daraen.fr', 123456789, 2, 0, 165, 'SB'),
(174, NULL, 'Combattants', 66, 'KingKRool@KingKRool.fr', 123456789, 2, 0, 167, 'SB');

-- --------------------------------------------------------

--
-- Table structure for table `joueur`
--

CREATE TABLE `joueur` (
  `idJoueur` int(11) NOT NULL,
  `idEquipeJ` int(11) NOT NULL,
  `pseudo` varchar(128) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `telephone` int(11) DEFAULT NULL,
  `adresse` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `joueur`
--

INSERT INTO `joueur` (`idJoueur`, `idEquipeJ`, `pseudo`, `nom`, `prenom`, `telephone`, `adresse`) VALUES
(1, 121, 'test', 'nomTest', 'PrenomTest', 123456789, 'test@test.fr'),
(2, 121, 'Utili', 'nomUtilisateur', 'prenomUtilisateur', NULL, NULL),
(3, 122, 'Aatrox', 'Aatrox', 'Aatrox', 123456789, 'Aatrox@Aatrox.fr'),
(4, 122, 'Ahri', 'Ahri', 'Ahri', NULL, NULL),
(5, 122, 'Akali', 'Akali', 'Akali', NULL, NULL),
(6, 122, 'Alistar', 'Alistar', 'Alistar', NULL, NULL),
(7, 122, 'Amumu', 'Amumu', 'Amumu', NULL, NULL),
(8, 123, 'Anivia', 'Anivia', 'Anivia', 123456789, 'Anivia@Anivia.fr'),
(9, 123, 'Annie', 'Annie', 'Annie', NULL, NULL),
(10, 123, 'Aphelios', 'Aphelios', 'Aphelios', NULL, NULL),
(11, 123, 'Ashe', 'Ashe', 'Ashe', NULL, NULL),
(12, 123, 'Aurelion_Sol', 'Sol', 'Aurelion', NULL, NULL),
(13, 124, 'Azir', 'Azir', 'Azir', 123456789, 'Azir@Azir.fr'),
(14, 124, 'Bard', 'Bard', 'Bard', NULL, NULL),
(15, 124, 'Blitzcrank', 'Blitzcrank', 'Blitzcrank', NULL, NULL),
(16, 124, 'Brand', 'Brand', 'Brand', NULL, NULL),
(17, 124, 'Braum', 'Braum', 'Braum', NULL, NULL),
(18, 125, 'Caitlyn', 'Caitlyn', 'Caitlyn', 123456789, 'Caitlyn@Caitlyn.fr'),
(19, 125, 'Camille', 'Camille', 'Camille', NULL, NULL),
(20, 125, 'Cassiopeia', 'Cassiopeia', 'Cassiopeia', NULL, NULL),
(21, 125, 'ChoGath', 'ChoGath', 'ChoGath', NULL, NULL),
(22, 125, 'Corki', 'Corki', 'Corki', NULL, NULL),
(23, 126, 'Darius', 'Darius', 'Darius', 123456789, 'Darius@Darius.fr'),
(24, 126, 'Diana', 'Diana', 'Diana', NULL, NULL),
(25, 126, 'DrMundo', 'DrMundo', 'DrMundo', NULL, NULL),
(26, 126, 'Draven', 'Draven', 'Draven', NULL, NULL),
(27, 126, 'Ekko', 'Ekko', 'Ekko', NULL, NULL),
(28, 127, 'Elise', 'Elise', 'Elise', 123456789, 'Elise@Elise.fr'),
(29, 127, 'Evelynn', 'Evelynn', 'Evelynn', NULL, NULL),
(30, 127, 'Ezreal', 'Ezreal', 'Ezreal', NULL, NULL),
(31, 127, 'Fiddlesticks', 'Fiddlesticks', 'Fiddlesticks', NULL, NULL),
(32, 127, 'Fiora', 'Fiora', 'Fiora', NULL, NULL),
(33, 128, 'Fizz', 'Fizz', 'Fizz', 123456789, 'Fizz@Fizz.fr'),
(34, 128, 'Galio', 'Galio', 'Galio', NULL, NULL),
(35, 128, 'Gangplank', 'Gangplank', 'Gangplank', NULL, NULL),
(36, 128, 'Garen', 'Garen', 'Garen', NULL, NULL),
(37, 128, 'Gnar', 'Gnar', 'Gnar', NULL, NULL),
(38, 129, 'Gragas', 'Gragas', 'Gragas', 123456789, 'Gragas@Gragas.fr'),
(39, 129, 'Graves', 'Graves', 'Graves', NULL, NULL),
(40, 129, 'Hecarim', 'Hecarim', 'Hecarim', NULL, NULL),
(41, 129, 'Heimerdinger', 'Heimerdinger', 'Heimerdinger', NULL, NULL),
(42, 129, 'illaoi', 'illaoi', 'illaoi', NULL, NULL),
(43, 130, 'Irelia', 'Irelia', 'Irelia', 123456789, 'Irelia@Irelia.fr'),
(44, 130, 'Ivern', 'Ivern', 'Ivern', NULL, NULL),
(45, 130, 'Janna', 'Janna', 'Janna', NULL, NULL),
(46, 130, 'JarvanIV', 'JarvanIV', 'JarvanIV', NULL, NULL),
(47, 130, 'Jax', 'Jax', 'Jax', NULL, NULL),
(48, 131, 'Jayce', 'Jayce', 'Jayce', 123456789, 'Jayce@Jayce.fr'),
(49, 131, 'Jhin', 'Jhin', 'Jhin', NULL, NULL),
(50, 131, 'Jinx', 'Jinx', 'Jinx', NULL, NULL),
(51, 131, 'KaiSa', 'KaiSa', 'KaiSa', NULL, NULL),
(52, 131, 'Kalista', 'Kalista', 'Kalista', NULL, NULL),
(53, 132, 'Karma', 'Karma', 'Karma', 123456789, 'Karma@Karma.fr'),
(54, 132, 'Karthus', 'Karthus', 'Karthus', NULL, NULL),
(55, 132, 'Kassadin', 'Kassadin', 'Kassadin', NULL, NULL),
(56, 132, 'Katarina', 'Katarina', 'Katarina', NULL, NULL),
(57, 132, 'Kayle', 'Kayle', 'Kayle', NULL, NULL),
(58, 133, 'Kayn', 'Kayn', 'Kayn', 123456789, 'Kayn@Kayn.fr'),
(59, 133, 'Kennen', 'Kennen', 'Kennen', NULL, NULL),
(60, 133, 'KhaZix', 'KhaZix', 'KhaZix', NULL, NULL),
(61, 133, 'Kindred', 'Kindred', 'Kindred', NULL, NULL),
(62, 133, 'Kled', 'Kled', 'Kled', NULL, NULL),
(63, 134, 'KogMaw', 'KogMaw', 'KogMaw', 123456789, 'KogMaw@KogMaw.fr'),
(64, 134, 'LeBlanc', 'LeBlanc', 'LeBlanc', NULL, NULL),
(65, 134, 'LeeSin', 'LeeSin', 'LeeSin', NULL, NULL),
(66, 134, 'Leona', 'Leona', 'Leona', NULL, NULL),
(67, 134, 'Lilia', 'Lilia', 'Lilia', NULL, NULL),
(68, 135, 'Lissandra', 'Lissandra', 'Lissandra', 123456789, 'Lissandra@Lissandra.fr'),
(69, 135, 'Lucian', 'Lucian', 'Lucian', NULL, NULL),
(70, 135, 'Lulu', 'Lulu', 'Lulu', NULL, NULL),
(71, 135, 'Lux', 'Lux', 'Lux', NULL, NULL),
(72, 135, 'MaitreYi', 'MaitreYi', 'MaitreYi', NULL, NULL),
(73, 136, 'Malphite', 'Malphite', 'Malphite', 123456789, 'Malphite@Malphite.fr'),
(74, 136, 'Malzahar', 'Malzahar', 'Malzahar', NULL, NULL),
(75, 136, 'Maokai', 'Maokai', 'Maokai', NULL, NULL),
(76, 136, 'MissFortune', 'MissFortune', 'MissFortune', NULL, NULL),
(77, 136, 'Mordekaiser', 'Mordekaiser', 'Mordekaiser', NULL, NULL),
(78, 137, 'Morgana', 'Morgana', 'Morgana', 123456789, 'Morgana@Morgana.fr'),
(79, 137, 'Nami', 'Nami', 'Nami', NULL, NULL),
(80, 137, 'Nasus', 'Nasus', 'Nasus', NULL, NULL),
(81, 137, 'Nautilus', 'Nautilus', 'Nautilus', NULL, NULL),
(82, 137, 'Neeko', 'Neeko', 'Neeko', NULL, NULL),
(83, 138, 'ZywOo', 'ZywOo', 'ZywOo', 123456789, 'ZywOo@ZywOo.fr'),
(84, 138, 's1mple', 's1mple', 's1mple', NULL, NULL),
(85, 138, 'device', 'device', 'device', NULL, NULL),
(86, 138, 'EliGe', 'EliGe', 'EliGe', NULL, NULL),
(87, 138, 'Magisk', 'Magisk', 'Magisk', NULL, NULL),
(88, 139, 'electronic', 'electronic', 'electronic', 123456789, 'electronic@electronic.fr'),
(89, 139, 'NAF', 'NAF', 'NAF', NULL, NULL),
(90, 139, 'Brehze', 'Brehze', 'Brehze', NULL, NULL),
(91, 139, 'Twistzz', 'Twistzz', 'Twistzz', NULL, NULL),
(92, 139, 'ropz', 'ropz', 'ropz', NULL, NULL),
(93, 140, 'Niko', 'Niko', 'Niko', 123456789, 'Niko@Niko.fr'),
(94, 140, 'woxic', 'woxic', 'woxic', NULL, NULL),
(95, 140, 'sergej', 'sergej', 'sergej', NULL, NULL),
(96, 140, 'Xyp9x', 'Xyp9x', 'Xyp9x', NULL, NULL),
(97, 140, 'jks', 'jks', 'jks', NULL, NULL),
(98, 141, 'dupreeh', 'dupreeh', 'dupreeh', 123456789, 'dupreeh@dupreeh.fr'),
(99, 141, 'KRIMZ', 'KRIMZ', 'KRIMZ', NULL, NULL),
(100, 141, 'CeRq', 'CeRq', 'CeRq', NULL, NULL),
(101, 141, 'Brollan', 'Brollan', 'Brollan', NULL, NULL),
(102, 141, 'Ethan', 'Ethan', 'Ethan', NULL, NULL),
(103, 142, 'Sephiroth', 'Sephiroth', 'Sephiroth', 123456789, 'Sephiroth@Sephiroth.fr'),
(104, 142, 'Cloud', 'Strike', 'Cloud', NULL, NULL),
(105, 143, 'Samus', 'Aran', 'Samus', 123456789, 'AranSamus@Samus.fr'),
(106, 143, 'Ridley', 'Ridley', 'Ridley', NULL, NULL),
(107, 144, 'Mario', 'Mario', 'Mario', 123456789, 'Mario@Mario.fr'),
(108, 144, 'Luigi', 'Luigi', 'Luigi', NULL, NULL),
(109, 145, 'Pikachu', 'Pikachu', 'Pikachu', 123456789, 'Pikachu@Pikachu.fr'),
(110, 145, 'Pichu', 'Pichu', 'Pichu', NULL, NULL),
(111, 146, 'Fox', 'Fox', 'Fox', 123456789, 'Fox@Fox.fr'),
(112, 146, 'Falco', 'Falco', 'Falco', NULL, NULL),
(113, 147, 'DonkeyKong', 'DonkeyKong', 'DonkeyKong', 123456789, 'DonkeyKong@DonkeyKong.fr'),
(114, 147, 'DiddyKong', 'DiddyKong', 'DiddyKong', NULL, NULL),
(115, 148, 'Link', 'Link', 'Link', 123456789, 'Link@Link.fr'),
(116, 148, 'Link Cartoon', 'Link Cartoon', 'Link Cartoon', NULL, NULL),
(117, 149, 'Zelda', 'Zelda', 'Zelda', 123456789, 'Zelda@Zelda.fr'),
(118, 149, 'Sheik', 'Sheik', 'Sheik', NULL, NULL),
(119, 150, 'Peach', 'Peach', 'Peach', 123456789, 'Peach@Peach.fr'),
(120, 150, 'Daisy', 'Daisy', 'Daisy', NULL, NULL),
(121, 151, 'Marth', 'Marth', 'Marth', 123456789, 'Marth@Marth.fr'),
(122, 151, 'Lucina', 'Lucina', 'Lucina', NULL, NULL),
(123, 152, 'Kirby', 'Kirby', 'Kirby', 123456789, 'Kirby@Kirby.fr'),
(124, 152, 'Meta knight', 'Meta knight', 'Meta knight', NULL, NULL),
(125, 153, 'Kid', 'Kid', 'Kid', 123456789, 'Kid@Kid.fr'),
(126, 153, 'Palutena', 'Palutena', 'Palutena', NULL, NULL),
(127, 154, 'MewTwo', 'MewTwo', 'MewTwo', 123456789, 'MewTwo@MewTwo.fr'),
(128, 154, 'Red', 'Red', 'Red', NULL, NULL),
(129, 155, 'LinkEnfant', 'LinkEnfant', 'LinkEnfant', 123456789, 'LinkEnfant@LinkEnfant.fr'),
(130, 155, 'Ganondorf', 'Ganondorf', 'Ganondorf', NULL, NULL),
(131, 156, 'Roy', 'Roy', 'Roy', 123456789, 'Roy@Roy.fr'),
(132, 156, 'Chrom', 'Chrom', 'Chrom', NULL, NULL),
(133, 157, 'Ness', 'Ness', 'Ness', 123456789, 'Ness@Ness.fr'),
(134, 157, 'Lucas', 'Lucas', 'Lucas', NULL, NULL),
(135, 158, 'IceClimbers', 'IceClimbers', 'IceClimbers', 123456789, 'IceClimbers@IceClimbers.fr'),
(136, 158, 'wario', 'wario', 'wario', NULL, NULL),
(137, 159, 'Bowser', 'Bowser', 'Bowser', 123456789, 'Bowser@Bowser.fr'),
(138, 159, 'BowserJr', 'BowserJr', 'BowserJr', NULL, NULL),
(139, 160, 'Yoshi', 'Yoshi', 'Yoshi', 123456789, 'Yoshi@Yoshi.fr'),
(140, 160, 'harmonie&Luna', 'harmonie&Luna', 'harmonie&Luna', NULL, NULL),
(141, 161, 'CaptainFalcon', 'CaptainFalcon', 'CaptainFalcon', 123456789, 'CaptainFalcon@CaptainFalcon.fr'),
(142, 161, 'MrGame&Watch', 'MrGame&Watch', 'MrGame&Watch', NULL, NULL),
(143, 162, 'Snake', 'Snake', 'Snake', 123456789, 'Snake@Snake.fr'),
(144, 162, 'SamusSansArmure', 'SamusSansArmure', 'SamusSansArmure', NULL, NULL),
(145, 163, 'Wolf', 'Wolf', 'Wolf', 123456789, 'Wolf@Wolf.fr'),
(146, 163, 'Ike', 'Ike', 'Ike', NULL, NULL),
(147, 164, 'RoiDadidou', 'RoiDadidou', 'RoiDadidou', 123456789, 'RoiDadidou@RoiDadidou.fr'),
(148, 164, 'PitMaléfique', 'PitMaléfique', 'PitMaléfique', NULL, NULL),
(149, 165, 'Sonic', 'Sonic', 'Sonic', 123456789, 'Sonic@Sonic.fr'),
(150, 165, 'Lucario', 'Lucario', 'Lucario', NULL, NULL),
(151, 166, 'R.O.B.', 'R.O.B.', 'R.O.B.', 123456789, 'R.O.B@R.O.B.fr'),
(152, 166, 'MegaMan', 'MegaMan', 'MegaMan', NULL, NULL),
(153, 167, 'Olimar', 'Olimar', 'Olimar', 123456789, 'Olimar@Olimar.fr'),
(154, 167, 'Mii', 'Mii', 'Mii', NULL, NULL),
(155, 168, 'Amphinobi', 'Amphinobi', 'Amphinobi', 123456789, 'Amphinobi@Amphinobi.fr'),
(156, 168, 'EntraineurWiiFit', 'EntraineurWiiFit', 'EntraineurWiiFit', NULL, NULL),
(157, 169, 'Pacman', 'Pacman', 'Pacman', 123456789, 'Pacman@Pacman.fr'),
(158, 169, 'Villageois', 'Villageois', 'Villageois', NULL, NULL),
(159, 170, 'Felinferno', 'Felinferno', 'Felinferno', 123456789, 'Felinferno@Felinferno.fr'),
(160, 170, 'Ryu', 'Ryu', 'Ryu', NULL, NULL),
(161, 171, 'Ritcher', 'Belmont', 'Ritcher', 123456789, 'Ritcher@Ritcher.fr'),
(162, 171, 'Simon', 'Simon', 'Simon', NULL, NULL),
(163, 172, 'DuoDuckHunt', 'DuoDuckHunt', 'DuoDuckHunt', 123456789, 'DuoDuckHunt@DuoDuckHunt.fr'),
(164, 172, 'Bayonetta', 'Bayonetta', 'Bayonetta', NULL, NULL),
(165, 173, 'Daraen', 'Daraen', 'Daraen', 123456789, 'Daraen@Daraen.fr'),
(166, 173, 'Corrin', 'Corrin', 'Corrin', NULL, NULL),
(167, 174, 'LittleMac', 'LittleMac', 'LittleMac', 123456789, 'LittleMac@LittleMac.fr'),
(168, 174, 'KingKRool', 'KingKRool', 'KingKRool', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `poule`
--

CREATE TABLE `poule` (
  `idPoule` int(11) NOT NULL,
  `idTournois` int(11) DEFAULT NULL,
  `idEP1` int(11) NOT NULL,
  `idEP2` int(11) NOT NULL,
  `idEP3` int(11) NOT NULL,
  `idEP4` int(11) NOT NULL,
  `idGagnant1` int(11) DEFAULT NULL,
  `idGagnant2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rencontre`
--

CREATE TABLE `rencontre` (
  `idMatch` int(11) NOT NULL,
  `idTournois` int(11) NOT NULL,
  `dateRencontre` date NOT NULL,
  `heureRencontre` time NOT NULL,
  `idEquipe1` int(11) NOT NULL,
  `idEquipe2` int(11) NOT NULL,
  `idGagnant` int(11) NOT NULL DEFAULT 0,
  `score1` int(11) DEFAULT NULL,
  `score2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rencontre`
--

INSERT INTO `rencontre` (`idMatch`, `idTournois`, `dateRencontre`, `heureRencontre`, `idEquipe1`, `idEquipe2`, `idGagnant`, `score1`, `score2`) VALUES
(1, 4, '2021-05-14', '19:00:00', 122, 123, 123, 0, 1),
(2, 4, '2021-05-14', '19:00:00', 122, 124, 124, 0, 1),
(3, 4, '2021-05-14', '19:09:00', 122, 124, 124, 0, 1),
(4, 4, '2021-05-14', '19:09:00', 122, 125, 125, 0, 1),
(5, 4, '2021-05-14', '19:10:00', 123, 124, 124, 0, 1),
(6, 4, '2021-05-14', '19:10:00', 123, 125, 125, 0, 1),
(7, 4, '2021-05-14', '19:10:00', 124, 125, 125, 0, 1),
(8, 4, '2021-05-14', '19:12:00', 122, 123, 123, 0, 1),
(9, 4, '2021-05-14', '19:12:00', 122, 125, 125, 0, 1),
(10, 4, '2021-05-14', '19:13:00', 123, 124, 124, 0, 1),
(11, 4, '2021-05-14', '19:13:00', 123, 125, 125, 0, 1),
(12, 4, '2021-05-14', '19:14:00', 124, 125, 125, 0, 1),
(13, 2, '2021-05-14', '21:41:00', 121, 142, 0, NULL, NULL),
(14, 2, '2021-05-14', '21:41:00', 143, 144, 0, NULL, NULL),
(15, 2, '2021-05-14', '21:41:00', 145, 146, 0, NULL, NULL),
(16, 2, '2021-05-14', '21:41:00', 147, 148, 148, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL DEFAULT 0,
  `is_gestionnaire` int(11) NOT NULL DEFAULT 0,
  `is_joueur` int(11) NOT NULL DEFAULT 0,
  `is_capitaine` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `is_admin`, `is_gestionnaire`, `is_joueur`, `is_capitaine`) VALUES
(1, 0, 1, 1, 0),
(2, 0, 0, 2, 0),
(3, 0, 0, 0, 1),
(4, 0, 0, 2, 0),
(5, 0, 0, 3, 0),
(6, 0, 0, 4, 0),
(7, 0, 0, 5, 0),
(8, 0, 0, 6, 0),
(9, 0, 0, 7, 0),
(10, 0, 0, 0, 3),
(11, 0, 0, 4, 0),
(12, 0, 0, 5, 0),
(13, 0, 0, 6, 0),
(14, 0, 0, 7, 0),
(15, 0, 0, 8, 0),
(16, 0, 0, 9, 0),
(17, 0, 0, 10, 0),
(18, 0, 0, 11, 0),
(19, 0, 0, 12, 0),
(20, 0, 0, 0, 8),
(21, 0, 0, 9, 0),
(22, 0, 0, 10, 0),
(23, 0, 0, 11, 0),
(24, 0, 0, 12, 0),
(25, 0, 0, 13, 0),
(26, 0, 0, 14, 0),
(27, 0, 0, 15, 0),
(28, 0, 0, 16, 0),
(29, 0, 0, 17, 0),
(30, 0, 0, 0, 13),
(31, 0, 0, 14, 0),
(32, 0, 0, 15, 0),
(33, 0, 0, 16, 0),
(34, 0, 0, 17, 0),
(35, 0, 0, 18, 0),
(36, 0, 0, 19, 0),
(37, 0, 0, 20, 0),
(38, 0, 0, 21, 0),
(39, 0, 0, 22, 0),
(40, 0, 0, 0, 18),
(41, 0, 0, 19, 0),
(42, 0, 0, 20, 0),
(43, 0, 0, 21, 0),
(44, 0, 0, 22, 0),
(45, 0, 0, 23, 0),
(46, 0, 0, 24, 0),
(47, 0, 0, 25, 0),
(48, 0, 0, 26, 0),
(49, 0, 0, 27, 0),
(50, 0, 0, 0, 23),
(51, 0, 0, 24, 0),
(52, 0, 0, 25, 0),
(53, 0, 0, 26, 0),
(54, 0, 0, 27, 0),
(55, 0, 0, 28, 0),
(56, 0, 0, 29, 0),
(57, 0, 0, 30, 0),
(58, 0, 0, 31, 0),
(59, 0, 0, 32, 0),
(60, 0, 0, 0, 28),
(61, 0, 0, 29, 0),
(62, 0, 0, 30, 0),
(63, 0, 0, 31, 0),
(64, 0, 0, 32, 0),
(65, 0, 0, 33, 0),
(66, 0, 0, 34, 0),
(67, 0, 0, 35, 0),
(68, 0, 0, 36, 0),
(69, 0, 0, 37, 0),
(70, 0, 0, 0, 33),
(71, 0, 0, 34, 0),
(72, 0, 0, 35, 0),
(73, 0, 0, 36, 0),
(74, 0, 0, 37, 0),
(75, 0, 0, 38, 0),
(76, 0, 0, 39, 0),
(77, 0, 0, 40, 0),
(78, 0, 0, 41, 0),
(79, 0, 0, 42, 0),
(80, 0, 0, 0, 38),
(81, 0, 0, 39, 0),
(82, 0, 0, 40, 0),
(83, 0, 0, 41, 0),
(84, 0, 0, 42, 0),
(85, 0, 0, 43, 0),
(86, 0, 0, 44, 0),
(87, 0, 0, 45, 0),
(88, 0, 0, 46, 0),
(89, 0, 0, 47, 0),
(90, 0, 0, 0, 43),
(91, 0, 0, 44, 0),
(92, 0, 0, 45, 0),
(93, 0, 0, 46, 0),
(94, 0, 0, 47, 0),
(95, 0, 0, 48, 0),
(96, 0, 0, 49, 0),
(97, 0, 0, 50, 0),
(98, 0, 0, 51, 0),
(99, 0, 0, 52, 0),
(100, 0, 0, 0, 48),
(101, 0, 0, 49, 0),
(102, 0, 0, 50, 0),
(103, 0, 0, 51, 0),
(104, 0, 0, 52, 0),
(105, 0, 0, 53, 0),
(106, 0, 0, 54, 0),
(107, 0, 0, 55, 0),
(108, 0, 0, 56, 0),
(109, 0, 0, 57, 0),
(110, 0, 0, 0, 53),
(111, 0, 0, 54, 0),
(112, 0, 0, 55, 0),
(113, 0, 0, 56, 0),
(114, 0, 0, 57, 0),
(115, 0, 0, 58, 0),
(116, 0, 0, 59, 0),
(117, 0, 0, 60, 0),
(118, 0, 0, 61, 0),
(119, 0, 0, 62, 0),
(120, 0, 0, 0, 58),
(121, 0, 0, 59, 0),
(122, 0, 0, 60, 0),
(123, 0, 0, 61, 0),
(124, 0, 0, 62, 0),
(125, 0, 0, 63, 0),
(126, 0, 0, 64, 0),
(127, 0, 0, 65, 0),
(128, 0, 0, 66, 0),
(129, 0, 0, 67, 0),
(130, 0, 0, 0, 63),
(131, 0, 0, 64, 0),
(132, 0, 0, 65, 0),
(133, 0, 0, 66, 0),
(134, 0, 0, 67, 0),
(135, 0, 0, 68, 0),
(136, 0, 0, 69, 0),
(137, 0, 0, 70, 0),
(138, 0, 0, 71, 0),
(139, 0, 0, 72, 0),
(140, 0, 0, 0, 68),
(141, 0, 0, 69, 0),
(142, 0, 0, 70, 0),
(143, 0, 0, 71, 0),
(144, 0, 0, 72, 0),
(145, 0, 0, 73, 0),
(146, 0, 0, 74, 0),
(147, 0, 0, 75, 0),
(148, 0, 0, 76, 0),
(149, 0, 0, 77, 0),
(150, 0, 0, 0, 73),
(151, 0, 0, 74, 0),
(152, 0, 0, 75, 0),
(153, 0, 0, 76, 0),
(154, 0, 0, 77, 0),
(155, 0, 0, 78, 0),
(156, 0, 0, 79, 0),
(157, 0, 0, 80, 0),
(158, 0, 0, 81, 0),
(159, 0, 0, 82, 0),
(160, 0, 0, 0, 78),
(161, 0, 0, 79, 0),
(162, 0, 0, 80, 0),
(163, 0, 0, 81, 0),
(164, 0, 0, 82, 0),
(165, 0, 0, 83, 0),
(166, 0, 0, 84, 0),
(167, 0, 0, 85, 0),
(168, 0, 0, 86, 0),
(169, 0, 0, 87, 0),
(170, 0, 0, 0, 83),
(171, 0, 0, 84, 0),
(172, 0, 0, 85, 0),
(173, 0, 0, 86, 0),
(174, 0, 0, 87, 0),
(175, 0, 0, 88, 0),
(176, 0, 0, 89, 0),
(177, 0, 0, 90, 0),
(178, 0, 0, 91, 0),
(179, 0, 0, 92, 0),
(180, 0, 0, 0, 88),
(181, 0, 0, 89, 0),
(182, 0, 0, 90, 0),
(183, 0, 0, 91, 0),
(184, 0, 0, 92, 0),
(185, 0, 0, 93, 0),
(186, 0, 0, 94, 0),
(187, 0, 0, 95, 0),
(188, 0, 0, 96, 0),
(189, 0, 0, 97, 0),
(190, 0, 0, 0, 93),
(191, 0, 0, 94, 0),
(192, 0, 0, 95, 0),
(193, 0, 0, 96, 0),
(194, 0, 0, 97, 0),
(195, 0, 0, 98, 0),
(196, 0, 0, 99, 0),
(197, 0, 0, 100, 0),
(198, 0, 0, 101, 0),
(199, 0, 0, 102, 0),
(200, 0, 0, 0, 98),
(201, 0, 0, 99, 0),
(202, 0, 0, 100, 0),
(203, 0, 0, 101, 0),
(204, 0, 0, 102, 0),
(205, 0, 0, 103, 0),
(206, 0, 0, 104, 0),
(207, 0, 0, 0, 103),
(208, 0, 0, 104, 0),
(209, 0, 0, 105, 0),
(210, 0, 0, 106, 0),
(211, 0, 0, 0, 105),
(212, 0, 0, 106, 0),
(213, 0, 0, 107, 0),
(214, 0, 0, 108, 0),
(215, 0, 0, 0, 107),
(216, 0, 0, 108, 0),
(217, 0, 0, 109, 0),
(218, 0, 0, 110, 0),
(219, 0, 0, 0, 109),
(220, 0, 0, 110, 0),
(221, 0, 0, 111, 0),
(222, 0, 0, 112, 0),
(223, 0, 0, 0, 111),
(224, 0, 0, 112, 0),
(225, 0, 0, 113, 0),
(226, 0, 0, 114, 0),
(227, 0, 0, 0, 113),
(228, 0, 0, 114, 0),
(229, 0, 0, 115, 0),
(230, 0, 0, 116, 0),
(231, 0, 0, 0, 115),
(232, 0, 0, 116, 0),
(233, 0, 0, 117, 0),
(234, 0, 0, 118, 0),
(235, 0, 0, 0, 117),
(236, 0, 0, 118, 0),
(237, 0, 0, 119, 0),
(238, 0, 0, 120, 0),
(239, 0, 0, 0, 119),
(240, 0, 0, 120, 0),
(241, 0, 0, 121, 0),
(242, 0, 0, 122, 0),
(243, 0, 0, 0, 121),
(244, 0, 0, 122, 0),
(245, 0, 0, 123, 0),
(246, 0, 0, 124, 0),
(247, 0, 0, 0, 123),
(248, 0, 0, 124, 0),
(249, 0, 0, 125, 0),
(250, 0, 0, 126, 0),
(251, 0, 0, 0, 125),
(252, 0, 0, 126, 0),
(253, 0, 0, 127, 0),
(254, 0, 0, 128, 0),
(255, 0, 0, 0, 127),
(256, 0, 0, 128, 0),
(257, 0, 0, 129, 0),
(258, 0, 0, 130, 0),
(259, 0, 0, 0, 129),
(260, 0, 0, 130, 0),
(261, 0, 0, 131, 0),
(262, 0, 0, 132, 0),
(263, 0, 0, 0, 131),
(264, 0, 0, 132, 0),
(265, 0, 0, 133, 0),
(266, 0, 0, 134, 0),
(267, 0, 0, 0, 133),
(268, 0, 0, 134, 0),
(269, 0, 0, 135, 0),
(270, 0, 0, 136, 0),
(271, 0, 0, 0, 135),
(272, 0, 0, 136, 0),
(273, 0, 0, 137, 0),
(274, 0, 0, 138, 0),
(275, 0, 0, 0, 137),
(276, 0, 0, 138, 0),
(277, 0, 0, 139, 0),
(278, 0, 0, 140, 0),
(279, 0, 0, 0, 139),
(280, 0, 0, 140, 0),
(281, 0, 0, 141, 0),
(282, 0, 0, 142, 0),
(283, 0, 0, 0, 141),
(284, 0, 0, 142, 0),
(285, 0, 0, 143, 0),
(286, 0, 0, 144, 0),
(287, 0, 0, 0, 143),
(288, 0, 0, 144, 0),
(289, 0, 0, 145, 0),
(290, 0, 0, 146, 0),
(291, 0, 0, 0, 145),
(292, 0, 0, 146, 0),
(293, 0, 0, 147, 0),
(294, 0, 0, 148, 0),
(295, 0, 0, 0, 147),
(296, 0, 0, 148, 0),
(297, 0, 0, 149, 0),
(298, 0, 0, 150, 0),
(299, 0, 0, 0, 149),
(300, 0, 0, 150, 0),
(301, 0, 0, 151, 0),
(302, 0, 0, 152, 0),
(303, 0, 0, 0, 151),
(304, 0, 0, 152, 0),
(305, 0, 0, 153, 0),
(306, 0, 0, 154, 0),
(307, 0, 0, 0, 153),
(308, 0, 0, 154, 0),
(309, 0, 0, 155, 0),
(310, 0, 0, 156, 0),
(311, 0, 0, 0, 155),
(312, 0, 0, 156, 0),
(313, 0, 0, 157, 0),
(314, 0, 0, 158, 0),
(315, 0, 0, 0, 157),
(316, 0, 0, 158, 0),
(317, 0, 0, 159, 0),
(318, 0, 0, 160, 0),
(319, 0, 0, 0, 159),
(320, 0, 0, 160, 0),
(321, 0, 0, 161, 0),
(322, 0, 0, 162, 0),
(323, 0, 0, 0, 161),
(324, 0, 0, 162, 0),
(325, 0, 0, 163, 0),
(326, 0, 0, 164, 0),
(327, 0, 0, 0, 163),
(328, 0, 0, 164, 0),
(329, 0, 0, 165, 0),
(330, 0, 0, 166, 0),
(331, 0, 0, 0, 165),
(332, 0, 0, 166, 0),
(333, 0, 0, 167, 0),
(334, 0, 0, 168, 0),
(335, 0, 0, 0, 167),
(336, 0, 0, 168, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tournois`
--

CREATE TABLE `tournois` (
  `idTournois` int(11) NOT NULL,
  `dateDebut` date NOT NULL,
  `duree` int(11) NOT NULL,
  `idGestionnaire` int(11) NOT NULL,
  `lieu` varchar(64) NOT NULL,
  `type` varchar(64) NOT NULL,
  `nom_tournois` varchar(64) NOT NULL,
  `idEquipe` varchar(1024) DEFAULT NULL,
  `nbr_equipes` int(11) NOT NULL,
  `Esport` varchar(64) NOT NULL,
  `resultats_aleatoires` tinyint(1) NOT NULL,
  `equipes_aleatoires` tinyint(1) NOT NULL,
  `arbreBin` varchar(2048) DEFAULT NULL,
  `Agagnant` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tournois`
--

INSERT INTO `tournois` (`idTournois`, `dateDebut`, `duree`, `idGestionnaire`, `lieu`, `type`, `nom_tournois`, `idEquipe`, `nbr_equipes`, `Esport`, `resultats_aleatoires`, `equipes_aleatoires`, `arbreBin`, `Agagnant`) VALUES
(2, '2021-05-14', 12, 1, 'Montpellier', 'coupe', 'maCoupe', '121,142,143,144,145,146,147,148,', 8, 'SB', 1, 1, 'a:1:{i:0;a:8:{i:0;a:1:{s:6:\"Equipe\";s:3:\"121\";}i:1;a:1:{s:6:\"Equipe\";s:3:\"142\";}i:2;a:1:{s:6:\"Equipe\";s:3:\"143\";}i:3;a:1:{s:6:\"Equipe\";s:3:\"144\";}i:4;a:1:{s:6:\"Equipe\";s:3:\"145\";}i:5;a:1:{s:6:\"Equipe\";s:3:\"146\";}i:6;a:1:{s:6:\"Equipe\";s:3:\"147\";}i:7;a:1:{s:6:\"Equipe\";s:3:\"148\";}}}', 0),
(3, '2021-05-17', 13, 1, 'Montpellier', 'tournois', 'monTournoi', NULL, 16, 'CS', 1, 1, NULL, 0),
(4, '2021-05-14', 0, 1, 'Paris', 'championnat', 'monChampionnat', '122,123,124,125,', 4, 'LOL', 1, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idUser` int(11) NOT NULL,
  `mail` varchar(64) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `pseudo` varchar(128) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`idUser`, `mail`, `mdp`, `pseudo`, `nom`, `prenom`, `num`) VALUES
(1, 'test@test.fr', '$2y$10$XkPqJKwC2K2UdX0twrzO0etKYXhSiM9swkWCUXjFQAm5zyHV.4Jg.', 'test', 'nomTest', 'PrenomTest', 123456789),
(2, 'utilisateur@gmail.com', '$2y$10$P4eSIWs84Vvc3Pqt2ucmC.zeFkUIocZhs/rgaA4Tpo0phdQk7yUhy', 'Utili', 'nomUtilisateur', 'prenomUtilisateur', 123456789),
(3, 'Aatrox@Aatrox.fr', '$2y$10$yjy84/BYKMv5lp0s8ZzN9urYN1NzG37xlAd6N9it.pCWRjGkWBfQO', 'Aatrox', 'Aatrox', 'Aatrox', 123456789),
(4, 'Ahri@Ahri.fr', '$2y$10$3gnobsOV0s9INHisNEugxuN2WszstznFDpwibKOaIePabV3gqaDvi', 'Ahri', 'Ahri', 'Ahri', 123456789),
(5, 'Akali@Akali.fr', '$2y$10$RoI9HT74jPm9xXgNuQhhz.EsI3Uqw7XRyvbl1mhpHT23RBjSeUY96', 'Akali', 'Akali', 'Akali', 123456789),
(6, 'Alistar@Alistar.fr', '$2y$10$XU3wYRqijnyL92aff95o7.EmXvhBbGfxqQlrlsnY1xkizU/ZvYu0W', 'Alistar', 'Alistar', 'Alistar', 123456789),
(7, 'Amumu@Amumu.fr', '$2y$10$bcM4gwjF3GRYdGgVeGnslekCZFVJ1X/Amj3RYFQjUdJHOs4co8r6C', 'Amumu', 'Amumu', 'Amumu', 123456789),
(8, 'Anivia@Anivia.fr', '$2y$10$cyg0IYj.FfskryemxHDfwefbLNbwUtUXDspDxGFdVa0TnSFEkhjI.', 'Anivia', 'Anivia', 'Anivia', 123456789),
(9, 'Annie@Annie.fr', '$2y$10$AJAB6AW.xf7xGWYb20LLsO9c1EYUepw6bjc34Nasjt.lKQoiwmxOq', 'Annie', 'Annie', 'Annie', 123456789),
(10, 'Aphelios@Aphelios.fr', '$2y$10$z4mCFmAxyOg6VKq5Ce.q6.uu6zt569prwgAnXCuarCfG/uP8ETaTa', 'Aphelios', 'Aphelios', 'Aphelios', 123456789),
(11, 'Ashe@Ashe.fr', '$2y$10$GF0fNcD6o9npJKhZdNG6POaXvKKZOI4a.h7MZN/Gthwi3TOHQ2bA2', 'Ashe', 'Ashe', 'Ashe', 123456789),
(12, 'Aurelion_Sol@Sol.fr', '$2y$10$LASoTQEspRMbqUFO9ZEf3eoAdXH0uHWGAvTfEwGtzrecFhKDEhfei', 'Aurelion_Sol', 'Sol', 'Aurelion', 123456789),
(13, 'Azir@Azir.fr', '$2y$10$SB2yIOnCkWmRN2eNfdfRYuXVkzG8X97xuz1AWI14h28JAI6XbTIF2', 'Azir', 'Azir', 'Azir', 123456789),
(14, 'Bard@Bard.fr', '$2y$10$XC0JiiNLuQNlkjJAcn4LN..eCORejnnaezU2PTFcLUvxkIW34o8ei', 'Bard', 'Bard', 'Bard', 123456789),
(15, 'Blitzcrank@Blitzcrank.fr', '$2y$10$vSGSgd0Ke/nxjRXyrQc7eOciZMNtgXDq7BSUPSevtOVHyrhkMT69i', 'Blitzcrank', 'Blitzcrank', 'Blitzcrank', 123456789),
(16, 'Brand@Brand.fr', '$2y$10$yuZOXr9EsRoGu/2zK2dCYeaFw4LvtVQEnucZ2Z7UsKLAtEBHsilay', 'Brand', 'Brand', 'Brand', 123456789),
(17, 'Braum@Braum.fr', '$2y$10$kVtCdn1ZbHR.rHNDaM7W2.I/wLXasdqBCXNGsnO2tyQPBvXAOBh8a', 'Braum', 'Braum', 'Braum', 123456789),
(18, 'Caitlyn@Caitlyn.fr', '$2y$10$aPKrsn15IfwhPROVmXhtzO6xLM2Y/otcSTBNdCl8XT9iRlg.lJtgW', 'Caitlyn', 'Caitlyn', 'Caitlyn', 123456789),
(19, 'Camille@Camille.fr', '$2y$10$iAgioLMoXU2iU0mvkyuRAeOqDxU1byfxh68GuJxOIYPpxGNcJb2Le', 'Camille', 'Camille', 'Camille', 123456789),
(20, 'Cassiopeia@Cassiopeia.fr', '$2y$10$50By6ploD2tLZqVWUS8QhOt95E2eMVHsGbzxqZHrJcE.MoH1XSOH6', 'Cassiopeia', 'Cassiopeia', 'Cassiopeia', 123456789),
(21, 'Cho_Gath@ChoGath.fr', '$2y$10$bJldyeMwl80X82jPoCZlhOgYkSOs2rgz5NIqn1D55W0sAzDrHKRj.', 'ChoGath', 'ChoGath', 'ChoGath', 123456789),
(22, 'Corki@Corki.fr', '$2y$10$2CAMoH2IpLCxZgiwv5PiOuQ5cbsoi1VBdJNNJ9xW10nd7.exrIOvm', 'Corki', 'Corki', 'Corki', 123456789),
(23, 'Darius@Darius.fr', '$2y$10$wisfZNwrgsCsyQy2ggv.6e8.bBB29GBZ.WjGqmX/SdxQgoMr10GgK', 'Darius', 'Darius', 'Darius', 123456789),
(24, 'Diana@Diana.fr', '$2y$10$ql.R27o1916mkVI2miJAhOy3eXJ32caAdeal8FObGmQ/Qbo4xRdRy', 'Diana', 'Diana', 'Diana', 123456789),
(25, 'DrMundo@DrMundo.fr', '$2y$10$aSdYTct4WA1pMLS0rt2VeOrTxr4XlFVfxNtBDFihXHzgtqiU5sYJq', 'DrMundo', 'DrMundo', 'DrMundo', 123456789),
(26, 'Draven@Draven.fr', '$2y$10$FZKLpGc8rC1q0YpegckLd.K.twHIijOYw3tFXviMKS5sEUMMjOUBW', 'Draven', 'Draven', 'Draven', 123456789),
(27, 'Ekko@Ekko.fr', '$2y$10$lwLXMV1jV8cNgfKBkOLea.FWZBcX.VwGiK4RxP2bEOxupEZTqeZcm', 'Ekko', 'Ekko', 'Ekko', 123456789),
(28, 'Elise@Elise.fr', '$2y$10$sLuIUOLyBrCEwghU0EMrLuyqXlp24Hv7Et8ruwnrq29Yht3qRqxPu', 'Elise', 'Elise', 'Elise', 123456789),
(29, 'Evelynn@Evelynn.fr', '$2y$10$/qTMtmK8Yaqzz89142V/feOghOvuIy1VhyWXxSzHrMJdts9y.yH4a', 'Evelynn', 'Evelynn', 'Evelynn', 123456789),
(30, 'Ezreal@Ezreal.fr', '$2y$10$EZsoP2eNHomNq8dVdyvyBeCaeIMMSrmVUC2iqFBUV9SFuIfrAjNcC', 'Ezreal', 'Ezreal', 'Ezreal', 123456789),
(31, 'Fiddlesticks@Fiddlesticks.fr', '$2y$10$H64t3c0iik2CcndpCIccV.x8AXLbJky76t.sl9zyVHqHj3NAuoce.', 'Fiddlesticks', 'Fiddlesticks', 'Fiddlesticks', 123456789),
(32, 'Fiora@Fiora.fr', '$2y$10$BsaPSYN8ZEMMg4Ku9c9fjO5pFUR4zYeWYyeRvQLoxkpGj2uHChAFy', 'Fiora', 'Fiora', 'Fiora', 123456789),
(33, 'Fizz@Fizz.fr', '$2y$10$89FI7XqNXI6IT38u1g.n3.Fg8l.Lgfn8H.L7nzY3DgbUFkkaj5ggm', 'Fizz', 'Fizz', 'Fizz', 123456789),
(34, 'Galio@Galio.fr', '$2y$10$GDhqXo2HlPHhpQLdLdJQIOwDpPjcL9P6h2/wDNG/VOJgNS0uGFVsm', 'Galio', 'Galio', 'Galio', 123456789),
(35, 'Gangplank@Gangplank.fr', '$2y$10$q47JfT9X2jX6ylGogOA3euZspRsMIqVaRxnE/0QRfRaBioMnDEhtW', 'Gangplank', 'Gangplank', 'Gangplank', 123456789),
(36, 'Garen@Garen.fr', '$2y$10$Gn.FPLExPyFc/LIoI/t8NuMvB7hx4pP0E4MVNk7CG.pCbyqTp0jVW', 'Garen', 'Garen', 'Garen', 123456789),
(37, 'Gnar@Gnar.fr', '$2y$10$zSlH48g7lowknlMjX./wnOpk14jMoZnQ9wfPm3d1TBb8p9bmEwJ7y', 'Gnar', 'Gnar', 'Gnar', 123456789),
(38, 'Gragas@Gragas.fr', '$2y$10$0t.BeEmd3C/Yi1hhbxLubO76a5/2cnL5w4Zw8iu4OXVC9Si6P.kua', 'Gragas', 'Gragas', 'Gragas', 123456789),
(39, 'Graves@Graves.fr', '$2y$10$CfLLcQQ0FM1299vMd2QGz.6NGk23wccdQbfOwNCIGlIe.FVgw1nJ6', 'Graves', 'Graves', 'Graves', 123456789),
(40, 'Hecarim@Hecarim.fr', '$2y$10$yb6C/WdVrkkeHB8NkeFfDuVRR2Re9Ou8x3LqjF1UqHRPR/jyKwSeu', 'Hecarim', 'Hecarim', 'Hecarim', 123456789),
(41, 'Heimerdinger@Heimerdinger.fr', '$2y$10$i4g3NCgbCR9gF4HkBeeEZOefiwdy4ImDPIshNJZqzNld9NVnbuZci', 'Heimerdinger', 'Heimerdinger', 'Heimerdinger', 123456789),
(42, 'illaoi@illaoi.fr', '$2y$10$rvaoSk.48zk.Bjt/onh0PucZiD87oyj3b/gfpyFtqSnuEie5bWb4W', 'illaoi', 'illaoi', 'illaoi', 123456789),
(43, 'Irelia@Irelia.fr', '$2y$10$OhQ549uS.9cccgJCS06B9OXojvSAs9luJ5H9QLrTBy8fxEEAyPWWW', 'Irelia', 'Irelia', 'Irelia', 123456789),
(44, 'Ivern@Ivern.fr', '$2y$10$wRLmpXeCLckMuRYsTnk.aexMdVh8fXUqdxsp89bfZe6s9iW2npQbW', 'Ivern', 'Ivern', 'Ivern', 123456789),
(45, 'Janna@Janna.fr', '$2y$10$4RA9STvUMRUkuzsNCL6q/uJ64qKhT9e37zoU2v1PLDQOfjo44HVuG', 'Janna', 'Janna', 'Janna', 123456789),
(46, 'Jarvan_IV@Jarvan.fr', '$2y$10$uBOfOxV9EjQgtV8U1FfZ9OmpSQ6V5.i1G0ffgz0JkEieYI9RQxOVO', 'JarvanIV', 'JarvanIV', 'JarvanIV', 123456789),
(47, 'Jax@Jax.fr', '$2y$10$IQKcUME2KBCDEedUHGZ7keOAvDyU1S3xoTs.zbA9EQ9As3AA2bIVm', 'Jax', 'Jax', 'Jax', 123456789),
(48, 'Jayce@Jayce.fr', '$2y$10$pxoEhlxw1OK3xOaHgArU4ub7VTkOBxqQhsDb1geIeF7RrxU9yGuIm', 'Jayce', 'Jayce', 'Jayce', 123456789),
(49, 'Jhin@Jhin.fr', '$2y$10$p6s3OzX4jRLkhfvamGMgpee.1jZoOow4QswQwuZAYrdesPMDDcBqW', 'Jhin', 'Jhin', 'Jhin', 123456789),
(50, 'Jinx@Jinx.fr', '$2y$10$i8jTG/mTBpBmsTarxe3Cdu2s8Xn2ooQTSQFrjZHVeGUwNgrSadE9a', 'Jinx', 'Jinx', 'Jinx', 123456789),
(51, 'KaiSa@KaiSa.fr', '$2y$10$MwP6kR0NVl9/CzATBu6d2e8kyUIe8Lerl.9NP5NxaSIxa8NZrDYEK', 'KaiSa', 'KaiSa', 'KaiSa', 123456789),
(52, 'Kalista@Kalista.fr', '$2y$10$81jFwqwssmicACiJO1lK0ujDzdLo/Cow5QrmSoJL8I1UJvHuzUtkW', 'Kalista', 'Kalista', 'Kalista', 123456789),
(53, 'Karma@Karma.fr', '$2y$10$O23XQt9EGDcYtJSEwT3on.mOn1B/zLqqboJU6D9aaGOJ.oe8dyxa.', 'Karma', 'Karma', 'Karma', 123456789),
(54, 'Karthus@Karthus.fr', '$2y$10$rE5HwSFk3w1Gnw8SsioFVei5CsmTx839RwtjiO.sna1hro2bjjBUW', 'Karthus', 'Karthus', 'Karthus', 123456789),
(55, 'Kassadin@Kassadin.fr', '$2y$10$ty/wZEl8wj9ikYx4vXxrNurzoeVwRMvWOO3HRVjbEfPj0dZ9YmkYG', 'Kassadin', 'Kassadin', 'Kassadin', 123456789),
(56, 'Katarina@Katarina.fr', '$2y$10$BBo0r.0Bucm/zZmRlXTceeXQW3HEifreqP1zFSbQKbTa.UEIRO9QK', 'Katarina', 'Katarina', 'Katarina', 123456789),
(57, 'Kayle@Kayle.fr', '$2y$10$PEYuXtLsy7HVv7NEMnY2cOwUZBvayqJQ8em2RURP393W.LWmJ3Ui.', 'Kayle', 'Kayle', 'Kayle', 123456789),
(58, 'Kayn@Kayn.fr', '$2y$10$ZvihkpiLMGgDzNNodIf8YOuU5M0Z4ThhyicKvQY09HZ79SZlSB6j2', 'Kayn', 'Kayn', 'Kayn', 123456789),
(59, 'Kennen@Kennen.fr', '$2y$10$acmWfpiyl1UqEZKshVs4cenCPycltsGYQLjWHZqxu3QbvmKL0kZ0m', 'Kennen', 'Kennen', 'Kennen', 123456789),
(60, 'KhaZix@KhaZix.fr', '$2y$10$n/xUOTpGgwyCYZ/g/fRDK.xUtgY.1kdCkz/PaaB56RtWK1NLU.uMW', 'KhaZix', 'KhaZix', 'KhaZix', 123456789),
(61, 'Kindred@Kindred.fr', '$2y$10$XUuCP5R4.FUl2aB1nRwnS.lf1Xeu.V2wAA3CCALMtLcyh4ow33fVK', 'Kindred', 'Kindred', 'Kindred', 123456789),
(62, 'Kled@Kled.fr', '$2y$10$OwamoAL2NvjSi2tQlW9.C.gyknKTFwZypSbyfj7etLuDluZSnzCZm', 'Kled', 'Kled', 'Kled', 123456789),
(63, 'KogMaw@KogMaw.fr', '$2y$10$8SqAHFWKmqMdn.ctnleC9O74pEn5ZGpSyTUaDUcQByZ2c9u/EnF2O', 'KogMaw', 'KogMaw', 'KogMaw', 123456789),
(64, 'LeBlanc@LeBlanc.fr', '$2y$10$CGA8SdteAtxbsIkR4FQXNu4Qiry08fXusSrBMrprgOhV/Ewgku/J2', 'LeBlanc', 'LeBlanc', 'LeBlanc', 123456789),
(65, 'LeeSin@LeeSin.fr', '$2y$10$7V09f2VaExR1Ox3BR7FID.oPeyiDnkGWAPvUDIotGCprA9KXYZ/bS', 'LeeSin', 'LeeSin', 'LeeSin', 123456789),
(66, 'Leona@Leona.fr', '$2y$10$MAW/hWPJZ.kaLJuNLjp0J.VxZPKJuUfwGC0pxow1kCvAka.tXGnIy', 'Leona', 'Leona', 'Leona', 123456789),
(67, 'Lilia@Lilia.fr', '$2y$10$5UhQI6f1oYiUHyHlsLeFYen/GIFoKGAe5P/pC3zb7/3VQfhpt7ebe', 'Lilia', 'Lilia', 'Lilia', 123456789),
(68, 'Lissandra@Lissandra.fr', '$2y$10$xUVh.yQ5xjXNe2P9GRSske/EFLnbqVIpwWofF6vpprorK28APl/F.', 'Lissandra', 'Lissandra', 'Lissandra', 123456789),
(69, 'Lucian@Lucian.fr', '$2y$10$9X0fu/ZoMJoHZ9vStXMkbemP0uy1VvGCweawOMsiADvwlK8u2rnZG', 'Lucian', 'Lucian', 'Lucian', 123456789),
(70, 'Lulu@Lulu.fr', '$2y$10$WclC/NbrBp6IAcSGbd0k1OCn8N/H/lEiezx2.eNZ1Q8pXg5ONB3Uu', 'Lulu', 'Lulu', 'Lulu', 123456789),
(71, 'Lux@Lux.fr', '$2y$10$Qcte6jKRSI61shqO.ga1kujyBq58CFwgen2j3GqT2nJkKWyxSHkFe', 'Lux', 'Lux', 'Lux', 123456789),
(72, 'MaitreYi@MaitreYi.fr', '$2y$10$M5S/rv.d8ui1/uNmudDns.qvS7CnqBcNRrt52rsO9AWamzuIFlNSy', 'MaitreYi', 'MaitreYi', 'MaitreYi', 123456789),
(73, 'Malphite@Malphite.fr', '$2y$10$EQjDrEBXqgEYzGTY3MRS7uPI0kjZRk1SUc/CPjaclJK3k5z21Rrou', 'Malphite', 'Malphite', 'Malphite', 123456789),
(74, 'Malzahar@Malzahar.fr', '$2y$10$bMaZAVf/le5OXGH1UuQ0.u69e4prrXgKO.ivwcWzKgbpVMQLVst/u', 'Malzahar', 'Malzahar', 'Malzahar', 123456789),
(75, 'Maokai@Maokai.fr', '$2y$10$ciOnYi866MqLzMP0zSVmuuvDnun8qMGHrCvo0OjfKFz3Ykenp1WsS', 'Maokai', 'Maokai', 'Maokai', 123456789),
(76, 'MissFortune@MissFortune.fr', '$2y$10$44iwSoSbcO3mRO/mkr1odeL04dNaTa6UCtSi4isuIfbXRRVuTIhMO', 'MissFortune', 'MissFortune', 'MissFortune', 123456789),
(77, 'Mordekaiser@Mordekaiser.fr', '$2y$10$b8Fz9FXNUHnwMndiusA1/e2mgIu2Ievg5UK4W9HBncRGTd0Q6P5OO', 'Mordekaiser', 'Mordekaiser', 'Mordekaiser', 123456789),
(78, 'Morgana@Morgana.fr', '$2y$10$DnTH9/XfNgf2ZjhOXzGOUu6FVXLi8I3XdJyE5la.kv.srVYCk4O5K', 'Morgana', 'Morgana', 'Morgana', 123456789),
(79, 'Nami@Nami.fr', '$2y$10$P.x8vVfKD4/6CUdyuc9a.uxVyxRsvEHUFFQWfJmzoaqT0THxf3Ciq', 'Nami', 'Nami', 'Nami', 123456789),
(80, 'Nasus@Nasus.fr', '$2y$10$6duXW4rY4C8yI4/J2bFgmuX59alVjICyhV4F5q5vYqzyyoeHZ/ZH.', 'Nasus', 'Nasus', 'Nasus', 123456789),
(81, 'Nautilus@Nautilus.fr', '$2y$10$7ZSKuDO8utwbuRGQZgkDEeZExSOQ2qJF/P2w9K/FRXHztWhZifGeS', 'Nautilus', 'Nautilus', 'Nautilus', 123456789),
(82, 'Neeko@Neeko.fr', '$2y$10$b78P2YV2FjUn2dkB0dbS0O4dcvtjAIZj/7unYGIm6bra.3qZNSQ6i', 'Neeko', 'Neeko', 'Neeko', 123456789),
(83, 'ZywOo@ZywOo.fr', '$2y$10$pGfykkwHQGnNr3guMIlL.OBTwL7n3reYJdWTpgKl8gmS4bBolUp3.', 'ZywOo', 'ZywOo', 'ZywOo', 123456789),
(84, 's1mple@s1mple.fr', '$2y$10$XAZTwx/USCfjRd44g0rdauFmykYxIx.NZfd2XG44pcc2CldtQk83u', 's1mple', 's1mple', 's1mple', 123456789),
(85, 'device@device.fr', '$2y$10$IMtwwkruTBBWhTnixLnPZeeOpy0BdZN.s2L00cN9uuQJBsw7ph5X6', 'device', 'device', 'device', 123456789),
(86, 'EliGe@EliGe.fr', '$2y$10$7OBjv6h1aV5Wmgu8lljNq.uUnnYf3FYAPeRV1jShJkLVOACrmETbi', 'EliGe', 'EliGe', 'EliGe', 123456789),
(87, 'Magisk@Magisk.fr', '$2y$10$78syW3o1V5kUCAHdiBNvdejhtsrK7y512PfuVZHe5tp7vJ.n8Ft02', 'Magisk', 'Magisk', 'Magisk', 123456789),
(88, 'electronic@electronic.fr', '$2y$10$K9aVoS.XLit1IkYYctAXJuCFs3sN5r9kinLqCLxCsizZp.OspNCim', 'electronic', 'electronic', 'electronic', 123456789),
(89, 'NAF@NAF.fr', '$2y$10$eHIuzA85MZ6qJ1cOmhSZmOkBy36A1Y1VulhHaRBue16huUuxQg1mu', 'NAF', 'NAF', 'NAF', 123456789),
(90, 'Brehze@Brehze.fr', '$2y$10$EALuL0rE5wBAOQkpX1HcdeflLWuULPsh9uNXViYwHWIZ8zRirpM4.', 'Brehze', 'Brehze', 'Brehze', 123456789),
(91, 'Twistzz@Twistzz.fr', '$2y$10$VOdqp//kYLlK/sXUZkqO8.7Efs9VEVdqfKJU6cswccHn/lyaIEcFS', 'Twistzz', 'Twistzz', 'Twistzz', 123456789),
(92, 'ropz@ropz.fr', '$2y$10$aU8ZgqEmCs6trSMaj.4xiOfZB6utUEd6SixgOuNO8E8I9udpCrRBG', 'ropz', 'ropz', 'ropz', 123456789),
(93, 'Niko@Niko.fr', '$2y$10$RAntsEaR4WzLaneyynJBLOWWMWEm7AN0fonDA2keDacSTgbUJ2YI.', 'Niko', 'Niko', 'Niko', 123456789),
(94, 'woxic@woxic.fr', '$2y$10$9TH9BNIidZnN46AbNAa4bOHUA2TQ71t2s3WNW8InT.oNTJfcE7RT.', 'woxic', 'woxic', 'woxic', 123456789),
(95, 'sergej@sergej.fr', '$2y$10$/3GovM1qq0/rerpYblfJsu9.mAm9XptwxzQYfdOzvTvG7qCxHdunm', 'sergej', 'sergej', 'sergej', 123456789),
(96, 'Xyp9x@Xyp9x.fr', '$2y$10$I/01Wr8wKXY.dmygXPhgReesHtHYmgOuchg8Es0Ia6fCh9Oz9rHGW', 'Xyp9x', 'Xyp9x', 'Xyp9x', 123456789),
(97, 'jks@jks.fr', '$2y$10$WusBaMBCQFteb7rFvLq04OOi38RDHelE.EK2w2s6mYGzZZKPyo8tu', 'jks', 'jks', 'jks', 123456789),
(98, 'dupreeh@dupreeh.fr', '$2y$10$pX3T4y1VHt6ZakKwdC1uv.CQMFya.DQ/5WqSZ34stcHJ//ClgwCfi', 'dupreeh', 'dupreeh', 'dupreeh', 123456789),
(99, 'KRIMZ@KRIMZ.fr', '$2y$10$lyyTARdrTBwqhEgNtQVrceQkNR3lLEPNaRqqRgAL8nTNzkuA/rtJS', 'KRIMZ', 'KRIMZ', 'KRIMZ', 123456789),
(100, 'CeRq@CeRq.fr', '$2y$10$s/nqFBb77t4GOM4bLF2nP.OQB2sIwA3dcnJVNHtzoHif1kMc0nE8e', 'CeRq', 'CeRq', 'CeRq', 123456789),
(101, 'Brollan@Brollan.fr', '$2y$10$BoZQkOI1/duWoOnj.6ce6.yPFMK0cAAY4Eswsy1pc7WwsFLMEO7e.', 'Brollan', 'Brollan', 'Brollan', 123456789),
(102, 'Ethan@Ethan.fr', '$2y$10$Rcf4NP3xjTs4dpl5wEuhje1FtQF7BXMIXgZ6ODs0hHFOtySrIG7xS', 'Ethan', 'Ethan', 'Ethan', 123456789),
(103, 'Sephiroth@Sephiroth.fr', '$2y$10$6O9T.oQbjJdmuK6LSq5sF.Om4RC1l9DwZ8K1RSIRro6YNgBuN.eU6', 'Sephiroth', 'Sephiroth', 'Sephiroth', 123456789),
(104, 'CloudStrike@Strike.fr', '$2y$10$SwYWkn3ApizoOqsgh/KUWeswl2am44IO1yBuXUJx2Il4IEPk0wazS', 'Cloud', 'Cloud', 'Strike', 123456789),
(105, 'AranSamus@Samus.fr', '$2y$10$UfPmnIMcGl01mP/nbNLlCe0EZBtpzl5pOz0nrBMGf90VjGd8gnEQq', 'Samus', 'Aran', 'Samus', 123456789),
(106, 'Ridley@Ridley.fr', '$2y$10$XKTuHgVfQuFwvJAnkgmRyO1ieMBtnR0UcFn/EmfzzJhCaI5M2FjUG', 'Ridley', 'Ridley', 'Ridley', 123456789),
(107, 'Mario@Mario.fr', '$2y$10$G2OgkP0jnZMRZsehaDEtwuhFQy0E681a43joy0giQw6hcWWRf4ax2', 'Mario', 'Mario', 'Mario', 123456789),
(108, 'Luigi@Luigi.fr', '$2y$10$qzkW71/l8j0J0QEjU99WgeREf1ZHawpdiMyy67TTrirGkmWL6Dkhi', 'Luigi', 'Luigi', 'Luigi', 123456789),
(109, 'Pikachu@Pikachu.fr', '$2y$10$IMiFyoNw/Ps7jVroGK7Ri.5SG7Y.Ya/ohU1GPtgmDEiRIVFKXYmzW', 'Pikachu', 'Pikachu', 'Pikachu', 123456789),
(110, 'Pichu@Pichu.fr', '$2y$10$MzTIcpV40F91dJTRX2xXourutciAgBrYNuXWml0Tom8hRXyLlr19K', 'Pichu', 'Pichu', 'Pichu', 123456789),
(111, 'Fox@Fox.fr', '$2y$10$qqpfDxaS1GGLc4vrL.9iVualWkDHQQKs8z6FehNDHnrlpZYtnUFfO', 'Fox', 'Fox', 'Fox', 123456789),
(112, 'Falco@Falco.fr', '$2y$10$9FuxIpNNQR90JYBpuRC4pO9SFklaC80ZOXYJufmQEJYgrQ6RkbPLK', 'Falco', 'Falco', 'Falco', 123456789),
(113, 'DonkeyKong@DonkeyKong.fr', '$2y$10$EFAcTugVaYNgs9G6mSlo7ugJsRshhNX.wt2VkRCsN9j6PUiPveTe.', 'DonkeyKong', 'DonkeyKong', 'DonkeyKong', 123456789),
(114, 'DiddyKong@DiddyKong.fr', '$2y$10$V8doHmrmRikm9DEpu53gjO7fCVfnI6wYtHNSNIs1TdxaaFJpfQU/y', 'DiddyKong', 'DiddyKong', 'DiddyKong', 123456789),
(115, 'Link@Link.fr', '$2y$10$6a1wSuiFxiSSX6l9i8lCCe3kVubvMUG7eVd3Tif8n66Vow/MuYkcC', 'Link', 'Link', 'Link', 123456789),
(116, 'Link_Cartoon@Link.fr', '$2y$10$g9TTd5O3nR3KvsGvme/GselslgyRDicBsG/qvTO1dPn/qh3ABCYRq', 'Link Cartoon', 'Link Cartoon', 'Link Cartoon', 123456789),
(117, 'Zelda@Zelda.fr', '$2y$10$qi/CisCHoszaXmkDoadPP.OKPwlHo.Ptb8slTB0Y/sUIA9gqOBNxW', 'Zelda', 'Zelda', 'Zelda', 123456789),
(118, 'Sheik@Sheik.fr', '$2y$10$0rrsf/jNVVTZUGatgtcJeeral6DEARVMRVbM/m74e89RIuZibkiu.', 'Sheik', 'Sheik', 'Sheik', 123456789),
(119, 'Peach@Peach.fr', '$2y$10$/u4F20vDwQLC5DeowuEJ4.4T9fFvvJ0V68VAWOg7qkohR4PIynJxO', 'Peach', 'Peach', 'Peach', 123456789),
(120, 'Daisy@Daisy.fr', '$2y$10$AJ7f/qPefXR6v0lRFCfxnutLiz7doQSx7C.JxHv.6/0nW.4HKFPgW', 'Daisy', 'Daisy', 'Daisy', 123456789),
(121, 'Marth@Marth.fr', '$2y$10$PGNCuF94k.zwZXIDq8aLrOCJzhuod/.bE0VJqQ.KA1QF.8LJF.jly', 'Marth', 'Marth', 'Marth', 123456789),
(122, 'Lucina@Lucina.fr', '$2y$10$msMC88cR6b8Jnd/QHO3eBOr7QKm53wbh55eXteD111xavPW5oePrO', 'Lucina', 'Lucina', 'Lucina', 123456789),
(123, 'Kirby@Kirby.fr', '$2y$10$3vCx3uTf1jzxGQ0yjpgvo.C6ax1fMfuhOxvX0WNhj..PdS2RtuYJ2', 'Kirby', 'Kirby', 'Kirby', 123456789),
(124, 'Meta_knight@meta.fr', '$2y$10$aAD4SuXSALlry75T6I/3HOXKCXDol.uo7zV1MMaG/hgo7tqVUbmxm', 'Meta knight', 'Meta knight', 'Meta knight', 123456789),
(125, 'Kid@Kid.fr', '$2y$10$s/713jH91sGBRwS7guo/Yegmx6Y7YzUGNVveTeNItT8HS.mDA21ke', 'Kid', 'Kid', 'Kid', 123456789),
(126, 'Palutena@Palutena.fr', '$2y$10$F83xX91ip.uFUgGJEDwF5eIRcph14HgKMOodvF4VAC33jD9qMusaq', 'Palutena', 'Palutena', 'Palutena', 123456789),
(127, 'MewTwo@MewTwo.fr', '$2y$10$OqiUpaWupCknIlorWgNMP.Ndd6sxBwPGtpx9No5LpVOdhmDpM7OYG', 'MewTwo', 'MewTwo', 'MewTwo', 123456789),
(128, 'Red@Red.fr', '$2y$10$ioFO7KYAa81ISJ1B.65hIeTUDQkYW/Sbf0IvdQwMmZWaNET7NGTEC', 'Red', 'Red', 'Red', 123456789),
(129, 'LinkEnfant@LinkEnfant.fr', '$2y$10$4awY/21ARxFCIbbYK46G/.7/FtJG/mVFT5S5/pP9/qJAFkWRURXGG', 'LinkEnfant', 'LinkEnfant', 'LinkEnfant', 123456789),
(130, 'Ganondorf@Ganondorf.fr', '$2y$10$RzCZcZSOcaMib068qVRwxu9FqvukG5ADp2zBGYburmWAbTTo/ct7u', 'Ganondorf', 'Ganondorf', 'Ganondorf', 123456789),
(131, 'Roy@Roy.fr', '$2y$10$.UKojl/Jw13wSnrPzvTQXuLl1nsbS8T5tV8xGFxX1.g.IF.Gvy2ZC', 'Roy', 'Roy', 'Roy', 123456789),
(132, 'Chrom@Chrom.fr', '$2y$10$ROYpcoIJ5Hp/QiELBeLcwuPnSBUA1ug0A8J1uQo1XmwoJMpy/SKpS', 'Chrom', 'Chrom', 'Chrom', 123456789),
(133, 'Ness@Ness.fr', '$2y$10$BVxk1vgDgVZ7sKbUqOSlsuUphdoaRu27USBSAvaDBUhOZfEXfbIyG', 'Ness', 'Ness', 'Ness', 123456789),
(134, 'Lucas@Lucas.fr', '$2y$10$snsULnVMoK1i1Xi995lCS.WdilV/COCwijzedyciBys/XNml.zvQG', 'Lucas', 'Lucas', 'Lucas', 123456789),
(135, 'IceClimbers@IceClimbers.fr', '$2y$10$b6fX1v.HtyzGZBWffaati.iXqKsdBpsExUNds7D9WfTRIvhUlZHE6', 'IceClimbers', 'IceClimbers', 'IceClimbers', 123456789),
(136, 'wario@wario.fr', '$2y$10$4a.GR4JC382EK9d3j5MxEeggtio98cFwydNBro9UQ6xMMwKKVZ8B6', 'wario', 'wario', 'wario', 123456789),
(137, 'Bowser@Bowser.fr', '$2y$10$3Uc459Sz0RDDKYlRD0z/W.ZYVQ7z2FzdQNE5.cM86IM6VbaeChbAe', 'Bowser', 'Bowser', 'Bowser', 123456789),
(138, 'BowserJr@BowserJr.fr', '$2y$10$YdWWSFrIrF6Ni.1s7iJvk.fFVPMHK8cGj.hjnV2TmAIHNlYd9Xr46', 'BowserJr', 'BowserJr', 'BowserJr', 123456789),
(139, 'Yoshi@Yoshi.fr', '$2y$10$cvb4BnBsb85QYuHc/vBREuQ8iLmoJXQfGKCf8ejvInIUsIOZFM4zq', 'Yoshi', 'Yoshi', 'Yoshi', 123456789),
(140, 'harmonie&Luna@harmonieLuna.fr', '$2y$10$OoVcaxMu4fWqMr/gOTC7POdELw8NRhqqZTU3IeyT9CqGDkqMxec9G', 'harmonie&Luna', 'harmonie&Luna', 'harmonie&Luna', 123456789),
(141, 'CaptainFalcon@CaptainFalcon.fr', '$2y$10$N/jDPdUPShNhUgazew9K.u1vVmaV5gOKWpW2UPaa8TXDzyhPXe32i', 'CaptainFalcon', 'CaptainFalcon', 'CaptainFalcon', 123456789),
(142, 'MrGame&Watch@MrGameWatch.fr', '$2y$10$CMFwtmfC5sJ83B9L/tnusewKHcYbuFN/cb6pb/Fo7DYHacg2JP6du', 'MrGame&Watch', 'MrGame&Watch', 'MrGame&Watch', 123456789),
(143, 'Snake@Snake.fr', '$2y$10$4WjFQEGHJe5V5XIXlyc8tOgPpErKlvGl8/3G8J4j2NIg1miUDWFb6', 'Snake', 'Snake', 'Snake', 123456789),
(144, 'SamusSansArmure@SamusSansArmure.fr', '$2y$10$pCM/xBzM1f6n0eRlWyHU7e3HoyvBgaieKpgJTpS2tF8ezqeHGx7OG', 'SamusSansArmure', 'SamusSansArmure', 'SamusSansArmure', 123456789),
(145, 'Wolf@Wolf.fr', '$2y$10$snmgLz.GmXGPq85tvI/1We89jFP67kc2oY6RVDOwe3RPzl8USvaN6', 'Wolf', 'Wolf', 'Wolf', 123456789),
(146, 'Ike@Ike.fr', '$2y$10$XA8hiZdGXaWKc98DhX6htOxVJGaM6mAGej6aYOXv1NncJppmMqMWi', 'Ike', 'Ike', 'Ike', 123456789),
(147, 'RoiDadidou@RoiDadidou.fr', '$2y$10$Qob21EhnrDI0p7jtbVtkTeYZk2zVDxINxxiGJeeVZ2VaZKPjo1Xya', 'RoiDadidou', 'RoiDadidou', 'RoiDadidou', 123456789),
(148, 'PitMalefique@PitMalefique.fr', '$2y$10$7.D/j3aOnujQECcnX5CO0OcjiSqFXusrx6RYzyV8lrstHMfDfoBtO', 'PitMaléfique', 'PitMaléfique', 'PitMaléfique', 123456789),
(149, 'Sonic@Sonic.fr', '$2y$10$OB5xfTAbVcjaEkBdi.5mKOKKE0I1D1NlZ6A/yT43tXUg6xE.oyK76', 'Sonic', 'Sonic', 'Sonic', 123456789),
(150, 'Lucario@Lucario.fr', '$2y$10$GCTQov4/4HwPTMeZwEIdzurFWakgOoAIQgpE9KV3ePD9ibOvIeo0a', 'Lucario', 'Lucario', 'Lucario', 123456789),
(151, 'R.O.B@R.O.B.fr', '$2y$10$6RumcAyh0hjUxsCAwPut1uohMl/ibcn4Nm0XEMjx/S.D2cfmlkZP6', 'R.O.B.', 'R.O.B.', 'R.O.B.', 123456789),
(152, 'MegaMan@MegaMan.fr', '$2y$10$k.HVsS64H11/a4RI//qHxebl1h2LCAxdDOok6nV1nxbMR2XK1t65O', 'MegaMan', 'MegaMan', 'MegaMan', 123456789),
(153, 'Olimar@Olimar.fr', '$2y$10$hG0L3dcGbPXu02YH2bQW9OCRx/WlWwi9HPuFwQ3F4YKMuo4mGHZ0O', 'Olimar', 'Olimar', 'Olimar', 123456789),
(154, 'Mii@Mii.fr', '$2y$10$JUIZ0g.adJvZpn0MIYooTeHg9M6SWUUa6mbVH.ce6ybaKSqdjirBy', 'Mii', 'Mii', 'Mii', 123456789),
(155, 'Amphinobi@Amphinobi.fr', '$2y$10$hdC8.j1rVoP7vM4N6HR68..yj8wAG3zQ43wYWbx4VUKgJSLAg0zJ6', 'Amphinobi', 'Amphinobi', 'Amphinobi', 123456789),
(156, 'EntraineurWiiFit@EntraineurWiiFit.fr', '$2y$10$YhXxzpiftFKOuZaun3VSk..n9Yyz2Ru31qv42UYiVdjULoIJW4OY.', 'EntraineurWiiFit', 'EntraineurWiiFit', 'EntraineurWiiFit', 123456789),
(157, 'Pacman@Pacman.fr', '$2y$10$nz0j7Qp.QDKjhqkWSg7LlOGv76o4fMG5IEjKFHqqKu9iGDfwt01A6', 'Pacman', 'Pacman', 'Pacman', 123456789),
(158, 'Villageois@Villageois.fr', '$2y$10$ehefCe1rrQObkVv7lk3ug.dm05HjQlgc4ySovsp846JxF/GXQbttu', 'Villageois', 'Villageois', 'Villageois', 123456789),
(159, 'Felinferno@Felinferno.fr', '$2y$10$/zxhSXlJ.GtgZe8mfBMpw.Oq461uAGSFXDjClKw3GAkv2kd3db0va', 'Felinferno', 'Felinferno', 'Felinferno', 123456789),
(160, 'Ryu@Ryu.fr', '$2y$10$1I0RYxlceaNWQd37HcbbJ.ITWMi1DVhcVia/IW7LK134iu3YrGghK', 'Ryu', 'Ryu', 'Ryu', 123456789),
(161, 'Ritcher@Ritcher.fr', '$2y$10$lK0yuiNWW7hJT4JprwkNH.3lmI5h1qyocxhqJWz461v0dVmIMLZj.', 'Ritcher', 'Belmont', 'Ritcher', 123456789),
(162, 'Simon@Simon.fr', '$2y$10$JZh2tgMch6ZpjRs/O76/Hu9SPtqP.VXceinYHwKEiqkvS/4CnzVKC', 'Simon', 'Belmont', 'Simon', 123456789),
(163, 'DuoDuckHunt@DuoDuckHunt.fr', '$2y$10$ye41VtQcWxrUXJ//CArr/.RKiIvpcm07pFiVuCj6Ad3blDt6ohKLG', 'DuoDuckHunt', 'DuoDuckHunt', 'DuoDuckHunt', 123456789),
(164, 'Bayonetta@Bayonetta.fr', '$2y$10$1UZWlq5PSARQgLwVMIFmWuwiYgl.Ftf5C8ruWMlmaYhrGHST/MnIe', 'Bayonetta', 'Bayonetta', 'Bayonetta', 123456789),
(165, 'Daraen@Daraen.fr', '$2y$10$1l99JUXnlj2ENG0y75HWQeg17Vt3v6/ZTcSFZPscKAKik.WEeGbQK', 'Daraen', 'Daraen', 'Daraen', 123456789),
(166, 'Corrin@Corrin.fr', '$2y$10$qjBqI66IuUNLg.LiCwVLoensjdKFX0q07JCkimn6AfQ2gU8ppOi/G', 'Corrin', 'Corrin', 'Corrin', 123456789),
(167, 'LittleMac@LittleMac.fr', '$2y$10$LHe/JnT.9zq8XZ5SnYCIY.yw1NutmnEQ6qkQs2hWohucTNbWzTHE6', 'LittleMac', 'LittleMac', 'LittleMac', 123456789),
(168, 'KingKRool@KingKRool.fr', '$2y$10$dKZ2zoG7v7cJSc/aFJ1wvOly/BIKooyz4Vo3Jt6lDeVpGlfVRWjoW', 'KingKRool', 'KingKRool', 'KingKRool', 123456789);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`idEquipe`);

--
-- Indexes for table `joueur`
--
ALTER TABLE `joueur`
  ADD PRIMARY KEY (`idJoueur`);

--
-- Indexes for table `poule`
--
ALTER TABLE `poule`
  ADD PRIMARY KEY (`idPoule`);

--
-- Indexes for table `rencontre`
--
ALTER TABLE `rencontre`
  ADD PRIMARY KEY (`idMatch`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `tournois`
--
ALTER TABLE `tournois`
  ADD PRIMARY KEY (`idTournois`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `idEquipe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT for table `joueur`
--
ALTER TABLE `joueur`
  MODIFY `idJoueur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `poule`
--
ALTER TABLE `poule`
  MODIFY `idPoule` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rencontre`
--
ALTER TABLE `rencontre`
  MODIFY `idMatch` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=337;

--
-- AUTO_INCREMENT for table `tournois`
--
ALTER TABLE `tournois`
  MODIFY `idTournois` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
