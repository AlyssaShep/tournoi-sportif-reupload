<!DOCTYPE html>
<!-- modification des liens vers les autres pages pour garder l authentification -->
<?php
      // include 'controller/functions_connextions.php';
      if(!isset($_GET["pseudo"])) {
          session_start();
      } ?>
<html lang="fr" dir="ltr">

  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="assets/images/logo.png"/>
    <link rel="stylesheet" href="assets/mainCSS.css" />
    <link rel="stylesheet" href="assets/accueil.css" />
    <title>Tournois Sportif</title>
  </head>

  <body>

    <div class="caroussel" ></div>

    <?php if(!isset($_GET["pseudo"])){ ?>
        <div class="rectAccueil">
              <h2 class="fieldAccueil"><br />
                <a href="vue/connexion.php"><button class="buttonAccueil">Créer un tournoi</button></a> <br /><br /><br />
                <a href="vue/connexion.php"><button class="buttonAccueil">Créer mon équipe</button></a>
              </h2>
        </div>

    <?php }else { ?>

      <div class="rectAccueil">
            <h2 class="fieldAccueil"><br />
              <?php echo '<a href="vue/creation_competition.php?pseudo='.$_GET["pseudo"].'"><button class="buttonAccueil">Créer un tournoi</button></a> <br /><br /><br />' ?>
              <?php echo '<a href="vue/creation_equipe.php?pseudo='.$_GET["pseudo"].'"><button class="buttonAccueil">Créer mon équipe</button></a>' ?>

            </h2>
      </div>

    <?php } ?>
    <?php
    include 'vue/sidebarA.php';
    include 'vue/headerA.php';
    ?>
  </body>
</html>
