<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <title>Ajout des équipes</title>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/ajouterEquipe.css" />
  </head>
  <body>
    <?php
    include '../controller/functions_ajouter_equipes_monTournois.php';
    $idTournois = $_GET['id'];
    $Esport = Esport($idTournois);
    // echo "$Esport";
    $places_restantes = nbres_places_restantes($idTournois);
    equipes_aleatoire($idTournois, $places_restantes, $Esport);
    $equipes = preinscription_tournois($idTournois);
    $nbr_equipes_total = count($equipes[0]);
    $equipes_pour_ajout = 0;
    for ($i=0; $i < $nbr_equipes_total ; $i++) {
      $info_equipe = info_equipe($equipes[0][$i]['idEquipe']);
      $size = count($info_equipe[0]);
      for ($j=0; $j < $size; $j++) {
        $inscrits = equipes_inscriptes($idTournois, $info_equipe[0][$j]['idEquipe']);
        if(!$inscrits){
          $equipes_pour_ajout++;
        }
      }
    }
     ?>

<?php if($places_restantes == 0){
  $liberation_equipes = liberation_equipe($idTournois);
  ?>
  <div class = "rect1">
  <p>Votre tournois est déjà complet.</p>
  <a href="<?php echo "mesTournois.php?pseudo=".$_GET['pseudo']; ?>"> <button class="bouton">Mes Tournois</button> </a>
  </div>
<?php } else {
             if($equipes_pour_ajout == 0){ ?>
              <!-- mettre ca dans une grosse division au millieu  -->
              <div class = "rect1">
              <p>Vous n'avez pas encore d'équipes à ajouter</p>
              <a href="<?php echo "mesTournois.php?pseudo=".$_GET['pseudo']; ?>"> <button class="bouton">Mes Tournois</button> </a>
             </div>
      <?php }
            else { ?>
            <div class = "rect2">
                  <form class="" action="../controller/functions_ajouter_equipes_monTournois.php" method="post">
                    <input type="hidden" name="pseudo" value="<?php echo $_GET['pseudo']; ?>">
                    <input type="hidden" name="idTournois" value="<?php echo $_GET['id']; ?>">
                    <input type="hidden" name="nbr_equipes_total" value="<?php echo $nbr_equipes_total; ?>">
                    <input type="hidden" name="places_restantes" value="<?php echo $places_restantes; ?>">

                    <?php
                    for ($i=0; $i < $nbr_equipes_total ; $i++) {
                      $info_equipe = info_equipe($equipes[0][$i]['idEquipe']);
                      $size = count($info_equipe[0]); ?>

              <?php   for ($j=0; $j < $size; $j++) {
                      $inscrits = equipes_inscriptes($idTournois, $info_equipe[0][$j]['idEquipe']);
                        if($i == 0){
                            ?>
                            <p>Vous pouvez ajouter jusqu'à <?php echo $places_restantes; ?> équipes à votre compétition</p>
                            <p>Choisissez les équipes que vous voulez ajouter à votre compétition</p>
                            <table>
                              <col span="7">
                              <tr>
                                <th>Nom équipe</th>
                                <th>niveau</th>
                                <th>adresse équipe</th>
                                <th>équipe téléphone</th>
                                <th>nombre de joueurs</th>
                                <th>Pseudo Capitaine</th>
                                <th>Ajouter à mon Tournois</th>
                              </tr>
                  <?php }
                        if(!$inscrits){ ?>
                            <input type="hidden" name="<?php echo "idEquipe".$i; ?>" value="<?php echo $info_equipe[0][$j]['idEquipe']; ?>">
                              <tr>
                                <td><?php echo $info_equipe[0][$j]['nom_equipe']; ?></td>
                                <td><?php echo $info_equipe[0][$j]['niveau']; ?></td>
                                <td><?php echo $info_equipe[0][$j]['adresse_equipe']; ?></td>
                                <td><?php echo "0".$info_equipe[0][$j]['equipe_tel']; ?></td>
                                <td><?php echo $info_equipe[0][$j]['nb_joueur']; ?></td>
                                <td><?php echo info_capitaine($info_equipe[0][$j]['idCapitaine'])[0][0]['pseudo']; ?></td>
                                <td><input type="checkbox" name="<?php echo "Ajout".$i; ?>"/></td>
                              </tr>
                <?php   }
                      }
                    }
                       ?>
                            </table>
                          </div>
                          <div class = "buttonpos">
                          <input class = "boutonAjout" type="submit" name="ajout" value="Ajouter">
                          </div>
                  </form>

     <?php }
        } ?>
     <?php
     include 'sidebar.php';
     include 'header.php';
    ?>

  </body>
</html>
