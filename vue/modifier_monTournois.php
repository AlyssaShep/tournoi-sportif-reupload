<!DOCTYPE html>
<?php if(!isset($_GET["pseudo"])) {
          session_start();
      }
      $pseudo = $_GET['pseudo'];
      $idTournois = $_GET['id'];
      include '../controller/functions_modifier_monTournois.php';
?>
<html lang="fr" dir="ltr">

  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
     <link rel="stylesheet" href="../assets/modifTournois.css" />
    <title>Modification Tournois</title>
  </head>

<!-- modification impossible si le tournois a commencé -->

  <body>
    <form class="" action="../controller/functions_modifier_monTournois.php" method="post">

      <div class="rect">
        <br class="rectbr">

      <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
      <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">

      <p class="onePerLine">
      Date de début <input type="date" name="dateDebut" value="<?php echo $dateD; ?>"> </p>

       <p class="onePerLine">
       Durée <input type="number" min="1" name="duree" value="<?php echo $duree; ?>"> </p>

      <p class="onePerLine">
      Lieu <input type="text" name="lieu" value="<?php echo $lieu; ?>"> </p>

      <p class="onePerLine">
      Type de compétition
              <select name="Competition" id="tipe" onchange="js()">
              <option value="tournois" <?php if($type == "tournois"){ ?> selected <?php } ?>>Tournoi</option>
              <option value="championnat" <?php if($type == "championnat"){ ?> selected <?php } ?>>Championnat</option>
              <option value="coupe" <?php if($type == "coupe"){ ?> selected <?php } ?>>Coupe</option>
              </select></p>

      <p class="onePerLine">
      Nom du tournois <input type="text" name="Nom_Tournois" value="<?php echo $nom_tournois; ?>"> </p>
      <p class="onePerLine">
      Nombre d'équipes <input type="number" min="2" onchange="js()" id="equipeN" name="Nbr_equipes" value="<?php echo $nbr_equipes; ?>"> </p>
      <p class="onePerLine">
      Esport            <select name="Esport">
                                <option value="LOL" <?php if($Esport == "LOL"){ ?> selected <?php } ?>>League of Legends</option>
                                <option value="CS" <?php if($Esport == "CS"){ ?> selected <?php } ?>>Counter-Strike</option>
                                <option value="SB" <?php if($Esport == "SB"){ ?> selected <?php } ?>>Smash Bross</option>
                          </select> </p>

      <p class="onePerLine">
      Résultats aléatoires ?
              <input type="radio" name="resultat_aleatoire" id="choix2ra" value="non" <?php if($resultats_aleatoires == false){ ?> checked <?php } ?> />
              <label for="choix2ra">Non</label>
              <input type="radio" name="resultat_aleatoire" id="choix1ra" value="oui" <?php if($resultats_aleatoires == true){ ?> checked <?php } ?> />
              <label for="choix1ra">Oui</label> </p>


      <p class="onePerLine">
      Équipes aléatoires ?
              <input type="radio" name="equipe_aleatoire" id="choix2ea" value="non" <?php if($equipes_aleatoires == false){ ?> checked <?php } ?>/>
              <label for="choix2ea">Non</label>
              <input type="radio" name="equipe_aleatoire" id="choix1ea" value="oui" <?php if($equipes_aleatoires == true){ ?> checked <?php } ?>/>
              <label for="choix1ea">Oui</label> </p>


        <div class="validateRect">
            <input type="submit" name="modifier" value="Modifier" class="validateButton">
        </div>
        </div>

        <script "text/javascript">
            function js() {

                    var t = document.getElementById("tipe").value;

                    if(t == "tournois"){
                        document.getElementById("equipeN").min = 2;
                        document.getElementById("equipeN").max = 32;
                    }
                    if(t == "championnat"){
                        document.getElementById("equipeN").min = 2;
                        document.getElementById("equipeN").max = 20;
                    }
                    if(t == "coupe"){
                        document.getElementById("equipeN").min = 2;
                        document.getElementById("equipeN").max = 16;
                    }

                    if(t == "tournois" || t == "coupe")
                    {
                        var two = 2;
                        for(let i = 1; i <= 5 ; i++)
                        {
                            if(document.getElementById("equipeN").value <= two)
                            {
                                document.getElementById("equipeN").value = two;
                                break;
                            }
                            two = two * 2;
                        }
                    }
                    else{
                        var r = document.getElementById("equipeN").value % 2;
                        document.getElementById("equipeN").value = document.getElementById("equipeN").value - r;
                    }
                }
        </script>
    </form>

    <?php
    include 'sidebar.php';
    include 'header.php';
    ?>
  </body>
</hmtl>
