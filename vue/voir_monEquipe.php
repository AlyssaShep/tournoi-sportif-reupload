<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mon équipe</title>
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/monCompte.css" />
  </head>
  <body>
  <div class="pageName">
      <h1>Information de l'équipe</h1>
  </div>
    <?php
    include '../controller/functions_mesEquipes.php';
    if(isset($_GET["pseudo"])){
      $pseudo = $_GET["pseudo"];
      $idEquipe = $_GET["id"];
      $infoEquipe = get_info_equipe($idEquipe);
      $size = count($infoEquipe[0]);
      // var_dump($sizeIJ);
    }?>

    <div class="rect2">
      <form action="../controller/functions_mesEquipes.php" method="post">
        <input type="hidden" name="idEquipe" value="<?php echo $idEquipe; ?>">
        <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">

          <h2>Nom de l'équipe <input type="text" name="nom" value="<?php echo $infoEquipe[0][0]['nom_equipe']; ?>" required> </h2>
          <h2>Niveau <input type="number" name="" disabled="disabled" value="<?php echo $infoEquipe[0][0]['niveau']; ?>" required> </h2>
          <h2>Email <input type="email" name="mail" value="<?php echo $infoEquipe[0][0]['adresse_equipe']; ?>" required> </h2>
          <h2>Téléphone <input type="tel" name="telephone" value="<?php echo "0".$infoEquipe[0][0]['equipe_tel']; ?>" pattern="[0-9]{10}" required> </h2>
          <h2>Nombre de victoire <input type="number" name="" disabled="disabled" value="<?php echo $infoEquipe[0][0]['nb_victoire']; ?>" required> </h2>
          <h2>Capitaine <input type="text" name="" disabled="disabled" value="<?php echo $pseudo; ?>" required> </h2>
          <h2>Esport <input type="text" name="" disabled="disabled" value="<?php echo $infoEquipe[0][0]['Esport']; ?>" required> </h2>

         <input type="submit" name="Modifier" class="bouton" value="Modifier">
      </form>
    </div>


    <?php
     include 'sidebar.php';
     include 'header.php';
    ?>
  </body>
</html>
