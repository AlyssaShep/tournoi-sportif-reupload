<!DOCTYPE html>
<?php if(!isset($_GET["pseudo"])) {
          session_start();
      }
      ?>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <title>Creation Equipe</title>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/creation_equipe.css" />
  </head>
  <body>
    <?php
    $pseudo = $_GET["pseudo"];
    include '../controller/functions_monCompte.php'; ?>

    <form method="post" action="../controller/functions_creation_equipe.php">

      <p><input type="hidden" name="idTournois"/></p>

      <div>

        <div class="rect1">
            <p class="onePerLine">  Nom de l'équipe    <input type="text" name="Nom_Tournois" required/><br/></p>

            <p class="largeField"> Esport             <select name="Esport" onchange="nbjtest()" id="esportValue" ><br/>
                            <option value="LOL">League of Legends</option>
                            <option value="CS">Counter-Strike</option>
                            <option value="SB">Smash Bross</option>
                         </select></p> <br/>

            <p class="onePerLine"> Niveau de l'équipe  <input type="number" name="Niveau" min="1" max="100" required/><br/></p>
            <p class="onePerLine"> Email de l'équipe   <input type="email" name="Email" required/><br/></p>
            <p class="onePerLine"> Numero de l'équipe  <input type="text" name="Numero" pattern="[0-9]{10}" required/><br/></p>
        </div>

        <div class="rect2">

            <p class="largeField"> Nombre de Joueurs  <input type="number" id="nbj" disabled="disabled" min="1" max="5"  required/> </p><br/>
                                                      <input type="hidden" value="nbj" name="nb_joueurs"/>
            <div>
                <p class="twoPerLine">
                    Capitaine :

                  <input type="hidden" name="pseudoCapitaine" value="<?php echo $pseudo; ?>" required/>
                  <input type="hidden" name="nomCapitaine" value="<?php echo $nom; ?>" required/>
                  <input type="hidden" name="prenomCapitaine" value="<?php echo $prenom; ?>" required/>
                  <input type="hidden" name="emailCapitaine" value="<?php echo $mail; ?>" required/>
                  <input type="hidden" name="NumeroCapitaine" value="<?php echo $num; ?>" required />

                    Pseudo   <input type="text" disabled="disabled" value="<?php echo $pseudo; ?>" required/></p>

                <p class="twoPerLine">
                    Nom      <input type="text" disabled="disabled"  value="<?php echo $nom; ?>" required/>
                    Prenom   <input type="text" disabled="disabled" value="<?php echo $prenom; ?>" required/></p>
                <p class="twoPerLine">
                    Email    <input type="email" disabled="disabled" value="<?php echo $mail; ?>" required/>
                    Numero   <input type="text" disabled="disabled" value="<?php echo $num; ?>" required /> </p>
            </div>

            <div id="eq2">
                <p class="twoPerLine">
                    Equipier 2 :
                    Pseudo   <input type="text" name="pseudo2" required/>
                    </p>
                <p class="twoPerLine">
                    Nom   <input type="text" name="nomEP2" required/>
                    Prenom   <input type="text" name="prenomEP2" required/></p>
                <p class="twoPerLine">
                    Email    <input type="email" name="emailEP2" />
                    Numero   <input type="text" name="NumeroEP2" /></p>
            </div>

            <div id="eq3">
                <p class="twoPerLine">
                    Equipier 3 :
                    Pseudo   <input type="text" name="pseudo3" /></p>
                <p class="twoPerLine">
                    Nom   <input type="text" name="nomEP3" />
                    Prenom   <input type="text" name="prenomEP3" /></p>
                <p class="twoPerLine">
                    Email    <input type="email" name="emailEP3" />
                    Numero   <input type="text" name="NumeroEP3" /></p>
            </div>

            <div id="eq4">
                <p class="twoPerLine">
                    Equipier 4 :
                     Pseudo   <input type="text" name="pseudo4" /></p>
                <p class="twoPerLine">
                    Nom   <input type="text" name="nomEP4" />
                    Prenom   <input type="text" name="prenomEP4" /></p>
                <p class="twoPerLine">
                    Email    <input type="email" name="emailEP4" />
                    Numero   <input type="text" name="NumeroEP4" /></p>
            </div>

            <div id="eq5">
                <p class="twoPerLine">
                    Equipier 5 :
                     Pseudo   <input type="text" name="pseudo5" /></p>
                <p class="twoPerLine">
                    Nom   <input type="text" name="nomEP5" />
                    Prenom   <input type="text" name="prenomEP5" /></p>
                <p class="twoPerLine">
                    Email    <input type="email" name="emailEP5" />
                    Numero   <input type="text" name="NumeroEP5" /></p>
            </div>

            <script "text/javascript">
            function nbjtest() {

                    var espo = document.getElementById("esportValue").value;

                    if(espo == "LOL"){
                        document.getElementById("nbj").value = 5;
                    }
                    if(espo == "CS"){
                        document.getElementById("nbj").value = 5;
                    }
                    if(espo == "SB"){
                        document.getElementById("nbj").value = 2;
                    }

                    var nb = parseInt(document.getElementById("nbj").value);

                    if(Number.isInteger(nb))
                    {
                        if(nb >= 2){document.getElementById("eq2").style.display = "block"; }else{document.getElementById("eq2").style.display = "none"; }
                        if(nb >= 3){document.getElementById("eq3").style.display = "block"; }else{document.getElementById("eq3").style.display = "none"; }
                        if(nb >= 4){document.getElementById("eq4").style.display = "block"; }else{document.getElementById("eq4").style.display = "none"; }
                        if(nb >= 5){document.getElementById("eq5").style.display = "block"; }else{document.getElementById("eq5").style.display = "none"; }
                    }
                    else
                    {
                        document.getElementById("eq2").style.display = "none";
                        document.getElementById("eq3").style.display = "none";
                        document.getElementById("eq4").style.display = "none";
                        ocument.getElementById("eq5").style.display = "none";
                    }
                }
            </script>

        </div>

        <div class="rect3">
             <input type="submit" value="Valider" class="validateButton" />
        </div>

       </div>

    </form>
                   <?php
     include 'sidebar.php';
    include 'header.php';
    ?>
  </body>
</html>
