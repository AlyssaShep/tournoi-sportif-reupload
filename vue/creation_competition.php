<!DOCTYPE html>
<?php if(!isset($_GET["pseudo"])) {
          session_start();
      }
      $pseudo = $_GET['pseudo'];
      include '../controller/functions_monCompte.php';
?>

<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <title>Creation Competition </title>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/creationcompetion.css" />
  </head>
  <body>

<?php if($isadmin || $isgestionnaire){ ?>
    <div class="rect1">
    <form method="post" action="../controller/functions_competition_creation.php">
      <!-- si c est un admin doit donner le nom et les coordonnes du gestionnaire -->
<?php if($isadmin){ ?>
          <p class="text1">Vous êtes un administrateur. Vous pouvez créer un tournois en nommant un gestionnaire ou être gestionnaire. </p>
          <p class="text1"> Pour cela, vous devez donner le pseudo d'un de nos membres. </p>
          <p class="onePerLine">Pseudo  <input type="text" name="Pseudo" value="<?php echo $pseudo; ?>" /></p>

<?php } else {
           if($isgestionnaire){
?>
        <p class="text1">Vous êtes gestionnaire. Vous pouvez créer votre compétition mais vous ne pouvez pas nommer un autre gestionnaire. </p>

        <br class="rect1br"/>

        <p class="twoPerLine">
        <input type="hidden" name="Pseudo" value="<?php echo $pseudo; ?>">
        Nom                 <input type="text" disabled="disabled" name="Nom" value="<?php echo $nom; ?>" />
        Prénom              <input type="text" disabled="disabled" name="Prenom" value="<?php echo $prenom; ?>"/></p>
        <p class="twoPerLine">
        Email               <input type="email" disabled="disabled" name="Email" value="<?php echo $mail; ?>"/>
        Numéro              <input type="tel" disabled="disabled" name="Numero" pattern="[0-9]{10}" value="<?php echo $num; ?>"/></p>
        <!-- le pattern permet de faire en sorte qu il n y ait pas un numero superieur a 10 -->

<?php     }
       }
?>

      <p class="onePerLine">Nom de la Compétition  <input type="text" name="Nom_Competition" required/></p>
      <p class="largeField">Esport            <select name="Esport">
                              <option value="LOL">League of Legends</option>
                              <option value="CS">Counter-Strike</option>
                              <option value="SB">Smash Bross</option>
                           </select> </p>

        </div>

        <div class="rect2">

            <br class="rect2br"/>
            <p class="onePerLine">Type de compétition
                    <select name="Competition" id="tipe" onchange="js()" >
                    <option value="tournois">Tournoi</option>
                    <option value="championnat">Championnat</option>
                    <option value="coupe">Coupe</option>
                    </select></p>

            <p class="onePerLine">Nombre d'équipes    <input type="number" onchange="js()" name="NbrEquipe" id="equipeN" min="2" required/></p>

            <br/>

            <p class="twoChoices">Résultats aléatoires ?
                    <input type="radio" name="resultat_aleatoire" id="choix2ra" value="non" required/>
                    <label for="choix2ra">Non</label>
                    <input type="radio" name="resultat_aleatoire" id="choix1ra" value="oui" />
                    <label for="choix1ra">Oui</label> </p>


            <p class="twoChoices">Équipes aléatoires ?
                    <input type="radio" name="equipe_aleatoire" id="choix2ea" value="non" required/>
                    <label for="choix2ea">Non</label>
                    <input type="radio" name="equipe_aleatoire" id="choix1ea" value="oui" />
                    <label for="choix1ea">Oui</label> </p>

            <script "text/javascript">
            function js() {

                    var t = document.getElementById("tipe").value;

                    if(t == "tournois"){
                        document.getElementById("equipeN").min = 2;
                        document.getElementById("equipeN").max = 32;
                    }
                    if(t == "championnat"){
                        document.getElementById("equipeN").min = 2;
                        document.getElementById("equipeN").max = 20;
                    }
                    if(t == "coupe"){
                        document.getElementById("equipeN").min = 2;
                        document.getElementById("equipeN").max = 16;
                    }

                    if(t == "tournois" || t == "coupe")
                    {
                        var two = 2;
                        for(let i = 1; i <= 5 ; i++)
                        {
                            if(document.getElementById("equipeN").value <= two)
                            {
                                document.getElementById("equipeN").value = two;
                                break;
                            }
                            two = two * 2;
                        }
                    }
                    else{
                        var r = document.getElementById("equipeN").value % 2;
                        document.getElementById("equipeN").value = document.getElementById("equipeN").value - r;
                    }
                }
            </script>

        </div>

        <div class="rect3">

            <p class="largeField">Lieu de rencontre  </p><p class="largeField">  <input type="text" name="Lieu" required/></p>
            <br class="rect3br"/>
            <p class="largeField">Date de début    </p><p class="largeField">     <input type="date" name="Debut" required/></p>
            <br class="rect3br"/>
            <p class="largeField">Durée     </p><p class="largeField">     <input type="number" name="Duree" min="1" required/></p>
            <br/>
            <div class="validateRect">
              <input type="submit" value="Valider" class="validateButton"/>
            </div>
         </div>

         <!--<div class="infoText"> <p>Si un des champs ne vous plait pas après validation, vous pouvez toujours les modifier dans la section "Mes Tournois" ;) </p></div>-->

        </form></div><?php }
    else { ?>
        <div class="playerError">

            <br/><br/><br/>

            <p>Vous n'êtes ni administrateur ni gestionnaire. <br/> Modifier votre compte. </p>
            <br/><br/>
            <a href="<?php echo "monCompte.php?pseudo=".$_GET['pseudo']; ?>"> <button class="bouton">Modifier mon Compte</button> </a>

        </div>

        <?php } ?>

     <?php
     include 'sidebar.php';
     include 'header.php';
    ?>
  </body>
</html>
