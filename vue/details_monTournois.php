<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Voir détail</title>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/detailsTournois.css" />
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
  </head>
  <body>

      <?php
      if (isset($_GET['pseudo'])) {
            $pseudo = $_GET['pseudo'];
      }
      $idTournois = $_GET['id'];
      include '../controller/functions_details_monTournois.php';
      $info = get_info_competition($idTournois);
      $type = $info[0][0]['type'];
      $nbr_equipes = $info[0][0]['nbr_equipes'];
      $rencontres = get_all_rencontre($idTournois);
      $idEquipes = get_all_equipes($info[0][0]['idEquipe']);
      $dateFin = date("Y-m-d", strtotime($info[0][0]['dateDebut']."+".$info[0][0]['duree']."days"));
      ?>
      <div class="NomTournois">
      <h3><?php echo $info[0][0]['nom_tournois']; ?></h3>
      </div>

      <div class="infoTournoi">
        <div class="sousPart">
        <p>Informations sur la compétition</p>
        </div>
        <div class="centre">
        <table>
          <col span="8">
          <tr class = "Nomcolonne">
            <td>Nom de la compétition</td>
            <td>Date de Début</td>
            <td>Duree</td>
            <td>Date de fin</td>
            <td>Gestionnaire</td>
            <td>Lieu</td>
            <td>Type</td>
            <td>Nombre d'équipes</td>
            <td>Esport</td>
          </tr>

          <tr>
            <td><?php echo $info[0][0]['nom_tournois']; ?></td>
            <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
            <td><?php echo strftime('  %d %B %Y ', strtotime($info[0][0]['dateDebut'])); ?></td>
            <td><?php echo $info[0][0]['duree']; ?></td>
            <td><?php echo strftime('  %d %B %Y ', strtotime($dateFin)); ?></td>
            <td><?php echo getPseudoGestionnaire($info[0][0]['idGestionnaire']); ?></td>
            <td><?php echo $info[0][0]['lieu']; ?></td>
            <td><?php echo $info[0][0]['type']; ?></td>
            <td><?php echo $info[0][0]['nbr_equipes']; ?></td>
            <td><?php echo $info[0][0]['Esport']; ?></td>
          </tr>
        </table>
        </div>
      </div>

      <div class="listeEquipe">
      <div class="sousPart">
        <p>Informations sur les équipes participantes</p>
      </div>
        <?php
        $size =  count($idEquipes);
        for ($i=0; $i < $size; $i++) {
          $info = get_info_equipe($idEquipes[$i]);
          if($i == 0){
            ?>

            <table>
              <col span="8">
              <tr class = "Nomcolonne">
                <td>Nom de l'équipe</td>
                <td>Niveau</td>
                <td>Email</td>
                <td>Téléphone</td>
                <td>Nombre de joueurs</td>
                <td>Nombre de victoires</td>
                <td>Capitaine</td>
                <td>Joueurs</td>
              </tr>

            <?php
          }
          ?>

          <tr>
            <td><?php echo $info[0][0]['nom_equipe']; ?></td>
            <td><?php echo $info[0][0]['niveau']; ?></td>
            <td><?php echo $info[0][0]['adresse_equipe']; ?></td>
            <td><?php echo "0".$info[0][0]['equipe_tel']; ?></td>
            <td><?php echo $info[0][0]['nb_joueur']; ?></td>
            <td><?php echo $info[0][0]['nb_victoire']; ?></td>
            <td><?php echo get_pseudo_capitaine($info[0][0]['idCapitaine']); ?></td>
            <td><?php
            $joueurs = get_joueurs_equipe($idEquipes[$i]);
            for ($j=0; $j < $info[0][0]['nb_joueur']; $j++) {
              ?>
              <p>
              <?php

              if (isset($joueurs[0][$j]['pseudo'])) {
                echo $joueurs[0][$j]['pseudo'];
              }
              ?>
              </p>
              <?php
            } ?></td>
          </tr>

          <?php
        }
         ?>
         </table>
      </div>

      <div class="DivChamp">
        <?php
        if ($type == "championnat") {
          $all_equipes = array();

          for ($i=0; $i < $nbr_equipes; $i++) {
            $all_equipes[0][$i]['pts'] = 0;
            $all_equipes[0][$i]['Equipe'] = recup_idEquipe($idTournois, $i);
          }
          $all_rencontres = get_rencontre_tournois($idTournois);
          // var_dump(count($all_rencontres[0]));

          $size = count($all_rencontres[0]);
          for ($i=0; $i < $size; $i++) {
            for ($j=0; $j < $nbr_equipes; $j++) {
              if(!empty($all_rencontres[0][$i]['idGagnant'])){
                if ($all_equipes[0][$j]['Equipe'] == $all_rencontres[0][$i]['idGagnant']) {
                  $all_equipes[0][$j]['pts'] += 3;
                }
              }
            }
          }
          // var_dump($all_equipes);

          $max = 0;
          for ($i=0; $i < $nbr_equipes; $i++) {
            if ($max < $all_equipes[0][$i]['pts']) {
              $max = $all_equipes[0][$i]['pts'];
            }
          }

          $gagnant = array();
          // var_dump($all_equipes);
          // var_dump($nbr_equipes);
          // var_dump($max);
          $id = 0;
          for ($i=0; $i < $nbr_equipes; $i++) {
            if ($max == $all_equipes[0][$i]['pts']) {
              $gagnant[0][$id]['Equipe'] = $all_equipes[0][$i]['Equipe'];
              $gagnant[0][$id]['pts'] = $all_equipes[0][$i]['pts'];
              $id++;
            }
          }

          // var_dump($all_equipes);
          // var_dump($gagnant);

          if (count($gagnant[0]) == 1) {
            $classement = classement_championnat_egalite($all_equipes, $gagnant);
            // var_dump($gagnant);
            ?>
            <div class="sousPart">
            <p>Le gagnant de votre championnat est <?php echo get_info_equipe($gagnant[0][0]['Equipe'])[0][0]['nom_equipe']; ?>.</p>
            <p>Classement</p>
            </div>
            <?php
            $size = count($classement[0]);
            for ($i=0; $i < $size; $i++) {
              if ($i == 0) {
                ?>
                <div class="centre">
                <table>
                  <col span="7">
                  <tr class = "Nomcolonne">
                    <th>Places</th>
                    <th>Equipes</th>
                    <th>scores</th>
                  </tr>

                <?php
              }
              ?>
              <tr>
                <td><?php echo $i+1; ?></td>
                <td><?php echo get_info_equipe($classement[0][$i]['Equipe'])[0][0]['nom_equipe']; ?></td>
                <td><?php echo $classement[0][$i]['pts']; ?></td>
              </tr>
              <?php
            }
             ?>
             </table>
          </div>
            <?php
          }
          else {
            $sizeG = count($gagnant[0]);
            $selecti = array();
            for($i = 0; $i < $sizeG; $i++){
              $selecti[0][$i] = $gagnant[0][$i]['Equipe'];
            }

            $legagnant = array_rand($selecti[0], 1);
            $etGa = array();
            $etGa[0][0]['Equipe'] = $selecti[0][$legagnant];
            $etGa[0][0]['pts'] = 0;
            for($i = 0; $i < $sizeG; $i++){
              if($gagnant[0][$i]['Equipe'] == $etGa[0][0]['Equipe']){
                $etGa[0][0]['pts'] = $gagnant[0][$i]['pts'];
              }
            }
            // var_dump($selecti[0]);
            // var_dump($legagnant);
            // var_dump($selecti[0][$legagnant]);
            $classement = classement_championnat_egalite($all_equipes,$etGa);
            ?>
            <div class="sousPart">
             <p>Le gagnant de votre championnat est <?php echo get_info_equipe($selecti[0][$legagnant])[0][0]['nom_equipe']; ?>.</p>
             <p>Classement</p>
            </div>
             <?php
             $size = count($classement[0]);
             for ($i=0; $i < $size; $i++) {
               if ($i == 0) {
                 ?>
                 <div class="centre">
                 <table>
                   <col span="7">
                   <tr class = "Nomcolonne">
                     <th>Places</th>
                     <th>Equipes</th>
                     <th>scores</th>
                   </tr>

                 <?php
               }
               ?>
               <tr>
                 <td><?php echo $i+1; ?></td>
                 <td><?php echo get_info_equipe($classement[0][$i]['Equipe'])[0][0]['nom_equipe']; ?></td>
                 <td><?php echo $classement[0][$i]['pts']; ?></td>
               </tr>
               <?php
             }
              ?>
              </table>
            </div>
            <?php
          }
          ?>

          <?php
        }else {
          ?>
          <!-- <p>Manque quelques choses?</p> -->
          <?php
        } ?>
      </div>

      <div class="DivRencontre">
      <div class="sousPart">
        <p>Les rencontres</p>
      </div>
        <?php
        $toutes_rencontres = get_all_rencontre($idTournois);
         ?>

         <?php
         if (empty($toutes_rencontres[0])) {
           ?>
           <p>Il n'y a pas encore de rencontres.</p>
           <?php
         }
         else {

           if ($nbr_equipes == 32 && $type != "championnat") {
             ?>
             <div class="">
             <?php
             $size =  count($toutes_rencontres[0]);
             for ($i=0; $i < $size; $i++) {
               if($i == 0){
                 ?>
                 <div class="">
                 <div class="soussousPart">
                 <p>Sélections en poule</p>
                 </div>
                 <table>
                   <col span="5">
                   <tr class = "Nomcolonne">
                     <td>Nom de l'équipe 1</td>
                     <td>Nom de l'équipe 2</td>
                     <td>score 1</td>
                     <td>score 2</td>
                     <td>voir rencontre</td>
                   </tr>

                 <?php
               }
               ?>

               <?php
               if($i == 48){
                 ?>
                 </table>
                 </div>
                 <div class="">
                 <div class="soussousPart">
                 <p>8eme de finale</p>
                 </div>
                 <table>
                   <col span="5">
                   <tr class = "Nomcolonne">
                     <td>Nom de l'équipe 1</td>
                     <td>Nom de l'équipe 2</td>
                     <td>score 1</td>
                     <td>score 2</td>
                     <td>voir rencontre</td>
                   </tr>
                 <?php
               }
                ?>

                <?php
                if($i == 56){
                  ?>
                  </table>
                  </div>
                  <div class="">
                  <div class="soussousPart">
                  <p>Quart de finale</p>
                  </div>
                  <table>
                    <col span="5">
                    <tr class = "Nomcolonne">
                      <td>Nom de l'équipe 1</td>
                      <td>Nom de l'équipe 2</td>
                      <td>score 1</td>
                      <td>score 2</td>
                      <td>voir rencontre</td>
                    </tr>
                  <?php
                }
                 ?>

                 <?php
                 if($i == 60){
                   ?>
                   </table>
                   </div>
                   <div class="">
                   <div class="soussousPart">
                   <p>Demi-finale</p>
                   </div>
                   <table>
                     <col span="5">
                     <tr class = "Nomcolonne">
                       <td>Nom de l'équipe 1</td>
                       <td>Nom de l'équipe 2</td>
                       <td>score 1</td>
                       <td>score 2</td>
                       <td>voir rencontre</td>
                     </tr>
                   <?php
                 }
                  ?>

                 <?php
                 if($i == 62){
                   ?>
                   </table>
                   </div>
                   <div class="">
                   <div class="soussousPart">
                   <p>Finale</p>
                   </div>
                   <table>
                     <col span="5">
                     <tr class = "Nomcolonne">
                       <td>Nom de l'équipe 1</td>
                       <td>Nom de l'équipe 2</td>
                       <td>score 1</td>
                       <td>score 2</td>
                       <td>voir rencontre</td>
                     </tr>
                   <?php
                 }
                  ?>

               <tr>
                 <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                 <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                 <td><?php echo $toutes_rencontres[0][$i]['score1']; ?></td>
                 <td><?php echo $toutes_rencontres[0][$i]['score2']; ?></td>
                 <?php if(isset($pseudo)){ ?>
                 <td><a href="<?php echo "voir_detail_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
               <?php }
                     else { ?><td><a href="<?php echo "voir_detail_mesTournois.php?id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
               <?php } ?>
               </tr>
           <?php } ?>
           </table>
           </div>
           </div>

             <?php
           }else {
             ?>
             <div class="">
             <?php
             if($nbr_equipes == 16 && $type == "coupe" && $type != "championnat"){
               $size =  count($toutes_rencontres[0]);
               // var_dump($size);
               for ($i=0; $i < $size; $i++) {
                 if($i == 0){
                   ?>
                   <div class="">
                   <div class="soussousPart">
                   <p>8eme de finale</p>
                   </div>
                   <table>
                     <col span="5">
                     <tr class = "Nomcolonne">
                       <td>Nom de l'équipe 1</td>
                       <td>Nom de l'équipe 2</td>
                       <td>score 1</td>
                       <td>score 2</td>
                       <td>voir rencontre</td>
                     </tr>
                   <?php
                 }
                 ?>

                 <?php
                 if($i == 8){
                   ?>
                   </table>
                   </div>
                   <div class="">
                   <div class="soussousPart">
                   <p>Quart de finale</p>
                   </div>
                   <table>
                     <col span="5">
                     <tr class = "Nomcolonne">
                       <td>Nom de l'équipe 1</td>
                       <td>Nom de l'équipe 2</td>
                       <td>score 1</td>
                       <td>score 2</td>
                       <td>voir rencontre</td>
                     </tr>
                   <?php
                 }
                  ?>

                  <?php
                  if($i == 12){
                    ?>
                    </table>
                    </div>
                    <div class="">
                    <div class="soussousPart">
                    <p>demi-finale</p>
                    </div>
                    <table>
                      <col span="5">
                      <tr class = "Nomcolonne">
                        <td>Nom de l'équipe 1</td>
                        <td>Nom de l'équipe 2</td>
                        <td>score 1</td>
                        <td>score 2</td>
                        <td>voir rencontre</td>
                      </tr>
                    <?php
                  }
                   ?>

                   <?php
                   if($i == 14){
                     ?>
                     </table>
                     </div>
                     <div class="">
                     <div class="soussousPart">
                     <p>Finale</p>
                     </div>
                     <table>
                       <col span="5">
                       <tr class = "Nomcolonne">
                         <td>Nom de l'équipe 1</td>
                         <td>Nom de l'équipe 2</td>
                         <td>score 1</td>
                         <td>score 2</td>
                         <td>voir rencontre</td>
                       </tr>
                     <?php
                   }
                    ?>

                 <tr>
                   <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                   <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                   <td><?php echo $toutes_rencontres[0][$i]['score1']; ?></td>
                   <td><?php echo $toutes_rencontres[0][$i]['score2']; ?></td>
                   <?php if(isset($pseudo)){ ?>
                   <td><a href="<?php echo "voir_detail_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                 <?php }
                       else { ?><td><a href="<?php echo "voir_detail_mesTournois.php?id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                 <?php } ?>
                 </tr>
             <?php } ?>
             </table>
             </div>
             </div>

  <?php
             }
             else {
               ?>
               <div>
               <?php
               if($nbr_equipes == 8 && $type != "championnat"){
                 $size =  count($toutes_rencontres[0]);
                 // var_dump($size);
                 for ($i=0; $i < $size; $i++) {
                   if($i == 0){
                     ?>
                     <div class="">
                     <div class="soussousPart">
                       <p>Quart de finale</p>
                     </div>
                     <table>
                       <col span="5">
                       <tr class = "Nomcolonne">
                         <td>Nom de l'équipe 1</td>
                         <td>Nom de l'équipe 2</td>
                         <td>score 1</td>
                         <td>score 2</td>
                         <td>voir rencontre</td>
                       </tr>

                     <?php
                   }
                   ?>

                    <?php
                    if($i == 12){
                      ?>
                      </table>
                      </div>
                      <div class="">
                      <div class="soussousPart">
                      <p>demi-finale</p>
                      </div>
                      <table>
                        <col span="5">
                        <tr class = "Nomcolonne">
                          <td>Nom de l'équipe 1</td>
                          <td>Nom de l'équipe 2</td>
                          <td>score 1</td>
                          <td>score 2</td>
                          <td>voir rencontre</td>
                        </tr>
                      <?php
                    }
                     ?>

                     <?php
                     if($i == 14){
                       ?>
                       </table>
                       </div>
                       <div class="">
                       <div class="soussousPart">
                       <p>Finale</p>
                        </div>
                       <table>
                         <col span="5">
                         <tr class = "Nomcolonne">
                           <td>Nom de l'équipe 1</td>
                           <td>Nom de l'équipe 2</td>
                           <td>score 1</td>
                           <td>score 2</td>
                           <td>voir rencontre</td>
                         </tr>
                       <?php
                     }
                      ?>

                   <tr>
                     <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                     <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                     <td><?php echo $toutes_rencontres[0][$i]['score1']; ?></td>
                     <td><?php echo $toutes_rencontres[0][$i]['score2']; ?></td>
                     <?php if(isset($pseudo)){ ?>
                     <td><a href="<?php echo "voir_detail_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                   <?php }
                         else { ?><td><a href="<?php echo "voir_detail_mesTournois.php?id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                   <?php } ?>
                   </tr>
               <?php } ?>
               </table>
             </div>
             </div>
               <?php
             }else {

               if ($nbr_equipes == 4 && $type != "championnat") {
                 $size =  count($toutes_rencontres[0]);
                 // var_dump($size);
                 for ($i=0; $i < $size; $i++) {
                   if($i == 0){
                     ?>
                     <div class="">
                     <div class="soussousPart">
                     <p>demi-finale</p>
                      </div>
                     <table>
                       <col span="5">
                       <tr class = "Nomcolonne">
                         <td>Nom de l'équipe 1</td>
                         <td>Nom de l'équipe 2</td>
                         <td>score 1</td>
                         <td>score 2</td>
                         <td>voir rencontre</td>
                       </tr>

                     <?php
                   }
                   ?>

                     <?php
                     if($i == 2){
                       ?>
                       </table>
                       </div>
                       <div class="">
                       <div class="soussousPart">
                       <p>Finale</p>
                       </div>
                       <table>
                         <col span="5">
                         <tr class = "Nomcolonne">
                           <td>Nom de l'équipe 1</td>
                           <td>Nom de l'équipe 2</td>
                           <td>score 1</td>
                           <td>score 2</td>
                           <td>voir rencontre</td>
                         </tr>
                       <?php
                     }
                      ?>

                   <tr>
                     <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                     <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                     <td><?php echo $toutes_rencontres[0][$i]['score1']; ?></td>
                     <td><?php echo $toutes_rencontres[0][$i]['score2']; ?></td>
                     <?php if(isset($pseudo)){ ?>
                     <td><a href="<?php echo "voir_detail_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                   <?php }
                         else { ?><td><a href="<?php echo "voir_detail_mesTournois.php?id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                   <?php } ?>
                   </tr>
               <?php } ?>
               </table>
             </div>
             </div>
                 <?php
               }else {

                 if($nbr_equipes == 2 && $type != "championnat"){

                     $size =  count($toutes_rencontres[0]);
                     // var_dump($size);
                     for ($i=0; $i < $size; $i++) {
                       if($i == 0){
                         ?>
                         <div class="">
                         <div class="soussousPart">
                         <p>Finale</p>
                         </div>
                         <table>
                           <col span="5">
                           <tr class = "Nomcolonne">
                             <td>Nom de l'équipe 1</td>
                             <td>Nom de l'équipe 2</td>
                             <td>score 1</td>
                             <td>score 2</td>
                             <td>voir rencontre</td>
                           </tr>

                         <?php
                       }
                       ?>

                       <tr>
                         <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                         <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                         <td><?php echo $toutes_rencontres[0][$i]['score1']; ?></td>
                         <td><?php echo $toutes_rencontres[0][$i]['score2']; ?></td>
                         <?php if(isset($pseudo)){ ?>
                         <td><a href="<?php echo "voir_detail_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                       <?php }
                             else { ?><td><a href="<?php echo "voir_detail_mesTournois.php?id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                       <?php } ?>
                       </tr>
                   <?php } ?>
                   </table>
                 </div>
                 <!-- </div> -->
                     <?php
                 }
                 else {
                   ?>
                   <div class="">
                   <?php
                   if($nbr_equipes == 16 && $type == "tournois" && $type != "championnat"){
                     $size =  count($toutes_rencontres[0]);
                     // var_dump($size);
                     for ($i=0; $i < $size; $i++) {
                       if($i == 0){
                         ?>
                         <div class="">
                         <div class="sousPart">
                         <p>Sélections en poule</p>
                         </div>
                         <table>
                           <col span="5">
                           <tr class = "Nomcolonne">
                             <td>Nom de l'équipe 1</td>
                             <td>Nom de l'équipe 2</td>
                             <td>score 1</td>
                             <td>score 2</td>
                             <td>voir rencontre</td>
                           </tr>

                         <?php
                       }
                       ?>

                       <?php
                       if($i == 24){
                         ?>
                         </table>
                         </div>
                         <div class="">
                         <div class="soussousPart">
                         <p>Quart de finale</p>
                         </div>
                         <table>
                           <col span="5">
                           <tr class = "Nomcolonne">
                             <td>Nom de l'équipe 1</td>
                             <td>Nom de l'équipe 2</td>
                             <td>score 1</td>
                             <td>score 2</td>
                             <td>voir rencontre</td>
                           </tr>
                         <?php
                       }
                        ?>

                        <?php
                        if($i == 28){
                          ?>
                          </table>
                          </div>
                          <div class="">
                          <div class="soussousPart">
                          <p>demi-finale</p>
                          </div>
                          <table>
                            <col span="5">
                            <tr class = "Nomcolonne">
                              <td>Nom de l'équipe 1</td>
                              <td>Nom de l'équipe 2</td>
                              <td>score 1</td>
                              <td>score 2</td>
                              <td>voir rencontre</td>
                            </tr>
                          <?php
                        }
                         ?>

                         <?php
                         if($i == 30){
                           ?>
                           </table>
                           </div>
                           <div class="">
                           <div class="soussousPart">
                           <p>Finale</p>
                           </div>
                           <table>
                             <col span="5">
                             <tr class = "Nomcolonne">
                               <td>Nom de l'équipe 1</td>
                               <td>Nom de l'équipe 2</td>
                               <td>score 1</td>
                               <td>score 2</td>
                               <td>voir rencontre</td>
                             </tr>
                           <?php
                         }
                          ?>

                       <tr>
                         <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                         <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                         <td><?php echo $toutes_rencontres[0][$i]['score1']; ?></td>
                         <td><?php echo $toutes_rencontres[0][$i]['score2']; ?></td>
                         <?php if(isset($pseudo)){ ?>
                         <td><a href="<?php echo "voir_detail_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                       <?php }
                             else { ?><td><a href="<?php echo "voir_detail_mesTournois.php?id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                       <?php } ?>
                       </tr>
                   <?php } ?>
                   </table>
                   </div>
                   </div>

                   <?php
                 }
                 else {

                   $size =  count($toutes_rencontres[0]);
                   // var_dump($size);
                   for ($i=0; $i < $size; $i++) {
                     if($i == 0){
                       ?>
                       <div class="">
                       <table>
                         <col span="5">
                         <tr class = "Nomcolonne">
                           <td>Nom de l'équipe 1</td>
                           <td>Nom de l'équipe 2</td>
                           <td>score 1</td>
                           <td>score 2</td>
                           <td>voir rencontre</td>
                         </tr>

                       <?php
                     }
                     ?>

                     <tr>
                       <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                       <td><?php echo get_info_equipe($toutes_rencontres[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                       <td><?php echo $toutes_rencontres[0][$i]['score1']; ?></td>
                       <td><?php echo $toutes_rencontres[0][$i]['score2']; ?></td>
                       <?php if(isset($pseudo)){ ?>
                       <td><a href="<?php echo "voir_detail_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                     <?php }
                           else { ?><td><a href="<?php echo "voir_detail_mesTournois.php?id=".$toutes_rencontres[0][$i]['idMatch']; ?>">Voir</a></td>
                     <?php } ?>
                     </tr>
                 <?php } ?>
                 </table>
                 </div>
                 <!-- </div> -->

                   <?php
                 }
               }

               }
             }
           }

         }
       } ?>
      <!-- </div> -->
      <?php
          include 'sidebar.php';
          include 'header.php';
       ?>

  </body>
</html>
