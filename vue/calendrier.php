<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Calendrier</title>
        <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
        <link href='../fullcalendar/main.css' rel='stylesheet' />
        <script src='../fullcalendar/main.js'></script>
        
        <script>
            document.addEventListener(
                'DOMContentLoaded',
                function()
                {
                    var calendarEl = document.getElementById('calendar');
                    
                    var calendar = new FullCalendar.Calendar(
                        calendarEl,
                        {
                            initialView: 'listWeek',
                            locale:'fr',
                            headerToolbar: {
                              left: 'prev,next today',
                              center: 'title',
                              right: 'dayGridMonth,dayGridWeek,listWeek'
                            },
                            dayMaxEvents: true, // ajoute un bouton more quand il y a trop d'événement 
                            buttonText :{
                              today : "Aujourd'hui",
                              month : 'Mois',
                              week : 'Semaine',
                              list : 'Liste',
                              prev :'Précédent',
                              next :'Suivant',
                              
                            },
                            
                            allDayContent : 'Toute la journée',
                            noEventsContent :"Il n'y a aucun événement pour le moment",
                           
                            
                            events: '../controller/calendar_json.php',

                            eventDidMount : function(isToday){
                              var dotEl = isToday.el.getElementsByClassName('fc-list-event-dot')[0];

                              if(dotEl)
                                    {
                                        dotEl.style.backgroundColor = 'red';
                                    }
                            },
                        }
                    );
                    
                    calendar.render();
                }
            );
        </script>
        <style>

body {
  margin-top:10%;
  padding: 0px 1% 0px 11%;
  font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
  font-size: 14px;
}


</style>
    </head>
    
    <body>
        <div id='calendar'></div>

        <?php
      
      include 'header.php';
      include 'sidebar.php';
    ?>
    </body>
</html> 