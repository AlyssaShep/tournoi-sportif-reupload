<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/detailsRencontre.css" />
    <title>Détail Rencontre</title>
  </head>
  <body>

    <?php
    if (isset($_GET["pseudo"])) {
      $pseudo = $_GET["pseudo"];
      include '../controller/functions_monCompte.php';
    }

      $idMatch = $_GET["id"];
      include '../controller/functions_voir_detail_mesTournois.php';
      $match = get_info_match($idMatch);
      $info = get_info_compet($match[0][0]['idTournois']);
      $equipe1 = get_info_equipe($match[0][0]['idEquipe1']);
      $equipe2 = get_info_equipe($match[0][0]['idEquipe2']);
      if ($info[0][0]['Esport'] == "LOL" || $info[0][0]['Esport'] == "CS") {
        $nbj = 5;
      }
      else {
        $nbj = 2;
      }
     ?>

    <div class="NomTournois">
     <h2><?php echo $info[0][0]['nom_tournois']; ?></h2>
    </div>

    <div class="BigDiv">
      <div class="sousPart">
        <p>Informations sur la compétition</p>
      </div>

      <div class="centre">
        <table>
          <col span="7">
          <tr class = "Nomcolonne">
            <td>Nom compétition</td>
            <td>Type</td>
            <td>date de début</td>
            <td>durée</td>
            <td>lieu</td>
            <td>nombre d'équipes</td>
            <td>Esport</td>
          </tr>

          <tr>
            <td><?php echo $info[0][0]['nom_tournois']; ?></td>
            <td><?php echo $info[0][0]['type']; ?></td>
            <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
            <td><?php echo strftime('  %d %B %Y ', strtotime($info[0][0]['dateDebut'])); ?></td>
            <td><?php echo $info[0][0]['duree']; ?></td>
            <td><?php echo $info[0][0]['lieu']; ?></td>
            <td><?php echo $info[0][0]['nbr_equipes']; ?></td>
            <td><?php echo $info[0][0]['Esport']; ?></td>
          </tr>
        </table>
      </div>



        <div class="sousPart">
          <p>Informations sur la rencontre</p>
        </div>

        <div class="centre">
          <table>
            <col span="8">
            <tr class = "Nomcolonne">
              <td>Date de rencontre</td>
              <td>Heure de rencontre</td>
              <td>Equipe 1</td>
              <td>Equipe 2</td>
              <td>Gagnant</td>
              <td>Score 1</td>
              <td>Score 2</td>
            </tr>

            <tr>
              <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
              <td><?php echo strftime('  %d %B %Y ', strtotime($match[0][0]['dateRencontre'])); ?></td>
              <td><?php echo strftime('  %H : %M ', strtotime($match[0][0]['heureRencontre'])); ?></td>
              <td><?php echo $equipe1[0][0]['nom_equipe']; ?></td>
              <td><?php echo $equipe2[0][0]['nom_equipe']; ?></td>
              <?php
              if (empty($match[0][0]['idGagnant'])) {
                ?>
                <td><?php echo "pas de gagnant"; ?></td>
                <?php
              }
              else {
                ?>
                <td><?php echo get_info_equipe($match[0][0]['idGagnant'])[0][0]['nom_equipe']; ?></td>
                <?php
              }
              ?>
              <td><?php
              if (empty($match[0][0]['score1']) && empty($match[0][0]['score2'])) {
                ?>
                <p>Pas de scores</p>
                <?php
              }
              else {
                echo $match[0][0]['score1'];
              }
               ?></td>
              <td><?php
              if(empty($match[0][0]['score2']) && empty($match[0][0]['score1'])){
                ?>
                <p>Pas de scores</p>
                <?php
              }
              else {
                echo $match[0][0]['score2'];
              }
               ?></td>
            </tr>
          </table>
        </div>

         <div class="sousPart">
           <p>Les équipes</p>
         </div>

         <div class="centre">
           <table>
             <col span="7">
             <tr class = "Nomcolonne">
               <td>Nom équipe</td>
               <td>Niveau</td>
               <td>Email</td>
               <td>Téléphone</td>
               <td>Nombre de joueurs</td>
               <td>nombre de victoires</td>
               <td>Capitaine</td>
             </tr>

             <tr>
               <td><?php echo $equipe1[0][0]['nom_equipe']; ?></td>
               <td><?php echo $equipe1[0][0]['niveau']; ?></td>
               <td><?php echo $equipe1[0][0]['adresse_equipe']; ?></td>
               <td><?php echo "0".$equipe1[0][0]['equipe_tel']; ?></td>
               <td><?php echo $equipe1[0][0]['nb_joueur']; ?></td>
               <td><?php echo $equipe1[0][0]['nb_victoire']; ?></td>
               <td><?php echo get_pseudo_capitaine($equipe1[0][0]['idCapitaine']); ?></td>
             </tr>

             <tr>
               <td><?php echo $equipe2[0][0]['nom_equipe']; ?></td>
               <td><?php echo $equipe2[0][0]['niveau']; ?></td>
               <td><?php echo $equipe2[0][0]['adresse_equipe']; ?></td>
               <td><?php echo "0".$equipe2[0][0]['equipe_tel']; ?></td>
               <td><?php echo $equipe2[0][0]['nb_joueur']; ?></td>
               <td><?php echo $equipe2[0][0]['nb_victoire']; ?></td>
               <td><?php echo get_pseudo_capitaine($equipe2[0][0]['idCapitaine']); ?></td>
             </tr>
           </table>
         </div>
    </div>

    <div class ="SmallDiv">
         <div class="sousPart">
           <p>Les joueurs</p>
         </div>


            <p>Equipe : <?php echo $equipe1[0][0]['nom_equipe']; ?></p>
            <div class="centre">
             <table>
               <col span="3">
               <tr class = "Nomcolonne">
                 <td>Pseudo</td>
                 <td>Nom</td>
                 <td>Prénom</td>
               </tr>
               <?php $infoJ = get_info_joueurs($match[0][0]['idEquipe1']);
                     for ($i=0; $i < $nbj; $i++) {
                 ?>
                 <tr>
                   <td><?php echo $infoJ[0][$i]['pseudo']; ?></td>
                   <td><?php echo $infoJ[0][$i]['nom']; ?></td>
                   <td><?php echo $infoJ[0][$i]['prenom']; ?></td>
                 </tr>
                 <?php
               } ?>
             </table>
           </div>

            <p>Equipe : <?php echo $equipe2[0][0]['nom_equipe']; ?></p>

           <div class = "centre">
             <table>
               <col span="3">
               <tr class = "Nomcolonne">
                 <td>Pseudo</td>
                 <td>Nom</td>
                 <td>Prénom</td>
               </tr>
               <?php $infoJ = get_info_joueurs($match[0][0]['idEquipe2']);
                     for ($i=0; $i < $nbj; $i++) {
                 ?>
                 <tr>
                   <td><?php echo $infoJ[0][$i]['pseudo']; ?></td>
                   <td><?php echo $infoJ[0][$i]['nom']; ?></td>
                   <td><?php echo $infoJ[0][$i]['prenom']; ?></td>
                 </tr>
                 <?php
               } ?>
             </table>
           </div>
         </div>
      <?php
        include 'sidebar.php';
        include 'header.php';
     ?>
  </body>
</html>
