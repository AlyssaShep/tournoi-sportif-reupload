<!DOCTYPE html>
<?php if(!isset($_GET["pseudo"])) {
          session_start();
      }
      $pseudo = $_GET['pseudo'];
      include '../controller/functions_monCompte.php';
?>
<html lang="fr" dir="ltr">

  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/modifCompte.css" />
    <title>Modifier Compte</title>
  </head>

  <body>
  <div class="pageName">
      <h1>Modification du compte</h1>
  </div>
  <div class="rect">
    <?php if ($isadmin) {
      ?>
      <br>
      <?php
    } ?>
    <form action="../controller/functions_modification_monCompte.php" method="post">
      <h2>             <input type="hidden" name="Pseudo" value="<?php echo $pseudo; ?>" /> </h2>
      <h2>Nom          <input type="text" name="newNom" value="<?php echo $nom; ?>" required /></br></br> </h2>
      <h2>Prenom       <input type="text" name="newPrenom" value="<?php echo $prenom; ?>" required /> </br></br>  </h2>
      <h2>Pseudo       <input type="text" name="newPseudo" value="<?php echo $pseudo; ?>" required /> </br></br>  </h2>
      <h2>Email        <input type="email" name="newMail" value="<?php echo $mail; ?>" required /> </br></br> </h2>
      <h2>Numero       <input type="text" name="newNum" value="<?php echo $num; ?>" pattern="[0-9]{10}" required /> </br></br> </h2>
<?php if(!$isadmin){
     if($isgestionnaire){ ?>
                <h2>Vous êtes un Gestionnaire, vous pouvez devenir administrateur: ce qui vous permettra de créer des compétitions, nommer un gestionnaire ou de les gérer vous-même</h2>
                <h2>Devenir administrateur ?<h2>
                <input type="radio" name="devenir_admin" id="choix1a" value="non" required/>
                <label for="choix1a">Non</label>
                <input type="radio" name="devenir_admin" id="choix1b" value="oui" />
                <label for="choix1b">Oui</label>

<?php     } else {
              if ($isJoueur) { ?>
                <h2>Vous êtes un Joueur, vous pouvez devenir gestionnaire: ce qui vous permettra de créer et gérer des compétitions. </h2>
                <h2>Devenir gestionnaire ?
                  <input type="radio" name="devenir_gestionnaire" id="choix2a" value="non" required/>
                  <label for="choix2a">Non</label>
                  <input type="radio" name="devenir_gestionnaire" id="choix1b" value="oui" />
                  <label for="choix2b">Oui</label>
                </h2>
<?php          }
            }
      }
      else { ?>
        <h2> Vous êtes un administrateur. Vous pouvez créer, gérer ou jouer des compétitions </h2>
<?php } ?>
      <h2>confirmation mot de passe (*)  <br><input  class="" type="password" name="Mdp" required /> </br> </h2>
      <input type="submit" class="validateButton" value="Valider"/> </br> </br> </br>
    </form>
    <a href="<?php echo "../controller/functions_suppression_monCompte.php?pseudo=".$_GET['pseudo']; ?>"> <button class="suppr">Suppression de mon Compte</button> </a>
    </div>
    <?php
    include 'sidebar.php';
    include 'header.php';
    ?>
  </body>
</html>
