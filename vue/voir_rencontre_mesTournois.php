<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/voirRencontre.css" />
    <title>Voir Rencontre</title>
  </head>
  <body>

    <?php
      $pseudo = $_GET["pseudo"];
      $idMatch = $_GET["id"];
      include '../controller/functions_monCompte.php';
      include '../controller/functions_voir_rencontre_mesTournois.php';
      $rencontre = get_info_rencontre($idMatch);
      $tournois = get_info_competition($rencontre[0][0]['idTournois']);
      $idEquipe1 = $rencontre[0][0]['idEquipe1'];
      $idEquipe2 = $rencontre[0][0]['idEquipe2'];
      $infoE1 = get_info_equipe($idEquipe1);
      $infoE2 = get_info_equipe($idEquipe2);
     ?>

    <div class="NomTournois">
     <h2><?php echo $tournois[0][0]['nom_tournois']; ?></h2>
    </div>

    <div class=BigDiv>
      <div class="sousPart">
        <p>Informations sur la compétition</p>
      </div>

      <div class="centre">
        <table>
          <col span="7">
          <tr class = "Nomcolonne">
            <td>Nom compétition</td>
            <td>Type</td>
            <td>date de début</td>
            <td>durée</td>
            <td>lieu</td>
            <td>nombre d'équipes</td>
            <td>Esport</td>
          </tr>


          <tr>
            <td><?php echo $tournois[0][0]['nom_tournois']; ?></td>
            <td><?php echo $tournois[0][0]['type']; ?></td>
            <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
            <td><?php echo strftime('  %d %B %Y ', strtotime($tournois[0][0]['dateDebut'])); ?></td>
            <td><?php echo $tournois[0][0]['duree']; ?></td>
            <td><?php echo $tournois[0][0]['lieu']; ?></td>
            <td><?php echo $tournois[0][0]['nbr_equipes']; ?></td>
            <td><?php echo $tournois[0][0]['Esport']; ?></td>
          </tr>
        </table>
      </div>

      <div class="sousPart">
        <p>Informations sur les équipes</p>
      </div>

      <div class="centre">
        <table>
          <col span="7">
          <tr class = "Nomcolonne">
            <td>Nom équipe</td>
            <td>niveau</td>
            <td>adresse de l'équipe</td>
            <td>téléphone</td>
            <td>nombre de victoires</td>
            <td>nombre de joueurs</td>
            <td>pseudo Capitaine</td>
          </tr>

          <tr>
            <td><?php echo $infoE1[0][0]['nom_equipe']; ?></td>
            <td><?php echo $infoE1[0][0]['niveau']; ?></td>
            <td><?php echo $infoE1[0][0]['adresse_equipe']; ?></td>
            <td><?php echo "0".$infoE1[0][0]['equipe_tel']; ?></td>
            <td><?php echo $infoE1[0][0]['nb_victoire']; ?></td>
            <td><?php echo $infoE1[0][0]['nb_joueur']; ?></td>
            <td><?php echo get_pseudo_capitaine($infoE1[0][0]['idCapitaine']); ?></td>
          </tr>

          <tr>
            <td><?php echo $infoE2[0][0]['nom_equipe']; ?></td>
            <td><?php echo $infoE2[0][0]['niveau']; ?></td>
            <td><?php echo $infoE2[0][0]['adresse_equipe']; ?></td>
            <td><?php echo "0".$infoE2[0][0]['equipe_tel']; ?></td>
            <td><?php echo $infoE2[0][0]['nb_victoire']; ?></td>
            <td><?php echo $infoE2[0][0]['nb_joueur']; ?></td>
            <td><?php echo get_pseudo_capitaine($infoE2[0][0]['idCapitaine']); ?></td>
          </tr>
        </table>
      </div>

      <div class="sousPart">
        <p>Informations sur la rencontre</p>
      </div>

      <div class="centre">
        <table>
          <col span="6">
          <tr class = "Nomcolonne">
            <td>date de rencontre</td>
            <td>heure de rencontre</td>
            <td>nom équipe 1</td>
            <td>nom équipe 2</td>
            <td>score 1</td>
            <td>score 2</td>
          </tr>

          <tr>
            <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
            <td><?php echo strftime('  %d %B %Y ', strtotime($rencontre[0][0]['dateRencontre'])); ?></td>
            <td><?php echo strftime('  %H : %M ', strtotime($rencontre[0][0]['heureRencontre'])); ?></td>
            <td><?php echo get_info_equipe($rencontre[0][0]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
            <td><?php echo get_info_equipe($rencontre[0][0]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
            <td><?php
            if ($rencontre[0][0]['score1'] == null) {
              ?>
              <p>Pas de scores</p>
              <?php
            }else {
              echo $rencontre[0][0]['score1'];
            } ?></td>
            <td><?php
            if ($rencontre[0][0]['score2'] == null) {
              ?>
              <p>Pas de scores</p>
              <?php
            }
            else {
              echo $rencontre[0][0]['score2'];
            } ?></td>
          </tr>
        </table>
          </div>
      </div>
     <?php
        include 'sidebar.php';
        include 'header.php';
     ?>
  </body>
</html>
