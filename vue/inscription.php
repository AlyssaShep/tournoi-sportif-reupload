<!DOCTYPE html>
<?php session_start(); ?>
<html lang="fr" dir="ltr">

  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/inscription.css" />
    <title>Inscription</title>
  </head>

  <body>
    <div class="title">
        <h1 class="title1">
          Inscrivez-vous et participez à la joie de l'Esport!
        </h1>
    </div>

    <div class="logo">
      <a href="../index.php"> <input class="img" type="image" id="image" alt="Login" src="../assets/images/logo.png"> </a>
    </div>

    <div class="rect">
      <form method="post" action="../controller/functions_inscriptions.php">
      <h2>

        <p>Nom          <br>     <input  class="fielder" type="text" name="Nom" required/> </br> </p>
        <p>Prénom       <br>     <input  class="fielder" type="text" name="Prenom" required/> </br> </p>
        <p>Email        <br>     <input  class="fielder" type="email" name="Email" required/> </br>  </p>
        <p>Numero       <br>     <input  class="fielder" type="tel" name="Numero" pattern="[0-9]{10}" required/></p>
        <p>Pseudo       <br>     <input class="fielder" type="text" name="Pseudo" required/> </br> </p>
        <p>Mot de Passe <br>     <input class="fielder" type="password" name="Mdp" required/> </br>  </p>
        <p>Confirmation Mot de Passe <br>     <input class="fielder" type="password" name="Confirmation" required/> </br>  </p>
      </h2>
      <input type="submit" class="button" value="Valider"/>

      <h2> <br /><br /><br /><br /><br />
        <a class="a" href="connexion.php"> Déja un compte? Connectez-vous!</a>
      </h2>
      </form>
    </div>

  </body>
</html>
