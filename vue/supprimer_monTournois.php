<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
     <link rel="stylesheet" href="../assets/mainCSS.css" />
     <link rel="stylesheet" href="../assets/suprTournois.css" />
    <title>Supprimer Tournois</title>
  </head>
  <body>

    <form class="" action="../controller/functions_supprimer_monTournois.php" method="post">

    <div class="rect">

         <br/><br/>

        <input type="hidden" name="idTournois" value="<?php echo $_GET['id']; ?>">
        <input type="hidden" name="pseudo" value="<?php echo $_GET['pseudo']; ?>">
        <p class="text">Êtes-vous sûr de vouloir supprimer ce tournoi ?</p>

        <br/><br/>

        <div class="twoChoices">

        <input type="radio" name="suppression" id="choixsup1" value="non" checked />
        <label for="choixsup1">Non</label>
        <input type="radio" name="suppression" id="choixsup2" value="oui" />
        <label for="choixsup2">Oui</label>  </p>
        <br/><br/><br/>
        </div>

        <div class="validateRect">

        <input class="validateButton" type="submit" name="" value="Valider">

        </div>
       </div>

    </form>

        <?php
    include 'sidebar.php';
    include 'header.php';
    ?>
  </body>
</html>
