<!DOCTYPE html>
<?php session_start(); ?>
<!-- pour tester la connexion et le reste
  pseudo = test
  email = test@test.fr
  mdp = test 
 -->
<html>
    <head>
        <meta charset="utf-9" />
        <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
        <title>Connexion</title>
        <link rel="stylesheet" href="../assets/mainCSS.css" />
        <link rel="stylesheet" href="../assets/connexion.css" />
    </head>

    <body>
            <div class="title">
                <h1 class="title1">
                    Connectez vous pour acceder aux fonctionalités !
                </h1>
            </div>

            <div class="logo">
                <a href="../index.php"> <input class="img" type="image" id="image" alt="Login" src="../assets/images/logo.png"> </a>
            </div>

           <div class="rect">
            <form method="post" action="../controller/functions_connexions.php">
                <h2 class="formulaire">
                  Pseudo :<br /> <input type="text" class="fielder" name="Pseudo" required/> <br /><br />
                  Adresse email :<br /> <input type="email" class="fielder" name="Email" required/> <br /><br />
                  Mot de passe :<br /> <input type="password" class="fielder" name="Mdp" required/> <br /><br />
                  <input type="submit" class="button" value="Se connecter"/>
                </h2>
                <h2> <br /><br /><br /><br /><br /><br /><br />
                  <a class="hyper" href="inscription.php"> Pas encore inscrit? Créez un compte!</a>
                </h2>
            </form>
            </div>
       </body>

</html>
