<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <title>Mes Equipes</title>
    <?php
    include '../controller/functions_mesEquipes.php';
    if(isset($_GET["pseudo"])){
      $pseudo = $_GET["pseudo"];
      $infoJoueur = get_info_joueur($pseudo);
      $sizeIJ = count($infoJoueur[0]);
      // var_dump($sizeIJ);
    }?>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/mesTournois.css" />
  </head>
  <body>

    <div class="BigDiv">
      <div class="gestionRect">
        <p>Mes équipes</p>
        <?php
        if (isset($infoJoueur) && $sizeIJ != 0) {
          for ($j=0; $j < $sizeIJ; $j++) {
            $gestion = get_equipe($infoJoueur[0][$j]['idJoueur']);
            if ($j == 0) {
              ?>
              <table>
                <col span="12">
                <tr class = "Nomcolonne">
                  <th>Nom de l'équipe</th>
                  <th>Niveau</th>
                  <th>Adresse de l'équipe</th>
                  <th>Téléphone</th>
                  <th>Nombre de joueurs</th>
                  <th>Nombre de victoire</th>
                  <th>Esport</th>
                  <th>Liste des joueurs</th>
                  <th>Voir</th>
                </tr>
              <?php
            }
            // var_dump($gestion);
            // var_dump($size);
            if(isset($gestion) && !empty($gestion[0])){
                $size = count($gestion[0]);
                ?>

            <?php
            for ($i=0; $i < $size ; $i++) { ?>
                    <tr>
                      <td><?php echo $gestion[0][$i]['nom_equipe']; ?></td>
                      <td><?php echo $gestion[0][$i]['niveau']; ?></td>
                      <td><?php echo $gestion[0][$i]['adresse_equipe']; ?></td>
                      <td><?php echo "0".$gestion[0][$i]['equipe_tel']; ?></td>
                      <td><?php echo $gestion[0][$i]['nb_joueur']; ?></td>
                      <td><?php echo $gestion[0][$i]['nb_victoire']; ?></td>
                      <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                      <td><?php
                      $joueurs = get_joueurs_equipe($gestion[0][$i]['idEquipe']);
                      for ($h=0; $h < $gestion[0][$i]['nb_joueur']; $h++) {
                        ?>
                        <p><?php echo $joueurs[0][$h]['pseudo']; ?></p>
                        <?php
                      } ?>
                      </td>
                      <?php $_SESSION["id"] = $gestion[0][$i]['idEquipe']; ?>
                      <td><a href="<?php echo "voir_monEquipe.php?pseudo=".$_GET["pseudo"]."&id=".$_SESSION["id"]; ?>">Voir</a></td>
                    </tr>
      <?php         }?>


    <?php   }

          }
          ?>
          </table>
          <?php
        }
        else { ?>
            <p>Vous n'avez pas d'équipes !</p>
  <?php } ?>


        </div>
      </div>

      <div class="BigDiv">
        <div class="gestionRect">
          <p>Mes équipes en tant qu'équipier</p>
          <?php
          if (isset($infoJoueur) && $sizeIJ != 0) {
            for ($j=0; $j < $sizeIJ; $j++) {
              // var_dump($infoJoueur[0][$j]['idJoueur']);
              $equipe = get_equipe_is_joueur($infoJoueur[0][$j]['idJoueur']);
              // var_dump($equipe[0][0]['idEquipeJ']);

              if ($j == 0) {
                ?>
                <table>
                  <col span="12">
                  <tr class = "Nomcolonne">
                    <th>Nom de l'équipe</th>
                    <th>Niveau</th>
                    <th>Adresse de l'équipe</th>
                    <th>Téléphone</th>
                    <th>Nombre de joueurs</th>
                    <th>Nombre de victoire</th>
                    <th>Esport</th>
                    <th>Liste des joueurs</th>
                  </tr>
                <?php
              }


              if(isset($equipe) && !empty($equipe[0])){
                $sizeE = count($equipe[0]);
                $gestion = get_info_equipe($equipe[0][0]['idEquipeJ']);
                // var_dump($gestion);
                $size = count($equipe);
                // var_dump($size);
                ?>

                      <tr>
                        <td><?php echo $gestion[0][0]['nom_equipe']; ?></td>
                        <td><?php echo $gestion[0][0]['niveau']; ?></td>
                        <td><?php echo $gestion[0][0]['adresse_equipe']; ?></td>
                        <td><?php echo "0".$gestion[0][0]['equipe_tel']; ?></td>
                        <td><?php echo $gestion[0][0]['nb_joueur']; ?></td>
                        <td><?php echo $gestion[0][0]['nb_victoire']; ?></td>
                        <td><?php echo $gestion[0][0]['Esport']; ?></td>
                        <td><?php
                        $joueurs = get_joueurs_equipe($gestion[0][0]['idEquipe']);
                        for ($h=0; $h < $gestion[0][0]['nb_joueur']; $h++) {
                          ?>
                          <p><?php echo $joueurs[0][$h]['pseudo']; ?></p>
                          <?php
                        } ?>

      <?php   }

            }
            ?>
            </table>
            <?php
          }
          else {
            ?>
            <p>Vous ne faites parti d'aucune équipe.</p>
            <?php
          } ?>


          </div>
        </div>

    <?php
      // include 'sidebar.php';
      include 'header.php';
    ?>
  </body>
</html>
