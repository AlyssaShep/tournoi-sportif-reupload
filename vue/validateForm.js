function validateForm()
{
    var errors = [];
    if($('#nom_competition').val().trim() == '') {
        errors.push('Le nom de la compete est requis');
    }
    if($('#NbrEquipe').val().trim() == '') {
        errors.push("Le nombre d'équipe est requis");
    }   
    if($('#Lieu').val().trim() == '') {
        errors.push("Le lieu est requis");
    } 
    if($('#Mdp').val().trim() == '') {
        errors.push("Le mot de passe est requise");
    }     

    //affichage des erreurs en alerte si errors n'est pas vide
     if(errors =''){
         return true;
     }else{
        alert(errors.join('\n'));
         return false; 
     }
}