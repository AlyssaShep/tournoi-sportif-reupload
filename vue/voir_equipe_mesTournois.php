<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/voirRencontre.css" />
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <title>Liste Equipes</title>
  </head>
  <body>

    <?php
    if (isset($_GET["pseudo"])) {
      $pseudo = $_GET["pseudo"];
      include '../controller/functions_monCompte.php';
    }

    $idTournois = $_GET["id"];
    include '../controller/functions_voir_equipe_mesTournois.php';
    $idEquipes = get_equipes_participantes($idTournois);
    $tournois = get_info_competition($idTournois);
     ?>

    <div class="NomTournois">
     <h2><?php echo $tournois[0][0]['nom_tournois']; ?></h2>
    </div>

    <div class="BigDiv2">
      <div class="sousPart">
        <p>Informations sur la compétition</p>
      </div>

      <div class="centre">
        <table>
          <col span="7">
          <tr class = "Nomcolonne">
            <td>Nom compétition</td>
            <td>Type</td>
            <td>date de début</td>
            <td>durée</td>
            <td>lieu</td>
            <td>nombre d'équipes</td>
            <td>Esport</td>
          </tr>


          <tr>
            <td><?php echo $tournois[0][0]['nom_tournois']; ?></td>
            <td><?php echo $tournois[0][0]['type']; ?></td>
            <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
            <td><?php echo strftime('  %d %B %Y ', strtotime($tournois[0][0]['dateDebut'])); ?></td>
            <td><?php echo $tournois[0][0]['duree']; ?></td>
            <td><?php echo $tournois[0][0]['lieu']; ?></td>
            <td><?php echo $tournois[0][0]['nbr_equipes']; ?></td>
            <td><?php echo $tournois[0][0]['Esport']; ?></td>
          </tr>
        </table>
    </div>

     <div class="listeEquipe">
      <div class = "sousPart">
       <p>Informations sur les équipes participantes</p>
      </div>

       <?php
       if (empty($idEquipes[0])) {
         ?>
         <p>Il n'y a pas encore d'équipes inscriptes à cette compétition.</p>
         <?php
       }
       else {
         $sizeE = count($idEquipes[0]);
         for ($i=0; $i < $sizeE ; $i++) {
                 $infoE = get_info_equipe($idEquipes[0][$i]);
                 if($i == 0){   ?>

                   <table>
                     <col span="7">
                       <tr class = "Nomcolonne">
                       <td>Nom équipe</td>
                       <td>Niveau</td>
                       <td>adresse de l'équipe</td>
                       <td>téléphone</td>
                       <td>nombre de joueurs</td>
                       <td>nombre de victoires</td>
                       <td>Esport</td>
                     </tr>

                   <?php
                 } ?>

                  <tr>
                   <td><?php echo $infoE[0][0]['nom_equipe']; ?></td>
                   <td><?php echo $infoE[0][0]['niveau']; ?></td>
                   <td><?php echo $infoE[0][0]['adresse_equipe']; ?></td>
                   <td><?php echo "0".$infoE[0][0]['equipe_tel']; ?></td>
                   <td><?php echo $infoE[0][0]['nb_joueur']; ?></td>
                   <td><?php echo $infoE[0][0]['nb_victoire']; ?></td>
                   <td><?php echo $infoE[0][0]['Esport']; ?></td>
                 </tr>

                 <?php
               }
               ?>
               </table>
               <?php
       } ?>
     </div>
      </div>
     <?php
      include 'header.php';
      include 'sidebar.php';
    ?>
  </body>
</html>
