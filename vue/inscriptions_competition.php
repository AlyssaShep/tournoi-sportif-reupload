<!DOCTYPE html>
<?php if(!isset($_GET["pseudo"])) {
          session_start();
      } ?>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <title>Inscription compétition</title>
    <!-- <link rel="stylesheet" href="../assets/connexion.css" /> -->
  </head>
  <body>

    <form method="post" action="../controller/functions_competition.php">

      <input type="hidden" name="idTournois"/>

      Nom de l'équipe    <input type="text" name="Nom_Tournois" required/><br/>
      Esport             <select name="Competition"><br/>
                            <option value="LOL">League of Legends</option>
                            <option value="CS">Counter-Strike</option>
                            <option value="SB">Smash Bross</option>
                         </select> <br/>

      Niveau de l'équipe  <input type="number" name="Niveau" min="1" max="100" required/><br/>
      Email de l'équipe   <input type="email" name="Email" required/><br/>
      Numero de l'équipe  <input type="text" name="Numero" pattern="[0-9]{10}" required/><br/>
      <!-- recuperation de la creation de Competition -->
      Nombre de Joueurs  <input type="number" name="nb_joueurs" min="1" max="4" required/> <br/>

      Capitaine
            Nom      <input type="text" name="nomCapitaine" required/>
            Prenom   <input type="text" name="prenomCapitaine" required/>
            Email    <input type="email" name="emailCapitaine" required/>
            Numero   <input type="text" name="NumeroCapitaine" required /><br/>

      Equipier 2
            Nom   <input type="text" name="nomEP2" required/>
            Prenom   <input type="text" name="prenomEP2" required/>
            Email    <input type="email" name="emailEP2" />
            Numero   <input type="text" name="NumeroEP2" /><br/>

      Equipier 3
            Nom   <input type="text" name="nomEP3" required/>
            Prenom   <input type="text" name="prenomEP3" required/>
            Email    <input type="email" name="emailEP3" />
            Numero   <input type="text" name="NumeroEP3" /><br/>

      Equipier 4
            Nom   <input type="text" name="nomEP4" required/>
            Prenom   <input type="text" name="prenomEP4" required/>
            Email    <input type="email" name="emailEP4" />
            Numero   <input type="text" name="NumeroEP4" /><br/>

      Equipier 5
            Nom   <input type="text" name="nomEP5" required/>
            Prenom   <input type="text" name="prenomEP5" required/>
            Email    <input type="email" name="emailEP5" />
            Numero   <input type="text" name="NumeroEP5" /><br/>

      <input type="submit" value="Valider"/>

    </form>
     <?php
        include 'sidebar.php';
        include 'header.php';
     ?>
  </body>
</html>
