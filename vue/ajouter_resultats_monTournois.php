<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/ajouterResultas.css" />
    <title>Ajouter résultats</title>
  </head>
  <body>
    <?php
        $pseudo = $_GET["pseudo"];
        $idMatch = $_GET["id"];
        include '../controller/functions_monCompte.php';
        include '../controller/functions_ajouter_resultats_monTournois.php';
        $info = get_info_rencontre($idMatch);
        $tournois = get_info_competition($info[0][0]['idTournois']);
        $sport = $tournois[0][0]['Esport'];
        $aleatoire = $tournois[0][0]['resultats_aleatoires'];
        $equipe1 = get_info_equipe($info[0][0]['idEquipe1']);
        $equipe2 = get_info_equipe($info[0][0]['idEquipe2']);
        $resultats_deja_ajoutes = resultats_deja_ajoutes($idMatch);

        if($sport == "LOL"){
          $max = 1;
        }
        elseif ($sport == "CS") {
          $max = 16;
        }
        else {
          $max = 3;
        }
        ?>


     <h2><?php echo $tournois[0][0]['nom_tournois']; ?></h2>

     <div class = "rect1">
     <p>Informations sur la compétition</p>
     <table>
       <col span="7">
       <tr>
         <td>Nom compétition</td>
         <td>Type</td>
         <td>date de début</td>
         <td>durée</td>
         <td>lieu</td>
         <td>nombre d'équipes</td>
         <td>Esport</td>
       </tr>

       <tr>
         <td><?php echo $tournois[0][0]['nom_tournois']; ?></td>
         <td><?php echo $tournois[0][0]['type']; ?></td>
         <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
         <td><?php echo strftime('  %d %B %Y ', strtotime($tournois[0][0]['dateDebut'])); ?></td>
         <td><?php echo $tournois[0][0]['duree']; ?></td>
         <td><?php echo $tournois[0][0]['lieu']; ?></td>
         <td><?php echo $tournois[0][0]['nbr_equipes']; ?></td>
         <td><?php echo $tournois[0][0]['Esport']; ?></td>
       </tr>
     </table>
      </div>

     <div class = "rect2">
     <p>Informations sur les équipes</p>

     <table>
       <col span="7">
       <tr>
         <td>Nom équipe</td>
         <td>niveau</td>
         <td>adresse équipe</td>
         <td>téléphone</td>
         <td>nombre de joueurs</td>
         <td>nombre de victoires</td>
         <td>pseudo du capitaine</td>
       </tr>

       <tr>
         <td><?php echo $equipe1[0][0]['nom_equipe']; ?></td>
         <td><?php echo $equipe1[0][0]['niveau']; ?></td>
         <td><?php echo $equipe1[0][0]['adresse_equipe']; ?></td>
         <td><?php echo "0".$equipe1[0][0]['equipe_tel']; ?></td>
         <td><?php echo $equipe1[0][0]['nb_joueur']; ?></td>
         <td><?php echo $equipe1[0][0]['nb_victoire']; ?></td>
         <td><?php echo get_info_pseudo_capitaine($equipe1[0][0]['idCapitaine']); ?></td>
       </tr>

       <tr>
         <td><?php echo $equipe2[0][0]['nom_equipe']; ?></td>
         <td><?php echo $equipe2[0][0]['niveau']; ?></td>
         <td><?php echo $equipe2[0][0]['adresse_equipe']; ?></td>
         <td><?php echo "0".$equipe2[0][0]['equipe_tel']; ?></td>
         <td><?php echo $equipe2[0][0]['nb_joueur']; ?></td>
         <td><?php echo $equipe2[0][0]['nb_victoire']; ?></td>
         <td><?php echo get_info_pseudo_capitaine($equipe2[0][0]['idCapitaine']); ?></td>
       </tr>
     </table>
      </div>

     <?php
     if ($resultats_deja_ajoutes) {
       ?>
       <div class="rect3b">
         <p>Vous avez déjà ajouté les scores de cette rencontre.</p>
         <p>Equipe 1 : <?php echo $equipe1[0][0]['nom_equipe']; ?> </p>
         <p>score : <?php echo $info[0][0]['score1']; ?> </p>
         <p>Equipe 2 : <?php echo $equipe2[0][0]['nom_equipe']; ?> </p>
         <p>score : <?php echo $info[0][0]['score2']; ?> </p>
       </div>

       <?php
     }
     else {
       ?>
       <div class="rect3">
         <p>Attention, vous ne pouvez pas mettre de scores égaux.</p>
         <?php
         if ($aleatoire) {
           $valeur_aleatoire = val_aleatoire($sport);
           // var_dump($valeur_aleatoire);
           ?>
             <form action="../controller/functions_ajouter_resultats_monTournois.php" method="post">

               <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
              <input type="hidden" name="idMatch" value="<?php echo $idMatch; ?>">
              <input type="hidden" name="idEquipe1" value="<?php echo $equipe1[0][0]['idEquipe']; ?>">
              <input type="hidden" name="idEquipe2" value="<?php echo $equipe2[0][0]['idEquipe']; ?>">

              <p>Ajouter un résultat à l'équipe <?php echo $equipe1[0][0]['nom_equipe'];?> <input type="number" min="0" max="<?php echo $max; ?>" name="score1" value="<?php echo $valeur_aleatoire[0]; ?>" required></p>
              <p>Ajouter un résultat à l'équipe <?php echo $equipe2[0][0]['nom_equipe'];?> <input type="number" min="0" max="<?php echo $max; ?>" name="score2" value="<?php echo $valeur_aleatoire[1]; ?>" required></p>

              <input type="submit" class="boutonAjout" name="Ajouter" value="Ajout">
             </form>
           <?php
         }
         else {
           ?>
             <form action="../controller/functions_ajouter_resultats_monTournois.php" method="post">

              <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
              <input type="hidden" name="idMatch" value="<?php echo $idMatch; ?>">
              <input type="hidden" name="idEquipe1" value="<?php echo $equipe1[0][0]['idEquipe']; ?>">
              <input type="hidden" name="idEquipe2" value="<?php echo $equipe2[0][0]['idEquipe']; ?>">

              <p>Ajouter un résultat à l'équipe <?php echo $equipe1[0][0]['nom_equipe'];?> <input type="number" class="fielder" min="0" max="<?php echo $max; ?>" name="score1" required></p>
              <p>Ajouter un résultat à l'équipe <?php echo $equipe2[0][0]['nom_equipe'];?> <input type="number" class="fielder" min="0" max="<?php echo $max; ?>" name="score2" required></p>

              <input type="submit" class="boutonAjout" name="Ajouter" value="Ajout">
             </form>
           <?php
         }
          ?>
       </div>
       <?php
     }
  ?>
    <?php
     include 'sidebar.php';
     include 'header.php';
    ?>
  </body>
</html>
