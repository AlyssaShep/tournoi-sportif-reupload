<!DOCTYPE html>
<?php if(!isset($_GET["pseudo"])) {
          session_start();
      }
      $pseudo = $_GET['pseudo'];
      include '../controller/functions_monCompte.php';
?>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/monCompte.css" />
    <title>Mon Compte</title>
  </head>

  <body>
    <div class="pageName">
      <h1>Information de compte</h1>
    </div>

    <div class="rect">
      <h2> Pseudo : <?php echo $pseudo; ?> </h2>
      <h2> Nom : <?php echo $nom; ?> </h2>
      <h2> Prenom : <?php echo $prenom; ?> </h2>
      <h2> Mail : <?php echo $mail; ?> </h2>
      <h2> Téléphone : <?php echo $num; ?> </h2>

      <br></br>
      <a href="<?php echo "modifierCompte.php?pseudo=".$_GET['pseudo']; ?>"> <div class="validateRect"> <input type="button" value="Modifier mon Compte" class="bouton"/> </div> </a>
    </div>
  </body>

  <?php
    include 'sidebar.php';
    include 'header.php';
    ?>
</html>
