<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Inscription compétition</title>
    <link rel="stylesheet" href="../assets/connexion.css" />
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
  </head>
  <body>

    <form method="post" action="../controller/functions_competition.php">

      Nom                <input type="text" name="Nom"/><br/>
      Prénom             <input type="text" name="Prenom"/><br/>
      Date de naissance  <input type="date" name="Date" /><br/>
      Email              <input type="email" name="Email"/><br/>
      Numero             <input type="tel" name="Numero" pattern="[0-9]{10}"/><br/>
      <!-- le pattern permet de faire en sorte qu il n y ait pas un numero superieur a 10 -->

      Compétition        <select name="Competition"><br/>
                            <option value="tournois">Tournois</option>
                            <option value="championnat">Championnat</option>
                            <option value="poule">Poule</option>
                         </select> <br/>

      Competition        <input type="checkbox" name="case1" id="case" />
                              <label for="case">Tournois</label>
                         <input type="checkbox" name="case" id="case1" />
                              <label for="case">Poule</label>
                         <input type="checkbox" name="case" id="case2" />
                              <label for="case">Championnat</label><br/>

      Nom de l'équipe    <input type="text" name="equipe"/><br/>
      Nom du capitaine   <input type="text" name="capitaine"/><br/>
      Niveau             <input type="number" name="Numero" min="0" max="100"/><br/>
      Nombre d'équipiers <input type="number" name="nbrEquipiers" min="1/"><br/>

      <input type="submit" value="Valider"/>

    </form>
     <!-- < ?php
        include 'sidebar.php';
        include 'header.php';
     ?>  -->
  </body>
</html>
