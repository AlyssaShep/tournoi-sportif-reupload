<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <title>Inscription compétition</title>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/inscriptionTournois.css" />
  </head>
  <body>

    <?php
      $pseudo = $_GET["pseudo"];
      $idTournois = $_GET["id"];
      include '../controller/functions_monCompte.php';
      include '../controller/functions_inscription_equipes_monTournois.php';
      $idEquipes = get_equipes_participantes($idTournois);
      $tournois = get_info_competition($idTournois);
      $Esport = $tournois[0][0]['Esport'];
     ?>

     <h2><?php echo $tournois[0][0]['nom_tournois']; ?></h2>

      <div class="infoCompet">
        <p>Informations sur la compétition</p>

        <table>
          <col span="7">
          <tr>
            <td>Nom compétition</td>
            <td>Type</td>
            <td>date de début</td>
            <td>durée</td>
            <td>lieu</td>
            <td>nombre d'équipes</td>
            <td>Esport</td>
          </tr>


          <tr>
            <td><?php echo $tournois[0][0]['nom_tournois']; ?></td>
            <td><?php echo $tournois[0][0]['type']; ?></td>
            <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
            <td><?php echo strftime('  %d %B %Y ', strtotime($tournois[0][0]['dateDebut'])); ?></td>
            <td><?php echo $tournois[0][0]['duree']; ?></td>
            <td><?php echo $tournois[0][0]['lieu']; ?></td>
            <td><?php echo $tournois[0][0]['nbr_equipes']; ?></td>
            <td><?php echo $tournois[0][0]['Esport']; ?></td>
          </tr>
        </table>
      </div>

     <div class="infoEquipes">
       <p>Informations sur les équipes participantes</p>

       <?php
       $size = count($idEquipes);
       if(isset($idEquipes) && $size != 0){
        $sizeE = count($idEquipes[0]);
         for ($i=0; $i < $sizeE ; $i++) {
                 $infoE = get_info_equipe($idEquipes[0][$i]);
                 if($i == 0){   ?>

                   <table>
                     <col span="7">
                     <tr>
                       <td>Nom équipe</td>
                       <td>Niveau</td>
                       <td>adresse de l'équipe</td>
                       <td>téléphone</td>
                       <td>nombre de joueurs</td>
                       <td>nombre de victoires</td>
                       <td>Esport</td>
                     </tr>

                   <?php
                 } ?>

                 <tr>
                   <td><?php echo $infoE[0][0]['nom_equipe']; ?></td>
                   <td><?php echo $infoE[0][0]['niveau']; ?></td>
                   <td><?php echo $infoE[0][0]['adresse_equipe']; ?></td>
                   <td><?php echo "0".$infoE[0][0]['equipe_tel']; ?></td>
                   <td><?php echo $infoE[0][0]['nb_joueur']; ?></td>
                   <td><?php echo $infoE[0][0]['nb_victoire']; ?></td>
                   <td><?php echo $infoE[0][0]['Esport']; ?></td>
                 </tr>

                 <?php
               } ?>
             </table>
      <?php }
      else {
        ?>
          <p>Il n'y a pas d'équipes inscrites à cette compétition</p>
          <a href="<?php echo "mesTournois.php?pseudo=".$_GET['pseudo']; ?>"> <button class="bouton">Mes Tournois</button> </a>
        <?php
      } ?>

     </div>

     <div class="mesEquipes">
       <p>Vos équipes</p>
       <?php $recupEquipe = get_all_equipes($pseudo, $Esport);
             $deja_inscrits_autre_competition = deja_inscrits_autre_competition($recupEquipe);
             $deja_preinscrit = deja_preinscrits_autre_competition($recupEquipe);
             // var_dump($deja_inscrits_autre_competition);
             // var_dump($recupEquipe);
             $dejaInscript = verif_inscription($recupEquipe, $idTournois);
             // var_dump($dejaInscript);
             if (isset($recupEquipe) && $recupEquipe != false) {
               $sizeR = count($recupEquipe);
             }
             else {
               $sizeR = 0;
             }

             if ($dejaInscript) {
               ?>
               <p>Vous êtes déjà incripts à cette compétition</p>
               <?php
             }
             else {
              ?>
                <?php
                if ($deja_inscrits_autre_competition || $deja_preinscrit) {
                  ?>
                  <p>Vous êtes déjà inscrits ou préinscrits dans une compétition encore en cours.</p>
                  <?php
                }
                else {
                  $competComplete = verif_compet_complete($idTournois);
                  if ($competComplete) {
                    ?>
                    <p>Cette compétition est complète.</p>
                    <?php
                  }
                  else {
                    if(isset($recupEquipe) && $sizeR != 0){
                      $sizeRecup = count($recupEquipe[0]); ?>
                      <form class="" action="../controller/functions_inscription_equipes_monTournois.php" method="post">
                        <input type="hidden" name="nbre_equipes" value="<?php echo $sizeRecup; ?>">
                        <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                        <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">

                        <?php
                        $preinsc = verif_preinscription($recupEquipe, $idTournois);
                        if ($preinsc) {
                          ?>
                          <p>Vous etes déjà préinscript pour cette compétition</p>
                          <?php
                        }
                        else {
                          for ($i=0; $i < $sizeRecup; $i++) {
                            $infoE = get_info_equipe($recupEquipe[0][$i]);
                            if ($i == 0) {
                              ?>
                              <p>Pour que votre équipe puisse être inscripte à une compétition, il faut qu'elle ne soit pas déjà inscripte dans une autre.</p>
                              <p>Attention, vous devez sélectionner qu'une seule équipe</p>
                              <input type="hidden" name="<?php echo "idEquipe".$i; ?>" value="<?php echo $recupEquipe[0][$i]; ?>">
                              <div class = "listeEquipes">
                              <table>
                                <col span="9">
                                <tr>
                                  <td>Nom équipe</td>
                                  <td>Pseudo Capitaine</td>
                                  <td>Niveau</td>
                                  <td>adresse de l'équipe</td>
                                  <td>téléphone</td>
                                  <td>nombre de joueurs</td>
                                  <td>nombre de victoires</td>
                                  <td>Esport</td>
                                  <td>Liste des joueurs</td>
                                  <td>Sélectionner</td>
                                </tr>

                              <?php
                            } ?>
                              <tr>
                                <td><?php echo $infoE[0][0]['nom_equipe']; ?></td>
                                <td><?php echo get_pseudo_capitaine($infoE[0][0]['idCapitaine']); ?></td>
                                <td><?php echo $infoE[0][0]['niveau']; ?></td>
                                <td><?php echo $infoE[0][0]['adresse_equipe']; ?></td>
                                <td><?php echo "0".$infoE[0][0]['equipe_tel']; ?></td>
                                <td><?php echo $infoE[0][0]['nb_joueur']; ?></td>
                                <td><?php echo $infoE[0][0]['nb_victoire']; ?></td>
                                <td><?php echo $infoE[0][0]['Esport']; ?></td>
                                <td><?php $joueurs = get_joueurs_equipe($recupEquipe[0][$i]);
                                for ($j=0; $j < $infoE[0][0]['nb_joueur']; $j++) {
                                  ?>
                                  <p><?php echo $joueurs[0][$j]['pseudo']; ?></p>
                                  <?php
                                } ?></td>
                                <td> <input type="checkbox" name="<?php echo "check".$i; ?>"> </td>
                              </tr>


                            <?php
                          }
                          ?>
                        </table>
                        </div>
                         <input class = "boutonAjout" type="submit" name="Valider" value="Valider">
                       </form>

                      <p>Vous pouvez aussi créer une autre équipe</p>
                      <?php // session_start();
                            $_SESSION['pseudo'] = $pseudo; ?>
                      <a href="<?php echo "creation_equipe.php?pseudo=".$_SESSION['pseudo']; ?>">Créer mon équipe</a>


               <?php }
                    } else {
                      ?>
                      <p>Vous n'avez pas d'équipes disponible dans ce Esport. Vous pouvez la créer !</p>
                      <?php // session_start();
                            $_SESSION['pseudo'] = $pseudo; ?>
                      <a href="<?php echo "creation_equipe.php?pseudo=".$_SESSION['pseudo']; ?>">Créer mon équipe</a>
                      <?php
                    }

                  } ?>

                  <?php
                } ?>

              <?php
             }
             ?>

     </div>
     <?php
     include 'sidebar.php';
     include 'header.php';
    ?>
  </body>
</html>
