<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mes tournois</title>
    <!-- <link rel="stylesheet" href="../assets/mainCSS.css" /> -->
    <link rel="stylesheet" href="../assets/mesTournois.css">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <?php
      // include 'sidebar.php';
     include 'header.php';
    ?>
  </head>
  <body>
<?php session_start();
      $pseudo = $_GET["pseudo"];
      include '../controller/functions_monCompte.php';
      include '../controller/functions_mesTournois.php';
      report_tournois();
      prolonge_tournoi();
?>

<div class="BigDiv">
<!-- gestion de mes tournois -->
    <div class="gestionRect">

          <h3 class ="tableTitle"> Gestion de mes compétitions</h3>

          <?php if($isgestionnaire || $isadmin){ ?>
            <p>Attention, si votre tournoi n'a pas le nombre d'équipes requis, il sera reporter de 3 jours.</p>
          <div class="rectGestion1">
                    <p class="subtableTitle">Modification de mes tournois</p>
                    <!-- requete et recherche des tournois gerer par la personne -->
                    <!-- faire une verification si la personne a des tournois a gerer sinon lui proposer le bouton creer tournois -->
                    <!-- faire un affichage en tableau -> remplacer les echos par html en tableau-->
                            <?php
                              $gestion = get_tournois_gestion($idUser);
                              $size = count($gestion[0]);
                              if(isset($gestion) && $size != 0){ ?>

                                <table>
                                  <col span="12">
                                  <tr class='Nomcolonne'>
                                    <th>Nom de la compétion</th>
                                    <th>Type de compétition</th>
                                    <th>Esport</th>
                                    <th>Date de début</th>
                                    <th>Durée</th>
                                    <th>lieu</th>
                                    <th>Nombre d'équipes</th>
                                    <th>Résultats aléatoires</th>
                                    <th>Equipes aléatoires</th>
                                    <th>Supprimer ma compétition</th>
                                    <th>Modifier ma compétition</th>
                                  </tr>

                            <?php
                                   for ($i=0; $i < $size ; $i++) { ?>

                                     <tr>
                                      <td><?php echo $gestion[0][$i]['nom_tournois']; ?></td>
                                      <td><?php echo $gestion[0][$i]['type']; ?></td>
                                      <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                                      <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                             <td><?php echo  strftime('  %d %B %Y ', strtotime($gestion[0][$i]['dateDebut']))?></td>
                                       <td><?php echo $gestion[0][$i]['duree']; ?></td>
                                       <td><?php echo $gestion[0][$i]['lieu']; ?></td>
                                       <td><?php echo $gestion[0][$i]['nbr_equipes']; ?></td>
                                       <td><?php echo $gestion[0][$i]['resultats_aleatoires']; ?></td>
                                       <td><?php echo $gestion[0][$i]['equipes_aleatoires']; ?></td>
                                       <td><a href="<?php echo "supprimer_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$i]['idTournois']; ?>">Supprimer</a></td>
                                       <td><a href="<?php echo "modifier_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$i]['idTournois']; ?>">Modifier</a></td>
                                     </tr>

                      <?php         }?>

                            </table>

                    <?php   }
                        else { ?>
                            <p>Vous n'avez pas encore de compétitions de planifiées.</p>
                  <?php } ?>

          </div>
          <div class="rectGestion2">
                  <p class="subtableTitle">Ajouter les équipes</p>
                  <!-- requete et recherche des tournois gerer par la personne -->
                  <!-- faire une verification si la personne a des tournois a gerer sinon lui proposer le bouton creer tournois -->
                  <!-- faire un affichage en tableau -> remplacer les echos par html en tableau-->
                          <?php
                            $gestion = get_tournois_gestion($idUser);
                             $size = count($gestion[0]);
                            if(isset($gestion) && $size != 0){ ?>

                              <table>
                                <col span="12">
                                <tr class='Nomcolonne'>
                                  <th>Nom de la compétition</th>
                                  <th>Type de compétition</th>
                                  <th>Esport</th>
                                  <th>Date de début</th>
                                  <th>Durée</th>
                                  <th>Lieu</th>
                                  <th>Nombre d'équipes</th>
                                  <th>Ajouter les équipes à ma compétition</th>
                                </tr>

                          <?php
                                 for ($i=0; $i < $size ; $i++){ ?>

                                 <tr>
                                   <td><?php echo $gestion[0][$i]['nom_tournois']; ?></td>
                                   <td><?php echo $gestion[0][$i]['type']; ?></td>
                                   <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                                   <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                                   <td><?php echo  strftime('  %d %B %Y ', strtotime($gestion[0][$i]['dateDebut']))?></td>
                                   <td><?php echo $gestion[0][$i]['duree']; ?></td>
                                   <td><?php echo $gestion[0][$i]['lieu']; ?></td>
                                   <td><?php echo $gestion[0][$i]['nbr_equipes']; ?></td>
                                   <td><a href="<?php echo "ajouter_equipes_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$i]['idTournois']; ?>">Ajouter</a></td>
                                 </tr>

                    <?php         }?>

                          </table>

                  <?php   }
                          else { ?>

                              <p>Vous n'avez pas encore de compétitions.</p>

                    <?php } ?>
          </div>
          <div class="rectGestion3">
              <p class="subtableTitle">Planifier les compétitions</p>
            <!-- requete et recherche des tournois gerer par la personne -->
            <!-- faire une verification si la personne a des tournois a gerer sinon lui proposer le bouton creer tournois -->
            <!-- faire un affichage en tableau -> remplacer les echos par html en tableau-->
                    <?php
                      $gestion = get_tournois_plannification($idUser);
                      $size = count($gestion[0]);
                      if(isset($gestion) && $size != 0){ ?>

                        <table>
                          <col span="12">
                          <tr class='Nomcolonne'>
                          <th>Nom de la compétition</th>
                          <th>Type de compétition</th>
                          <th>Esport</th>
                            <th>Date de début</th>
                            <th>Durée</th>
                            <th>Lieu</th>
                            <th>Nombre d'équipes</th>
                            <th>Planifier les rencontres a ma compétition</th>
                          </tr>

                    <?php
                           for ($i=0; $i < $size ; $i++) { ?>

                             <tr>
                             <td><?php echo $gestion[0][$i]['nom_tournois']; ?></td>
                             <td><?php echo $gestion[0][$i]['type']; ?></td>
                             <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                             <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                             <td><?php echo  strftime('  %d %B %Y ', strtotime($gestion[0][$i]['dateDebut']))?></td>
                               <td><?php echo $gestion[0][$i]['duree']; ?></td>
                               <td><?php echo $gestion[0][$i]['lieu']; ?></td>
                               <td><?php echo $gestion[0][$i]['nbr_equipes']; ?></td>
                               <td><a href="<?php echo "planifier_rencontres_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$i]['idTournois']; ?>">Planifier</a></td>
                             </tr>

              <?php       } ?>

                    </table>

            <?php   }
                    else { ?>

                        <p>Vous n'avez pas encore de compétitions de commencées.</p>

              <?php } ?>

          </div>

          <div class="">
            <p>Modification de mes rencontres</p>
            <?php $gestion = get_tournois_plannification($idUser);
                  $size = count($gestion[0]);
                  if(isset($gestion) && $size != 0){ ?>

                  <?php for ($j=0; $j < $size; $j++) {
                    // var_dump($gestion[0]);
                      $rencontre = get_modif_rencontre($gestion[0][$j]['idTournois']);
                      $sizeT = count($rencontre[0]);
                      // var_dump($rencontre);
                    ?>
                    <?php if(isset($rencontre[0]) && $sizeT != 0){ ?>
                      <?php for ($i=0; $i < $sizeT ; $i++) { ?>

                           <?php if($i == 0){ ?>
                             <table>
                               <col span="7">
                               <tr class='Nomcolonne'>
                                 <th>Nom tournois</th>
                                 <th>Date de rencontre</th>
                                 <th>Heure de rencontre</th>
                                 <th>Equipe 1</th>
                                 <th>Equipe 2</th>
                                 <th>Modifier rencontre</th>
                                 <th>Supprimer rencontre</th>
                               </tr>
                          <?php } ?>

                           <tr>
                             <td><?php echo $gestion[0][$j]['nom_tournois']; ?></td>
                             <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                             <td><?php echo strftime('  %d %B %Y ', strtotime($rencontre[0][$i]['dateRencontre'])); ?></td>
                             <td><?php echo strftime('  %H : %M ', strtotime($rencontre[0][$i]['heureRencontre'])); ?></td>
                             <td><?php echo get_info_equipe($rencontre[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                             <td><?php echo get_info_equipe($rencontre[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                             <td><a href="<?php echo "modifier_rencontre_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$rencontre[0][$i]['idMatch']; ?>">Modifier</a></td>
                             <td><a href="<?php echo "supprimer_rencontre_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$rencontre[0][$i]['idMatch']; ?>">Supprimer</a></td>
                           </tr>

          <?php            }
                        } else { ?>
                          <p> <?php echo $gestion[0][$j]['nom_tournois']; ?> n'a pas encore de rencontres de planifier ou les rencontres se sont déjà déroulées.</p>
                    <?php }
                   } ?>
            </table>

            <?php
                } else { ?>

                        <p>Vous n'avez pas encore de compétitions de commencées.</p>

              <?php } ?>

          </div>

          <div class="">
            <p class="subtableTitle">Ajouter les resultats aux rencontres</p>
            <!-- requete et recherche des tournois gerer par la personne -->
            <!-- faire une verification si la personne a des tournois a gerer sinon lui proposer le bouton creer tournois -->
            <!-- faire un affichage en tableau -> remplacer les echos par html en tableau-->
                  <?php
                    $gestion = get_tournois_plannification($idUser);
                    $size = count($gestion[0]);
                    // var_dump($gestion[0][1]);
                    if(isset($gestion) && $size != 0){ ?>

                    <?php for ($j=0; $j < $size; $j++) {
                      // var_dump($gestion[0]);
                        $rencontre = get_rencontre_for_score($gestion[0][$j]['idTournois']);
                        $sizeT = count($rencontre[0]);
                        //var_dump($rencontre);
                      ?>
                      <?php if(isset($rencontre[0]) && $sizeT != 0){ ?>
                        <?php for ($i=0; $i < $sizeT ; $i++) { ?>

                             <?php if($i == 0){ ?>
                               <table>
                                 <col span="12">
                                 <tr class='Nomcolonne'>
                                   <th>Nom tournois</th>
                                   <th>Date de rencontre</th>
                                   <th>Heure de rencontre</th>
                                   <th>Equipe 1</th>
                                   <th>Equipe 2</th>
                                   <th>Ajouter les résultats</th>
                                 </tr>
                            <?php } ?>

                             <tr>
                               <td><?php echo $gestion[0][$j]['nom_tournois']; ?></td>
                               <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                               <td><?php echo strftime('  %d %B %Y ', strtotime($rencontre[0][$i]['dateRencontre'])); ?></td>
                               <td><?php echo strftime('  %H : %M ', strtotime($rencontre[0][$i]['heureRencontre'])); ?></td>
                               <td><?php echo get_info_equipe($rencontre[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                               <td><?php echo get_info_equipe($rencontre[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                               <td><a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$rencontre[0][$i]['idMatch']; ?>">Ajouter resultats</a></td>
                             </tr>

            <?php            }
                          } else { ?>
                            <p> <?php echo $gestion[0][$j]['nom_tournois']; ?> n'a pas encore de rencontres de terminer ou les résultats ont déja été entrés.</p>
                      <?php }
                     } ?>
              </table>

              <?php
                  } else { ?>

                          <p>Vous n'avez pas encore de compétitions de commencées.</p>

                <?php } ?>

          </div>

          <div class="">
            <h3>Modification résultats rencontres</h3>
            <p>Vous ne pouvez plus modifier les résultats 2 jours après la rencontre.</p>
            <?php
              $gestion = get_tournois_plannification($idUser);
              $size = count($gestion[0]);
              // var_dump($gestion[0][1]);
              if(isset($gestion) && $size != 0){ ?>

              <?php for ($j=0; $j < $size; $j++) {
                // var_dump($gestion[0]);
                  $rencontre = get_rencontre_modif_scores($gestion[0][$j]['idTournois']);
                  $sizeT = count($rencontre[0]);
                ?>
                <?php if(isset($rencontre[0]) && $sizeT != 0){ ?>
                  <?php for ($i=0; $i < $sizeT ; $i++) { ?>
                    <?php if($i == 0){ ?>
                        <table>
                           <col span="12">
                           <tr class='Nomcolonne'>
                             <th>Nom tournois</th>
                             <th>Date de rencontre</th>
                             <th>Heure de rencontre</th>
                             <th>Equipe 1</th>
                             <th>Equipe 2</th>
                             <th>Score 1</th>
                             <th>Score 2</th>
                             <th>modifier les résultats</th>
                           </tr>
                      <?php } ?>

                       <tr>
                         <td><?php echo $gestion[0][$j]['nom_tournois']; ?></td>
                         <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                         <td><?php echo strftime('  %d %B %Y ', strtotime($rencontre[0][$i]['dateRencontre'])); ?></td>
                         <td><?php echo strftime('  %d %B %Y ', strtotime($rencontre[0][$i]['heureRencontre'])); ?></td>
                         <td><?php echo get_info_equipe($rencontre[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                         <td><?php echo get_info_equipe($rencontre[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                         <td><?php echo $rencontre[0][$i]['score1']; ?></td>
                         <td><?php echo $rencontre[0][$i]['score2']; ?></td>
                         <td><a href="<?php echo "modifier_resultats_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$rencontre[0][$i]['idMatch']; ?>">Modifier resultats</a></td>
                       </tr>

      <?php            }
                    } else { ?>
                      <p> <?php echo $gestion[0][$j]['nom_tournois']; ?> n'a pas encore de rencontres de terminer.</p>
                <?php }
               } ?>
        </table>
        <?php
            } else { ?>

                    <p>Vous n'avez pas encore de rencontres de terminées.</p>

          <?php } ?>

          <?php } else { ?>

             <p>Si vous voulez créer vos propres tournois: modifiez votre compte et devenez administrateur/gestionnaire.</p>

          <?php } ?>
          </div>
  </div>

<!-- Mes tournois en cours  -->
    <div class="enCourRect">
      <h3 class ="tableTitle">Mes compétitions en cours</h3>
      <!-- voir le tournois en general -->
      <div class="">
        <p>Participation aux compétitions</p>
        <!-- afficher les tournois ou la date  de fin est superieur ou egal a la date d ajd-->
              <?php
                $idEquipe = get_equipe_joueur_tournois_encours($pseudo);
                // var_dump($idEquipe);
                // var_dump($sizeid);
                  if(isset($idEquipe) && $idEquipe != false){
                      $sizeid = count($idEquipe[0]);
                      // $size = count($gestion[0]);
                      if($sizeid != 0 && isset($idEquipe)){
                           for ($j=0; $j < $sizeid ; $j++) {
                             $gestion = get_tournois_participation($idEquipe[0][$j]['idEquipeJ']);
                             // var_dump($gestion);
                                 if($j == 0){?>

                                  <div class="rectGestion1">
                                 <table>
                                   <col span="12">
                                   <tr class='Nomcolonne'>
                                   <th>Nom de la compétition</th>
                                   <th>nom équipe</th>
                                   <th>Pseudo du Gestionnaire</th>
                                   <th>Type de compétition</th>
                                   <th>Esport</th>
                                     <th>Date de début</th>
                                     <th>Durée</th>
                                     <th>Lieu</th>
                                     <th>Nombre d'éuipes</th>
                                     <th>Voir le détail</th>
                                   </tr>
                          <?php }
                          if ($gestion != false) {
                            ?>

                            <tr>
                                 <td><?php echo $gestion[0][0]['nom_tournois']; ?></td>
                                 <td><?php echo get_info_equipe($idEquipe[0][$j]['idEquipeJ'])[0][0]['nom_equipe']; ?></td>
                                 <td><?php echo get_pseudo_idGestionnaire($gestion[0][0]['idGestionnaire']); ?></td>
                                 <td><?php echo $gestion[0][0]['type']; ?></td>
                                 <td><?php echo $gestion[0][0]['Esport']; ?></td>
                                 <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                               <td><?php echo  strftime('  %d %B %Y ', strtotime($gestion[0][0]['dateDebut']))?></td>
                                   <td><?php echo $gestion[0][0]['duree']; ?></td>
                                   <td><?php echo $gestion[0][0]['lieu']; ?></td>
                                   <td><?php echo $gestion[0][0]['nbr_equipes']; ?></td>
                                   <td><a href="<?php echo "details_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][0]['idTournois']; ?>">Voir</a></td>
                                 </tr>

                            <?php
                          } ?>


            <?php         }?>
                    </table></div>
            <?php }
                  else {?>
                      <p>Il n'y a pas de compétitions en cours.</p>
              <?php
                  }

              } else { ?>
                     <p>Il n'y a pas de compétitions en cours.</p>
          <?php } ?>
      </div>

      <div class="">
        <p>Mes rencontres</p>
        <?php
          $idEquipe = get_equipe_joueur_tournois_encours($pseudo);
          $sizeid = count($idEquipe[0]);
          // var_dump($idEquipe);
          // var_dump($sizeid);
            if(isset($idEquipe) && $sizeid != 0){
                $gestion = get_rencontre_participation($idEquipe[0][0]['idEquipeJ']);
                // var_dump($gestion);
                $size = count($gestion[0]);
                if($size != 0 && isset($gestion)){
                     for ($j=0; $j < $size ; $j++) {
                           if($j == 0){?>
                            <div class="rectGestion1">
                             <table>
                               <col span="12">
                               <tr class='Nomcolonne'>
                                 <th>nom équipe</th>
                                 <th>nom compétition</th>
                                 <th>date de rencontre</th>
                                 <th>heure</th>
                                 <th>Pseudo du Gestionnaire</th>
                                 <th>Equipe 1</th>
                                 <th>Equipe 2</th>
                                 <th>Voir la rencontre</th>
                               </tr>

                      <?php } ?>

                           <tr>
                             <td><?php echo get_info_equipe($idEquipe[0][0]['idEquipeJ'])[0][0]['nom_equipe']; ?></td>
                             <td><?php echo get_info_competition($gestion[0][$j]['idTournois'])[0][0]['nom_tournois']; ?></td>
                             <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                             <td><?php echo strftime('  %d %B %Y ', strtotime($gestion[0][$j]['dateRencontre'])); ?></td>
                             <td><?php echo strftime('  %H : %M ', strtotime($gestion[0][$j]['heureRencontre'])); ?></td>
                             <td><?php echo get_pseudo_idGestionnaire(get_info_competition($gestion[0][$j]['idTournois'])[0][0]['idGestionnaire']); ?></td>
                             <td><?php echo get_info_equipe($gestion[0][$j]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                             <td><?php echo get_info_equipe($gestion[0][$j]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                             <td><a href="<?php echo "voir_rencontre_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$j]['idMatch']; ?>">Voir</a></td>
                           </tr>

            <?php         }?>
                    </table>
            <?php }
                  else {?>
                      <p>Il n'y a pas de compétitions en cours.</p>
              <?php
                  }

              } else { ?>
                     <p>Il n'y a pas de compétitions en cours.</p>
          <?php } ?>
      </div>
    </div>
    </div>

    <div class="incriRect">
              <h3 class ="tableTitle">Inscription a une compétition</h3>

              <!-- afficher les tournois ou la date de debut est inferieur a la date d ajd -->
              <?php
                $gestion = get_tournois_pour_inscription();
                $size = count($gestion[0]);
                if(isset($gestion) && $size != 0){ ?>

                <div class="rectGestion1">
                <p class="subtableTitle">  </p>

                  <table>
                    <col span="12">
                    <tr class='Nomcolonne'>
                    <th>Nom de la compétition</th>
                    <th>Pseudo du Gestionnaire</th>
                    <th>Type de compétition</th>
                    <th>Esport</th>
                      <th>Date de début</th>
                      <th>Durée</th>
                      <th>Lieu</th>
                      <th>Nombre d'équipes</th>
                      <th>Inscription a une compétition</th>
                    </tr>

                  <?php
                     for ($i=0; $i < $size ; $i++) { ?>
                       <tr>
                       <td><?php echo $gestion[0][$i]['nom_tournois']; ?></td>
                       <td><?php echo get_pseudo_idGestionnaire($gestion[0][$i]['idGestionnaire']); ?></td>
                       <td><?php echo $gestion[0][$i]['type']; ?></td>
                       <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                       <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                             <td><?php echo  strftime('  %d %B %Y ', strtotime($gestion[0][$i]['dateDebut']))?></td>
                         <td><?php echo $gestion[0][$i]['duree']; ?></td>
                         <td><?php echo $gestion[0][$i]['lieu']; ?></td>
                         <td><?php echo $gestion[0][$i]['nbr_equipes']; ?></td>
                         <td><a href="<?php echo "inscription_equipes_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$i]['idTournois']; ?>">Inscription</a></td>
                       </tr>
                  <?php }?>
                  </table>
                </div>

            <?php }
               else { ?>
                 <p>Il n'y a pas de compétitions disponibles.</p>
           <?php } ?>
    </div>

<!-- reprendre ici -> modifier tournois participés terminés + ajouter rencontres termines -->
    <div class="termineRect">
<h3 class="tableTitle">Compétitions participés terminés</h3>
      <div class="">
        <p>Mes tournois terminés</p>
        <?php
          $idEquipe = get_equipe_joueur_tournois_encours($pseudo);
          $sizeid = count($idEquipe[0]);
          // var_dump($idEquipe);
          // var_dump($sizeid);
            if(isset($idEquipe) && $sizeid != 0){
                $gestion = get_tournois_participation_terminee($idEquipe[0][0]['idEquipeJ']);
                $size = count($gestion[0]);
                if($size != 0 && isset($gestion)){
                     for ($j=0; $j < $size ; $j++) {
                           if($j == 0){?>
                           <div class="rectGestion1"> <table>


                               <col span="12">
                               <tr class='Nomcolonne'>
                               <th>Nom de la compétition</th>
                               <th>nom équipe</th>
                               <th>Pseudo du Gestionnaire</th>
                               <th>Type de compétition</th>
                               <th>Esport</th>
                                 <th>Date de début</th>
                                 <th>Durée</th>
                                 <th>Lieu</th>
                                 <th>Nombre d'équipes</th>
                                 <th>Voir le détail de la Compétition</th>
                               </tr>

                      <?php } ?>

                           <tr>
                           <td><?php echo $gestion[0][$j]['nom_tournois']; ?></td>
                           <td><?php echo get_info_equipe($idEquipe[0][0]['idEquipeJ'])[0][0]['nom_equipe']; ?></td>
                           <td><?php echo get_pseudo_idGestionnaire($gestion[0][$j]['idGestionnaire']); ?></td>
                           <td><?php echo $gestion[0][$j]['type']; ?></td>
                           <td><?php echo $gestion[0][$j]['Esport']; ?></td>
                           <?php $date = $gestion[0][$j]['dateDebut'];
                           setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                             <td><?php echo  strftime('  %d %B %Y ', strtotime($date))?></td>
                             <td><?php echo $gestion[0][$j]['duree']; ?></td>
                             <td><?php echo $gestion[0][$j]['lieu']; ?></td>
                             <td><?php echo $gestion[0][$j]['nbr_equipes']; ?></td>
                             <td><a href="<?php echo "details_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$j]['idTournois']; ?>">Voir détail</a></td>
                             </tr>

<?php         }?>
        </table>
<?php }
      else {?>
          <p>Il n'y a pas de compétitions terminées.</p>
  <?php
      }

  } else { ?>
         <p>Il n'y a pas de compétitions terminées.</p>
<?php } ?>
</div>

<div class="">
<p>Mes rencontres terminées</p>
<?php
$idEquipe = get_equipe_joueur_tournois_encours($pseudo);
$sizeid = count($idEquipe[0]);
// var_dump($idEquipe);
// var_dump($sizeid);
if(isset($idEquipe) && $sizeid != 0){
    $gestion = get_rencontre_terminees($idEquipe[0][0]['idEquipeJ']);
    $size = count($gestion[0]);
    if($size != 0 && isset($gestion)){
         for ($j=0; $j < $size ; $j++) {
               if($j == 0){?>

                <div class="rectGestion1">
                 <table>
                   <col span="12">
                   <tr class='Nomcolonne'>
                     <th>nom équipe</th>
                     <th>nom tournois</th>
                     <th>Pseudo du Gestionnaire</th>
                     <th>date de rencontre</th>
                     <th>heure</th>
                     <th>Equipe 1</th>
                     <th>Equipe 2</th>
                     <th>Equipe gagnante</th>
                     <th>score 1</th>
                     <th>score 2</th>
                     <th>Voir le détail</th>
                   </tr>

          <?php } ?>

               <tr>
                 <td><?php echo get_info_equipe($idEquipe[0][0]['idEquipeJ'])[0][0]['nom_equipe']; ?></td>
                 <td><?php echo get_info_competition($gestion[0][$j]['idTournois'])[0][0]['nom_tournois']; ?></td>
                 <td><?php echo get_info_membre(get_info_competition($gestion[0][$j]['idTournois'])[0][0]['idGestionnaire'])[0][0]['pseudo']; ?></td>
                 <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                 <td><?php echo strftime('  %d %B %Y ', strtotime($gestion[0][$j]['dateRencontre'])); ?></td>
                 <td><?php echo strftime('  %H : %M ', strtotime($gestion[0][$j]['heureRencontre'])); ?></td>
                 <td><?php echo get_info_equipe($gestion[0][$j]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                 <td><?php echo get_info_equipe($gestion[0][$j]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                 <td><?php echo get_info_equipe($gestion[0][$j]['idGagnant'])[0][0]['nom_equipe']; ?></td>
                 <td><?php echo $gestion[0][$j]['score1']; ?></td>
                 <td><?php echo $gestion[0][$j]['score2']; ?></td>
                 <td><a href="<?php echo "voir_detail_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$j]['idMatch']; ?>">Voir</a></td>
               </tr>

<?php         }?>
        </table>
<?php }
      else {?>
          <p>Il n'y a pas de compétitions terminées.</p>
  <?php
      }

  } else { ?>
         <p>Il n'y a pas de compétitions terminées.</p>
<?php } ?>
</div>

    </div>

    <div class="gestionTermineRect">
          <h3> Gestion de mes compétitions terminés</h3>
          <?php if($isgestionnaire || $isadmin){ ?>
          <!-- requete et recherche des tournois gerer par la personne -->
          <!-- faire une verification si la personne a des tournois a gerer sinon lui proposer le bouton creer tournois -->
          <!-- faire un affichage en tableau -> remplacer les echos par html en tableau-->
                  <?php
                    $gestion = get_tournois_gestion_termines($idUser);
                    $size = count($gestion[0]);
                    if(isset($gestion) && $size != 0){ ?>

                      <div class="rectGestion1">
                        <table>
                        <col span="12">
                        <tr class='Nomcolonne'>
                        <th>Nom de la compétition</th>
                        <th>Pseudo du Gestionnaire</th>
                        <th>Type de compétition</th>
                        <th>Esport</th>
                          <th>Date de début</th>
                          <th>Durée</th>
                          <th>Lieu</th>
                          <th>Nombre d'équipes</th>
                          <!-- <th>resultats_aleatoires</th> -->
                          <!-- <th>equipes_aleatoires</th> -->
                          <th>Voir le détail de ma compétition</th>
                        </tr>
                  <?php
                         for ($i=0; $i < $size ; $i++) {?>
                           <tr>
                           <td><?php echo $gestion[0][$i]['nom_tournois']; ?></td>
                           <td><?php echo get_pseudo_idGestionnaire($gestion[0][$i]['idGestionnaire']); ?></td>
                           <td><?php echo $gestion[0][$i]['type']; ?></td>
                           <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                           <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                             <td><?php echo  strftime('  %d %B %Y ', strtotime($gestion[0][$i]['dateDebut']))?></td>
                             <td><?php echo $gestion[0][$i]['duree']; ?></td>
                             <td><?php echo $gestion[0][$i]['lieu']; ?></td>
                             <td><?php echo $gestion[0][$i]['nbr_equipes']; ?></td>
                             <td><a href="<?php echo "details_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$i]['idTournois']; ?>">Voir détail</a></td>
                           </tr>
            <?php         }?>
                  </table></div>

          <?php   }
                  else { ?>
                      <p>Vous n'avez pas encore de compétitions.</p>
            <?php } ?>

            <div class="">
              <p>Mes rencontres planifiées terminées</p>
              <?php
                $gestion = get_all_gestion($idUser);
                if(isset($gestion) && $gestion != false && !empty($gestion[0])){
                  $size = count($gestion[0]);
                  ?>
                  <div class="rectGestion1"> <table>
                    <col span="12">
                    <tr class='Nomcolonne'>
                    <th>Nom de la compétition</th>
                    <th>Type de compétition</th>
                    <th>Esport</th>
                      <th>Date de Rencontre</th>
                      <th>Heure de rencontre</th>
                      <th>Equipe 1</th>
                      <th>Equipe 2</th>
                      <th>Voir le détail de ma compétition</th>
                    </tr>
              <?php
                     for ($i=0; $i < $size ; $i++) {
                       $rencontres = get_rencontre_terminees_gestion($gestion[0][$i]['idTournois']);
                       if(!empty($rencontres[0]) && $rencontres != false)
                       {

                       $size2 = count($rencontres[0]);

                       for ($j=0; $j < $size2 ; $j++)
                       {
                         ?>
                         <tr>
                         <td><?php echo $gestion[0][$i]['nom_tournois']; ?></td>
                         <td><?php echo $gestion[0][$i]['type']; ?></td>
                         <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                         <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                           <td><?php echo  strftime('  %d %B %Y ', strtotime($rencontres[0][$j]['dateRencontre']))?></td>
                           <td><?php echo strftime('  %H : %M ', strtotime($rencontres[0][$j]['heureRencontre'])); ?></td>
                           <td><?php echo get_info_equipe($rencontres[0][$j]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
                           <td><?php echo get_info_equipe($rencontres[0][$j]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
                           <td><a href="<?php echo "voir_detail_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$rencontres[0][$j]['idMatch']; ?>">Voir détail</a></td>
                         </tr>
                         <?php
                       } }else
                       {?>
                        <p>Vous n'avez pas de rencontres terminées pour <?php echo $gestion[0][$i]['nom_tournois'] ; ?></p>
                        <?php
                       }?>

        <?php         }?>
              </table></div>

      <?php   }
              else { ?>
                  <p>Vous n'avez pas encore de rencontres terminées.</p>
        <?php } ?>

            </div>

       <?php }
          else { ?>
            <p>Les compétitions, que vous gérez, ne sont peut-être pas encore terminées.</p>
      <?php } ?>
    <!-- </div> -->

</div>
  </body>
</html>
