<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/planifier.css">
    <title>Plannifier</title>
  </head>
  <body>
    <?php
        $pseudo = $_GET['pseudo'];
        $idTournois = $_GET['id'];
        session_start();
        include '../controller/functions_planifier_rencontres_monTournois.php';
        $info = recup_info_compet($idTournois);
        $nbr_equipes = $info[0][0]['nbr_equipes'];
        $rencontres = get_rencontre_tournois($idTournois);
        $type = $info[0][0]['type'];
        // echo "$type";
     ?>
    <div  class="rect">
       <!-- mettre les rencontres du tournoi -->
       <?php
       // $sizeR = count($rencontres);
       if(isset($rencontres) && !empty($rencontres[0])){
          $sizeR = count($rencontres[0]);
          for ($i=0; $i < $sizeR; $i++) {
            if($i == 0){
              ?>
              <div class = "centre">
              <table>
                <col span="7">
                <tr class="Nomcolonne">
                  <th>Date de rencontre</th>
                  <th>Heure de rencontre</th>
                  <th>Equipe 1</th>
                  <th>Equipe 2</th>
                </tr>
              <?php
            }?>

            <tr>
              <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
              <td><?php echo strftime('  %d %B %Y ', strtotime($rencontres[0][$i]['dateRencontre'])); ?></td>
              <td><?php echo strftime('  %H : %M ', strtotime($rencontres[0][$i]['heureRencontre'])); ?></td>
              <td><?php echo recup_info_equipe($rencontres[0][$i]['idEquipe1'])[0][0]['nom_equipe']; ?></td>
              <td><?php echo recup_info_equipe($rencontres[0][$i]['idEquipe2'])[0][0]['nom_equipe']; ?></td>
            </tr>

            <?php
          }
          ?>
          </table>
         </div>
          <?php
       }
        ?>

     <?php
     if ($type == "championnat") {
       $verif = championnat_termine($idTournois);
     }else {
         $verif = false;

     }
      ?>

      <?php
      if ($verif == true) {
        ?>
        <?php
        if ($type == "championnat") {
          $all_equipes = array();
          for ($i=0; $i < $nbr_equipes; $i++) {
            $all_equipes[0][$i]['pts'] = 0;
            $all_equipes[0][$i]['Equipe'] = recup_idEquipe($idTournois, $i);
          }
          $all_rencontres = get_rencontre_tournois($idTournois);
          $size = count($all_rencontres[0]);
          for ($i=0; $i < $size; $i++) {
            for ($j=0; $j < $nbr_equipes; $j++) {
              if (!empty($all_rencontres[0][$i]['idGagnant'])) {
                if ($all_equipes[0][$j]['Equipe'] == $all_rencontres[0][$i]['idGagnant']) {
                  $all_equipes[0][$j]['pts'] += 3;
                }
              }
            }
          }

          $max = 0;
          for ($i=0; $i < $nbr_equipes; $i++) {
            if ($max < $all_equipes[0][$i]['pts']) {
              $max = $all_equipes[0][$i]['pts'];
            }
          }

          $gagnant = array();
          $id = 0;
          for ($i=0; $i < $nbr_equipes; $i++) {
            if ($max == $all_equipes[0][$i]['pts']) {
              $gagnant[0][$id]['Equipe'] = $all_equipes[0][$i]['Equipe'];
              $gagnant[0][$id]['pts'] = $all_equipes[0][$i]['pts'];
              $id++;
            }
          }
          $validation = championnat_scores_et_termines($idTournois);;
          if($validation){
            termine_competition($idTournois);
          }

          if (count($gagnant[0]) == 1) {
            $classement = classement_championnat($all_equipes, $gagnant);
            // var_dump($classement);
            ?>
            <div class="sousPart">
            <p>Le gagnant de votre championnat est <?php echo recup_info_equipe($gagnant[0][0]['Equipe'])[0][0]['nom_equipe']; ?>.</p>
            <p>Classement</p>
            </div>
            <?php
            $size = count($classement[0]);
            for ($i=0; $i < $size; $i++) {
              if ($i == 0) {
                ?>
                <div class ="centre">
                <table>
                  <col span="7">
                  <tr class="Nomcolonne">
                    <th>Places</th>
                    <th>Equipes</th>
                    <th>scores</th>
                  </tr>

                <?php
              }
              ?>
              <tr>
                <td><?php echo $i+1; ?></td>
                <td><?php echo recup_info_equipe($classement[0][$i]['Equipe'])[0][0]['nom_equipe']; ?></td>
                <td><?php echo $classement[0][$i]['pts']; ?></td>
              </tr>
              <?php
            }
             ?>
             </table>
          </div>
            <?php
          }
          else {
            $legagnant = array_rand($gagnant, 1);
            $etGa = array();
            $size = count($gagnant[0]);
            for($i = 0; $i < $size; $i++){
              if($gagnant[0][$i]['Equipe'] == $gagnant[0][$legagnant]['Equipe']){
                $etGa[0][0]['Equipe'] = $gagnant[0][$legagnant]['Equipe'];
                $etGa[0][0]['pts'] = $gagnant[0][$i]['pts'];
              }
            }
            $classement = classement_championnat_egalite($all_equipes, $etGa);
            ?>
            <div class = "sousPart">
             <p>Le gagnant de votre championnat est <?php echo recup_info_equipe($gagnant[0][$legagnant]['Equipe'])[0][0]['nom_equipe']; ?>.</p>
             <p>Classement</p>
            </div>
             <?php
             $size = count($classement[0]);
             for ($i=0; $i < $size; $i++) {
               if ($i == 0) {
                 ?>
                 <div class="centre">
                 <table>
                   <col span="7">
                   <tr class="Nomcolonne">
                     <th>Places</th>
                     <th>Equipes</th>
                     <th>scores</th>
                   </tr>

                 <?php
               }
               ?>
               <tr>
                 <td><?php echo $i+1; ?></td>
                 <td><?php echo recup_info_equipe($classement[0][$i]['Equipe'])[0][0]['nom_equipe']; ?></td>
                 <td><?php echo $classement[0][$i]['pts']; ?></td>
               </tr>
               <?php
             }
              ?>
              </table>
            </div>
            <?php
          }
          ?>

          <?php
        } ?>
        <div class="soussousPart">
        <p>Toutes vos équipes se sont rencontrés dans votre <?php echo $type; ?></p>
        </div>
        <?php
      }
      else {
        if ($type == "championnat") {
          ?>

            <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
              <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
              <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
              <input type="hidden" name="nbr_equipes" value="<?php echo $nbr_equipes; ?>">
              <input type="hidden" name="type" value="<?php echo $info[0][0]['type']; ?>">
              <p>Date de la rencontre <input type="date" name="dateRencontre" required> </p>
              <p>heure de la rencontre <input type="time" name="heureRencontre" required> </p>

              <p>Choisissez 2 équipes pour planifier la rencontre </p>
              <?php
                  for ($i=0; $i < $nbr_equipes; $i++) {
                    $idEquipe = recup_idEquipe($idTournois, $i);
                    if($i == 0){ ?>
                    <div class="centre">
                      <table>
                        <col span="7">
                        <tr class="Nomcolonne">
                          <th>Nom équipe</th>
                          <th>niveau</th>
                          <th>adresse équipe</th>
                          <th>équipe téléphone</th>
                          <th>nombre de joueurs</th>
                          <th>Pseudo Capitaine</th>
                          <th>Selectionner</th>
                        </tr>
             <?php
                    }
                    $info_equipe = recup_info_equipe($idEquipe);
                    $size = count($info_equipe[0]);
                    for ($j=0; $j < $size; $j++) { ?>
                      <input type="hidden" name="<?php echo "idEquipe".$i; ?>" value="<?php echo $info_equipe[0][$j]['idEquipe']; ?>">
                        <tr>
                          <td><?php echo $info_equipe[0][$j]['nom_equipe']; ?></td>
                          <td><?php echo $info_equipe[0][$j]['niveau']; ?></td>
                          <td><?php echo $info_equipe[0][$j]['adresse_equipe']; ?></td>
                          <td><?php echo "0".$info_equipe[0][$j]['equipe_tel']; ?></td>
                          <td><?php echo $info_equipe[0][$j]['nb_joueur']; ?></td>
                          <td><?php echo info_capitaine($info_equipe[0][$j]['idCapitaine'])[0][0]['pseudo']; ?></td>
                          <td><input type="checkbox" name="<?php echo "Ajout".$i; ?>" /></td>
                        </tr>
             <?php   }
                  }
               ?>
               </table>
                </div>
              <input class="validateButton" type="submit" name="Valider" value="Valider">
            </form>

          <?php
        }

        else {
          if ($type == "tournois") {
            // affichage des poules du tournoi
            // affichage arbre binare
            $verf = poules($idTournois);
            $poules = get_poule_tournois($idTournois);
            $size1 = count($poules);
            $debTournois = 0;
            if(isset($poules) && $size1 != 0 && $nbr_equipes != 2 && $nbr_equipes != 4){

              $size2 = count($poules[0]);
              $toutgagnant = array();
              $id = 0;
              for ($i=0; $i < $size2; $i++) {
                  ?>

                  <div class="centre">
                    <table>
                      <col span="7">
                      <tr class="Nomcolonne">
                        <th>Numero de poule</th>
                        <th>Nom équipe</th>
                        <th>niveau</th>
                        <th>adresse équipe</th>
                        <th>équipe téléphone</th>
                        <th>nombre de joueurs</th>
                        <th>Pseudo Capitaine</th>
                      </tr>

                    <?php

                    $info_equipe1 = recup_info_equipe($poules[0][$i]['idEP1']);
                    $info_equipe2 = recup_info_equipe($poules[0][$i]['idEP2']);
                    $info_equipe3 = recup_info_equipe($poules[0][$i]['idEP3']);
                    $info_equipe4 = recup_info_equipe($poules[0][$i]['idEP4']);
                    ?>

                    <tr>
                      <td><?php echo $poules[0][$i]['idPoule']; ?></td>
                      <td><?php echo $info_equipe1[0][0]['nom_equipe']; ?></td>
                      <td><?php echo $info_equipe1[0][0]['niveau']; ?></td>
                      <td><?php echo $info_equipe1[0][0]['adresse_equipe']; ?></td>
                      <td><?php echo "0".$info_equipe1[0][0]['equipe_tel']; ?></td>
                      <td><?php echo $info_equipe1[0][0]['nb_joueur']; ?></td>
                      <td><?php echo info_capitaine($info_equipe1[0][0]['idCapitaine'])[0][0]['pseudo']; ?></td>
                    </tr>

                    <tr>
                      <td><?php echo $poules[0][$i]['idPoule']; ?></td>
                      <td><?php echo $info_equipe2[0][0]['nom_equipe']; ?></td>
                      <td><?php echo $info_equipe2[0][0]['niveau']; ?></td>
                      <td><?php echo $info_equipe2[0][0]['adresse_equipe']; ?></td>
                      <td><?php echo "0".$info_equipe2[0][0]['equipe_tel']; ?></td>
                      <td><?php echo $info_equipe2[0][0]['nb_joueur']; ?></td>
                      <td><?php echo info_capitaine($info_equipe2[0][0]['idCapitaine'])[0][0]['pseudo']; ?></td>
                    </tr>

                    <tr>
                      <td><?php echo $poules[0][$i]['idPoule']; ?></td>
                      <td><?php echo $info_equipe3[0][0]['nom_equipe']; ?></td>
                      <td><?php echo $info_equipe3[0][0]['niveau']; ?></td>
                      <td><?php echo $info_equipe3[0][0]['adresse_equipe']; ?></td>
                      <td><?php echo "0".$info_equipe3[0][0]['equipe_tel']; ?></td>
                      <td><?php echo $info_equipe3[0][0]['nb_joueur']; ?></td>
                      <td><?php echo info_capitaine($info_equipe3[0][0]['idCapitaine'])[0][0]['pseudo']; ?></td>
                    </tr>

                    <tr>
                      <tr><td><?php echo $poules[0][$i]['idPoule']; ?></td>
                      <td><?php echo $info_equipe4[0][0]['nom_equipe']; ?></td>
                      <td><?php echo $info_equipe4[0][0]['niveau']; ?></td>
                      <td><?php echo $info_equipe4[0][0]['adresse_equipe']; ?></td>
                      <td><?php echo "0".$info_equipe4[0][0]['equipe_tel']; ?></td>
                      <td><?php echo $info_equipe4[0][0]['nb_joueur']; ?></td>
                      <td><?php echo info_capitaine($info_equipe4[0][0]['idCapitaine'])[0][0]['pseudo']; ?></td>
                    </tr>

                    <?php
                    $all_prevus = all_poule_prevus($idTournois, $poules[0][$i]['idPoule']);
                    $verifPoule = verif_all_poule_passes($idTournois,  $poules[0][$i]['idPoule']);

                    if ($verifPoule) {
                      all_poule_passes($idTournois, $poules[0][$i]['idPoule']);
                    }

                    $fini = poule_termine($poules[0][$i]['idPoule']);

                    // var_dump($fini);
                    if ($fini) {
                      if ($nbr_equipes == 4) {
                        $gagnants = recup_gagnants_poules($poules[0][$i]['idPoule']);
                        // var_dump($gagnants[0]);
                        $toutgagnant[0][$id] = $gagnants[0][0];
                        $id++;
                        $toutgagnant[0][$id] = $gagnants[0][1];
                        $id++;
                      }
                      else {
                        $gagnants = recup_gagnants_poules($poules[0][$i]['idPoule']);
                        // var_dump($gagnants[0]);
                        $toutgagnant[0][$id]['Equipe'] = $gagnants[0][0];
                        $toutgagnant[0][$id]['Poule'] = $poules[0][$i]['idPoule'];
                        $id++;
                        $toutgagnant[0][$id]['Equipe'] = $gagnants[0][1];
                        $toutgagnant[0][$id]['Poule'] = $poules[0][$i]['idPoule'];
                        $id++;
                      }

                      ?>
                      <p>La poule <?php echo $poules[0][$i]['idPoule']; ?> est terminée.</p>
                      <p>Les gagnants sont <?php echo recup_info_equipe($gagnants[0][0])[0][0]['nom_equipe']; ?> et <?php echo recup_info_equipe($gagnants[0][1])[0][0]['nom_equipe']; ?>.</p>
                      <?php
                      $debTournois+=2;
                    }

                    else {
                      if ($all_prevus) {
                        ?>
                        <p>Cette poule a ses matchs de selection de prévus. Ils ne se sont pas encore tous déroulés ou vous n'avez pas encore ajouté les résultats.</p>
                        <?php
                      }
                      else {
                        ?>
                          <p>Planifier les rencontre de vos poules</p>
                          <p>Selectionner deux équipes et donner une date.</p>
                          <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                            <p>Numero de poule : <?php echo $poules[0][$i]['idPoule']; ?></p>
                            <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                            <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                            <input type="hidden" name="idEq1" value="<?php echo $poules[0][$i]['idEP1']; ?>">
                            <input type="hidden" name="idEq2" value="<?php echo $poules[0][$i]['idEP2']; ?>">
                            <input type="hidden" name="idEq3" value="<?php echo $poules[0][$i]['idEP3']; ?>">
                            <input type="hidden" name="idEq4" value="<?php echo $poules[0][$i]['idEP4']; ?>">
                            <p>Date de rencontre <input type="date" name="<?php echo "dateR"; ?>" value="" required> </p>
                            <p>Heure de rencontre <input type="time" name="<?php echo "heureR"; ?>" value="" required> </p>
                            <p><?php echo $info_equipe1[0][0]['nom_equipe']; ?> <input type="checkbox" name="Eq1" value=""> </p>
                            <p><?php echo $info_equipe2[0][0]['nom_equipe']; ?> <input type="checkbox" name="Eq2" value=""> </p>
                            <p><?php echo $info_equipe3[0][0]['nom_equipe']; ?> <input type="checkbox" name="Eq3" value=""> </p>
                            <p><?php echo $info_equipe4[0][0]['nom_equipe']; ?> <input type="checkbox" name="Eq4" value=""> </p>
                            <input class="validateButton" type="submit" name="Planifier" value="Planifier">
                          </form>
                          <?php
                          $_SESSION['idT'] = $idTournois;
                          $_SESSION['pseudo'] = $pseudo;
                          $_SESSION['idP'] = $poules[0][$i]['idPoule'];
                           ?>
                          <a href="<?php echo "../controller/functions_planifier_auto.php?pseudo=".$_SESSION['pseudo']."&idP=".$_SESSION['idP']."&idT=".$_SESSION['idT']; ?>">planification automatique de la poule</a>
                        <?php
                      }
                      ?>
                      <?php
                    } ?>

                  <?php
                }
                ?>
                </table>
                </div>

                <?php
              }
              else {

                if ($nbr_equipes == 2) {
                  $debTournois++;
                  $idEq1 = recup_idEquipe($idTournois, 0);
                  $idEq2 = recup_idEquipe($idTournois, 1);
                  $toutgagnant = array();
                  $toutgagnant[0][0] = $idEq1;
                  $toutgagnant[0][1] = $idEq2;
                  // var_dump($selection);
                  $send = envoie_tab_select($toutgagnant, $idTournois);
                  if ($send == false) {
                    return "erreur";
                  }
                }

                if ($nbr_equipes == 4) {
                  $debTournois = 2;
                  $idEq1 = recup_idEquipe($idTournois, 0);
                  $idEq2 = recup_idEquipe($idTournois, 1);
                  $idEq3 = recup_idEquipe($idTournois, 2);
                  $idEq4 = recup_idEquipe($idTournois, 3);
                  $toutgagnant = array();
                  $toutgagnant[0][0]['Equipe'] = $idEq1;
                  $toutgagnant[0][1]['Equipe'] = $idEq2;
                  $toutgagnant[0][2]['Equipe'] = $idEq3;
                  $toutgagnant[0][3]['Equipe'] = $idEq4;
                  // var_dump($selection);
                  $send = envoie_tab_select($toutgagnant, $idTournois);
                  if ($send == false) {
                    return "erreur";
                  }
                }
              }

              if ($nbr_equipes == ($debTournois * 2)) {
                // on verifie si le tableau des selectionnés est dans la bdd
                $verifTab = tab_select($idTournois);
                // var_dump($verifTab);

                if ($verifTab == false) {
                  $send = envoie_tab_select($toutgagnant, $idTournois);
                  if ($send == false) {
                    return "erreur";
                  }
                }
                ?>
                <?php
                if ($nbr_equipes != 2 && $nbr_equipes != 4) {
                  ?>
                  <p>Les gagnants des poules sont :</p>
                  <ul>
                    <?php $size = count($toutgagnant[0]);
                    for ($i=0; $i < $size; $i++) {
                      ?>
                      <li><?php echo recup_info_equipe($toutgagnant[0][$i]['Equipe'])[0][0]['nom_equipe']; ?> de la poule <?php echo $toutgagnant[0][$i]['Poule']; ?></li>
                      <?php
                    } ?>
                  </ul>

                    <?php

                }

                if (isset($toutgagnant) && $nbr_equipes != 2) {
                  $size = count($toutgagnant[0]);
                }
                else {
                  $size = 2;
                }?>

                <!-- si les poules de selection sont terminés, on peut afficher l abre binaire -->
                <?php if ($size == 16) {
                  // on recupere les equipes du tournoi
                  $base = recup_8($idTournois);
                  // on verifie si les 16 sont prevus
                  $prevu = prevu($idTournois, $base);
                  if ($prevu) {
                    // on verfie si les matchs sont passes
                    $passes = passes($idTournois, $base);
                    if ($passes == 8) {
                      // on regarde si les scores ont été mis
                      $scores_mis = scores_mis_size16($idTournois, $base);
                      if ($scores_mis) {
                        // on recupere les gagnants de ces matchs
                        $gagnants_8 = recup_gagnants_8($idTournois, $base);
                        // on verifie si les matchs sont prevus pour les gagnants
                        $verif_pour_8 = matchs_8($idTournois, $gagnants_8);
                        if ($verif_pour_8 == 4) {
                          // on verifie si les scores ont ete mis
                          // var_dump($gagnants_8);
                          $scores_mis_8 = scores_gagnants_8($idTournois, $gagnants_8);

                          if ($scores_mis_8) {
                            // on recupere les gagnants de ses matchs
                            $gagnants_4 = match_8_suivant($idTournois, $gagnants_8);
                            // on verifie si les matchs sont prevus
                            $prevus_4 = verif_quart_size8($idTournois, $gagnants_4);
                            if ($prevus_4) {
                              // on verifie si les matchs sont passes
                              $demi_passe = verif_quart_passe($idTournois, $gagnants_4);
                              if ($demi_passe) {
                                // on regarde si les scores ont ete ajoutes
                                $score_ajout_quart = verif_ajout_scores_size8($idTournois, $gagnants_4);
                                if ($score_ajout_quart) {
                                  // on recupere les gagnants
                                  $finaliste = get_equipe_demi($idTournois, $gagnants_4);
                                  // on regarde si la finale est prevu
                                  $final = demi_final($idTournois,$finaliste);
                                  if ($final) {
                                    // on verifie si la finale a eu lieu
                                    $lieu_final = verif_demi($idTournois, $finaliste[0][0], $finaliste[0][1]);
                                    if ($lieu_final) {
                                      // on verifie si les scores ont ete mis
                                      $scores_final = verif_miss_ajout_demi($idTournois, $finaliste[0][0], $finaliste[0][1]);
                                      if ($scores_final) {
                                        ?>
                                        <p>Vous n'avez pas encore mis les scores de la finale.</p>
                                        <?php
                                        // session_start();
                                        $_SESSION['pseudo'] = $pseudo;
                                        $eq1 = $finaliste[0][0];
                                        $eq2 = $finaliste[0][1];

                                        $recupid1 = recup_idrencontre($idTournois, $eq1, $eq2);
                                        $_SESSION['id'] = $recupid1;
                                        ?>

                                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                                        <?php
                                      }
                                      else {
                                        $gagnant = gagnant($idTournois, $finaliste[0][0], $finaliste[0][1]);
                                        termine_competition($idTournois);
                                        ?>
                                        <p>Votre tournoi est terminé. Le gagnant est <?php echo recup_info_equipe($gagnant)[0][0]['nom_equipe']; ?>.</p>
                                        <p>Merci d'avoir utilisé notre site pour la gestion de votre tournoi. A une prochaine fois.</p>
                                        <?php
                                      }
                                    }
                                    else {
                                      ?>
                                      <p>Votre finale n'a pas encore eu lieu.</p>
                                      <?php
                                    }
                                  }
                                  else {
                                    ?>
                                    <p>Votre final n'est pas encore prévue.</p>
                                    <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                                      <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                                      <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                                      <input type="hidden" name="idEP1" value="<?php echo $finaliste[0][0]; ?>">
                                      <input type="hidden" name="idEP2" value="<?php echo $finaliste[0][1]; ?>">
                                      <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($finaliste[0][0])[0][0]['nom_equipe']; ?>"> </p>
                                      <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($finaliste[0][1])[0][0]['nom_equipe']; ?>"> </p>
                                      <p>Date de rencontre <input type="date" name="dateR" value="" required> </p>
                                      <p>Heure de rencontre <input type="time" name="heureR" value="" required> </p>
                                      <input class="validateButton" type="submit" name="finalSize16" value="Valider">
                                    </form>
                                    <?php
                                  }
                                }
                                else {
                                  ?>
                                  <p>Vos scores de demi-finale ne sont pas encore ajoutés.</p>
                                  <?php
                                  // session_start();
                                  $_SESSION['pseudo'] = $pseudo;
                                  $eq1 = $gagnants_4[0][0];
                                  $eq2 = $gagnants_4[0][3];
                                  $eq3 = $gagnants_4[0][1];
                                  $eq4 = $gagnants_4[0][2];

                                  $recupid1 = recup_idrencontre($idTournois, $eq1, $eq2);
                                  $recupid2 = recup_idrencontre($idTournois, $eq3, $eq4);
                                  $_SESSION['id'] = $recupid1;
                                  ?>

                                  <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                                  <?php $_SESSION['id'] = $recupid2; ?>
                                  <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                            <?php
                                }
                              }
                              else {
                                ?>
                                <p>Vos rencontres  de demi-finale ne se sont pas encore passées.</p>
                                <?php
                              }
                            }
                            else {
                              ?>
                              <p>Les matchs de demi-final ne sont pas encore prevus.</p>
                              <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                                <input type="hidden" name="idEq1" value="<?php echo $gagnants_4[0][0]; ?>">
                                <input type="hidden" name="idEq2" value="<?php echo $gagnants_4[0][3]; ?>">
                                <input type="hidden" name="idEq3" value="<?php echo $gagnants_4[0][1]; ?>">
                                <input type="hidden" name="idEq4" value="<?php echo $gagnants_4[0][2]; ?>">
                                <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                                <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                                <p>1er match</p>
                                <p>Equipe 1 <input type="text" name="" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_4[0][0])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Equipe 2 <input type="text" name="" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_4[0][3])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Date de rencontre <input type="date" name="dateR1" value="" required> </p>
                                <p>Heure de rencontre <input type="time" name="heureR1" value="" required> </p>

                                <p>2eme match</p>
                                <p>Equipe 1 <input type="text" name="" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_4[0][1])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Equipe 2 <input type="text" name="" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_4[0][2])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Date de rencontre <input type="date" name="dateR2" value="" required> </p>
                                <p>Heure de rencontre <input type="time" name="heureR2" value="" required> </p>

                                <input class="validateButton" type="submit" name="size16_demi" value="Valider">
                              </form>
                              <?php
                            }
                          }
                          else {
                            ?>
                            <p>Vous n'avez pas encore mis des scores.</p>
                            <?php
                            // session_start();
                            $_SESSION['pseudo'] = $pseudo;
                            $eq1 = $gagnants_8[0][0];
                            $eq2 = $gagnants_8[0][1];
                            $eq3 = $gagnants_8[0][2];
                            $eq4 = $gagnants_8[0][3];
                            $eq5 = $gagnants_8[0][4];
                            $eq6 = $gagnants_8[0][5];
                            $eq7 = $gagnants_8[0][6];
                            $eq8 = $gagnants_8[0][7];

                            $recupid1 = recup_idrencontre($idTournois, $eq1, $eq2);
                            $recupid2 = recup_idrencontre($idTournois, $eq3, $eq4);
                            $recupid3 = recup_idrencontre($idTournois, $eq5, $eq6);
                            $recupid4 = recup_idrencontre($idTournois, $eq7, $eq8);
                            $_SESSION['id'] = $recupid1;
                            ?>

                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                            <?php $_SESSION['id'] = $recupid2; ?>
                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                            <?php $_SESSION['id'] = $recupid3; ?>
                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                            <?php $_SESSION['id'] = $recupid4; ?>
                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                            <?php
                          }
                        }
                        else {
                          ?>
                          <p>Vos matchs pour la 8eme de final ne sont pas encore prévus. </p>
                          <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                            <?php
                            for ($i=0; $i < 8; $i++) {
                              ?>
                              <input type="hidden" name="<?php echo "idEP".$i; ?>" value="<?php echo $gagnants_8[0][$i]; ?>">
                              <?php
                            }
                             ?>
                             <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                             <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">

                             <p>1er match</p>
                             <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_8[0][0])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_8[0][1])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Date de rencontre <input type="date" name="dateR1" value="" required> </p>
                             <p>Heure de rencontre <input type="time" name="heureR1" value="" required> </p>

                             <p>2eme match</p>
                             <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_8[0][2])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_8[0][3])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Date de rencontre <input type="date" name="dateR2" value="" required> </p>
                             <p>Heure de rencontre <input type="time" name="heureR2" value="" required> </p>

                             <p>3eme match</p>
                             <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_8[0][4])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_8[0][5])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Date de rencontre <input type="date" name="dateR3" value="" required> </p>
                             <p>Heure de rencontre <input type="time" name="heureR3" value="" required> </p>

                             <p>4eme match</p>
                             <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_8[0][6])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($gagnants_8[0][7])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Date de rencontre <input type="date" name="dateR4" value="" required> </p>
                             <p>Heure de rencontre <input type="time" name="heureR4" value="" required> </p>

                             <input class="validateButton" type="submit" name="size8_8eme" value="Valider">

                          </form>
                          <?php
                        }
                      }
                      else {
                        ?>
                        <p>Les résultats n'ont pas encore été entrés</p>
                        <?php
                        // session_start();
                        $_SESSION['pseudo'] = $pseudo;
                        $eq1 = $base[0][0]['Equipe'];
                        $eq2 = $base[0][1]['Equipe'];
                        $eq3 = $base[0][2]['Equipe'];
                        $eq4 = $base[0][3]['Equipe'];
                        $eq5 = $base[0][4]['Equipe'];
                        $eq6 = $base[0][5]['Equipe'];
                        $eq7 = $base[0][6]['Equipe'];
                        $eq8 = $base[0][7]['Equipe'];
                        $eq9 = $base[0][0]['Equipe'];
                        $eq10 = $base[0][1]['Equipe'];
                        $eq11 = $base[0][2]['Equipe'];
                        $eq12 = $base[0][3]['Equipe'];
                        $eq13 = $base[0][4]['Equipe'];
                        $eq14 = $base[0][5]['Equipe'];
                        $eq15 = $base[0][6]['Equipe'];
                        $eq16 = $base[0][7]['Equipe'];
                        $recupid1 = recup_idrencontre($idTournois, $eq1, $eq2);
                        $recupid2 = recup_idrencontre($idTournois, $eq3, $eq4);
                        $recupid3 = recup_idrencontre($idTournois, $eq5, $eq6);
                        $recupid4 = recup_idrencontre($idTournois, $eq7, $eq8);
                        $recupid5 = recup_idrencontre($idTournois, $eq9, $eq10);
                        $recupid6 = recup_idrencontre($idTournois, $eq11, $eq12);
                        $recupid7 = recup_idrencontre($idTournois, $eq13, $eq14);
                        $recupid8 = recup_idrencontre($idTournois, $eq15, $eq16);
                        $_SESSION['id'] = $recupid1;
                        ?>

                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid2; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid3; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid4; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid5; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid6; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid7; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid8; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>


                        <?php
                      }
                    }
                    else {
                      ?>
                      <p>Vos rencontres ne sont pas encore passées.</p>
                      <?php
                    }
                  }else {
                    ?>
                    <p>Votre tournoi n'a pas de matchs de prévus.</p>
                    <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                      <?php
                      for ($i=0; $i < 16; $i++) {
                        ?>
                        <input type="hidden" name="<?php echo "idEP".$i; ?>" value="<?php echo $base[0][$i]['Equipe']; ?>">
                        <?php
                      }
                       ?>
                       <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                       <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">

                       <?php
                       $id = 0;
                       for ($i=0; $i < 8; $i++) {
                         ?>
                         <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($base[0][$id]['Equipe'])[0][0]['nom_equipe']; ?>"> </p>
                         <?php $id++; ?>
                         <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($base[0][$id]['Equipe'])[0][0]['nom_equipe']; ?>"> </p>
                         <?php $id++; ?>
                         <p>Date de rencontre <input type="date" name="<?php echo "dateR".$i; ?>" value="" required> </p>
                         <p>Heure de rencontre <input type="time" name="<?php echo "heureR".$i; ?>" value="" required> </p>
                         <?php
                       }
                        ?>
                        <input class="validateButton" type="submit" name="size8prevu" value="Valider">
                    </form>
                    <?php
                  }
                  ?>

                  <?php
                }
                else {
                  if ($size == 8) {
                    // on recupere les equipes
                    $base = recup_8($idTournois);
                    // on verifie si les huitiemes de final sont prevues
                    $huitieme = huitieme($idTournois, $base);
                    // var_dump($base);
                    // var_dump($huitieme);
                    if ($huitieme == 4) {
                      // on verifie si les huitiemes sont passes
                      $verif_huitieme = huitieme_passe($idTournois, $base);
                      if ($verif_huitieme) {
                        // on regarde si les scores ont ete mis
                        $verif_ajout_huitieme = verif_ajout_resultats_huitieme_final($idTournois, $base);
                        // var_dump($verif_ajout_huitieme);
                        if ($verif_ajout_huitieme == 4) {
                          // on recupere les gagnants des huitiemes
                          $quart_final = recup_gagnants_huitieme($idTournois, $base);
                          // var_dump($quart_final);
                          // on verifie si les quarts sont prevus
                          $verif_quart = verif_quart_size8($idTournois, $quart_final);
                          // var_dump($verif_quart);
                          if ($verif_quart) {
                            // on verifie si les quarts se sont deroules
                            $verif_quart2 = verif_quart_passe($idTournois, $quart_final);
                            // var_dump($verif_quart2);
                            if ($verif_quart2) {
                              // on verifie si les scores ont été ajoutés
                              $scores_ajouts = verif_ajout_scores_size8($idTournois, $quart_final);
                              // var_dump($scores_ajouts);
                              if ($scores_ajouts) {
                                // on recupere les equipes qui ont gagné
                                $recup_eq_demi = get_equipe_demi($idTournois, $quart_final);
                                // var_dump($recup_eq_demi);
                                // on verifie si la demi final est planifier
                                $demi_final = demi_final($idTournois,$recup_eq_demi);
                                if ($demi_final) {
                                  // on regarde si la demi final est terminée
                                  $demi_passe = verif_demi($idTournois, $recup_eq_demi[0][0], $recup_eq_demi[0][1]);
                                  if ($demi_passe) {
                                    // on verifie si les scores ont ete ajoutes
                                    $score_ajout = verif_miss_ajout_demi($idTournois, $recup_eq_demi[0][0], $recup_eq_demi[0][1]);
                                    if ($score_ajout) {
                                      ?>
                                      <p>Vos scores n'ont pas encore été ajoutés.</p>
                                      <?php
                                      // session_start();
                                      $_SESSION['pseudo'] = $pseudo;
                                      $eq1 = $recup_eq_demi[0][0];
                                      $eq2 = $recup_eq_demi[0][1];
                                      $recupid1 = recup_idrencontre($idTournois, $eq1, $eq2);
                                      $_SESSION['id'] = $recupid1;
                                      ?>
                                      <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                                      <?php
                                    }else {
                                      // on recupere le gagnant
                                      $gagnant = gagnant($idTournois, $recup_eq_demi[0][0], $recup_eq_demi[0][1]);
                                      termine_competition($idTournois);
                                      // var_dump($gagnant);
                                      ?>
                                      <p>Votre tournoi est terminé, le gagnant est <?php echo recup_info_equipe($gagnant)[0][0]['nom_equipe']; ?></p>
                                      <?php
                                    }
                                  }
                                  else {
                                    ?>
                                    <p>Votre demi-final n'est pas encore terminé.</p>
                                    <?php
                                  }
                                }
                                else {
                                  ?>
                                  <p>Votre demi-final n'est pas encore prevu.</p>
                                  <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                                    <input type="hidden" name="idEP1" value="<?php echo $recup_eq_demi[0][0]; ?>">
                                    <input type="hidden" name="idEP2" value="<?php echo $recup_eq_demi[0][1]; ?>">
                                    <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                                    <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                                    <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($recup_eq_demi[0][0])[0][0]['nom_equipe']; ?>"> </p>
                                    <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($recup_eq_demi[0][1])[0][0]['nom_equipe']; ?>"> </p>
                                    <p>Date de Rencontre <input type="date" name="dateR" value="" required> </p>
                                    <p>Heure de Rencontre <input type="time" name="heureR" value="" required> </p>
                                    <input class="validateButton" type="submit" name="size8demi_final" value="Valider">
                                  </form>
                                  <?php
                                }
                              }else {
                                ?>
                                <p>Vous n'avez pas encore ajoutés les scores à vos rencontres.</p>
                                <?php
                                // session_start();
                                $_SESSION['pseudo'] = $pseudo;
                                $eq1 = $quart_final[0][0];
                                $eq2 = $quart_final[0][1];
                                $eq3 = $quart_final[0][2];
                                $eq4 = $quart_final[0][3];
                                $recupid1 = recup_idrencontre($idTournois, $eq1, $eq4);
                                $recupid2 = recup_idrencontre($idTournois, $eq2, $eq3);
                                $_SESSION['id'] = $recupid1;
                                ?>

                                <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                                <?php $_SESSION['id'] = $recupid2; ?>
                                <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                                <?php
                              }
                            }
                            else {
                              ?>
                              <p>Vos matchs ne se sont pas encore déroulés.</p>
                              <?php
                            }
                          }
                          else {
                            ?>
                            <p>Votre demi-final n'est pas prévu.</p>
                            <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                              <input type="hidden" name="idEq1" value="<?php echo $quart_final[0][0]; ?>">
                              <input type="hidden" name="idEq2" value="<?php echo $quart_final[0][1]; ?>">
                              <input type="hidden" name="idEq3" value="<?php echo $quart_final[0][2]; ?>">
                              <input type="hidden" name="idEq4" value="<?php echo $quart_final[0][3]; ?>">
                              <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                              <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                              <p>1er match</p>
                              <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($quart_final[0][0])[0][0]['nom_equipe']; ?>"> </p>
                              <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($quart_final[0][3])[0][0]['nom_equipe']; ?>"> </p>
                              <p>Date de rencontre <input type="date" name="dateR1" value="" required> </p>
                              <p>Heure de rencontre <input type="time" name="heureR1" value="" required> </p>

                              <p>2eme match</p>
                              <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($quart_final[0][1])[0][0]['nom_equipe']; ?>"> </p>
                              <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($quart_final[0][2])[0][0]['nom_equipe']; ?>"> </p>
                              <p>Date de rencontre <input type="date" name="dateR2" value="" required> </p>
                              <p>Heure de rencontre <input type="time" name="heureR2" value="" required> </p>
                              <input class="validateButton" type="submit" name="size8Quart" value="Valider">
                            </form>
                            <?php
                          }
                        }
                        else {
                          ?>
                          <p>Vous n'avez pas encore ajouté les scores de vos huitiemes.</p>
                          <?php
                          // session_start();
                          $_SESSION['pseudo'] = $pseudo;
                          $eq1 = $base[0][0]['Equipe'];
                          $eq2 = $base[0][1]['Equipe'];
                          $eq3 = $base[0][2]['Equipe'];
                          $eq4 = $base[0][3]['Equipe'];
                          $eq5 = $base[0][4]['Equipe'];
                          $eq6 = $base[0][5]['Equipe'];
                          $eq7 = $base[0][6]['Equipe'];
                          $eq8 = $base[0][7]['Equipe'];
                          $recupid1 = recup_idrencontre($idTournois, $eq1, $eq2);
                          $recupid2 = recup_idrencontre($idTournois, $eq3, $eq4);
                          $recupid3 = recup_idrencontre($idTournois, $eq5, $eq6);
                          $recupid4 = recup_idrencontre($idTournois, $eq7, $eq8);
                          $_SESSION['id'] = $recupid1;
                          ?>

                          <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                          <?php $_SESSION['id'] = $recupid2; ?>
                          <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                          <?php $_SESSION['id'] = $recupid3; ?>
                          <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                          <?php $_SESSION['id'] = $recupid4; ?>
                          <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                          <?php
                        }
                      }
                      else {
                        ?>
                        <p>Les quarts de finale ne sont pas encore terminées.</p>
                        <?php
                      }
                    }
                    else {
                      $rencontre1 = recup_idrencontre($idTournois, $base[0][0]['Equipe'], $base[0][2]['Equipe']);
                      $rencontre2 = recup_idrencontre($idTournois, $base[0][1]['Equipe'], $base[0][3]['Equipe']);
                      $rencontre3 = recup_idrencontre($idTournois, $base[0][4]['Equipe'], $base[0][6]['Equipe']);
                      $rencontre4 = recup_idrencontre($idTournois, $base[0][5]['Equipe'], $base[0][7]['Equipe']);
                      $vide = array();
                      $id = 0;
                      if (empty($rencontre1[0])) {
                        $vide[0][$id]['id1'] = $base[0][0]['Equipe'];
                        $vide[0][$id]['id2'] = $base[0][2]['Equipe'];
                        $id++;
                      }
                      if (empty($rencontre2[0])) {
                        $vide[0][$id]['id1'] = $base[0][1]['Equipe'];
                        $vide[0][$id]['id2'] = $base[0][3]['Equipe'];
                        $id++;
                      }
                      if (empty($rencontre3[0])) {
                        $vide[0][$id]['id1'] = $base[0][4]['Equipe'];
                        $vide[0][$id]['id2'] = $base[0][6]['Equipe'];
                        $id++;
                      }
                      if (empty($rencontre4[0])) {
                        $vide[0][$id]['id1'] = $base[0][5]['Equipe'];
                        $vide[0][$id]['id2'] = $base[0][7]['Equipe'];
                      }

                      $nbr_vide = count($vide[0]);
                      ?>
                      <p>Vous n'avez pas encore planifier vos quarts de finale.</p>
                      <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                        <input type="hidden" name="nbr_vides" value="<?php echo $nbr_vide; ?>">
                        <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                        <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                        <?php
                        for ($i=0; $i < $nbr_vide; $i++) {
                          ?>
                          <input type="hidden" name="<?php echo "idEq1-".$i; ?>" value="<?php echo $vide[0][$i]['id1']; ?>">
                          <input type="hidden" name="<?php echo "idEq2-".$i; ?>" value="<?php echo $vide[0][$i]['id2']; ?>">
                          <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($vide[0][$i]['id1'])[0][0]['nom_equipe']; ?>"> </p>
                          <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($vide[0][$i]['id2'])[0][0]['nom_equipe']; ?>"> </p>
                          <p>Date de Rencontre <input type="date" name="<?php echo "dateR".$i; ?>" value="" required> </p>
                          <p>Heure de Rencontre <input type="time" name="<?php echo "heureR".$i ?>" value="" required> </p>
                          <?php
                        }
                         ?>
                         <input class="validateButton" type="submit" name="huitiemeSize8" value="Valider">
                      </form>
                      <?php
                    }
                    ?>

                    <?php
                  }
                  else {
                    if ($size == 4) {
                      // on recupere les equipes
                      $base = recup_4($idTournois);
                      // var_dump($toutgagnant);
                      $quart = quart_final($idTournois, $base);
                      // on verifie si les quarts de final sont prevues
                      // sinon on créé les matchs de quart

                      // var_dump($quart);
                      if ($quart) {
                        // on verifie si la demi-final a eu lieu
                        $lieu4F = verif_quart($base, $idTournois);
                        if($lieu4F){
                          // on verifie si la demi-final est prévu
                          // d abord on recupere les equipes qui ont gagné les quarts
                          $verif_upload_resultats = miss_ajout_resultats_quart_final($idTournois, $base);

                          if ($verif_upload_resultats) {
                            ?>
                            <p>Vous n'avez pas encore ajouté les résultats de ses rencontres.</p>
                            <?php
                            // session_start();
                            $_SESSION['pseudo'] = $pseudo;
                            $eq1 = $base[0][0]['Equipe'];
                            $eq2 = $base[0][1]['Equipe'];
                            $eq3 = $base[0][2]['Equipe'];
                            $eq4 = $base[0][3]['Equipe'];
                            $recupid1 = recup_idrencontre($idTournois, $eq1, $eq3);
                            $recupid2 = recup_idrencontre($idTournois, $eq2, $eq4);
                            $_SESSION['id'] = $recupid1;
                            ?>

                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                            <?php $_SESSION['id'] = $recupid2; ?>
                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                            <?php
                          }else {
                            $recup_eq_demi = recup_2($idTournois, $base);
                            $demi_final = demi_final($idTournois,$recup_eq_demi);
                            if ($demi_final) {
                              // on verifie si la demi-final a eu lieu
                              $lieu2F = verif_demi($idTournois, $recup_eq_demi[0][0], $recup_eq_demi[0][1]);
                              if ($lieu2F) {
                                $ajout_demi = verif_miss_ajout_demi($idTournois, $recup_eq_demi[0][0], $recup_eq_demi[0][1]);
                                if ($ajout_demi) {
                                  ?>
                                  <p>Vous n'avez pas encore ajouté les résultats de la finale.</p>
                                  <?php
                                  $recupid = recup_idrencontre($idTournois, $recup_eq_demi[0][0], $recup_eq_demi[0][1]);
                                  $_SESSION['id'] = $recupid;
                                  $_SESSION['pseudo'] = $pseudo;
                                  ?>
                                  <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                                  <?php
                                }
                                else {
                                  $gagnant = gagnant($idTournois, $recup_eq_demi[0][0], $recup_eq_demi[0][1]);
                                  termine_competition($idTournois);
                                  ?>
                                  <p>Votre tournoi est ainsi terminé. Le gagnant est <?php echo recup_info_equipe($gagnant)[0][0]['nom_equipe']; ?> !</p>
                                  <p>Nous vous remercions d'avoir utilisé notre site pour la gestion de votre tournoi.</p>
                                  <?php
                                }
                              }else {
                                ?>
                                <p>Votre demi-final n'a pas encore eu lieu.</p>
                                <?php
                              }
                            }
                            else {
                              ?>
                              <!-- on prevoit la demi-final -->
                              <p>Vous n'avez pas encore prévu votre finale.</p>
                              <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                                <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                                <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                                <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                                <input type="hidden" name="idEquipe1" value="<?php echo $recup_eq_demi[0][0]; ?>">
                                <input type="hidden" name="idEquipe2" value="<?php echo $recup_eq_demi[0][1]; ?>">
                                <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($recup_eq_demi[0][0])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($recup_eq_demi[0][1])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Date de la Rencontre <input type="date" name="dateR" value="" required> </p>
                                <p>Heure de la rencontre <input type="time" name="heureR" value="" required> </p>
                                <input class="validateButton" type="submit" name="demiSize4" value="Valider">
                              </form>
                              <?php
                            }
                          }


                        }else {
                          ?>
                          <p>Les quarts de final n'ont pas encore eu lieu.</p>
                          <?php
                        }

                      }
                      else {
                        ?>
                        <p>La demi-final ne sont pas encore prévus.</p>

                        <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                          <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                          <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                          <?php for ($i=0; $i < 4; $i++) {
                            ?>
                            <input type="hidden" name="<?php echo "idEquipe".$i; ?>" value="<?php echo $base[0][$i]['Equipe']; ?>">
                            <?php
                          } ?>

                          <p>1er match</p>
                          <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($base[0][0]['Equipe'])[0][0]['nom_equipe']; ?>"> </p>
                          <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($base[0][2]['Equipe'])[0][0]['nom_equipe']; ?>"> </p>
                          <p>Date de la Rencontre <input type="date" name="dateR1" value="" required> </p>
                          <p>Heure de la rencontre <input type="time" name="heureR1" value="" required> </p>

                          <p>2eme match</p>
                          <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($base[0][1]['Equipe'])[0][0]['nom_equipe']; ?>"> </p>
                          <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($base[0][3]['Equipe'])[0][0]['nom_equipe']; ?>"> </p>
                          <p>Date de la Rencontre <input type="date" name="dateR2" value="" required> </p>
                          <p>Heure de la rencontre <input type="time" name="heureR2" value="" required> </p>


                          <input class="validateButton" type="submit" name="ValiderSize4" value="Valider">
                        </form>
                        <?php
                      }
                      ?>

                      <?php
                    }
                    else {
                      if ($size == 2) {
                        // on recupere les equipes
                        $base = recup_2_size2($idTournois);
                        // echo "<br>";
                        // var_dump($base);
                        // on verifie si la demi finale est prevue
                        $demi = demi_final($idTournois, $base);
                        if ($demi) {
                          // on verifie si la finale a eu lieu
                          $lieuF = verif_demi($idTournois, $base[0][0], $base[0][1]);
                          if ($lieuF) {
                            // on verifie que les scores ont ete ajoute
                            $ajout_demi = verif_miss_ajout_demi($idTournois, $base[0][0], $base[0][1]);
                            if ($ajout_demi) {
                              ?>
                              <p>Vous n'avez pas encore ajouter les résultats de votre finale.</p>
                              <?php
                              // session_start();
                              $_SESSION['pseudo'] = $pseudo;
                              $idrecup = recup_idrencontre($idTournois, $base[0][0], $base[0][1]);
                              $_SESSION['id'] = $idrecup;
                               ?>
                               <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                              <?php
                            }else {
                              $gagnant = gagnant($idTournois, $base[0][0], $base[0][1]);
                              termine_competition($idTournois);
                              ?>
                              <p>Votre tournoi est terminé, le gagnant est <?php echo recup_info_equipe($gagnant)[0][0]['nom_equipe']; ?></p>
                              <?php
                            }
                          }
                          else {
                            ?>
                            <p>Votre finale n'a pas encore eu lieu.</p>
                            <?php
                          }

                        }
                        else {
                          ?>
                          <p>Votre finale n'est pas prévue.</p>
                          <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                            <input type="hidden" name="idEquipe1" value="<?php echo $base[0][0]; ?>">
                            <input type="hidden" name="idEquipe2" value="<?php echo $base[0][1]; ?>">
                            <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                            <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">

                            <p>Equipe 1 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($base[0][0])[0][0]['nom_equipe']; ?>"> </p>
                            <p>Equipe 2 <input type="text" disabled="disabled" value="<?php echo recup_info_equipe($base[0][1])[0][0]['nom_equipe']; ?>"> </p>
                            <p>Date de rencontre <input type="date" name="dateR" value="" required> </p>
                            <p>Heure de rencontre <input type="time" name="heureR" value="" required> </p>
                            <input class="validateButton" type="submit" name="demiSize2" value="Valider">
                          </form>
                          <?php
                        }
                        ?>

                        <?php
                      }
                    }
                  }
                } ?>

                <?php

              ?>
              <!-- placement de l arbre binaire -->

              <?php
            }else {
                ?>
                <p>Vos poules de selection ne sont pas encore terminées pour continuer</p>
                <?php
              }
              ?>

              <?php
            }
          else {
            ?>
            <!-- sinon c est une coupe -->
              <?php
              if ($nbr_equipes == 16) {
                $recupEq = recup_4($idTournois);

                if ($recupEq == false) {

                  $recup_all_equipes = array();
                  $id = 0;
                  for ($i=0; $i < 16; $i++) {
                    $rec = recup_idEquipe($idTournois, $i);
                    // echo $rec;
                    $recup_all_equipes[0][$id]['Equipe'] = $rec;
                    $id++;
                  }

                  $send = envoie_tab_select($recup_all_equipes, $idTournois);

                  if ($send == false) {
                    echo "erreur";
                  }
                }

                $recupEq = recup_4($idTournois);
                // var_dump($recupEq[0]);
                // on verifie si les huitiemes sont prevus
                $prevus_huitieme = huitieme_coupe($idTournois, $recupEq);
                if ($prevus_huitieme == 8) {
                  // on verfie si les matchs sont passés
                  $passes_huitieme = huitieme_passes($idTournois, $recupEq);
                  if ($passes_huitieme) {
                    // on verifie si les scores ont ete mis
                    $scores_mis_huitieme = huitieme_scores($idTournois, $recupEq);
                    if ($scores_mis_huitieme) {
                      // on recupere ceux qui ont gagné
                      $quart = recup_gagnants_8($idTournois, $recupEq);
                      // on verifie si les quarts sont prevus
                      $quart_prevus = matchs_8($idTournois, $quart);
                      // var_dump($quart[0]);
                      if ($quart_prevus == 4) {
                        // on verifie si les matchs sont passes
                        $quart_passe = quart_passe_coupe($idTournois, $quart);
                        if ($quart_passe) {
                          // on verifie si les scores ont ete mis
                          $quart_scores_mis = quart_score_mis($idTournois, $quart);
                          if ($quart_scores_mis) {
                            // on recupere les equipes gagnantes des quarts de finale
                            $demi_final = get_equipe_demi_coupe($idTournois, $quart);
                            // var_dump($demi_final);

                            // on verifie si les demi-finale sont prevus
                            $verif_demi_final_prevu = demi_final_planifier($idTournois, $demi_final);
                            if ($verif_demi_final_prevu) {
                              // on verifie si les rencontres se sont deroules
                              $demi_deroules = demi_final_deroule($idTournois, $demi_final);
                              // var_dump($demi_final);
                              if ($demi_deroules) {
                                // on verifie si les scores ont ete mis
                                $demi_scores = demi_scores($idTournois, $demi_final);
                                if ($demi_scores) {
                                  // on recupere les equipes les demis
                                  $eq_finals = get_equipe_finale_coupe($idTournois, $demi_final);

                                  // on verifie si la finale est prevue
                                  $finale_prevue = demi_final($idTournois,$eq_finals);

                                  if ($finale_prevue) {
                                    // on verifie si la finale est passe
                                    $finale_passe = finale_passe($idTournois, $eq_finals);

                                    if ($finale_passe) {
                                      // on verifie si les scores ont ete mis
                                      $finale_scores = finale_scores($idTournois, $eq_finals);
                                      if ($finale_scores) {
                                        $gagnant = gagnant_coupe($idTournois, $eq_finals);
                                        termine_competition($idTournois);
                                        ?>
                                        <p>Votre coupe est terminée. Le gagnant est <?php echo recup_info_equipe($gagnant)[0][0]['nom_equipe']; ?>.</p>
                                        <p>Merci d'avoir utilisé notre site pour la gestion de votre tournoi.</p>
                                        <?php
                                      }
                                      else {
                                        ?>
                                        <p>Les scores n'ont pas été mis.</p>
                                        <?php
                                        // session_start();
                                        $_SESSION['pseudo'] = $pseudo;

                                        $recupid1 = recup_idrencontre($idTournois, $eq_finals[0][0], $eq_finals[0][1]);

                                        $_SESSION['id'] = $recupid1;
                                        ?>

                                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                                        <?php
                                      }
                                    }
                                    else {
                                      ?>
                                      <p>Votre finale ne s'est pas encore déroulée.</p>
                                      <?php
                                    }
                                  }
                                  else {
                                    ?>
                                    <p>Votre finale n'est pas prévue.</p>
                                    <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                                      <input type="hidden" name="ideq1" value="<?php echo $eq_finals[0][0]; ?>">
                                      <input type="hidden" name="ideq2" value="<?php echo $eq_finals[0][1]; ?>">
                                      <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                                      <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                                      <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($eq_finals[0][0])[0][0]['nom_equipe']; ?>"> </p>
                                      <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($eq_finals[0][1])[0][0]['nom_equipe']; ?>"> </p>
                                      <p>Date de rencontre <input type="date" name="dateR" value=""> </p>
                                      <p>Heure de rencontre <input type="time" name="heureR" value=""> </p>
                                      <input class="validateButton" type="submit" name="ValiderFinaleCoupe" value="Valider">
                                    </form>
                                    <?php
                                  }
                                }
                                else {
                                  ?>
                                  <p>Vous n'avez pas encore ajouté les scores de vos demi-finales.</p>
                                  <?php
                                  // session_start();
                                  $_SESSION['pseudo'] = $pseudo;

                                  $recupid1 = recup_idrencontre($idTournois, $demi_final[0][0], $demi_final[0][1]);
                                  $recupid2 = recup_idrencontre($idTournois, $demi_final[0][2], $demi_final[0][3]);

                                  $_SESSION['id'] = $recupid1;
                                  ?>

                                  <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                                  <?php $_SESSION['id'] = $recupid2; ?>
                                  <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                                  <?php
                                }
                              }
                              else {
                                ?>
                                <p>Vos rencontres ne se sont pas encore deroulées.</p>
                                <?php
                              }
                            }
                            else {
                              ?>
                              <p>Votre demi-finale n'est pas encore planifié.</p>
                              <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                                <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                                <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                                <input type="hidden" name="idEq0" value="<?php echo $demi_final[0][0]; ?>">
                                <input type="hidden" name="idEq1" value="<?php echo $demi_final[0][1]; ?>">
                                <input type="hidden" name="idEq2" value="<?php echo $demi_final[0][2]; ?>">
                                <input type="hidden" name="idEq3" value="<?php echo $demi_final[0][3]; ?>">

                                <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][0])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][1])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Date de rencontre <input type="date" name="dateR" value=""> </p>
                                <p>Heure de rencontre <input type="time" name="heureR" value=""> </p>

                                <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][2])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][3])[0][0]['nom_equipe']; ?>"> </p>
                                <p>Date de rencontre <input type="date" name="dateR1" value=""> </p>
                                <p>Heure de rencontre <input type="time" name="heureR1" value=""> </p>

                                <input class="validateButton" type="submit" name="ValiderDemiCoupe" value="Valider">
                              </form>
                              <?php
                            }
                          }
                          else {
                            ?>
                            <p>Vous n'avez pas encore mis les scores de vos quarts de finale.</p>
                            <?php
                            // session_start();
                            $_SESSION['pseudo'] = $pseudo;

                            $recupid1 = recup_idrencontre($idTournois, $quart[0][0], $quart[0][1]);
                            $recupid2 = recup_idrencontre($idTournois, $quart[0][2], $quart[0][3]);
                            $recupid3 = recup_idrencontre($idTournois, $quart[0][4], $quart[0][5]);
                            $recupid4 = recup_idrencontre($idTournois, $quart[0][6], $quart[0][7]);

                            $_SESSION['id'] = $recupid1;
                            ?>

                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                            <?php $_SESSION['id'] = $recupid2; ?>
                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                            <?php $_SESSION['id'] = $recupid3; ?>
                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                            <?php $_SESSION['id'] = $recupid4; ?>
                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                            <?php
                          }
                        }
                        else {
                          ?>
                          <p>Vos quarts de finale ne sont pas encore passés.</p>
                          <?php
                        }
                      }
                      else {
                        ?>
                        <p>Vos quarts de final ne sont pas encore planifier.</p>
                        <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                          <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                          <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                          <?php
                          for ($i=0; $i < 8; $i++) {
                            ?>
                            <input type="hidden" name="<?php echo "idEq".$i; ?>" value="<?php echo $quart[0][$i]; ?>">

                            <?php
                          }
                           ?>

                           <?php
                           for ($i=0; $i < 4; $i++) {
                             ?>
                             <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($quart[0][$i])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($quart[0][$i+1])[0][0]['nom_equipe']; ?>"> </p>
                             <p>Date de rencontre <input type="date" name="<?php echo "dateR".$i; ?>" value="" required> </p>
                             <p>heure de rencontre <input type="time" name="<?php echo "heureR".$i; ?>" value="" required> </p>
                             <?php
                           }
                            ?>
                            <input class="validateButton" type="submit" name="ValiderQuartCoupe" value="Valider">
                        </form>
                        <?php
                      }
                    }
                    else {
                      ?>
                      <p>Vous n'avez pas encore mis les scores de vos matchs.</p>
                      <?php
                      // session_start();
                      $_SESSION['pseudo'] = $pseudo;

                      $recupid1 = recup_idrencontre($idTournois, $recupEq[0][0]['Equipe'], $recupEq[0][1]['Equipe']);
                      $recupid2 = recup_idrencontre($idTournois, $recupEq[0][2]['Equipe'], $recupEq[0][3]['Equipe']);
                      $recupid3 = recup_idrencontre($idTournois, $recupEq[0][4]['Equipe'], $recupEq[0][5]['Equipe']);
                      $recupid4 = recup_idrencontre($idTournois, $recupEq[0][6]['Equipe'], $recupEq[0][7]['Equipe']);
                      $recupid5 = recup_idrencontre($idTournois, $recupEq[0][8]['Equipe'], $recupEq[0][9]['Equipe']);
                      $recupid6 = recup_idrencontre($idTournois, $recupEq[0][10]['Equipe'], $recupEq[0][11]['Equipe']);
                      $recupid7 = recup_idrencontre($idTournois, $recupEq[0][12]['Equipe'], $recupEq[0][13]['Equipe']);
                      $recupid8 = recup_idrencontre($idTournois, $recupEq[0][14]['Equipe'], $recupEq[0][15]['Equipe']);

                      $_SESSION['id'] = $recupid1;
                      ?>

                      <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                      <?php $_SESSION['id'] = $recupid2; ?>
                      <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                      <?php $_SESSION['id'] = $recupid3; ?>
                      <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                      <?php $_SESSION['id'] = $recupid4; ?>
                      <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                      <?php $_SESSION['id'] = $recupid5; ?>
                      <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                      <?php $_SESSION['id'] = $recupid6; ?>
                      <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                      <?php $_SESSION['id'] = $recupid7; ?>
                      <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                      <?php $_SESSION['id'] = $recupid8; ?>
                      <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                      <?php
                    }
                  }
                  else {
                    ?>
                    <p>Vos matchs de huitieme ne sont pas encore terminés.</p>
                    <?php
                  }
                }
                else {
                  ?>
                  <p>Vous n'avez pas prévus vos huitième de finale.</p>
                  <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                    <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                    <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                    <?php
                    for ($i=0; $i < 16; $i++) {
                      ?>
                      <input type="hidden" name="<?php echo "idEq".$i; ?>" value="<?php echo $recupEq[0][$i]['Equipe']; ?>">
                      <?php
                    }
                     ?>

                     <?php
                     for ($j=0; $j < 8; $j++) {
                       ?>
                       <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($recupEq[0][$j]['Equipe'])[0][0]['nom_equipe']; ?>"> </p>
                       <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($recupEq[0][$j+1]['Equipe'])[0][0]['nom_equipe']; ?>"> </p>
                       <p>Date de rencontre <input type="date" name="<?php echo "dateR".$j; ?>" value="" required> </p>
                       <p>Heure de rencontre <input type="time" name="<?php echo "heureR".$j; ?>" value="" required> </p>
                       <br>
                       <?php
                     }
                      ?>
                      <input class="validateButton" type="submit" name="PlanifierCoupe" value="Planifier">
                  </form>
                  <?php
                }
                ?>

                <?php
              } else {
                if($nbr_equipes == 8){
                  $recupEq = recup_4($idTournois);

                  if ($recupEq == false) {

                    $recup_all_equipes = array();
                    $id = 0;
                    for ($i=0; $i < 8; $i++) {
                      $rec = recup_idEquipe($idTournois, $i);
                      // echo $rec;
                      $recup_all_equipes[0][$id]['Equipe'] = $rec;
                      $id++;
                    }

                    $send = envoie_tab_select($recup_all_equipes, $idTournois);

                    if ($send == false) {
                      echo "erreur";
                    }
                  }

                  $recupEq = recup_4($idTournois);

                  for ($i=0; $i < 8; $i++) {
                    $quart[0][$i] = $recupEq[0][$i]['Equipe'];
                  }
                  // var_dump($recup);

                  $quart_prevus = matchs_8($idTournois, $quart);
                  // var_dump($quart_prevus);
                  if ($quart_prevus == 4) {
                    // on verifie si les matchs sont passes
                    $quart_passe = quart_passe_coupe($idTournois, $quart);
                    if ($quart_passe) {
                      // on verifie si les scores ont ete mis
                      $quart_scores_mis = quart_score_mis($idTournois, $quart);
                      if ($quart_scores_mis) {
                        // on recupere les equipes gagnantes des quarts de finale
                        $demi_final = get_equipe_demi_coupe($idTournois, $quart);
                        // var_dump($demi_final);

                        // on verifie si les demi-finale sont prevus
                        $verif_demi_final_prevu = demi_final_planifier($idTournois, $demi_final);
                        if ($verif_demi_final_prevu) {
                          // on verifie si les rencontres se sont deroules
                          $demi_deroules = demi_final_deroule($idTournois, $demi_final);
                          // var_dump($demi_final);
                          if ($demi_deroules) {
                            // on verifie si les scores ont ete mis
                            $demi_scores = demi_scores($idTournois, $demi_final);
                            if ($demi_scores) {
                              // on recupere les equipes les demis
                              $eq_finals = get_equipe_finale_coupe($idTournois, $demi_final);

                              // on verifie si la finale est prevue
                              $finale_prevue = demi_final($idTournois,$eq_finals);

                              if ($finale_prevue) {
                                // on verifie si la finale est passe
                                $finale_passe = finale_passe($idTournois, $eq_finals);

                                if ($finale_passe) {
                                  // on verifie si les scores ont ete mis
                                  $finale_scores = finale_scores($idTournois, $eq_finals);
                                  if ($finale_scores) {
                                    $gagnant = gagnant_coupe($idTournois, $eq_finals);
                                    termine_competition($idTournois);
                                    ?>
                                    <p>Votre coupe est terminée. Le gagnant est <?php echo recup_info_equipe($gagnant)[0][0]['nom_equipe']; ?>.</p>
                                    <p>Merci d'avoir utilisé notre site pour la gestion de votre tournoi.</p>
                                    <?php
                                  }
                                  else {
                                    ?>
                                    <p>Les scores n'ont pas été mis.</p>
                                    <?php
                                    // session_start();
                                    $_SESSION['pseudo'] = $pseudo;

                                    $recupid1 = recup_idrencontre($idTournois, $eq_finals[0][0], $eq_finals[0][1]);

                                    $_SESSION['id'] = $recupid1;
                                    ?>

                                    <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                                    <?php
                                  }
                                }
                                else {
                                  ?>
                                  <p>Votre finale ne s'est pas encore déroulée.</p>
                                  <?php
                                }
                              }
                              else {
                                ?>
                                <p>Votre finale n'est pas prévue.</p>
                                <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                                  <input type="hidden" name="ideq1" value="<?php echo $eq_finals[0][0]; ?>">
                                  <input type="hidden" name="ideq2" value="<?php echo $eq_finals[0][1]; ?>">
                                  <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                                  <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                                  <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($eq_finals[0][0])[0][0]['nom_equipe']; ?>"> </p>
                                  <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($eq_finals[0][1])[0][0]['nom_equipe']; ?>"> </p>
                                  <p>Date de rencontre <input type="date" name="dateR" value=""> </p>
                                  <p>Heure de rencontre <input type="time" name="heureR" value=""> </p>
                                  <input class="validateButton" type="submit" name="ValiderFinaleCoupe" value="Valider">
                                </form>
                                <?php
                              }
                            }
                            else {
                              ?>
                              <p>Vous n'avez pas encore ajouté les scores de vos demi-finales.</p>
                              <?php
                              // session_start();
                              $_SESSION['pseudo'] = $pseudo;

                              $recupid1 = recup_idrencontre($idTournois, $demi_final[0][0], $demi_final[0][1]);
                              $recupid2 = recup_idrencontre($idTournois, $demi_final[0][2], $demi_final[0][3]);

                              $_SESSION['id'] = $recupid1;
                              ?>

                              <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                              <?php $_SESSION['id'] = $recupid2; ?>
                              <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                              <?php
                            }
                          }
                          else {
                            ?>
                            <p>Vos rencontres ne se sont pas encore deroulées.</p>
                            <?php
                          }
                        }
                        else {
                          ?>
                          <p>Votre demi-finale n'est pas encore planifié.</p>
                          <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                            <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                            <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                            <input type="hidden" name="idEq0" value="<?php echo $demi_final[0][0]; ?>">
                            <input type="hidden" name="idEq1" value="<?php echo $demi_final[0][1]; ?>">
                            <input type="hidden" name="idEq2" value="<?php echo $demi_final[0][2]; ?>">
                            <input type="hidden" name="idEq3" value="<?php echo $demi_final[0][3]; ?>">

                            <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][0])[0][0]['nom_equipe']; ?>"> </p>
                            <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][1])[0][0]['nom_equipe']; ?>"> </p>
                            <p>Date de rencontre <input type="date" name="dateR" value=""> </p>
                            <p>Heure de rencontre <input type="time" name="heureR" value=""> </p>

                            <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][2])[0][0]['nom_equipe']; ?>"> </p>
                            <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][3])[0][0]['nom_equipe']; ?>"> </p>
                            <p>Date de rencontre <input type="date" name="dateR1" value=""> </p>
                            <p>Heure de rencontre <input type="time" name="heureR1" value=""> </p>

                            <input class="validateButton" type="submit" name="ValiderDemiCoupe" value="Valider">
                          </form>
                          <?php
                        }
                      }
                      else {
                        ?>
                        <p>Vous n'avez pas encore mis les scores de vos quarts de finale.</p>
                        <?php
                        // session_start();
                        $_SESSION['pseudo'] = $pseudo;

                        $recupid1 = recup_idrencontre($idTournois, $quart[0][0], $quart[0][1]);
                        $recupid2 = recup_idrencontre($idTournois, $quart[0][2], $quart[0][3]);
                        $recupid3 = recup_idrencontre($idTournois, $quart[0][4], $quart[0][5]);
                        $recupid4 = recup_idrencontre($idTournois, $quart[0][6], $quart[0][7]);

                        $_SESSION['id'] = $recupid1;
                        ?>

                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid2; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid3; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                        <?php $_SESSION['id'] = $recupid4; ?>
                        <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                        <?php
                      }
                    }
                    else {
                      ?>
                      <p>Vos quarts de finale ne sont pas encore passés.</p>
                      <?php
                    }
                  }
                  else {
                    ?>
                    <p>Vos quarts de final ne sont pas encore planifier.</p>
                    <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                      <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                      <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                      <?php
                      for ($i=0; $i < 8; $i++) {
                        ?>
                        <input type="hidden" name="<?php echo "idEq".$i; ?>" value="<?php echo $quart[0][$i]; ?>">

                        <?php
                      }
                       ?>

                       <?php
                       for ($i=0; $i < 4; $i++) {
                         ?>
                         <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($quart[0][$i])[0][0]['nom_equipe']; ?>"> </p>
                         <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($quart[0][$i+1])[0][0]['nom_equipe']; ?>"> </p>
                         <p>Date de rencontre <input type="date" name="<?php echo "dateR".$i; ?>" value="" required> </p>
                         <p>heure de rencontre <input type="time" name="<?php echo "heureR".$i; ?>" value="" required> </p>
                         <?php
                       }
                        ?>
                        <input class="validateButton" type="submit" name="ValiderQuartCoupe" value="Valider">
                    </form>
                    <?php
                  }

                } else {
                  if ($nbr_equipes == 4) {
                    $recupEq = recup_4($idTournois);

                    if ($recupEq == false) {

                      $recup_all_equipes = array();
                      $id = 0;
                      for ($i=0; $i < 4; $i++) {
                        $rec = recup_idEquipe($idTournois, $i);
                        // echo $rec;
                        $recup_all_equipes[0][$id]['Equipe'] = $rec;
                        $id++;
                      }

                      $send = envoie_tab_select($recup_all_equipes, $idTournois);

                      if ($send == false) {
                        echo "erreur";
                      }
                    }

                    $recupEq = recup_4($idTournois);

                    for ($i=0; $i < 4; $i++) {
                      $demi_final[0][$i] = $recupEq[0][$i]['Equipe'];
                    }

                    // on verifie si les demi-finale sont prevus
                    $verif_demi_final_prevu = demi_final_planifier($idTournois, $demi_final);
                    if ($verif_demi_final_prevu) {
                      // on verifie si les rencontres se sont deroules
                      $demi_deroules = demi_final_deroule($idTournois, $demi_final);
                      // var_dump($demi_final);
                      if ($demi_deroules) {
                        // on verifie si les scores ont ete mis
                        $demi_scores = demi_scores($idTournois, $demi_final);
                        if ($demi_scores) {
                          // on recupere les equipes les demis
                          $eq_finals = get_equipe_finale_coupe($idTournois, $demi_final);

                          // on verifie si la finale est prevue
                          $finale_prevue = demi_final($idTournois,$eq_finals);

                          if ($finale_prevue) {
                            // on verifie si la finale est passe
                            $finale_passe = finale_passe($idTournois, $eq_finals);

                            if ($finale_passe) {
                              // on verifie si les scores ont ete mis
                              $finale_scores = finale_scores($idTournois, $eq_finals);
                              if ($finale_scores) {
                                $gagnant = gagnant_coupe($idTournois, $eq_finals);
                                termine_competition($idTournois);
                                ?>
                                <p>Votre coupe est terminée. Le gagnant est <?php echo recup_info_equipe($gagnant)[0][0]['nom_equipe']; ?>.</p>
                                <p>Merci d'avoir utilisé notre site pour la gestion de votre tournoi.</p>
                                <?php
                              }
                              else {
                                ?>
                                <p>Les scores n'ont pas été mis.</p>
                                <?php
                                // session_start();
                                $_SESSION['pseudo'] = $pseudo;

                                $recupid1 = recup_idrencontre($idTournois, $eq_finals[0][0], $eq_finals[0][1]);

                                $_SESSION['id'] = $recupid1;
                                ?>

                                <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                                <?php
                              }
                            }
                            else {
                              ?>
                              <p>Votre finale ne s'est pas encore déroulée.</p>
                              <?php
                            }
                          }
                          else {
                            ?>
                            <p>Votre finale n'est pas prévue.</p>
                            <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                              <input type="hidden" name="ideq1" value="<?php echo $eq_finals[0][0]; ?>">
                              <input type="hidden" name="ideq2" value="<?php echo $eq_finals[0][1]; ?>">
                              <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                              <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                              <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($eq_finals[0][0])[0][0]['nom_equipe']; ?>"> </p>
                              <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($eq_finals[0][1])[0][0]['nom_equipe']; ?>"> </p>
                              <p>Date de rencontre <input type="date" name="dateR" value=""> </p>
                              <p>Heure de rencontre <input type="time" name="heureR" value=""> </p>
                              <input class="validateButton" type="submit" name="ValiderFinaleCoupe" value="Valider">
                            </form>
                            <?php
                          }
                        }
                        else {
                          ?>
                          <p>Vous n'avez pas encore ajouté les scores de vos demi-finales.</p>
                          <?php
                          // session_start();
                          $_SESSION['pseudo'] = $pseudo;

                          $recupid1 = recup_idrencontre($idTournois, $demi_final[0][0], $demi_final[0][1]);
                          $recupid2 = recup_idrencontre($idTournois, $demi_final[0][2], $demi_final[0][3]);

                          $_SESSION['id'] = $recupid1;
                          ?>

                          <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>
                          <?php $_SESSION['id'] = $recupid2; ?>
                          <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                          <?php
                        }
                      }
                      else {
                        ?>
                        <p>Vos rencontres ne se sont pas encore deroulées.</p>
                        <?php
                      }
                    }
                    else {
                      ?>
                      <p>Votre demi-finale n'est pas encore planifié.</p>
                      <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                        <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                        <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                        <input type="hidden" name="idEq0" value="<?php echo $demi_final[0][0]; ?>">
                        <input type="hidden" name="idEq1" value="<?php echo $demi_final[0][1]; ?>">
                        <input type="hidden" name="idEq2" value="<?php echo $demi_final[0][2]; ?>">
                        <input type="hidden" name="idEq3" value="<?php echo $demi_final[0][3]; ?>">

                        <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][0])[0][0]['nom_equipe']; ?>"> </p>
                        <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][1])[0][0]['nom_equipe']; ?>"> </p>
                        <p>Date de rencontre <input type="date" name="dateR" value=""> </p>
                        <p>Heure de rencontre <input type="time" name="heureR" value=""> </p>

                        <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][2])[0][0]['nom_equipe']; ?>"> </p>
                        <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($demi_final[0][3])[0][0]['nom_equipe']; ?>"> </p>
                        <p>Date de rencontre <input type="date" name="dateR1" value=""> </p>
                        <p>Heure de rencontre <input type="time" name="heureR1" value=""> </p>

                        <input class="validateButton" type="submit" name="ValiderDemiCoupe" value="Valider">
                      </form>
                      <?php
                    }
                    ?>
                    <?php
                  }else {
                    ?>
                    <div class="centre">
                    <?php
                    if ($nbr_equipes == 2) {
                      $recupEq = recup_4($idTournois);

                      if ($recupEq == false) {

                        $recup_all_equipes = array();
                        $id = 0;
                        for ($i=0; $i < 4; $i++) {
                          $rec = recup_idEquipe($idTournois, $i);
                          // echo $rec;
                          $recup_all_equipes[0][$id]['Equipe'] = $rec;
                          $id++;
                        }

                        $send = envoie_tab_select($recup_all_equipes, $idTournois);

                        if ($send == false) {
                          echo "erreur";
                        }
                      }

                      $recupEq = recup_4($idTournois);

                      for ($i=0; $i < 4; $i++) {
                        $eq_finals[0][$i] = $recupEq[0][$i]['Equipe'];
                      }

                      // on verifie si la finale est prevue
                      $finale_prevue = demi_final($idTournois,$eq_finals);

                      if ($finale_prevue) {
                        // on verifie si la finale est passe
                        $finale_passe = finale_passe($idTournois, $eq_finals);

                        if ($finale_passe) {
                          // on verifie si les scores ont ete mis
                          $finale_scores = finale_scores($idTournois, $eq_finals);
                          if ($finale_scores) {
                            $gagnant = gagnant_coupe($idTournois, $eq_finals);
                            termine_competition($idTournois);
                            ?>
                            <p>Votre coupe est terminée. Le gagnant est <?php echo recup_info_equipe($gagnant)[0][0]['nom_equipe']; ?>.</p>
                            <p>Merci d'avoir utilisé notre site pour la gestion de votre tournoi.</p>
                            <?php
                          }
                          else {
                            ?>
                            <p>Les scores n'ont pas été mis.</p>
                            <?php
                            // session_start();
                            $_SESSION['pseudo'] = $pseudo;

                            $recupid1 = recup_idrencontre($idTournois, $eq_finals[0][0], $eq_finals[0][1]);

                            $_SESSION['id'] = $recupid1;
                            ?>

                            <a href="<?php echo "ajouter_resultats_monTournois.php?pseudo=".$_SESSION['pseudo']."&id=".$_SESSION['id']; ?>">Ajouter les résultats</a>

                            <?php
                          }
                        }
                        else {
                          ?>
                          <p>Votre finale ne s'est pas encore déroulée.</p>
                          <?php
                        }
                      }
                      else {
                        ?>
                        <p>Votre finale n'est pas prévue.</p>
                        <form class="" action="../controller/functions_planifier_rencontres_monTournois.php" method="post">
                          <input type="hidden" name="ideq1" value="<?php echo $eq_finals[0][0]; ?>">
                          <input type="hidden" name="ideq2" value="<?php echo $eq_finals[0][1]; ?>">
                          <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                          <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
                          <p>Equipe 1 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($eq_finals[0][0])[0][0]['nom_equipe']; ?>"> </p>
                          <p>Equipe 2 <input type="text" disabled="disabled" name="" value="<?php echo recup_info_equipe($eq_finals[0][1])[0][0]['nom_equipe']; ?>"> </p>
                          <p>Date de rencontre <input type="date" name="dateR" value=""> </p>
                          <p>Heure de rencontre <input type="time" name="heureR" value=""> </p>
                          <input class="validateButton" type="submit" name="ValiderFinaleCoupe" value="Valider">
                        </form>
                        <?php
                      }
                      ?>

                      <?php
                    }
                    ?>
                    </div>
                    <?php
                  }
                }
              } ?>
            <?php
          }
          ?>

          <?php
        }
        ?>

        <?php
      }
       ?>
       </div>
       <?php
        include 'sidebar.php';
        include 'header.php';
     ?>
  </body>
</html>
