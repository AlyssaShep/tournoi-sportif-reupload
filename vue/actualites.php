<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <title>Actualités</title>
    <?php
    include '../controller/functions_actualites.php';
    if(isset($_GET["pseudo"])){
      $pseudo = $_GET["pseudo"];
    }?>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/mesTournois.css" />
  </head>
  <body>

    <div class="BigDiv">
      <div class="gestionRect">
        <p>Tournois à venir</p>
        <?php
        $gestion = tournois_a_venir();
        $size = count($gestion[0]);
        if(isset($gestion) && $size != 0){ ?>
            <table>
              <col span="12">
              <tr class = "Nomcolonne">
                <th>Nom du tournois</th>
                <th>Date de début</th>
                <th>Durée</th>
                <th>Pseudo du Gestionnaire</th>
                <th>Lieu</th>
                <th>Type</th>
                <th>Nombre d'équipes</th>
                <th>Esport</th>
                <th>Voir Equipes</th>
              </tr>

        <?php for ($i=0; $i < $size ; $i++) { ?>
                <tr>
                  <td><?php echo $gestion[0][$i]['nom_tournois']; ?></td>
                  <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                  <td><?php echo strftime('  %d %B %Y ', strtotime($gestion[0][$i]['dateDebut'])); ?></td>
                  <td><?php echo $gestion[0][$i]['duree']; ?></td>
                  <td><?php echo get_pseudo_idGestionnaire($gestion[0][$i]['idGestionnaire']); ?></td>
                  <td><?php echo $gestion[0][$i]['lieu']; ?></td>
                  <td><?php echo $gestion[0][$i]['type']; ?></td>
                  <td><?php echo $gestion[0][$i]['nbr_equipes']; ?></td>
                  <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                  <?php if(isset($pseudo)){ ?>
                  <td><a href="<?php echo "voir_equipe_mesTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$i]['idTournois']; ?>">Voir</a></td>
                  <?php }
                        else { ?><td><a href="<?php echo "voir_equipe_mesTournois.php?id=".$gestion[0][$i]['idTournois']; ?>">Voir</a></td>
                  <?php } ?>
                </tr>
  <?php         }?>
        </table>

<?php   }
        else { ?>
            <p>Il n'y a pas de tournois à venir !</p>
  <?php } ?>

      </div>

    </div>

    <div class="BigDiv">
      <div class="gestionRect">
            <p>Tournois en cours</p>
                    <?php
                    $gestion = tournois_en_cours();
                    $size = count($gestion[0]);
                    if(isset($gestion) && $size != 0){ ?>
                        <table>
                          <col span="12">
                          <tr class = "Nomcolonne">
                            <th>Nom du tournoi</th>
                            <th>Date de début</th>
                            <th>Durée</th>
                            <th>Pseudo du Gestionnaire</th>
                            <th>Lieu</th>
                            <th>Type</th>
                            <th>Nombre des équipes</th>
                            <th>Esport</th>
                            <th>Détail</th>
                          </tr>

                    <?php for ($i=0; $i < $size ; $i++) { ?>
                            <tr>
                              <td><?php echo $gestion[0][$i]['nom_tournois']; ?></td>
                              <?php setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');?>
                              <td><?php echo strftime('  %d %B %Y ', strtotime($gestion[0][$i]['dateDebut'])); ?></td>
                              <td><?php echo $gestion[0][$i]['duree']; ?></td>
                              <td><?php echo get_pseudo_idGestionnaire($gestion[0][$i]['idGestionnaire']); ?></td>
                              <td><?php echo $gestion[0][$i]['lieu']; ?></td>
                              <td><?php echo $gestion[0][$i]['type']; ?></td>
                              <td><?php echo $gestion[0][$i]['nbr_equipes']; ?></td>
                              <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                              <?php if(isset($pseudo)){ ?>
                              <td><a href="<?php echo "details_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$i]['idTournois']; ?>">Voir</a></td>
                              <?php }
                                    else { ?><td><a href="<?php echo "details_monTournois.php?id=".$gestion[0][$i]['idTournois']; ?>">Voir</a></td>
                              <?php } ?>
                            </tr>
              <?php         }?>
                    </table>

            <?php   }
                    else { ?>
                        <p>Il n'y a pas de tournois en cours !</p>
              <?php } ?>
      </div>
    </div>

    <div class="BigDiv">
      <div class="gestionRect">
            <p>Tournois terminés</p>
                    <?php
                    $todayDate = date("Y-m-d");
                    $gestion = tournois_termines();
                    $size = count($gestion[0]);
                    if(isset($gestion) && $size != 0){ ?>
                        <table>
                          <col span="12">
                          <tr class = "Nomcolonne">
                            <th>nom_tournois</th>
                            <th>dateDebut</th>
                            <th>duree</th>
                            <th>Pseudo du Gestionnaire</th>
                            <th>lieu</th>
                            <th>type</th>
                            <th>nbr_equipes</th>
                            <th>Esport</th>
                            <th>Détail</th>
                          </tr>

                    <?php
                          for ($i=0; $i < $size ; $i++) { ?>
                            <tr>
                              <td><?php echo $gestion[0][$i]['nom_tournois']; ?></td>
                              <td><?php echo $gestion[0][$i]['dateDebut']; ?></td>
                              <td><?php echo $gestion[0][$i]['duree']; ?></td>
                              <td><?php echo get_pseudo_idGestionnaire($gestion[0][$i]['idGestionnaire']); ?></td>
                              <td><?php echo $gestion[0][$i]['lieu']; ?></td>
                              <td><?php echo $gestion[0][$i]['type']; ?></td>
                              <td><?php echo $gestion[0][$i]['nbr_equipes']; ?></td>
                              <td><?php echo $gestion[0][$i]['Esport']; ?></td>
                              <?php if(isset($pseudo)){ ?>
                              <td><a href="<?php echo "details_monTournois.php?pseudo=".$_GET["pseudo"]."&id=".$gestion[0][$i]['idTournois']; ?>">Voir</a></td>
                              <?php }
                                    else { ?><td><a href="<?php echo "details_monTournois.php?id=".$gestion[0][$i]['idTournois']; ?>">Voir</a></td>
                              <?php } ?>
                            </tr>
              <?php         }?>
                    </table>

            <?php   }
                    else { ?>
                        <p>Les tournois ne sont pas encore terminés !</p>
              <?php } ?>
      </div>
    </div>

    <?php
      include 'header.php';
    ?>
  </body>
</html>
