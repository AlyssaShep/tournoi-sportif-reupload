<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/suprTournois.css" />
    <title>Supprimer Rencontre</title>
  </head>
  <body>

    <?php
        $pseudo = $_GET['pseudo'];
        $idMatch = $_GET['id'];
        include '../controller/functions_supprimer_rencontre_monTournois.php';
        $rencontre = get_info_rencontre($idMatch);
        $info = get_info_compet($rencontre[0][0]['idTournois']);
     ?>

    <div class="rect">
    <div class="sousPart">
      <p>Recap' de la rencontre et son tournoi</p>
      </div>
      <div class="centre">
      <table>
        <col span="7">
        <tr class = "Nomcolonne">
          <th>Nom tournois</th>
          <th>Date de debut</th>
          <th>Duree</th>
          <th>Lieu</th>
          <th>Type</th>
          <th>Esport</th>
        </tr>

        <tr>
          <td><?php echo $info[0][0]['nom_tournois']; ?></td>
          <td><?php echo $info[0][0]['dateDebut']; ?></td>
          <td><?php echo $info[0][0]['duree']; ?></td>
          <td><?php echo $info[0][0]['lieu']; ?></td>
          <td><?php echo $info[0][0]['type']; ?></td>
          <td><?php echo $info[0][0]['Esport']; ?></td>
        </tr>

      </table>
      </div>

      <div class="centre">
      <table>
        <col span="7">
        <tr class = "Nomcolonne">
          <th>Date de rencontre</th>
          <th>Heure de rencontre</th>
          <th>Equipe 1</th>
          <th>Equipe 2</th>
        </tr>

        <tr>
          <td><?php echo $rencontre[0][0]['dateRencontre']; ?></td>
          <td><?php echo $rencontre[0][0]['heureRencontre']; ?></td>
          <td><?php echo get_nom_equipe($rencontre[0][0]['idEquipe1']); ?></td>
          <td><?php echo get_nom_equipe($rencontre[0][0]['idEquipe2']); ?></td>
        </tr>
      </table>
      </div>

    <div class="">
     <form action="../controller/functions_supprimer_rencontre_monTournois.php" method="post">
       <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
       <input type="hidden" name="idMatch" value="<?php echo $idMatch; ?>">
       <div class="sousPart">
       <p>Êtes-vous sûr de vouloir supprimer cette rencontre ?</p>
       </div>
       <input type="radio" name="suppression" id="choixsup1" value="non" checked />
       <label for="choix2ra">Non</label>
       <input type="radio" name="suppression" id="choixsup2" value="oui" />
       <label for="choix1ra">Oui</label>  </p>

       <input class="validateButton" type="submit" name="supprimer" value="Valider">
     </form>
    </div>
    </div>
    <?php
    include 'sidebar.php';
    include 'header.php';
    ?>
  </body>
</html>
