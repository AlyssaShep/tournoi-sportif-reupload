<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Modification Résultats</title>
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/modifResultat.css" />
  </head>
  <body>

    <?php
        $pseudo = $_GET['pseudo'];
        $idMatch = $_GET['id'];
        include '../controller/functions_modifier_resultats_monTournois.php';
        $rencontre = get_info_rencontre($idMatch);
        $info = get_info_compet($rencontre[0][0]['idTournois']);

        $Esport = $info[0][0]['Esport'];
        if ($Esport == "LOL") {
          $max = 1;
        }
        else {
          if ($Esport == "CS") {
            $max = 16;
          }
          else {
            $max = 3;
          }
        }
     ?>

     <div class="rect1">
       <p>Recapitulatif de la rencontre et son tournoi</p>
       <table>
         <col span="7">
         <tr>
           <th>Nom tournois</th>
           <th>Date de debut</th>
           <th>Duree</th>
           <th>Lieu</th>
           <th>Type</th>
           <th>Esport</th>
         </tr>

         <tr>
           <td><?php echo $info[0][0]['nom_tournois']; ?></td>
           <td><?php echo $info[0][0]['dateDebut']; ?></td>
           <td><?php echo $info[0][0]['duree']; ?></td>
           <td><?php echo $info[0][0]['lieu']; ?></td>
           <td><?php echo $info[0][0]['type']; ?></td>
           <td><?php echo $info[0][0]['Esport']; ?></td>
         </tr>
       </table>
       </br>
       <table>
         <col span="7">
         <tr>
           <th>Date de rencontre</th>
           <th>Heure de rencontre</th>
           <th>Equipe 1</th>
           <th>Equipe 2</th>
         </tr>

         <tr>
           <td><?php echo $rencontre[0][0]['dateRencontre']; ?></td>
           <td><?php echo $rencontre[0][0]['heureRencontre']; ?></td>
           <td><?php echo get_info_nom_equipe($rencontre[0][0]['idEquipe1']); ?></td>
           <td><?php echo get_info_nom_equipe($rencontre[0][0]['idEquipe2']); ?></td>
         </tr>
       </table>
     </div>
    
    <div class = "rect2">
     <form action="../controller/functions_modifier_resultats_monTournois.php" method="post">
       <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
       <input type="hidden" name="idMatch" value="<?php echo $idMatch; ?>">
       <input type="hidden" name="idEquipe1" value="<?php echo $rencontre[0][0]['idEquipe1']; ?>">
       <input type="hidden" name="idEquipe2" value="<?php echo $rencontre[0][0]['idEquipe2']; ?>">
       <p>Date de rencontre <input type="date" disabled="disabled" name="" value="<?php echo $rencontre[0][0]['dateRencontre']; ?>"> </p>
       <p>Heure de rencontre <input type="time" name="" disabled="disabled" value="<?php echo $rencontre[0][0]['heureRencontre']; ?>"> </p>
       <p>Score <?php echo get_info_nom_equipe($rencontre[0][0]['idEquipe1']); ?> <input type="number" class ="fielder" name="score1" max="<?php echo $max; ?>" value="<?php echo $rencontre[0][0]['score1']; ?>"> </p>
       <p>Score <?php echo get_info_nom_equipe($rencontre[0][0]['idEquipe2']); ?> <input type="number" class ="fielder" name="score2" max="<?php echo $max; ?>" value="<?php echo $rencontre[0][0]['score2']; ?>"> </p>
       <input type="submit" class = "boutonAjout" name="Valider" value="Valider">
     </form>
     </div> 

     <?php
     include 'sidebar.php';
     include 'header.php';
    ?>
  </body>
</html>
