<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <title>Formulaire Gestionnaire</title>
  </head>
  <body>
    <form method="post" action="formGestionnaire.php">

      <p>Nom                 <input type="text" name="Nom" required/></p>
      <p>Prénom              <input type="text" name="Prenom"required/></p>
      <!-- <p>Date de naissance   <input type="date" name="Date" required /></p> -->
      <p>Email               <input type="email" name="Email" required/></p>
      <p>Numero              <input type="tel" name="Numero" pattern="[0-9]{10}" required/></p>
      <!-- le pattern permet de faire en sorte qu il n y ait pas un numero superieur a 10 -->

      <p>Nom de Competition  <input type="text" name="NomC" required/></p>
      <p>Lieu de rencontre   <input type="text" name="Lieu" required/></p>

      <p>Date de début       <input type="date" name="Debut" required/></p>
      <p>Date de fin         <input type="date" name="Fin" required/></p>
      <p>Nombre d'équipes    <input type="number" name="NbrEquipe" min="2" required/></p>

      <p>Type de compétition <select name="Competition">
      <option value="tournois">Tournois</option>
      <option value="championnat">Championnat</option>
      <option value="poule">Poule</option>
      </select></p>



      <input type="submit" value="Valider"/>
    </form>
  </body>
</html>
