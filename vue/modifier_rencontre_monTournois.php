<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Modifier Rencontre</title>
    <link rel="icon" type="image/png" href="../assets/images/logo.png"/>
    <link rel="stylesheet" href="../assets/mainCSS.css" />
    <link rel="stylesheet" href="../assets/modifRencontre.css" />
  </head>
  <body>

    <?php
    $idMatch = $_GET['id'];
    $pseudo = $_GET['pseudo'];
    include '../controller/functions_monCompte.php';
    include '../controller/functions_modifier_rencontre_monTournois.php';
    $info = get_info_rencontre($idMatch);
    $nom = get_nom_tournois($info[0][0]['idTournois']);
    $idTournois = $info[0][0]['idTournois'];
    $debut = get_info_competition($idTournois)[0][0]['dateDebut'];
    $duree = get_info_competition($idTournois)[0][0]['duree'];
    $fin = date("Y-m-d", strtotime($debut."+".$duree."days"));
     ?>

     <h2><?php echo $nom; ?></h2>
    
     <div class="rect1">
     <p>Vous pouvez modifier le temps votre rencontre</p>
     <p>Attention, la date de la rencontre doit être dans l intervalle de votre compétition: entre le <?php echo $debut; ?> et <?php echo $fin; ?></p>
    </div>

    <div class="rect2">
     <form action="../controller/functions_modifier_rencontre_monTournois.php" method="post">
       <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
       <input type="hidden" name="idMatch" value="<?php echo $idMatch; ?>">
       <input type="hidden" name="idTournois" value="<?php echo $idTournois; ?>">
       <p>Date de Rencontre <input type="date" class ="fielder" name="newDate" value="<?php echo $info[0][0]['dateRencontre']; ?>"></p>
       <p>Heure de rencontre <input type="time" class ="fielder" name="newTime" value="<?php echo $info[0][0]['heureRencontre']; ?>"></p>
       <p>Equipe 1 <input type="text" name="" disabled="disabled" value="<?php echo get_info_equipe($info[0][0]['idEquipe1'])[0][0]['nom_equipe']; ?>"> </p>
       <p>Equipe 2 <input type="text" name="" disabled="disabled" value="<?php echo get_info_equipe($info[0][0]['idEquipe2'])[0][0]['nom_equipe']; ?>"> </p>
       <input type="submit" class="boutonAjout" name="valider" value="Valider">
     </form>
    </div>

     <?php
     include 'sidebar.php';
     include 'header.php';
    ?>
  </body>
</html>
